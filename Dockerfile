FROM openjdk:11-jre
MAINTAINER Patrik Lind
LABEL maintainer "SuzoHapp"

ADD target/ccodSimulator-1.0-SNAPSHOT.jar /opt/
ADD keyFiles /opt/keyFiles

WORKDIR "/opt/"

ENTRYPOINT exec java \
    -XX:+UseContainerSupport \
    -XX:MaxRAMPercentage=80 \
    -jar ccodSimulator-1.0-SNAPSHOT.jar