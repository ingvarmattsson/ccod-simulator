[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------------< suzohapp:cccSamplers >------------------------
[INFO] Building cccSamplers 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-dependency-plugin:2.8:list (default-cli) @ cccSamplers ---
[INFO] 
[INFO] The following files have been resolved:
[INFO]    org.apache.cxf:cxf-rt-transports-http:jar:3.2.7:compile
[INFO]    org.apache.cxf:cxf-rt-frontend-jaxrs:jar:3.2.7:compile
[INFO]    org.bitbucket.mstrobel:procyon-compilertools:jar:0.5.32:compile
[INFO]    com.thoughtworks.xstream:xstream:jar:1.4.11:compile
[INFO]    net.jcip:jcip-annotations:jar:1.0:compile
[INFO]    org.itadaki:bzip2:jar:0.9.1:compile
[INFO]    com.google.protobuf:protobuf-java:jar:2.5.0:compile
[INFO]    org.bouncycastle:bcpkix-jdk15on:jar:1.60:compile
[INFO]    org.apache.cxf:cxf-core:jar:3.2.7:compile
[INFO]    javax.xml.bind:jaxb-api:jar:2.3.1:compile
[INFO]    org.bitbucket.mstrobel:procyon-core:jar:0.5.32:compile
[INFO]    com.googlecode.juniversalchardet:juniversalchardet:jar:1.0.3:compile
[INFO]    javax.mail:mail:jar:1.5.0-b01:compile
[INFO]    com.sun.xml.fastinfoset:FastInfoset:jar:1.2.15:compile
[INFO]    org.apache.pdfbox:pdfbox:jar:2.0.13:compile
[INFO]    com.github.jai-imageio:jai-imageio-core:jar:1.4.0:compile
[INFO]    org.jodd:jodd-lagarto:jar:5.0.6:compile
[INFO]    org.apache.httpcomponents:httpasyncclient:jar:4.1.4:compile
[INFO]    com.beust:jcommander:jar:1.35:compile
[INFO]    org.apache.xmlgraphics:xmlgraphics-commons:jar:2.3:compile
[INFO]    xml-apis:xml-apis:jar:1.4.01:compile
[INFO]    org.apache.commons:commons-text:jar:1.6:compile
[INFO]    jcharts:jcharts:jar:0.7.5:compile
[INFO]    org.apache.uima:uimaj-core:jar:3.0.1:compile
[INFO]    com.googlecode.json-simple:json-simple:jar:1.1.1:compile
[INFO]    org.apache.commons:commons-math3:jar:3.6.1:compile
[INFO]    javax.annotation:javax.annotation-api:jar:1.3.2:compile
[INFO]    org.apache.pdfbox:jbig2-imageio:jar:3.0.2:compile
[INFO]    com.google.code.gson:gson:jar:2.8.5:compile
[INFO]    net.java.dev.jna:jna:jar:5.1.0:compile
[INFO]    edu.ucar:grib:jar:4.5.5:compile
[INFO]    xalan:serializer:jar:2.7.2:compile
[INFO]    org.apache.pdfbox:fontbox:jar:2.0.13:compile
[INFO]    org.quartz-scheduler:quartz:jar:2.2.0:compile
[INFO]    javax.activation:javax.activation-api:jar:1.2.0:compile
[INFO]    xalan:xalan:jar:2.7.2:compile
[INFO]    org.apache.commons:commons-jexl3:jar:3.1:compile
[INFO]    com.helger:ph-commons:jar:9.2.1:compile
[INFO]    org.apache.jmeter:jorphan:jar:5.1:compile
[INFO]    xerces:xercesImpl:jar:2.12.0:compile
[INFO]    org.jvnet.staxex:stax-ex:jar:1.8:compile
[INFO]    com.rometools:rome-utils:jar:1.12.0:compile
[INFO]    org.apache-extras.beanshell:bsh:jar:2.0b6:compile
[INFO]    org.apache.logging.log4j:log4j-slf4j-impl:jar:2.11.1:compile
[INFO]    org.apache.commons:commons-collections4:jar:4.2:compile
[INFO]    edu.usc.ir:sentiment-analysis-parser:jar:0.1:compile
[INFO]    commons-net:commons-net:jar:3.6:compile
[INFO]    org.apache.commons:commons-csv:jar:1.6:compile
[INFO]    org.apache.logging.log4j:log4j-api:jar:2.11.1:compile
[INFO]    org.ow2.asm:asm:jar:7.0:compile
[INFO]    org.jdom:jdom2:jar:2.0.6:compile
[INFO]    com.sun.istack:istack-commons-runtime:jar:3.0.7:compile
[INFO]    c3p0:c3p0:jar:0.9.1.1:compile
[INFO]    org.apache.logging.log4j:log4j-core:jar:2.11.1:compile
[INFO]    org.apache.commons:commons-compress:jar:1.18:compile
[INFO]    commons-lang:commons-lang:jar:2.6:compile
[INFO]    javax.activation:activation:jar:1.1:compile
[INFO]    org.apache.xmlbeans:xmlbeans:jar:3.0.2:compile
[INFO]    org.jodd:jodd-props:jar:5.0.6:compile
[INFO]    org.apache.cxf:cxf-rt-rs-client:jar:3.2.7:compile
[INFO]    org.apache.commons:commons-exec:jar:1.3:compile
[INFO]    org.apache.poi:poi-ooxml:jar:4.0.1:compile
[INFO]    org.apache.pdfbox:pdfbox-tools:jar:2.0.13:compile
[INFO]    org.apache.pdfbox:jempbox:jar:1.8.16:compile
[INFO]    org.apache.james:apache-mime4j-dom:jar:0.8.2:compile
[INFO]    org.jsoup:jsoup:jar:1.11.3:compile
[INFO]    org.apache.commons:commons-lang3:jar:3.8.1:compile
[INFO]    org.apache.httpcomponents:httpcore:jar:4.4.11:compile
[INFO]    org.gagravarr:vorbis-java-core:jar:0.8:compile
[INFO]    joda-time:joda-time:jar:2.2:compile
[INFO]    org.mongodb:mongo-java-driver:jar:2.11.3:compile
[INFO]    com.jayway.jsonpath:json-path:jar:2.4.0:compile
[INFO]    org.apache.geronimo.specs:geronimo-jms_1.1_spec:jar:1.1.1:compile
[INFO]    com.pff:java-libpst:jar:0.8.1:compile
[INFO]    org.tukaani:xz:jar:1.8:compile
[INFO]    com.adobe.xmp:xmpcore:jar:5.1.3:compile
[INFO]    org.mozilla:rhino:jar:1.7.10:compile
[INFO]    org.apache.httpcomponents:httpmime:jar:4.5.7:compile
[INFO]    oro:oro:jar:2.0.8:compile
[INFO]    org.apache.james:apache-mime4j-core:jar:0.8.2:compile
[INFO]    org.slf4j:jul-to-slf4j:jar:1.7.25:compile
[INFO]    com.fasterxml.jackson.core:jackson-databind:jar:2.9.5:compile
[INFO]    org.tallison:jmatio:jar:1.5:compile
[INFO]    org.apache.poi:poi:jar:4.0.1:compile
[INFO]    org.eclipse.paho:org.eclipse.paho.client.mqttv3:jar:1.2.0:compile
[INFO]    net.sf.ehcache:ehcache-core:jar:2.6.2:compile
[INFO]    org.codehaus.groovy:groovy-all:jar:2.4.16:compile
[INFO]    net.minidev:json-smart:jar:2.3:compile
[INFO]    org.apache.poi:poi-ooxml-schemas:jar:4.0.1:compile
[INFO]    org.codelibs:jhighlight:jar:1.0.3:compile
[INFO]    org.apache.httpcomponents:httpcore-nio:jar:4.4.11:compile
[INFO]    org.apache.sis.core:sis-metadata:jar:0.8:compile
[INFO]    org.bouncycastle:bcprov-jdk15on:jar:1.60:compile
[INFO]    org.apache.sis.core:sis-referencing:jar:0.8:compile
[INFO]    org.glassfish.jaxb:txw2:jar:2.3.1:compile
[INFO]    org.apache.sis.storage:sis-storage:jar:0.8:compile
[INFO]    com.epam:parso:jar:2.0.10:compile
[INFO]    com.github.openjson:openjson:jar:1.0.10:compile
[INFO]    com.google.guava:guava:jar:17.0:compile
[INFO]    net.minidev:accessors-smart:jar:1.2:compile
[INFO]    com.drewnoakes:metadata-extractor:jar:2.11.0:compile
[INFO]    org.apache.ws.xmlschema:xmlschema-core:jar:2.2.3:compile
[INFO]    org.jodd:jodd-log:jar:5.0.6:compile
[INFO]    org.apache.commons:commons-jexl:jar:2.1.1:compile
[INFO]    javax.ws.rs:javax.ws.rs-api:jar:2.1.1:compile
[INFO]    org.apache.jmeter:ApacheJMeter_core:jar:5.1:compile
[INFO]    org.apache.uima:uimafit-core:jar:2.4.0:compile
[INFO]    commons-io:commons-io:jar:2.6:compile
[INFO]    com.rometools:rome:jar:1.12.0:compile
[INFO]    javax.measure:unit-api:jar:1.0:compile
[INFO]    org.opengis:geoapi:jar:3.0.1:compile
[INFO]    edu.ucar:netcdf4:jar:4.5.5:compile
[INFO]    org.slf4j:jcl-over-slf4j:jar:1.7.25:compile
[INFO]    xmlpull:xmlpull:jar:1.1.3.1:compile
[INFO]    dnsjava:dnsjava:jar:2.1.8:compile
[INFO]    com.github.ben-manes.caffeine:caffeine:jar:2.6.2:compile
[INFO]    org.hamcrest:hamcrest-core:jar:1.3:compile
[INFO]    edu.ucar:udunits:jar:4.5.5:compile
[INFO]    com.sun.activation:javax.activation:jar:1.2.0:compile
[INFO]    com.fifesoft:rsyntaxtextarea:jar:3.0.2:compile
[INFO]    edu.ucar:cdm:jar:4.5.5:compile
[INFO]    net.sf.saxon:Saxon-HE:jar:9.9.1-1:compile
[INFO]    com.helger:ph-css:jar:6.1.1:compile
[INFO]    edu.ucar:httpservices:jar:4.5.5:compile
[INFO]    org.apache.commons:commons-dbcp2:jar:2.5.0:compile
[INFO]    org.apache.jmeter:ApacheJMeter_java:jar:5.1:compile
[INFO]    commons-codec:commons-codec:jar:1.11:compile
[INFO]    org.apache.tika:tika-parsers:jar:1.20:compile
[INFO]    com.github.junrar:junrar:jar:2.0.0:compile
[INFO]    org.apache.commons:commons-pool2:jar:2.6.0:compile
[INFO]    com.fasterxml.jackson.core:jackson-core:jar:2.9.8:compile
[INFO]    com.google.code.findbugs:jsr305:jar:3.0.2:compile
[INFO]    de.l3s.boilerpipe:boilerpipe:jar:1.1.0:compile
[INFO]    bsf:bsf:jar:2.4.0:compile
[INFO]    org.brotli:dec:jar:0.1.2:compile
[INFO]    org.apache.sis.core:sis-utility:jar:0.8:compile
[INFO]    commons-logging:commons-logging:jar:1.0.4:compile
[INFO]    xpp3:xpp3_min:jar:1.1.4c:compile
[INFO]    org.apache.httpcomponents:httpclient:jar:4.5.7:compile
[INFO]    org.jodd:jodd-core:jar:5.0.6:compile
[INFO]    org.apache.tika:tika-core:jar:1.20:compile
[INFO]    org.gagravarr:vorbis-java-tika:jar:0.8:compile
[INFO]    org.apache.sis.core:sis-feature:jar:0.8:compile
[INFO]    org.ccil.cowan.tagsoup:tagsoup:jar:1.2.1:compile
[INFO]    com.fasterxml.woodstox:woodstox-core:jar:5.0.3:compile
[INFO]    org.apache.jmeter:ApacheJMeter_components:jar:5.1:compile
[INFO]    org.glassfish.jaxb:jaxb-runtime:jar:2.3.1:compile
[INFO]    org.apache.poi:poi-scratchpad:jar:4.0.1:compile
[INFO]    org.apache.sis.storage:sis-netcdf:jar:0.8:compile
[INFO]    net.sf.jtidy:jtidy:jar:r938:compile
[INFO]    org.bouncycastle:bcmail-jdk15on:jar:1.60:compile
[INFO]    org.apache.logging.log4j:log4j-1.2-api:jar:2.11.1:compile
[INFO]    org.codehaus.woodstox:stax2-api:jar:3.1.4:compile
[INFO]    commons-collections:commons-collections:jar:3.2.2:compile
[INFO]    junit:junit:jar:4.12:compile
[INFO]    com.healthmarketscience.jackcess:jackcess:jar:2.1.12:compile
[INFO]    org.freemarker:freemarker:jar:2.3.28:compile
[INFO]    org.slf4j:slf4j-api:jar:1.7.25:compile
[INFO]    com.fasterxml.jackson.core:jackson-annotations:jar:2.9.8:compile
[INFO]    com.healthmarketscience.jackcess:jackcess-encrypt:jar:2.1.4:compile
[INFO]    com.github.virtuald:curvesapi:jar:1.05:compile
[INFO]    com.googlecode.mp4parser:isoparser:jar:1.1.22:compile
[INFO]    org.apache.opennlp:opennlp-tools:jar:1.9.0:compile
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.999 s
[INFO] Finished at: 2019-05-06T17:02:58+02:00
[INFO] ------------------------------------------------------------------------
