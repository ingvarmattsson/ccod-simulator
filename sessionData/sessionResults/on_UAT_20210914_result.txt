Results for session UAT 20210914

Session Configuration:
	Run time expected: 1m, actual: 1m
	Amount of machines: 1
	Amount of prematurely failed machines: 0
	Event loop delay: 0ms
	Event delay: 0ms
	Machine ramp up time: 0ms
rmAlert:
	Amount of events: 2
	Failed amounts: 0
	Successfull: 100,00%
	Latency (ms) - lowest: 566, highest: 566, mean: 566, median: 566, mode: 566
rmBoxContent:
	Amount of events: 88
	Failed amounts: 0
	Successfull: 100,00%
	Latency (ms) - lowest: 556, highest: 613, mean: 566, median: 566, mode: 566
rmSystemStatuse:
	Amount of events: 17
	Failed amounts: 0
	Successfull: 100,00%
	Latency (ms) - lowest: 565, highest: 628, mean: 570, median: 566, mode: 565
rmTransaction:
	Amount of events: 10
	Failed amounts: 0
	Successfull: 100,00%
	Latency (ms) - lowest: 563, highest: 733, mean: 582, median: 565, mode: 565

###Total###
	Amount of events: 121
	Failed amounts: 0
	Throughput: 2,02 events/second
	Successfull: 100,00%
	Latency (ms) - lowest: 556, highest: 733, mean: 568, median: 566, mode: 566

###Data Validator###
dateFrom: 2021-09-14T15:30:14.517Z, dateTo: 2021-09-14T15:31:24.581Z

Transactions
	Successfull: 90,00%