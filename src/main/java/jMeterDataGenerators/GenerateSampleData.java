package jMeterDataGenerators;

import java.io.*;

import static data.DefaultStrings.pathSeparator;

public class GenerateSampleData {

    public String jmeterSampleDataPath =System.getProperty("user.dir")+pathSeparator+"Jmeter"+pathSeparator+"ccc-test-data";

    public static void main(String[] args) {
        GenerateSampleData g = new GenerateSampleData();
        try {
            //g.generateMachineUUIDsMachinesEndpoint(200);
            g.generateMachineUUIDsReliabilityEndpoint(200);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateMachineUUIDsReliabilityEndpoint(int amount) throws IOException {
        BufferedWriter bw =initWriter("machineUUIDs-reliability.txt");

        for(int i = 0;i<=amount;i++){
            String machineSuffix="";
            if (i<10)
                machineSuffix="0";
            machineSuffix+=i;
            bw.write("JM-"+machineSuffix+"\n");
        }

        bw.flush();
        bw.close();

    }

    private void generateMachineUUIDsMachinesEndpoint(int amount) throws IOException {
        BufferedWriter bw = initWriter("machineUUIDs.txt");
        String machinePrefix="JM-";

        for(int i = 1; i<=amount;i++ ){

            String machineSuffix="";
            if (i<10)
                machineSuffix="0";
            machineSuffix+=i;
            bw.write(machinePrefix+machineSuffix);
            if(i>0 && i%10==0)
                bw.write("\n");
            else
                bw.write(",");
        }

        bw.flush();
        bw.close();
    }

    private BufferedWriter initWriter(String fileName) throws IOException {
        File parent = new File(jmeterSampleDataPath);
        if(!parent.exists())
            parent.mkdir();

        return new BufferedWriter(new FileWriter(new File(parent,fileName)));
    }
}
