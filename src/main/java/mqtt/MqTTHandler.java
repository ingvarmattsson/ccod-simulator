package mqtt;

import ccodSession.Main;
import org.apache.jmeter.samplers.SampleResult;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.File;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static data.DefaultStrings.pathSeparator;

public class MqTTHandler implements MqttCallback{



    private String brokerHost, machineUUID;
    private String envPort = "1883";//2283,1883
    private int qos =2;

    private String envUserName;
    private String envUserPassword;
    private String publishTopic, subscribeTopic;
    private String response;
    private String tenant;


    private MqttClient client;
    private CountDownLatch processingFinished;
    private boolean publishedMEssageGotReply;
    private boolean waitForResponse;

    public MqTTHandler(String brokerHost, String tenant, String machineUUID, String mqttUserName, String mqqttUserPassword) throws MqttException {

        //this.brokerHost = "ssl://"+brokerHost+":"+envPort;
        String protocol="ssl://";
        publishTopic = tenant+"/inbound";
        subscribeTopic = tenant+"/device/"+machineUUID;

        if(brokerHost.equalsIgnoreCase("localhost") || brokerHost.equalsIgnoreCase("127.0.0.1")){
            protocol = "tcp://";
            publishTopic = tenant+"/device/"+machineUUID;
            subscribeTopic = tenant+"/inbound/"+machineUUID;
            envPort="1885";
        }

        this.brokerHost = protocol+brokerHost+":"+envPort;
        this.machineUUID =machineUUID;
        this.envUserName=mqttUserName;
        this.envUserPassword=mqqttUserPassword;
        this.tenant=tenant;

        if(Main.debugMode){
            System.out.println("MqttHandler Init\n\tbrokerhost: "+this.brokerHost+"\n\tpublishTopic: "+ publishTopic +"\n\tsubscribeTopic: "+ subscribeTopic+"\n\tbroker user: "+envUserName+"\n\tbroker password: "+envUserPassword);
        }


    }

    public void connect() throws MqttException {

        MemoryPersistence persistence = new MemoryPersistence();

        String randomClientId = tenant+"-"+machineUUID+":"+MqttAsyncClient.generateClientId();
        //String randomClientId = tenant+"-"+machineUUID+":"+Integer.toString(new Random().nextInt(Integer.MAX_VALUE));


        client= new MqttClient(brokerHost, randomClientId, persistence);
        client.setCallback(this);

        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setConnectionTimeout(30);
        connOpts.setKeepAliveInterval(15);
        connOpts.setUserName(envUserName);
        connOpts.setPassword(envUserPassword.toCharArray());
        connOpts.setCleanSession(true);

        connOpts.setSSLProperties(getSslProperties());

        client.connect(connOpts);

        if(waitForResponse){
            client.subscribe(subscribeTopic);
            processingFinished = new CountDownLatch(1);
        }

    }

    private Properties getSslProperties() {

        String path = System.getProperty("user.dir")+pathSeparator+"keyFiles"+pathSeparator;
        String keyStore ="mqtt-keystore.jks";
        String trustStore ="mqtt-truststore.jks";

        File parent = new File(path);

        Properties properties = new Properties();
        properties.setProperty("com.ibm.ssl.keyStore", new File(parent,keyStore).getAbsolutePath());
        properties.setProperty("com.ibm.ssl.keyStoreType", "JKS");
        properties.setProperty("com.ibm.ssl.keyStorePassword", "sitewhere");
        properties.setProperty("com.ibm.ssl.trustStore", new File(parent,trustStore).getAbsolutePath());
        properties.setProperty("com.ibm.ssl.trustStoreType", "JKS");
        properties.setProperty("com.ibm.ssl.trustStorePassword", "sitewhere");

        return properties;
    }


    public void publishMessage(String message) throws MqttException {

        MqttMessage payload = new MqttMessage(message.getBytes());
        payload.setQos(qos);
        client.publish(publishTopic,payload);
        publishedMEssageGotReply = false;

    }

    public void disconnect() throws MqttException, InterruptedException {
        if(client!=null && client.isConnected()){

            while (publishedMEssageGotReply=false){
                Thread.sleep(Main.rmProxyTimeoutThresholdSec*2000);
            }
            client.disconnect();
            client.close();
        }

    }

    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage message) throws Exception {

        response = new String(message.getPayload());
        processingFinished.countDown();
        publishedMEssageGotReply = true;

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    public void waitForProcessingToFinish(long timeout) throws InterruptedException {
        processingFinished = new CountDownLatch(1);
        processingFinished.await(timeout,TimeUnit.SECONDS);
    }

    public String getResponsedata() {
        return response;
    }

    public static void addErrorMessageToResult(SampleResult result, Exception e, String sessionData) {
        result.sampleEnd(); // stop stopwatch
        result.setSuccessful( false );

        //  result.setResponseMessage( sessionData+"\nException: " + e );

        // get stack trace as a String to return as document data
        java.io.StringWriter stringWriter = new java.io.StringWriter();
        e.printStackTrace( new java.io.PrintWriter( stringWriter ) );
        result.setResponseMessage(sessionData+"\n"+ stringWriter.toString() );
        result.setDataType( org.apache.jmeter.samplers.SampleResult.TEXT );
        result.setResponseCode( "500" );
    }

    public void setWaitForResponse(boolean waitForRMReply) {
        this.waitForResponse = waitForRMReply;
    }

    public void setQos(int mqttQos) {
        qos=mqttQos;
    }
}
