package crudTests;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import rest.RestHandler;
import rest.Token;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import static crudTests.RestOperation.*;

public class RestExecutor {

    private String tenant;
    private RestHandler restHandler;
    private Token token;
    private List<TestData> testDataExpected, testDataActual;
    private String baseUrl;
    private long timeOfTokenSet;

    public RestExecutor(RestHandler restHandler, String baseUrl, String tenant){
        this.restHandler=restHandler;
        this.baseUrl =baseUrl+"/api/v1";
        this.tenant=tenant;
        testDataExpected = new LinkedList<>();
        testDataActual = new LinkedList<>();
    }

    private TestData runTestInternal(TestData tData) throws IOException, TestCaseException, URISyntaxException {
        if (tData == null)
            throw new TestCaseException("TestData null");
        if (tokenExpired())
            if ((token = restHandler.getToken()) == null)
                throw new TestCaseException("Unable to refresh token");
        if (tData.operation == null)
            throw new TestCaseException("No operation set");
        if (tData.endpoint == null || tData.endpoint.isEmpty())
            throw new TestCaseException("No endpoint set");
        if ((tData.operation == RestOperation.PUT || tData.operation == DELETE) && (tData.token == null || tData.token.isEmpty()))
            throw new TestCaseException("Missing token for PUT / DELETE reguest");
        if ((tData.operation == POST || tData.operation == PUT) && (tData.payload == null || tData.payload.isEmpty()))
            throw new TestCaseException("Missing payload for POST / PUT request");

        URIBuilder builder = new URIBuilder(baseUrl + tData.endpoint);

        if (tData.parametersRequest != null && !tData.parametersRequest.isEmpty()) {
            List<String> keys = new LinkedList<>(tData.parametersRequest.keySet());
            for (String key : keys) {
                builder.setParameter(key, tData.parametersRequest.get(key));
            }
        }

        HttpRequestBase request = null;
        switch (tData.operation) {
            case GET:
                request = new HttpGet(builder.build());
                break;
            case PUT:
                request = new HttpPut(builder.build());
                break;
            case POST:
                request = new HttpPost(builder.build());
                ((HttpPost) request).setEntity(new StringEntity(tData.payload));
                break;
            case DELETE:
                request = new HttpDelete(builder.build());

                break;
        }

        restHandler.setGeneralRequestHeaders(request,tData.contentType);
        restHandler.setAuthRequestHeaders(request, tenant, token);

        long startTime = System.currentTimeMillis();

        do {
            HttpResponse response = restHandler.executeRequest(request);
            tData.responseTimes.add((int) (System.currentTimeMillis() - startTime));
            tData.responseCodes.add(response.getStatusLine().getStatusCode());
            tData.responseData.add(EntityUtils.toString(response.getEntity()));
        } while (pagedRequestHasMorePages(tData));

        return tData;

    }

    private boolean pagedRequestHasMorePages(TestData tData) {
        List<String> parameters = new LinkedList<>(tData.parametersRequest.keySet());
        if(parameters == null)
            return false;
        if (!parameters.contains("pageSize"))
            return false;
        else{
                //TODO handle paged reqest somehow, prob by converting response to obj and get numResults(if all endpoints return it)
        }

        return true;
    }

    public TestData executeRequest(TestData testDataExpected) throws URISyntaxException {

        TestData testDataActual = null;
        try {
            testDataActual =  runTestInternal(testDataExpected);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TestCaseException e) {
            e.printStackTrace();

        }

        return testDataActual;

    }

    private boolean tokenExpired() {
        long tokenAliveTime = System.currentTimeMillis()-timeOfTokenSet;
        if(Long.compare(tokenAliveTime,(token.expires_in*1000))>=0)
            return true;
        return false;
    }

    public void executeRequests(List<TestData> testCases) throws IOException, URISyntaxException {

        if (testCases != null && !testCases.isEmpty())
            for (TestData  testData : testCases)
               testData = executeRequest( testData);

    }

    public void setToken(Token token) {
        timeOfTokenSet = System.currentTimeMillis();
        this.token = token;
    }
}
