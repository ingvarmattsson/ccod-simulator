package crudTests;

public class TestCaseException extends Throwable {
    public TestCaseException(String unable_to_refresh_token) {
        super(unable_to_refresh_token);
    }
}
