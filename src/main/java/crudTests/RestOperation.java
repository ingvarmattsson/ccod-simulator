package crudTests;

public enum RestOperation {
    POST, PUT, GET, DELETE
}
