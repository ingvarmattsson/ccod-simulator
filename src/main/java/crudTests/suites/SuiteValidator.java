package crudTests.suites;

import crudTests.TestData;

import java.util.List;

public interface SuiteValidator {

    public void validate(List<TestData> testCases);
}
