package crudTests.suites;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import crudTests.RestExecutor;
import crudTests.RestOperation;
import crudTests.TestData;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.*;

import static crudTests.RestOperation.POST;

public class SitesSuite {

    private RestExecutor executor;
    private String endpoint = "/sites";
    private List<TestData> tests;
    private String contentType="application/json";
    private SitePayload site;

    private int siteNameSuffix = 0;
    private String imageUrlDefault ="https://connect.cashcomplete.com/assets/img/favicon/favicon.ico";
    private ObjectMapper objectMapper;

    public SitesSuite(RestExecutor executor, ObjectMapper objectMapper) {
        this.executor = executor;
        this.objectMapper=objectMapper;
        tests = new LinkedList<>();

    }

    public List<TestData > getSites() throws URISyntaxException {

        /*builder.setParameter("sortField","name");
        builder.setParameter("sortOrder","asc");
        builder.setParameter("pageSize","1000");*/

        //GET no params
        TestData tData = new TestData();
        tData.operation = RestOperation.GET;
        tData.endpoint = endpoint;
        TestData tResult = executor.executeRequest(tData);

        return null;
    }

    public List<TestData> putSites() {

        return null;
    }

    public List<TestData> postSites() throws URISyntaxException, JsonProcessingException {

        //only required fields
        TestData tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="200";
        tData.description ="Only required parametersRequest";
        tData.payload = generatePayload("Site TA - "+siteNameSuffix++,"","",null,null, null);
        tests.add(executor.executeRequest(tData));

        //with description, customer reference
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="200";
        tData.description ="With description and customerReference";
        tData.payload = generatePayload("Site TA - "+siteNameSuffix++,"Desc: "+tData.operation+", "+site.name,"cr-"+site.name,null,null,null);
  //      tests.add(executor.executeRequest(tData));

        //with geo location
        String lat ="59.314938560212426", lon="18.075750209391117";
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="200";
        tData.description ="With geo location";
        tData.payload = generatePayload("Site TA - "+siteNameSuffix++,"","cr-"+site.name,lat,lon,null);
    //    tests.add(executor.executeRequest(tData));

        //Missing fields 1 - name
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="400";
        tData.description ="Missing field - name";
        tData.payload = generatePayload(null,"","cr-"+site.name,lat,lon,null);
  //      tests.add(executor.executeRequest(tData));

        //Missing fields 2 - description
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="200";
        tData.description ="Missing field - description";
        tData.payload = generatePayload("Site TA - "+siteNameSuffix++,null,"cr-"+site.name,null,null,null);
   //     tests.add(executor.executeRequest(tData));

        // Missing fields 3 - customer reference
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="200";
        tData.description ="Missing field - customerReference";
        tData.payload = generatePayload("Site TA - "+siteNameSuffix++,"",null,null,null,null);
 //       tests.add(executor.executeRequest(tData));

        // Missing fields 4 - map
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="400";
        tData.description ="Missing field - map";
        tData.payload = generatePayload("no map - "+siteNameSuffix++,"",null,null,null,null);
        tests.add(executor.executeRequest(tData));

        //invalid payload
        tData = new TestData();
        tData.operation = POST;
        tData.endpoint=this.endpoint;
        tData.contentType=contentType;
        tData.expectedResponseCode="400";
        tData.description ="Invalid paylaod";
        tData.payload = generatePayload("no map - "+siteNameSuffix++,"",null,null,null,null);
        tData.payload = tData.payload.substring(0,tData.payload.length()-1);
        tests.add(executor.executeRequest(tData));


        System.out.println();
        return tests;
    }

    private String generatePayload(String name,String description, String customerReference, String centerLat, String centerLon, String token) throws JsonProcessingException {
        site = new SitePayload();
        site.name = name;
        site.description = description;
        site.imageUrl = imageUrlDefault;
        site.metadata.put("customerReference",customerReference);

        if(centerLat!=null && centerLon!=null){
            site.map.metadata.put("zoomLevel","7");
            site.map.metadata.put("centerLatitude",centerLat);
            site.map.metadata.put("centerLongitude",centerLon);
        }

        if(name!= null && name.startsWith("no map"))
            site.map = null;

        //String payload = objectMapper.writeValueAsString(site);
        String payload = new Gson().toJson(site);
        return payload;
    }

    public List<TestData> deleteSites() {
        return null;
    }

    public List<TestData> executeTests() throws URISyntaxException, JsonProcessingException {
        List<TestData> tests = new LinkedList<>();
        tests.addAll(postSites());
        tests.addAll(putSites());
        tests.addAll(getSites());
        tests.addAll(deleteSites());
        new SitesSuiteValidator().validate(tests);
        return tests;
    }

    private class SitePayload implements Serializable {
        private static final long serialVersionUID = 1L;

        private String name, imageUrl, description, token;
        private MapPayLoad map;
        private Map<String, String> metadata;

        private SitePayload(){
            map = new MapPayLoad();
            metadata = new HashMap();
        }

    }

    private class MapPayLoad implements Serializable{
        private static final long serialVersionUID = 1L;

        private String type;
        private Map<String,String> metadata;
        private MapPayLoad(){
            type="openstreetmap";
            metadata = new HashMap<>();
        }

    }

    private class SitesSuiteValidator implements SuiteValidator{

        @Override
        public void validate(List<TestData> testCases) {

        }
    }
}
