package crudTests;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TestData {
    public boolean passed;
    public String description;
    public RestOperation operation;
    public String endpoint;
    public String dateTime;
    public String payload;
    public String token;
    public Map<String,String> parametersRequest;
    public List<String> responseData;
    public List<Integer> responseCodes;
    public List<Integer> responseTimes;
    public String contentType;
    public String expectedResponseCode;


    public TestData(){
        responseData = new LinkedList<>();
        parametersRequest = new HashMap<>();
        responseCodes = new LinkedList<>();
        responseTimes = new LinkedList<>();
    }
}
