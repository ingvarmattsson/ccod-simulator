package crudTests;

import ccodSession.Environments;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import crudTests.suites.SitesSuite;
import data.DefaultStrings;
import rest.RestHandler;
import rest.Token;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import static ccodSession.Environments.UAT;

public class TestSuiteExecutor {

    protected RestHandler restHandler;
    private String tenant ="testautomation";
    protected String baseUrl;
    private Environments environment = UAT;
    protected Token token;
    private List<TestData> results;
    private RestExecutor executor;
    private ObjectMapper objectMapper;

    public static void main(String[] args) {
        new TestSuiteExecutor().runTests();
    }

    private void runTests() {
        results = new LinkedList<>();
        baseUrl = DefaultStrings.getBaseURL(environment);
        restHandler = new RestHandler(environment);
        executor = new RestExecutor(restHandler,baseUrl,tenant);
        initObjectMapper();

        TestData testResult = new TestData();
        testResult.operation=RestOperation.POST;
        testResult.endpoint="/token";
        try {
            token = restHandler.getToken();
            if(token==null){
                testResult.passed = false;
                System.out.println("Unable to get Authorization Token - terminating");
            }
            else
                testResult.passed = true;

            if(token!=null){
                executor.setToken(token);
                executeSuites();

            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initObjectMapper() {
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

    }

    private void executeSuites() throws URISyntaxException, JsonProcessingException {
        results.addAll(new SitesSuite(executor,objectMapper).executeTests());

    }


}
