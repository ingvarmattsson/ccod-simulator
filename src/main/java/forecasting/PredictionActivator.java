package forecasting;

import CCoD.Currency;
import CCoD.ValueType;
import ccodSession.Environments;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import data.DefaultStrings;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import rest.RestHandler;
import rest.Token;

import java.io.IOException;
import java.util.*;

import static CCoD.Currency.EUR;
import static CCoD.ValueType.NOTE;
import static ccodSession.Environments.UAT;

public class PredictionActivator {

    private String[] machineIds ={};


    private String machineIdPrefix ="JM-";
    private int machinesAmount = 30;

    private boolean setPredictable = true;
    private static Environments environment = UAT;
    private String tenant ="testautomation";



    private String[] denominations = {"500","1000"};
    private ValueType[] valueTypes = {NOTE, NOTE};
    private Currency[] currencies = {EUR,EUR};
    private String[] thresholds = {"0.15","0.15"};
    private int[] alertLevel = {500,500};
    private int[] decimals = {2,2};


    public static void main(String[] args) throws IOException {

        PredictionActivator activator = new PredictionActivator();
        activator.initPayloads();
        activator.activatePredictions(new RestHandler(environment));

    }

    private void activatePredictions(RestHandler restHandler) {
        try {

            Token token =restHandler.getToken();
            List<HttpResponse> responses = new LinkedList<>();
            String endpoint="/api/v1/forecasting/predictable";


            for(int i=0;i<machineIds.length;i++){

                for(int j =0;j<denominations.length;j++){

                    StringBuilder sb = new StringBuilder();
                    sb.append("uuid="+machineIds[i]+"&");
                    sb.append("predictable="+Boolean.toString(setPredictable)+"&");
                    sb.append("denomination="+denominations[j]+"&");
                    sb.append("type="+valueTypes[j]+"&");
                    sb.append("currency="+currencies[j].name()+"&");
                    sb.append("decimals="+decimals[j]+"&");
                    sb.append("threshold="+thresholds[j]+"&");
                    sb.append("alertlevel="+alertLevel[j]);

                    String params = "?"+sb.toString();
                    HttpPost postRequest = new HttpPost(DefaultStrings.getBaseURL(environment)+endpoint+params);


                    restHandler.setAuthRequestHeaders(postRequest,tenant,token);
                    postRequest.setHeader(HttpHeaders.ACCEPT, "application/json");
                    //postRequest.setHeader(HttpHeaders.ACCEPT_ENCODING,"gzip, deflate, br");
                    postRequest.setHeader(HttpHeaders.ACCEPT_LANGUAGE,"en-US,en;q=0.9,sv;q=0.8,sv-SE;q=0.7");
                    postRequest.setHeader(HttpHeaders.CONNECTION, "keep-alive");
                    postRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
                    postRequest.setHeader(HttpHeaders.HOST, "test-api.cashcomplete.com");
                    postRequest.setHeader("Origin","htpps://connect-test.cashcomplete.com");
                    postRequest.setHeader(HttpHeaders.REFERER, "htpps://connect-test.cashcomplete.com");
                    postRequest.setHeader("Sec-Fetch-Mode", "cors");
                    postRequest.setHeader("Sec-Fetch-Site", "same-site");
                    postRequest.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");


                    evalResponses(restHandler.executeRequest(postRequest));
                }

            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException | JsonSyntaxException exception) {
            System.out.println(exception);
        }
    }

    private void evalResponses(HttpResponse response) throws IOException {
        int status = response.getStatusLine().getStatusCode();
        System.out.print("Status: "+status);
        if(status==201){

            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity);
            System.out.println(content);
            //ForecastingResponse fRes = new Gson().fromJson(content,ForecastingResponse.class);


        }else
            System.out.println("Post unsuccessfull");

    }

    private void initPayloads() {

        if(machineIds.length<=0){
            machineIds = new String[machinesAmount];

            for (int i = 1; i<= machinesAmount;i++){
                String machineIdSuffix="";
                if(i<10)
                    machineIdSuffix="0";
                machineIdSuffix+=i;
                String machineID = machineIdPrefix+machineIdSuffix;
                machineIds[i-1] = machineID;
            }
        }

    }

    private class ForecastingResponse {
        public String uuid;
        public boolean predictable;
        public Map<String, ForecastingDenomination> denominations;
    }

    private class ForecastingDenomination{
        public String denomination;
        public int decimals;
        public String type;
        public String currency;
        public double threshold;
        public int alertlevel;
    }
}
