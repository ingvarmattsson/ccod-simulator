package tenantEditionAdder;

import ccodSession.Environments;
import com.fasterxml.jackson.databind.ObjectMapper;
import rest.RestHandler;
import tenantAuthorization.SiteWhereTenant;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class TenantEditionAdder {

    private Environments env = Environments.UAT;
    private String edition ="basic";
    private List<SiteWhereTenant> allTenants;
    private List<SiteWhereTenant> tenantsToUpdate;

    public static void main(String[] args) {
        TenantEditionAdder tea = new TenantEditionAdder();
        try {
            tea.getTenants();
            tea.findTenantsWithMissingEdition();
            tea.updateTenants();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void updateTenants() throws IOException, URISyntaxException {
        for(SiteWhereTenant tenant : tenantsToUpdate){
            tenant.metadata.put("edition",edition);
            RestHandler rest = new RestHandler(env);
            rest.getTokenAuth();
            String payload = new ObjectMapper().writeValueAsString(tenant);
            System.out.println(payload);
            System.out.println(
                    tenant.id+" - "+
                    rest.putTenant(
                        payload,
                        tenant.id
            ));;
        }
    }

    private void findTenantsWithMissingEdition() {
        tenantsToUpdate = new LinkedList<>();
        for(SiteWhereTenant tenant : allTenants)
            if(tenant.metadata.get("edition")==null && tenant.metadata.get("modules")==null){
                System.out.println("Found tenant with missing edition: "+tenant.id);
                tenantsToUpdate.add(tenant);
            }

    }

    private void getTenants() throws IOException, URISyntaxException {
        RestHandler rest = new RestHandler(env);
        rest.getTokenAuth();
        allTenants =rest.getTenants("");
    }
}
