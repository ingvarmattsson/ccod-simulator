package rmReplay;

import ccodSession.Environments;
import ccodSession.EventType;
import ccodSession.Main;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.javafaker.Faker;
import data.DefaultStrings;
import resultGenerator.ResultEvent;
import rmObjects.RmHandler;
import rmObjects.RmTransaction;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static data.DefaultStrings.csvDelimeter;

public class Anonymizer {

    protected static String pathFrom = System.getProperty("user.dir")+"\\rmDocuments\\rmMessagesRetained\\";
    protected static String pathTo = System.getProperty("user.dir")+"\\rmDocuments\\";
    protected static final String suffixTransactions="anonymizedTransactions.rm";
    protected static final String suffixMachineEvents ="doc.rm";

    static String oldMachineId ="NL-R802N11-3521AL6", newMachineID ="SEL-03";


    private boolean parseResults=true;
    private Environments environment;
    private String date;
    private List<String> results;

    public static void main(String[] args) {
        File folder = new File(pathFrom);
        Anonymizer listFiles = new Anonymizer();
        try {
            listFiles.initWriter();
            listFiles.listAllFiles(folder);
            listFiles.listAllFiles(pathFrom);
            listFiles.closeWriter();
            List<String> parsedTransactions =anonymizeTransactionUsers(tempTransactions);
            writeTransactions(parsedTransactions);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Anonymizer(){}

    public Anonymizer(String pathFrom, Environments environment, String date){
        this.pathFrom=pathFrom;
        this.environment = environment;
        this.date=date;
        File folder = new File(pathFrom);
        this.parseResults=true;
        results = new LinkedList<>();

        listAllFiles(folder);
        listAllFiles(pathFrom);

    }

    private void closeWriter() throws IOException {
        bw.flush();
        bw.close();
    }

    private  BufferedWriter bw;
    private void initWriter() throws IOException {
        bw = new BufferedWriter(new FileWriter(pathTo+ suffixMachineEvents));
    }

    private static void writeTransactions(List<String> parsedTransactions) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(pathTo+suffixTransactions)));

        for(String s : parsedTransactions)
            bw.write(s+"\n");
        bw.flush();
        bw.close();
    }

    private static List<String> anonymizeTransactionUsers(List<RmTransaction> tempTransactions) throws JsonProcessingException {
        Map<String, String> uniqueUsersOld = new HashMap<>();

        //get all uniquie users
        for (RmTransaction t : tempTransactions){
            uniqueUsersOld.put(t.USER.NAME,null);
            if(t.USER.ORIGINATING_USER_FULL!=null) {
                uniqueUsersOld.put(t.USER.NAME,t.USER.ORIGINATING_USER_FULL.NAME);
            }
        }

        HashMap<String,List<RmTransaction>> transactionsGroupedOnOldUser = new HashMap<>();

        //generate fake names
        List<String> uniqueOldNames = new LinkedList<>(uniqueUsersOld.keySet());
        List<String> fakeUsers = new LinkedList<>();
        String delimeter="<$>";
        for(String key :uniqueOldNames){
            transactionsGroupedOnOldUser.put(key,new LinkedList<>());
            String fakeUser = new Faker().name().fullName();
            if(uniqueUsersOld.get(key)!=null)
                fakeUser+=delimeter+ new Faker().name().fullName();
            fakeUsers.add(fakeUser);
        }


        List<String> parsedTransactions = new LinkedList<>();

        //group transactions on old users
        List<String> keysToUniqueUserTransaction = new LinkedList<>(transactionsGroupedOnOldUser.keySet());
        for(String key : keysToUniqueUserTransaction){
            for(RmTransaction t : tempTransactions)
                if(t.USER.NAME.equals(key))
                    transactionsGroupedOnOldUser.get(key).add(t);

        }

        //iterate through grouped users, change to fake user
        for(String key : transactionsGroupedOnOldUser.keySet()){
            List<RmTransaction> transactions = transactionsGroupedOnOldUser.get(key);
            String fakeUser = ((LinkedList<String>) fakeUsers).removeFirst();
            String[] userOriginatingUser = fakeUser.split(delimeter);
            for(RmTransaction t : transactions){
                t.USER.NAME=userOriginatingUser[0];
                if(userOriginatingUser.length>1)
                    t.USER.ORIGINATING_USER_FULL.NAME=userOriginatingUser[1];
                parsedTransactions.add(RmHandler.parseRmToString(t));
            }

        }

        return parsedTransactions;

    }


    // Uses listFiles method

    private String currentTenant;

    public void listAllFiles(File folder){
        File[] fileNames = folder.listFiles();
        for(File file : fileNames){
            // if directory call the same method again
            if(file.isDirectory()){
                listAllFiles(file);
            }else{
                if(parseResults){
                    String[] fileName = file.getName().split("_");
                    fileName[fileName.length-1] = fileName[fileName.length-1].substring(0,fileName[fileName.length-1].indexOf("."));
                    if(fileName[0].equals(environment.name()) && fileName[fileName.length-1].equals(date)){
                        currentTenant = fileName[1];
                        readContentSessionLogs(file);
                    }
                }

                else
                    readContentRmReplay(file);
            }
        }
    }
    // Uses Files.walk method
    public void listAllFiles(String path){
        try(Stream<Path> paths = Files.walk(Paths.get(path))) {
            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        if(parseResults)
                            readContentSessionLogs(filePath);
                        else
                            readContentRmReplay(filePath);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readContentSessionLogs(File file) {


        try(BufferedReader br  = new BufferedReader(new FileReader(file))){
            String strLine;
            results.add(DefaultStrings.resultTenantIndexer +currentTenant);

            while((strLine = br.readLine()) != null){
                String[] line= strLine.split(csvDelimeter);

                String timeStamp = line[0];

                if(timeStamp.equals("timeStamp")) {
                    continue;
                }

                results.add(strLine);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static List<RmTransaction> tempTransactions  = new LinkedList<RmTransaction>();


    public void readContentRmReplay(File file){
//new FileReader(file)
        try(BufferedReader br  = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))){

            String strLine;

            while((strLine = br.readLine()) != null){

                //replace all machine_id
                String replaced = strLine.replaceAll(oldMachineId,newMachineID);
                //   bw.write(replaced+"\n");

                //Internal Adapter state, BoxContent and Errors
                if(strLine.startsWith("{\"CLEARED\"")){
                    bw.write(replaced+"\n");
                }
                else if(strLine.startsWith("{\"INTERNAL_ADAPTER_STATE\"")){
                    //TODO investigate why boxcontents wont arrive then write to file too
                }

                //transactions - need to process each and anonymize user
                else{

                    RmTransaction t  =  RmHandler.parseStringToRmTransaction(replaced);
                    tempTransactions.add(t);
                }

            }
            br.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readContentRmReplay(Path filePath) throws IOException{
        List<String> fileList = Files.readAllLines(filePath);
    }

    public void readContentSessionLogs(Path filePath) throws IOException {
        List<String> fileList = Files.readAllLines(filePath);
    }

    public List<String> getResults() {
        return results;
    }
}
