package rmReplay;

import ccodSession.Environments;
import data.DefaultStrings;
import data.Utilities;
import mqtt.MqTTHandler;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class RmReplay implements Runnable{

    private String brokerHost;
    private String tenant;
    private String machineUUID;
    private String mqttUserName;
    private String mqttPassword;
    private String fileName;

    public static void main(String[] args) {
        new RmReplay().sendMessagesFromFile();
    }


    public RmReplay() {
        brokerHost=DefaultStrings.getBrokerHost(Environments.UAT);
        tenant="sdbusiness";
        machineUUID = "a3c2e3ac-0b9d-437f-80b6-c00de68d2371";
        this.mqttUserName = DefaultStrings.mqttUserName;
        this.mqttPassword = DefaultStrings.mqttPassWord;
        fileName="rct1.rm"; //suffixTransactions suffixMachineEvents for retained rm messages 58669a2a-4618-4455-b61e-2cb1914a2332.rm

    }

    public RmReplay(String brokerHost, String tenant, String machineUUID, String mqttUserName, String mqttPassword, String fileName){
        this.brokerHost=brokerHost;
        this.tenant=tenant;
        this.machineUUID=machineUUID;
        this.mqttUserName=mqttUserName;
        this.mqttPassword=mqttPassword;
        this.fileName = fileName;

    }

    public List<String> sendMessagesFromFile() {

        String path = System.getProperty("user.dir")+"\\rmDocuments\\"+fileName;
        MqTTHandler mqtt;
        BufferedReader br;
        List<String> failures = new LinkedList<>();
        List<String> sentMessages= new LinkedList<>();

        try {
            mqtt = new MqTTHandler(brokerHost,tenant,machineUUID,this.mqttUserName,this.mqttPassword);
            mqtt.connect();

            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(
                                    path
                            )
                    )
            );

            String payload=null;

            int messageNbr = 1;
            while((payload=br.readLine())!=null){

                System.out.println("Message nbr: "+messageNbr+",\t"+ Utilities.getCurrentDateStampIncludedCatchingUp()+"\t-\t");
                mqtt.publishMessage(payload);
                mqtt.waitForProcessingToFinish(30);
                String mqttResponse = mqtt.getResponsedata();

                if (mqttResponse==null){
                    failures.add(payload);
                    System.out.println("failed - null response: "+payload);
                }
                else{
                    sentMessages.add(payload);
                    System.out.println(payload);

                }

                Thread.sleep(500);
                messageNbr++;

            }

            mqtt.disconnect();

        } catch (MqttException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        printFailures(failures);
        return sentMessages;

    }

    private void printFailures(List<String> failures) {

        if(failures.isEmpty())
            System.out.println("All rm messages replayed");
        else{

            System.out.println("Following messages failed to be sent");

            for(String s : failures)
                System.out.println(s);

        }
    }

    @Override
    public void run() {
        sendMessagesFromFile();
    }
}
