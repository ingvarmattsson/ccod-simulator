package onlineStatusHistoryFetcher;

import cccObjects.DeviceCCC;
import cccObjects.DeviceCCCResponse;
import cccObjects.MachineOnlineStatusHistoricCCC;
import cccObjects.MachineOnlineStatusHistoricResponseCCC;
import ccodSession.Environments;
import data.Utilities;
import exceptions.ConfigFileException;
import rest.RestHandler;
import rest.RestUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class OnlineStatusHistoryFetcher {

    private String fromDate="2021-07-01T00:00:00.000Z";
    private String toDate="2021-07-09T00:00:00.000Z";
    private String tenant="brinksprod";
    private Environments env =Environments.US_EAST;
    private boolean readArgs=false;

    //args: Environment tenant fromDate toDate
    public static void main(String[] args) {
        new OnlineStatusHistoryFetcher().run(args);

    }

    private void run(String[] args) {
        try {
            if(readArgs)
                getValuesFromArgs(args);
            checkDaterangeValid();
            onlineStatusAllMachines(RestUtils.getMachinesPagedAllMachines(env,tenant));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ConfigFileException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void getValuesFromArgs(String[] args) throws ConfigFileException {
        if(args.length<4)
            throw new ConfigFileException("Missing args when running the jar. Expected: environment tenant fromDate toDate. " +
                    "\nExample: java -jar OnlineStatusHistoryFetcher.jar US_EAST brinksprod 2021-06-01T00:00:00.000Z 2021-06-15T00:00:00.000Z" +
                    "\nEnvironments: "+ Utilities.printEnvironments());
        env=Environments.valueOf(args[0]);
        tenant = args[1];
        fromDate = args[2];
        toDate = args[3];

    }

    private void checkDaterangeValid() throws ConfigFileException, ParseException {
        System.out.println("Checking date params validity");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        Date fDate = formatter.parse(fromDate);
        Date tDate = formatter.parse(toDate);
        long days14InMS = 1209600000;
        if(tDate.getTime()-fDate.getTime()>days14InMS)
            throw new ConfigFileException("Date range is too big, maximum allowed range is 14 days. Got: "+fromDate+" - "+toDate);

    }

    private void onlineStatusAllMachines(List<DeviceCCC> machines) throws IOException, URISyntaxException {
        System.out.println("Printing document");
        String dir = System.getProperty("user.dir")+"/";
        String fDate = fromDate.replace(":","-");
        fDate.replace(".","-");
        String tDate = toDate.replace(":","-");
        tDate.replace(".","-");
        String fileName="onlinehistory_"+env+"_"+tenant+"_"+fDate+"_"+tDate+".txt";

        BufferedWriter writer = new BufferedWriter(new FileWriter(dir+fileName));
        writer.write("############################## MACHINE ONLINE HISTORY ####################################");
        writer.write("\n\nNode: "+env);
        writer.write("\nTenant: "+tenant);
        writer.write("\nDate range: "+fromDate+" - "+toDate);
        writer.write("\n\n##########################################################################################");

        int machineNumber = 0;
        for(DeviceCCC machine : machines){
            writer.write("\n__________________________________________________________________________________________");
            writer.write("\nSITE TOKEN: "+machine.siteToken);
            writer.write("\nUUID: "+machine.hardwareId);

            if(machine.metadata.alias!= null)
                writer.write("\nALIAS: "+machine.metadata.alias);
            writer.write("\nONLINE HISTORY: \n");

            machineNumber++;
            System.out.println("Fetching onlinestatus for machine ("+machineNumber+"/"+machines.size()+"): "+machine.hardwareId);
            MachineOnlineStatusHistoricResponseCCC mos = getMachineOnlineStatus(machine.hardwareId);
            if(mos==null)
                writer.write("\n\t COULDN'T FIND ONLINE STATUS FOR THIS MACHINE (500)");
            else{
                for(MachineOnlineStatusHistoricCCC m : mos.results){
                    StringBuilder sb = new StringBuilder();
                    if(!m.onlineStatus.equalsIgnoreCase("Unknown"))
                        sb.append("\n\t\tSTATUS: "+m.onlineStatus);
                    if (m.receivedDate!=null)
                        sb.append("\t\tTIME OF STATUS CHANGE: "+m.receivedDate);
                    writer.write(sb.toString());
                }

            }
            writer.write("\n__________________________________________________________________________________________");
            writer.flush();
        }
        writer.close();
    }

    private MachineOnlineStatusHistoricResponseCCC getMachineOnlineStatus(String hardwareId) throws IOException, URISyntaxException {
        RestHandler restHandler = new RestHandler(env);
        restHandler.getTokenAuth();
        return restHandler.getMachineOnlineStatusHistoric(env,tenant,hardwareId,fromDate,toDate);
    }

}
