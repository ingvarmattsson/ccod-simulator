package ccodSession;

import CCoD.Currency;
import CCoD.MachineModels;
import CCoD.TransactionType;
import cli.Interface;
import data.AccountingDateOffset;
import data.DataGenerator;
import data.DefaultStrings;
import data.Utilities;
import exceptions.ConfigFileException;
import resultGenerator.ResultGenerator;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static CCoD.Currency.*;
import static CCoD.TransactionType.*;
import static ccodSession.Environments.*;
import static data.DefaultStrings.pathSeparator;


public class Main {

    /*target tenants (for forcedTenant variable):
    UAT: testautomation or whichever
    DEV: suzohappbasic, suzohappbusiness, suzohapptransport, suzohappretail
    PROD: salesdemobasic, salesdemobusiness,salesdemotransport (or salesdemotransportation on US), salesdemoretail (or salesdemoretailedition edition on DE)
    Each of these tenants have JM-01 to JM-30 machines registered.
    */

    //Configurables
    public static int sessionEventLoopDelayMs =0; // delay between each iteration of the run loop in CCoDSession
    public static int eventDelayMs =0; // delay between each rm message to send
    public static int machineIndexStart =1, amountMachines = 20; // used to initiate JM-machineIndexStart to JM-machineIndexStart+amountMachines for thenants that have JM-n machines registered (testautomation on uat, salesdemo tenants on prod nad suzohapptenants on dev)
    private static int amountMachinesSequentiallyinitiated =1400;
    private static boolean sequentiallyInitMachines=false;
    protected int machineRampUpMs =0; //delay between starting each ccod session
    private long runTimeMin =1; // how long each CCoDSession will run, if <=0 then will only send some initial boot messages and box contents

    private boolean waitForRMReply = false;
    private int mqttQos = 0;
    protected static Environments environment= UAT; // target environment, not used if mqttURLOverride!=null
    private static String forcedTenant="sdbusiness";//,salesdemobusiness,salesdemotransport,salesdemoretail";// target tenants, comma separetad string eg: "testautomation,alabama";
    private String mqttURLOverride; //optional, use to force custom mqtt broker host instead for environment variable

    public static boolean debugMode=true; // additional info printed in console
    public static boolean demoMode=false; // when true session logs will not be printed, leave as false
    public static boolean includeOptionalFieldsRm = true; // will randomly generate optional fields for rm messages

    public static String timeZoneOffset="+02:00";
    private static Currency currency = EUR; // currencies set for transactions and box contents

    //if customMachinesToInit!=null then these machine uuids will be initiated instead of JM-n machines. For each uuid you need to currencies in customMachinesCurrencies array
    private static String[] customMachinesToInit ={"JM-01"};
    private static Currency[] customMachinesCurrencies = {EUR};//set currency for each machine in array above, transaction/box content data

    //messageTypes to send
    private boolean sendErrors =true; //every fifth machine will send errors and will send clear errors periodically or on next startup of the simulator
    private boolean allMachinesSendingErrors = true; //if false only every fifth machine sends errors
    private boolean sendSystemStatuses=true;
    private boolean sendTransactions=true;
    private boolean sendBoxContents=true;
    private boolean syncWorkUnits = false;
    private boolean sendBootMessages =true;
    private static boolean sendUserUpdateRequests=true;

    private boolean testZTSRobustness=false;
    private boolean sendDLSTransactions = false;

    //lane allocations
    private boolean sendLaneAllocations=false;
    public static boolean sendLaneAllocationsEnd=false;
    private boolean sendLaneAllocationDayBreak=false;
    private int dayBreakHour=12, daybreakMinute=20;

    private boolean sendTransactionsInOrder=true;
    private boolean useDLSUsers = true;

    public static long minusSecondsRmEvent = 0; //set to > 300 to simulate catching up online status in CCC
    public static boolean excludeAccountingDate=false;
    private static AccountingDateOffset accountingDateOffset = null; // init this object to have different ACCOUNTING_DATE && DATE_TIME for rm-transactions
    private int dataValidatorSleepMin = 0; // wait time before making a rest request to GET /transactions and validate that all transactions generated within the session are in CCC


    private static boolean readConfigFile = false; //if true variables set from config.properties else above variables, only for debugging, should always be true when building jar

    public static int rmProxyTimeoutThresholdSec =30; // how long mqttHandler should wait for a reply before timing out and deeming the message as failed_


    private long sessionStartTime;
    private List<Thread> ccodSessions;
    private List<String> tenants;


    public SessionDataStore getStore() {
        return store;
    }

    private SessionDataStore store;
    private String filepathDataStore;
    private String brokerHost;
    private boolean initRCSs=false; //false for JM-n machines
    private int failedMachines;
    private String dateFromQParam;


    public Main(){
        filepathDataStore = System.getProperty("user.dir")+pathSeparator+"sessionData"+pathSeparator+"savedSessionStates";
        failedMachines=0;
    }

    public static void main(String[] args) {

        Main main = new Main();
        Interface.printLogo();

        if( readConfigFile && !main.loadConfiguration())
            System.exit(0);

        main.setEnvironment(environment,forcedTenant);

        main.loadSessionDataStore();

        try{

            if(sequentiallyInitMachines){
                int cutoff = machineIndexStart+amountMachinesSequentiallyinitiated;
                while (machineIndexStart< cutoff){
                    System.out.println("machineindex: "+machineIndexStart);
                    System.out.println("cutoff: "+cutoff);
                    if(customMachinesToInit !=null && customMachinesToInit.length>0){
                        if(!DataGenerator.initMachineUserPool(customMachinesToInit.length))
                            sendUserUpdateRequests = false;
                        main.initCustomSessions();
                    }
                    else{
                        if(!DataGenerator.initMachineUserPool(amountMachines))
                            sendUserUpdateRequests =false;
                        main.initSessions();
                    }

                    main.startSessions();
                    main.waitForSessionsToEnd();
                    main.saveSessionDataStore();

                    machineIndexStart+=amountMachines;
                }
                if(!demoMode)
                    main.printSessionResults();
            }
            else{
                if(customMachinesToInit !=null && customMachinesToInit.length>0){
                    if(!DataGenerator.initMachineUserPool(customMachinesToInit.length))
                        sendUserUpdateRequests = false;
                    main.initCustomSessions();
                }
                else{
                    if(!DataGenerator.initMachineUserPool(amountMachines))
                        sendUserUpdateRequests =false;
                    main.initSessions();
                }

                main.startSessions();
                main.waitForSessionsToEnd();
                main.saveSessionDataStore();
                if(!demoMode)
                    main.printSessionResults();
            }


        }
        catch (OutOfMemoryError error){
                //TODO - scale down concurrent sessions
        } catch (ConfigFileException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static AccountingDateOffset getAccountingDateOffset() {
        return accountingDateOffset;
    }

    private boolean loadConfiguration() {

        Properties prop = new Properties();
        try {

            File jarPath=new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            String propertiesPath=jarPath.getParentFile().getAbsolutePath();
            prop.load(new FileInputStream(propertiesPath+pathSeparator+"conf"+pathSeparator+"config.properties"));


            List<String> keys = new LinkedList(prop.keySet());
            Collections.sort(keys);
            System.out.println("Loading config.properties "+propertiesPath);
            //keys.forEach((key) -> System.out.println("Key : " + key + ", Value : " + prop.getProperty(key)));

            for(String key : keys){
                String value = prop.getProperty(key);
                if(value == null || value.isEmpty())
                    throw new ConfigFileException("Missing value for key - "+key+". Resolve in the config.properties");

                switch (key){
                    case "customMachinesToInit":
                        customMachinesToInit = value.split(",");
                        break;
                    case "customMachinesCurrencies":
                        customMachinesCurrencies = parseCustomMachineCurrencies(value);
                        break;
                    case "waitBeforeDataValidationMin":
                        dataValidatorSleepMin = Integer.parseInt(value);
                        if(dataValidatorSleepMin <0)
                            dataValidatorSleepMin =0;
                        break;
                    case "utcOffset":
                        timeZoneOffset = value;
                        break;
                    case "sessionEventLoopDelayMs":
                        sessionEventLoopDelayMs = Integer.parseInt(value);
                        if(sessionEventLoopDelayMs<0)
                            sessionEventLoopDelayMs=0;
                        break;

                    case "eventDelayMs":
                        eventDelayMs = Integer.parseInt(value);
                        if(eventDelayMs<0)
                            eventDelayMs=0;
                        break;

                    case "machineRampUpTimeMs":
                        machineRampUpMs = Integer.parseInt(value);
                        if (machineRampUpMs<0)
                            machineRampUpMs = 0;
                        break;

                    case "amountOfMachines":
                        amountMachines = Integer.parseInt(value);
                        if(amountMachines<0)
                            amountMachines =1;
                        break;

                    case "runtimeMin":
                        runTimeMin = Long.parseLong(value);
                        break;

                    case "environment":
                        if(value.equalsIgnoreCase(UAT.name()))
                            environment=UAT;
                        else if(value.equalsIgnoreCase(DEV.name()))
                            environment=DEV;
                        else if(value.equalsIgnoreCase(US_EAST.name()))
                            environment= US_EAST;
                        else if(value.equalsIgnoreCase(DE.name()))
                            environment=DE;
                        else if(value.equalsIgnoreCase(CH.name()))
                            environment=CH;
                        else if(value.equalsIgnoreCase(ASIA.name()))
                            environment=ASIA;
                        else{
                            throw new ConfigFileException("Incorrect environment value '"+value+"'." +
                                    "\n\tExpected: UAT/DEV/CH/US/ASIA/DE/LAT");
                        }

                        break;

                    case "debugMode":
                        if(value.equalsIgnoreCase("true")|| value.equalsIgnoreCase("false"))
                            debugMode = Boolean.parseBoolean(value);
                        else
                            throw new ConfigFileException(key+" with value '"+value+"' is not a valid boolean");
                        break;

                    case "demoMode":
                        if(value.equalsIgnoreCase("true")|| value.equalsIgnoreCase("false"))
                            demoMode = Boolean.parseBoolean(value);
                        else
                            throw new ConfigFileException(key+" with value '"+value+"' is not a valid boolean");
                        break;

                    case "forcedTenant":
                        forcedTenant = value;
                        break;

                    case "mqttURLOverride":
                        mqttURLOverride = value;
                        break;

                    case "sendErrors":
                        sendErrors = Boolean.parseBoolean(value);
                        break;

                    case "sendSystemStatuses":
                        sendSystemStatuses = Boolean.parseBoolean(value);
                        break;

                    case "sendUserUpdateRequests":
                        sendUserUpdateRequests = Boolean.parseBoolean(value);
                        break;

                    case "sendTransactions":
                        sendTransactions = Boolean.parseBoolean(value);
                        break;

                    case "sendBoxContents":
                        sendBoxContents = Boolean.parseBoolean(value);
                        break;

                    case "sendFullState":
                        sendBootMessages = Boolean.parseBoolean(value);
                        break;
                    case "waitForRMReply":
                        waitForRMReply = Boolean.parseBoolean(value);
                        break;
                    case "mqttQos":
                        mqttQos = Integer.parseInt(value);
                        break;

                }
                System.out.println("\t"+key+" = "+value);

            }


        } catch (IOException e1) {
            e1.printStackTrace();
            return false;
        } catch (ConfigFileException e) {
            e.printStackTrace();
            return false;
        } catch (NumberFormatException e){
            e.printStackTrace();
            return false;
        }


        return true;
    }

    private Currency[] parseCustomMachineCurrencies(String value) throws ConfigFileException {
        String[] currenciesStr = value.split(",");

        Currency[] currencies = new Currency[currenciesStr.length];
        for(int i = 0; i< currenciesStr.length;i++){
            switch (currenciesStr[i]){
                case "EUR":
                    currencies[i]=EUR;
                    break;
                case "USD":
                    currencies[i]=USD;
                    break;
                case "CLP":
                    currencies[i]=CLP;
                    break;
                default:
                    throw new ConfigFileException(currenciesStr[i]+" is not handled by the simulator. Remove currency from config file or update ccod-simulator");

            }
        }
        return  currencies;
    }

    private void printSessionResults() {

        long ranForMinActual = TimeUnit.MILLISECONDS.toMinutes((System.currentTimeMillis() -sessionStartTime));
        long runTimeMinExpected = runTimeMin+((machineRampUpMs*amountMachines)/60000);
        new ResultGenerator(environment, Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp()),ranForMinActual,
                ccodSessions.size(),failedMachines,runTimeMinExpected,sessionEventLoopDelayMs,eventDelayMs,machineRampUpMs, dateFromQParam, (dataValidatorSleepMin *60*1000),
                sendErrors,sendBoxContents,sendSystemStatuses,sendTransactions,sendUserUpdateRequests,waitForRMReply).processResults();
    }

    private void waitForSessionsToEnd() {

        for(Thread thread: ccodSessions){
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long timeNow = System.currentTimeMillis();
        long duration = timeNow-sessionStartTime;

        Date date = new Date(duration);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateFormatted = formatter.format(date);
        System.out.println("All CCoD sessions ended, ran for: "+dateFormatted);

    }

    public SessionDataStore loadSessionDataStore() {

        try {

            System.out.print("\nLoading session data store...");
            File parent = new File(filepathDataStore);
            if(!parent.exists())
                parent.mkdir();

            FileInputStream fileIn = new FileInputStream(new File(parent,pathSeparator+"SessionDataStore"));
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            store = (SessionDataStore) objectIn.readObject();

            objectIn.close();
            System.out.println(" Done!");


        } catch (Exception ex) {
            System.out.println(" data store not found, creating new");
            store = new SessionDataStore();

        }

        return store;
    }

    public void saveSessionDataStore(){
        try {

            File parent = new File(filepathDataStore);
            if(!parent.exists())
                parent.mkdir();
            FileOutputStream fileOut = new FileOutputStream(new File(parent,pathSeparator+"SessionDataStore"));

            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(store);
            objectOut.close();
            System.out.println("SessionDataStore  was succesfully written to a file, path: "+filepathDataStore);

        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }

    public  synchronized void putSessionData(String key,SessionData sessionData){
        store.put(key,sessionData);
    }


    public void startSessions() {
        System.out.print("Starting sessions, connecting to mqtt...");
        sessionStartTime = System.currentTimeMillis();
        dateFromQParam = Instant.now().toString();

        for(Thread t : ccodSessions){

            try {
                Thread.sleep(machineRampUpMs);
                t.start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
        System.out.println("Done!");

    }

    public void initSessions() throws IOException {

        ccodSessions = new LinkedList<Thread>();

        CCoDSession lastSession = null;

        String machineUUIDPrefix;

        for( String tenant : tenants){
            int amountOfMachines=10, indexStart;

            if(tenant.startsWith("salesdemo") || ((tenant.startsWith("suzohapp")))){
                if(this.amountMachines >0 && this.amountMachines <=30)
                    amountOfMachines = amountMachines;
                else
                    amountOfMachines = 30;
                machineUUIDPrefix="JM-";
                indexStart= machineIndexStart;

            }
            else if ((tenant.equalsIgnoreCase("testautomation")) || environment==DEV){
                if(this.amountMachines >0 && this.amountMachines <=50)
                    amountOfMachines = amountMachines;
                else
                    amountOfMachines=50;

                machineUUIDPrefix ="JM-";
                indexStart= machineIndexStart;


            }
            else if (forcedTenant!=null && tenant.equals(forcedTenant)){
                if(this.amountMachines >0 && this.amountMachines <=50)
                    amountOfMachines = amountMachines;
                else
                    amountOfMachines=1;

                if(initRCSs){
                    machineUUIDPrefix="RCS-";
                    indexStart=10;

                }else{
                    machineUUIDPrefix ="JM-";
                    amountOfMachines = this.amountMachines;
                    indexStart= machineIndexStart;

                }
            }

            else{
                if(this.amountMachines >0 && this.amountMachines <=50)
                    amountOfMachines = amountMachines;
                else
                    amountOfMachines=30;


                machineUUIDPrefix="RCS-";
                indexStart=10;
            }

            int machineNbr=0;


            boolean alertsInOrder = false;
            System.out.print("Initiating sessions - "+tenant+", machines: ");
            for(int i = indexStart;i<(amountOfMachines+indexStart);i++){
                machineNbr++;
                StringBuilder sb = new StringBuilder();
                if(i<10){
                    sb.append("0");
                }
                sb.append(i);

                String machineUUiD = machineUUIDPrefix+sb.toString();
                String key = environment.name()+"_"+tenant+"_"+machineUUiD;

                //TODO init some machines with multicurrency content,
                CCoDSession session = new CCoDSession(MachineModels.RCS_500,
                        "3.7.0",
                        machineUUiD,
                        currency,
                        sessionEventLoopDelayMs,
                        eventDelayMs,
                        brokerHost,
                        tenant,
                        runTimeMin,
                        rmProxyTimeoutThresholdSec,
                        machineNbr,
                        environment,
                        key,
                        store.get(key),
                        this);


                //configurable
                session.setSendSystemStatuses(sendSystemStatuses);
                session.setSendBoxContents(sendBoxContents);
                session.setSendTransactions(sendTransactions);
                session.setTransacionsInOrder(sendTransactionsInOrder);
                session.setTransactionTypes(readConfigTransactions());
                session.setSendErrors(false);
                session.setSendFullState(sendBootMessages);
                session.setSendUserUpdateRequest(sendUserUpdateRequests);
                session.setSendZTSRobustness(testZTSRobustness);
                session.setSendLaneAllocations(sendLaneAllocations);
                session.setSendLaneAllocationDaybreak(sendLaneAllocationDayBreak);
                session.setDayBreak(dayBreakHour,daybreakMinute);
                session.setSendDLSTransactions(sendDLSTransactions);
                session.setLoadDLSUser(useDLSUsers);
                session.setSyncWorkunits(syncWorkUnits);
                session.setWaitForRmReply(waitForRMReply);
                session.setMqttQos(mqttQos);

                if(sendUserUpdateRequests)
                    session.initMuMUserPool();

                if(i % 2 == 0)
                    session.setTransacionsInOrder(false);
                if(i==indexStart || (i%5==0)){

                    session.setSendErrors(sendErrors);
                    session.setSendErrorsInOrder(alertsInOrder);
                    alertsInOrder=!alertsInOrder;
                }

                if(allMachinesSendingErrors)
                    session.setSendErrors(sendErrors);

                /*session.setStatuses(false);
                session.setErrorWarnings(false);
                */

                System.out.print(session.toString()+" ");

                //last session will print some session events
                ccodSessions.add(new Thread(session));
                lastSession=session;
            }
            System.out.println();
        }

        lastSession.isLastSession(true);
        System.out.println("Sessions initiated, amount: "+ccodSessions.size());

    }

    private void initCustomSessions() throws ConfigFileException, IOException {
        if(customMachinesCurrencies==null || customMachinesCurrencies.length<=0)
            throw new ConfigFileException("customMachinesCurrencies is null/empty");
        if(customMachinesToInit.length!= customMachinesCurrencies.length)
            throw new   ConfigFileException("customMachinesToInit and customMachineCurrencies arrays are not of same size");

        ccodSessions = new LinkedList<>();
        System.out.println("Machines:");
        for(String tenant : tenants){
            System.out.print(("\n\t"+tenant+": "));
            for(int i = 0; i< customMachinesToInit.length; i++){
                System.out.print(customMachinesToInit[i]+" ");

                String key = environment.name()+"_"+tenant+"_"+ customMachinesToInit[i];

                CCoDSession session = new CCoDSession(MachineModels.RCS_500,
                        "3.7.0",
                        customMachinesToInit[i],
                        customMachinesCurrencies[i],
                        sessionEventLoopDelayMs,
                        eventDelayMs,
                        brokerHost,
                        tenant,
                        runTimeMin,
                        rmProxyTimeoutThresholdSec,
                        i+1,
                         environment,
                        key,
                        store.get(key),
                        this);

                //configurable
                session.setSendSystemStatuses(sendSystemStatuses);
                session.setSendBoxContents(sendBoxContents);
                session.setSendTransactions(sendTransactions);
                session.setSendFullState(sendBootMessages);
                session.setTransacionsInOrder(sendTransactionsInOrder);
                session.setTransactionTypes(readConfigTransactions());
                session.setSendUserUpdateRequest(sendUserUpdateRequests);
                session.setSendZTSRobustness(testZTSRobustness);
                session.setSendLaneAllocations(sendLaneAllocations);
                session.setSendLaneAllocationDaybreak(sendLaneAllocationDayBreak);
                session.setDayBreak(dayBreakHour,daybreakMinute);
                session.setSendDLSTransactions(sendDLSTransactions);
                session.setLoadDLSUser(useDLSUsers);
                session.setSyncWorkunits(syncWorkUnits);
                session.setWaitForRmReply(waitForRMReply);
                session.setMqttQos(mqttQos);

                if(sendUserUpdateRequests)
                    session.initMuMUserPool();

                if(sendErrors &&  (i==0 || i%5==0))
                    session.setSendErrors(true);

                if(i == customMachinesToInit.length-1)
                    session.isLastSession(true);

                ccodSessions.add(new Thread(session));


            }
        }

        System.out.println("\nSessions initiated, amount: "+ccodSessions.size());
    }

    public List<TransactionType> readConfigTransactions() {
        //TODO implement read from config
        LinkedList<TransactionType> transactionTypes = new LinkedList<>();

        /*THESE TRANSACTIONS SHOULDNT BE INCLUDED. THE ALGORITHM PERFORMS THE TYPES WHENEVER CONTAINERS REACH THRESHOLD
        forcing these transactions may or may not perform them depending on the machines content. if you want REFILL or MANUAL_REFILL then add only DEPOSIT
        to transactionTypes list. For EMPTY_OUT and PURGE add only DISPENSE to the list. Eventually when the boxes reach their thresholds then these transaction types will be performed
        */

        /*transactionTypes.add(REFILL);
        transactionTypes.add(MANUAL_REFILL);
        transactionTypes.add(EMPTY_OUT);
        transactionTypes.add(PURGE);*/
        // transactionTypes.add(EXCHANGE);


        transactionTypes.add(DISPENSE);
        transactionTypes.add(DEPOSIT);
        transactionTypes.add(MANUAL_DEPOSIT);
        transactionTypes.add(UNFINISHED_TRANSACTION);
        transactionTypes.add(POSSIBLE_TAMPERING);
       // transactionTypes.add(END_OF_SHIFT);


        return transactionTypes;
    }

    public void setEnvironment(Environments environment, String forcedTenant) {

        this.environment=environment;
        if(mqttURLOverride == null || mqttURLOverride.isEmpty())
            brokerHost = DefaultStrings.getBrokerHost(environment);
        else {
            brokerHost = mqttURLOverride;
            this.environment = OTHER;

        }

        tenants = new LinkedList<>();
        List<String> allTenants = DefaultStrings.getTenants(environment);

        if(debugMode && forcedTenant == null)
            tenants.add(allTenants.get(0));
        else if(!debugMode && forcedTenant==null)
            tenants.addAll(allTenants);
        else if(forcedTenant!=null){

            String[] tenants = forcedTenant.split(",");
            for(int i=0;i<tenants.length;i++){
                this.tenants.add(tenants[i]);
            }
        }

        System.out.println("\nEnvironment: "+environment.name()
                +"\nBroker Host: "+brokerHost+"\nSession duration: "+runTimeMin);

        System.out.print("Tenants: ");
        for(String t : tenants)
            System.out.print(t+" ");
        System.out.println();
    }

    public synchronized void incrementFailedMachines() {
        failedMachines++;

    }
}
