package ccodSession;

import CCoD.TransactionType;

import java.util.*;

public enum EventType {

    SYSTEM_STATUS, TRANSACTION, ERROR, BOX_CONTENT, USER_UPDATE_ADD, USER_UPDATE, USER_UPDATE_UPDATE,USER_UPDATE_DELETE, FULL_STATE, IDENTIFY;

    public static final List<EventType> EVENT_TYPES = Collections.unmodifiableList(Arrays.asList(values()));

    public static List<EventType> getEventTypes()  {
        return EVENT_TYPES;
    }
}
