package ccodSession;

import CCoD.*;
import CCoD.Currency;
import DLStransactionGenerator.DLSTransactionGenerator;
import cccObjects.RemoteConfigurationCCC;
import cccObjects.RemoteSoftwareUpgradeCCC;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import data.*;
import exceptions.*;
import mqtt.MqTTHandler;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.xml.sax.SAXException;
import rmObjects.*;
import xml.WorkUnitReader;
import ztsAutomation.LaneAllocationData;
import ztsAutomation.PackageTypes;
import ztsAutomation.SetDayBreak;
import ztsAutomation.RCRSURobustnessTestingZTS;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static CCoD.SystemStatusStatus.*;
import static CCoD.TransactionType.DISPENSE;
import static CCoD.TransactionType.END_OF_SHIFT;
import static CCoD.UserUpdateType.*;
import static CCoD.ValueType.COIN;
import static CCoD.ValueType.NOTE;
import static ccodSession.EventType.*;
import static ccodSession.Main.*;
import static data.DefaultStrings.*;

public class CCoDSession implements Runnable{


    //thread specific variables
    private MqTTHandler mqtt;



    public SessionLogger logger;

    private boolean sendZTSRobustness;
    private boolean sendLaneAllocations;
    private LinkedList<RmLaneAllocation> laneAllocations;
    private LaneAllocationData laneAllocationData;
    private boolean sendLaneAllocationDayBreak;
    private int dayBreakMinute;
    private int dayBreakHour;
    private boolean sendDLSTransactions;
    private List<String> dlsMessages;
    private boolean loadDLSUser;
    private boolean syncWorkunits;
    private boolean waitForRMReply;
    private int mqttQos;

    public CCoDMachine getMachine() {
        return machine;
    }

    private CCoDMachine machine;

    private String sessionName;
    private String brokerHost, tenant;

    private boolean keepRunning = false;
    private int sleepIntervall = 0;
    private long sessionStartTime;
    private final long runTimeMin;
    private final int eventDelayMs;
    private final int machineNbr;
    private final int mqttTimeoutLimit;
    private final String sessionStoreKey;

    Main main;
    public SessionData sessionData;

    //session data to generate
    private List<SystemStatusStatus> statuses;
    private List<ErrorWarning> errorWarnings;
    private List<TransactionType> transactionTypes;
    private LinkedList<Cashier> machineUsersCommon;
    private HashMap<String, String[]> workUnits;
    private boolean sendTransactionInOrder;
    private TransactionType nextTransactionType;

    private boolean sendBoxContents, sendSystemStatuses, sendErrors, sendUserUpdateRequest, sendTransactions;
    private boolean lastSession;

    private List<RmError> unclearedAlerts;
    private boolean sendAlertsInOrder;
    private List<RmError> alerts;
    private int failedEventsAmount;

    private List<RmUserMuM> machineUsersMuMPool;

    private boolean sendFullState;

    private RCRSURobustnessTestingZTS ztsRobustness;


    public CCoDSession(MachineModels type, String sfVersion, String machineUUID, Currency currency, int messageInervall,
                       int eventDelayMs, String brokerHost, String tenant, long runTimeMin, int mqttTimeoutLimit, int machineNbr, Environments environment, String sessionStoreKey, SessionData sessionData, Main main) throws IOException {

        machine = new CCoDMachine(type,sfVersion,machineUUID,currency);
        this.sleepIntervall = messageInervall;
        this.brokerHost = brokerHost;
        this.tenant = tenant;
        sessionName = environment.name()+"_"+tenant+"_"+machineUUID+"_"+type.name()+"_"+ Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp());
        this.runTimeMin = runTimeMin;
        this.eventDelayMs = eventDelayMs;
        this.mqttTimeoutLimit = mqttTimeoutLimit;
        this.machineNbr=machineNbr;
        this.sessionStoreKey = sessionStoreKey;
        this.main=main;
        sendTransactionInOrder = false;
        lastSession=false;
        failedEventsAmount=0;
        logger = new SessionLogger(sessionName);
        logger.initLoggers();


        if(sessionData==null){
            this.sessionData = new SessionData(sessionStoreKey,machine,machineNbr,null);
            this.sessionData.unclearedAlerts = new LinkedList<RmError>();
            this.unclearedAlerts = new LinkedList<>();
        }
        else{
            this.sessionData=sessionData;
            machine = sessionData.machine;
            unclearedAlerts = sessionData.unclearedAlerts;
            if(unclearedAlerts==null) {
                unclearedAlerts= new LinkedList<>();
            }
            if(unclearedAlerts!=null && !unclearedAlerts.isEmpty()) {
                for(RmError alert : unclearedAlerts)
                    alert.ERROR_ID="0-0";
            }

        }

        ztsRobustness = new RCRSURobustnessTestingZTS(this,environment,tenant,machineUUID,timeZoneOffset,0);
        logger.log(logMachineContentInCurrentState(), 0, null,null, null);
        logger.log(logMachineUserState("Init"),0,null,null,null);


    }


    public boolean isLoadDLSUser() {
        return loadDLSUser;
    }

    public void setLoadDLSUser(boolean loadDLSUser) {
        this.loadDLSUser = loadDLSUser;
    }

    public boolean isSendZTSRobustness() {
        return sendZTSRobustness;
    }

    public void setSendZTSRobustness(boolean sendZTSRobustness) {
        this.sendZTSRobustness = sendZTSRobustness;
    }

    public void setDayBreak(int dayBreakHour, int dayBreakMinute) {
        this.dayBreakHour= dayBreakHour;
        this.dayBreakMinute = dayBreakMinute;
    }

    public SessionLogger getLogger() {
        return logger;
    }

    private String logMachineUserState(String when){
        StringBuilder sb = new StringBuilder();
        sb.append("### Machine User Sync State - "+when+" ###\n");
        sb.append("\tFILE_VERSIONS: "+machine.getMachineUserSyncState().getFileVersion()+"\n");
        Map<String,RmUserMuM> users = machine.getMachineUserSyncState().getAuthorizedUsers();
        sb.append("\tAMOUNT AUTHORIZED USERS: "+users.keySet().size()+"\n");
        for(String key : users.keySet())
            sb.append("\t"+users.get(key).toString()+"\n");

        sb.append("\n###\n");
        return sb.toString();
    }
    private String logMachineContentInCurrentState() {
       LinkedList boxes = machine.getBoxesList();


        StringBuilder sb = new StringBuilder();

        for(int i = 0; i< boxes.size();i++){
           RmBox box = (RmBox) boxes.get(i);
           for(RmValue v : box.VALUES){
               String capabilities = " [";
               for(String c : box.CAPABILITIES)
                   capabilities+=c+",";
               capabilities = capabilities.substring(0,capabilities.length()-1)+"]";
               sb.append(box.NAME +
                       capabilities+
                       " default level: "+box.DEFAULT_LEVEL+
                       ", min level: "+box.MINIMUM_LEVEL+
                       ", low level: "+box.LOW_LIMIT_LEVEL+
                       ", high level: "+box.HIGH_LIMIT_LEVEL+
                       ", max level: "+box.MAXIMUM_LEVEL+
                       ", denomination: "+v.DENOMINATION+
                       ", count: "+v.COUNT+
                       ", "+v.CURRENCY+"\n"
               );

           }
       }

        return "\n"+sb.toString();
    }

    public void initMessageData() {
        if(statuses==null)
            statuses = DataGenerator.getSystemStatusStatuses();
        if(errorWarnings == null)
            errorWarnings = DataGenerator.getErrorWarnings();
        if(transactionTypes == null)
            transactionTypes = DataGenerator.getTransactionTypes(false);
        if(alerts==null)
            alerts = DataGenerator.getErrors();

        if(/*!sessionStoreKey.startsWith("DLS_") ||*/ !loadDLSUser){
            machineUsersCommon =DataGenerator.getCashiers();
            workUnits = DataGenerator.getWorkUnits(false);
        }
        else{

            try {
                machineUsersCommon = DataGenerator.getCashiersDLS();
                workUnits = DataGenerator.getWorkUnitsDLS();
            } catch (IOException | SAXException | ParserConfigurationException e) {
                e.printStackTrace();
                System.out.println("XML error, check xml users. Using default users");
                workUnits = DataGenerator.getWorkUnits(false);
                machineUsersCommon =DataGenerator.getCashiers();
            }

        }
//        workUnits = DataGenerator.getWorkUnits(false);

    }

    public void setSendBoxContents(boolean sendBoxContents) {
        this.sendBoxContents = sendBoxContents;
    }

    public void setSendSystemStatuses(boolean sendSystemStatuses) {
        this.sendSystemStatuses = sendSystemStatuses;
    }

    public void setSendErrors(boolean sendAlerts) {
        this.sendErrors = sendAlerts;
    }

    public void setSendFullState(boolean sendFullState) {
        this.sendFullState = sendFullState;
    }

    public void setSendUserUpdateRequest(boolean sendUserUpdate) {
        this.sendUserUpdateRequest = sendUserUpdate;
    }

    public void setSendTransactions(boolean sendTransactions) {
        this.sendTransactions = sendTransactions;
    }

    @Override
    public void run() {

        initMessageData();



        //connect to ccod, send first systemStatuses and BoxContent on boot
        try {


            connectToMqtt();

            if(sendDLSTransactions){
                logger.log("###DLS TRANSACTIONS###");
                initDLSData();
                sendDLSTransactions();
                logger.log("###DLS TRANSACTIONS DONE");
            }
            if(sendZTSRobustness){
                logger.log("###ZTS ROBUSTNESS TESTING");
                testZtsRobustness(PackageTypes.REMOTE_SOFTWARE_UPGRADE);
                testZtsRobustness(PackageTypes.REMOTE_CONFIGURATION);
                logger.log("###ZTS ROBUSTNESS TESTING DONE");
            }

            if(sendLaneAllocations || sendLaneAllocationsEnd){

                try {
                    logger.log("###ZTS LANE ALLOCATION TESTING");
                    initLaneAllocationData();

                    if(sendLaneAllocations){
                        sendSyncWorkunits(); //TODO add PARENT string and remove PARENT obj from wu
                        sendLaneAllocationsStart();
                    }

                } catch (SAXException e) {
                    e.printStackTrace();
                    sendLaneAllocations=false;
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                    sendLaneAllocations=false;
                } catch (DropSafeFullException e) {
                    e.printStackTrace();
                } catch (BoxContentFullException e) {
                    e.printStackTrace();
                } catch (BoxContentEmptyExcception boxContentEmptyExcception) {
                    boxContentEmptyExcception.printStackTrace();
                } catch (EmptiableBoxesFullException e) {
                    e.printStackTrace();
                }
                logger.log("###ZTS LANE ALLOCATION TESTING - ALLOCATIONS ARE SENT");
            }



            if(lastSession)
                System.out.println("all sessions started and connected to mqtt");

            sessionStartTime = System.currentTimeMillis();

            if(sendFullState)
                sendFullState();

            if(sendSystemStatuses)
                sendSystemStatus(null);
            if(sendBoxContents)
                sendBoxContent(Integer.MIN_VALUE);

            if(syncWorkunits)
                sendSyncWorkunits();


            if(sendUserUpdateRequest){
                sendUserUpdateRequest(BOOT,null);
                if(debugMode)
                    logger.log(logMachineUserState("Initial user update request after boot"),0,null,null,null);
            }

        } catch (MqttException e) {
            try {
                e.printStackTrace();
                keepRunning = false;
                logger.log("MqttException", 0, null,null, e.getMessage());
                stopSession();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (MqttException e1) {
                e1.printStackTrace();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

        } catch (JsonProcessingException e){
            try {
                logger.log("Json Processing Exception: ", 0, null,null, e.getMessage());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println(sessionName+", Failed to init logger\n"+e.getMessage());
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        //machine loop
        while (keepRunning){

            try {

                if(runTimeReached()){


                    if(sendLaneAllocationsEnd){
                        logger.log("###ZTS LANE ALLOCATION TESTING - SENDING DEALLOCATE MESSAGES");
                        sendLaneAllocationsEnd();
                        logger.log("###ZTS LANE ALLOCATION TESTING DONE");
                    }
                    if(sendSystemStatuses){
                        List<SystemStatusStatus> statuses = new LinkedList<>();
                        statuses.add(Idle);
                        statuses.add(Closed);
                        statuses.add(Disconnected);
                        sendSystemStatus(statuses);
                    }

                    if(sendUserUpdateRequest){
                        sendUserUpdateRequest(DELETE,null);
                    }

                    stopSession();
                    break;
                }

                //check if any alerts reached time to clear
                if(unclearedAlerts!=null && !unclearedAlerts.isEmpty())
                    sendClearAlert();


                if(sendErrors)
                    sendErrors(alerts,sendAlertsInOrder);
                //systemStatus msg

                String fileVersionDiffer = null;
                if(sendSystemStatuses){

                    Random rng = new Random();
                    int randomMessage = rng.nextInt(statuses.size());
                    List list = new LinkedList();
                    list.add(statuses.get(randomMessage));
                    fileVersionDiffer = sendSystemStatus(list);

                }

                if(sendTransactions){
                    RmUserCommon rmUser = selectUser();

                    sendUserLogin(rmUser);
                    sendTransactionBoxContent(rmUser);
                    sendUserLogout();
                }

                if(sendUserUpdateRequest){
                    if(fileVersionDiffer!=null)
                        sendUserUpdateRequest(BOOT,fileVersionDiffer);

                    sendUserUpdateRequest(RANDOM,null);
                    if(debugMode)
                        logger.log(logMachineUserState("in run loop"),0,null,null,null);
                }



                Thread.sleep(sleepIntervall);


            } catch (IOException e) {
                System.out.println("IOException in main event loop. terminatingn"+e.getMessage());
                main.putSessionData(sessionStoreKey,sessionData);
                e.printStackTrace();

            } catch (InterruptedException e) {
                try {
                    logger.log("Thread <"+sessionName+"> was interrupted.\n"+e.getMessage(), 0, null,null, null);
                    logger.closeLoggers();
                    main.putSessionData(sessionStoreKey,sessionData);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            } catch (MqttException e) {
                try {
                    logger.log("MqttException in main event loop.\n"+e.getMessage(), 0, null,null, null);
                    main.putSessionData(sessionStoreKey,sessionData);
                    stopSession();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (MqttException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
        }

    }

    private List<RmWorkUnit> loadWorkUnitData() throws IOException, SAXException, ParserConfigurationException {
        return WorkUnitReader.readWorkUnits("workUnitsLaneAllocations.xml");
    }

    private void sendSyncWorkunits() throws ParserConfigurationException, SAXException, IOException, MqttException, InterruptedException {
        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(
                machine.getMachineUUID(),
                "Idle",
                "Unknown",
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                sessionData.messageSequence,
                sessionData.machine.getSfVersion()
        );
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetWokrUnits =generateWorkunitBootMessage(loadWorkUnitData());

        if(sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampLog(),RmHandler.parseRmToString(ss))!=null || !waitForRMReply)
            sessionData.incrementSequence(SYSTEM_STATUS);

    }

    private List<RmWorkUnitParent> generateWorkunitBootMessage(List<RmWorkUnit> workunits) {
        Map<String,List<RmWorkUnit>> workunitGroups = new HashMap();
        for(RmWorkUnit wu : workunits){
            if(workunitGroups.get(wu.PARENT.NAME)==null){
                List<RmWorkUnit> list = new LinkedList<>();
                list.add(wu);
                workunitGroups.put(wu.PARENT.NAME,list);

            }else
                workunitGroups.get(wu.PARENT.NAME).add(wu);

        }

        List<RmWorkUnitParent> parents = new LinkedList<>();
        for(String key : workunitGroups.keySet()){
            RmWorkUnitParent p = new RmWorkUnitParent();
            p.CHILDREN = workunitGroups.get(key);
            p.NAME = p.CHILDREN.get(0).PARENT.NAME;
            p.DISPLAY_NAME = p.CHILDREN.get(0).PARENT.DISPLAY_NAME;
            p.DISPLAY_NAME = p.CHILDREN.get(0).PARENT.DISPLAY_NAME;
            p.ROLE_IDS = p.CHILDREN.get(0).PARENT.ROLE_IDS;
            p.MONEY_MIXES = p.CHILDREN.get(0).PARENT.MONEY_MIXES;
            p.IGNORE_ALLOCATION_DAY_BREAK = p.CHILDREN.get(0).PARENT.IGNORE_ALLOCATION_DAY_BREAK;
            p.IS_MONEY_MOVER = p.CHILDREN.get(0).PARENT.IS_MONEY_MOVER;
            parents.add(p);

        }
        System.out.println();
        return parents;
    }

    private void sendUserLogout() throws InterruptedException, IOException, MqttException {

        logger.log("IN SEND_LOGIN_MESSAGES - logout");
        String rmMessage = RmHandler.parseRmToString(
                RmHandler.generateSystemStatusMessage(
                        machine.getMachineUUID(),
                        "Idle",
                        "User",
                        Utilities.getCurrentDateStampIncludedCatchingUp(),
                        timeZoneOffset,
                        sessionData.messageSequence,
                        ccodVersionBoot)
        );
        if(sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampLog(),rmMessage)!=null || !waitForRMReply)
            sessionData.incrementSequence(SYSTEM_STATUS);

    }

    private void sendUserLogin(RmUserCommon rmUser) throws IOException, InterruptedException, MqttException {

        List<String > rmMessages = new LinkedList<>();
        RmWorkUnit wu = new RmWorkUnit();
        wu.NAME = rmUser.WORK_UNIT_NAME;
        wu.PARENT = new RmWorkUnitParent();
        wu.PARENT.NAME= wu.PARENT.DISPLAY_NAME = rmUser.WORK_UNIT.PARENT.NAME;
        rmUser.WORK_UNIT_NAME=null;
        rmUser.WORK_UNIT = null;

        logger.log("IN SEND_LOGIN_MESSAGES Identify User -> Workunit");
        RmSystemStatus systemStatusIdentify = RmHandler.generateSystemStatusMessage(
                machine.getMachineUUID(),
                "Identify",
                "Idle",
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                ++sessionData.messageSequence, null);

        rmMessages.add(RmHandler.parseRmToString(systemStatusIdentify));
        logger.log("\t"+rmMessages.get(0));

        RmIdentify identifyUser = RmHandler.generateIdentifyMessage(
                machine.getMachineUUID(),
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                ++sessionData.messageSequence,
                rmUser,
                null,
                MachineModels.RCS_500.name()
        );

        rmMessages.add(RmHandler.parseRmToString(identifyUser));
        logger.log("\t"+rmMessages.get(1));

        rmUser.WORK_UNIT=wu;
        rmUser.WORK_UNIT_NAME=wu.NAME;

        RmIdentify identifyWorkUnit = RmHandler.generateIdentifyMessage(
                machine.getMachineUUID(),
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                ++sessionData.messageSequence,
                rmUser,
                wu,
                MachineModels.RCS_500.name()
        );

        rmMessages.add(RmHandler.parseRmToString(identifyWorkUnit));
        logger.log("\t"+rmMessages.get(2));

        for (int i = 0 ; i< rmMessages.size();i++){
            String rmMessage = rmMessages.get(i);
            if(sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampLog(),rmMessage)!=null || !waitForRMReply)
                if(i == 0)
                    sessionData.incrementSequence(SYSTEM_STATUS);
                else
                    sessionData.incrementSequence(IDENTIFY);
        }

        logger.log("IN Identify User -> Workunit - login messages sent");

    }

    private void initDLSData() throws IOException, ParserConfigurationException, SAXException {
        DLSTransactionGenerator dls = new DLSTransactionGenerator(machine.getMachineUUID());
        dlsMessages = dls.generateDDLMessages(dls.readTransactionTemplate(),dls.readUsersXml(),dls.readWorkUnitsXml(),true);
    }

    private void sendDLSTransactions() throws IOException, InterruptedException, MqttException {

        for (String dlsMessage : dlsMessages){
            RmMessageHeader rm = RmHandler.parseToRmMessageHeader(dlsMessage);
            String dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();
            EventType eventType;

            switch (rm.MESSAGE_TYPE){
                case "SystemStatus":
                    eventType=SYSTEM_STATUS;

                    break;
                case "Transaction":
                    eventType=TRANSACTION;
                    break;
                default:
                    eventType=SYSTEM_STATUS;
                    break;
            }

            //dlsMessage = RmHandler.parseRmToString(rm);
            if(sendMqttEvent(eventType,dateTime,dlsMessage)!=null || !waitForRMReply)
                sessionData.incrementSequence(eventType);
        }

    }

    private void initLaneAllocationData() throws IOException, SAXException, ParserConfigurationException {
        List<RmWorkUnit> workunitsLaneAllocations = loadWorkUnitData();
        laneAllocations = new LinkedList<RmLaneAllocation>();
        List<RmUserCommon> rmUsers = new LinkedList<>();
        List<RmLaneAllocation> laneAllocations = new LinkedList<>();

        for(int i = 0 ; i<workunitsLaneAllocations.size();i++){
            RmUserCommon user = selectUser();
            user.WORK_UNIT = workunitsLaneAllocations.get(i);
            user.WORK_UNIT_NAME = user.WORK_UNIT.NAME;
            rmUsers.add(user);

            RmLaneAllocation laneAllocation = new RmLaneAllocation();
            laneAllocation.BARCODE="bc-100"+i;
            laneAllocation.ID ="id-11"+i;
            laneAllocation.WORK_UNIT_NAME = user.WORK_UNIT_NAME;
            laneAllocations.add(laneAllocation);
        }

        laneAllocationData = new LaneAllocationData(laneAllocations,rmUsers);


    }

    private void sendLaneAllocationsStart() throws BoxContentEmptyExcception, EmptiableBoxesFullException, DropSafeFullException, BoxContentFullException, IOException, InterruptedException, MqttException {

        String dateTime;

        if(sendLaneAllocationDayBreak){

            dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();
            RmSystemStatus ss = RmHandler.generateSystemStatusMessage(
                    machine.getMachineUUID(),
                    "Idle",
                    "Boot",
                    dateTime,
                    timeZoneOffset,
                    sessionData.messageSequence,
                    ccodVersionBoot
            );

            ss.SETTINGS = new RmSettings();
            ss.SETTINGS.SetDayBreak = new SetDayBreak(dayBreakHour,dayBreakMinute);



            if(sendMqttEvent(SYSTEM_STATUS,dateTime,RmHandler.parseRmToString(ss))!=null || !waitForRMReply)
                sessionData.incrementSequence(SYSTEM_STATUS);
        }

        for (int i = 0; i<laneAllocationData.laneAllocations.size(); i++){
            RmUserCommon user = laneAllocationData.rmUsers.get(i);
            //TRANSACTION DISPENSE
            dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();
            RmTransaction t = DataGenerator.getTransactionRow(
                  DISPENSE,
                    machine,
                    user,
                    dateTime,
                    timeZoneOffset,
                    sessionData.transactionSequence,
                    sessionData.messageSequence,
                    sessionData.sequennce
            );
            t.ALLOCATION_ADDED = laneAllocationData.laneAllocations.get(i);


            if(sendMqttEvent(TRANSACTION,dateTime,RmHandler.parseRmToString(t))!=null || !waitForRMReply)
                sessionData.incrementSequence(TRANSACTION);
        }

    }


    private void sendLaneAllocationsEnd() throws InterruptedException, IOException, MqttException {
        for (int i = 0; i<laneAllocationData.laneAllocations.size(); i++){
            RmUserCommon user = laneAllocationData.rmUsers.get(i);
            //TRANSACTION END OF SHIFT
            String dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();
            RmTransaction t = null;
            try {
                t = DataGenerator.getTransactionRow(
                        END_OF_SHIFT,
                        machine,
                        user,
                        dateTime,
                        timeZoneOffset,
                        sessionData.transactionSequence,
                        sessionData.messageSequence,
                        sessionData.sequennce
                );

                t.ALLOCATION_REMOVED = laneAllocationData.laneAllocations.get(i);


                if(sendMqttEvent(TRANSACTION,dateTime,RmHandler.parseRmToString(t))!=null || !waitForRMReply)
                    sessionData.incrementSequence(TRANSACTION);

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (BoxContentFullException e) {
                e.printStackTrace();
            } catch (BoxContentEmptyExcception boxContentEmptyExcception) {
                boxContentEmptyExcception.printStackTrace();
            } catch (EmptiableBoxesFullException e) {
                e.printStackTrace();
            } catch (DropSafeFullException e) {
                e.printStackTrace();
            }

        }

    }

    private void testZtsRobustness(PackageTypes ztsService) throws IOException {

        if(ztsService==PackageTypes.REMOTE_CONFIGURATION)
            testRemoteConfiguration();
        if(ztsService==PackageTypes.REMOTE_SOFTWARE_UPGRADE)
            testRemoteSoftwareUpgrade();


    }

    private void testRemoteSoftwareUpgrade() throws IOException {
        try {

            logger.log("### REMOTE SOFTWARE UPGRADE");
            ztsRobustness.initRemoteSoftwareUpgradeForMachine(logger);
            RemoteSoftwareUpgradeCCC remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected PENDING): ", remoteSoftwareUpgradeDeplotmant);
            main.minusSecondsRmEvent = 305;
            logger.log("RSU - Cathing Up - no package info expected");
            ztsRobustness.sendSystemStatuses(true);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected PENDING): ", remoteSoftwareUpgradeDeplotmant);
            main.minusSecondsRmEvent = 0;

            logger.log("RSU - caught up - package info expected");
            ztsRobustness.sendSystemStatuses(true);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected SENT): ", remoteSoftwareUpgradeDeplotmant);

            logger.log("RSU WAITING SetConfigurationUpdate - expected no change");

            ztsRobustness.sendWaitingRemoteConfiguration();
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected SENT): ", remoteSoftwareUpgradeDeplotmant);

            ztsRobustness.sendWaitingRemoteSoftwareUpgrade();
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected WAITING): ", remoteSoftwareUpgradeDeplotmant);


            logger.log("RSU DONE SetConfigurationUpdate - expected no change");

            ztsRobustness.sendDoneRemoteConfiguration();
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected WAITING): ", remoteSoftwareUpgradeDeplotmant);

            ztsRobustness.sendDoneRemoteUpgrade();
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected DONE): ", remoteSoftwareUpgradeDeplotmant);

            //failed state
            logger.log("RSU test FAILED state");
            ztsRobustness.initRemoteSoftwareUpgradeForMachine(logger);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected PENDING): ", remoteSoftwareUpgradeDeplotmant);

            ztsRobustness.sendSystemStatuses(false);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected SENT): ", remoteSoftwareUpgradeDeplotmant);
            logger.log("RSU test SENT -> FAILED");
            ztsRobustness.sendErrorSZT(DataGenerator.remoteConfigurationKind);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected SENT): ", remoteSoftwareUpgradeDeplotmant);
            ztsRobustness.sendErrorSZT(DataGenerator.remoteSoftwareUpgradeKind);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected FAILED): ", remoteSoftwareUpgradeDeplotmant);

            ztsRobustness.initRemoteSoftwareUpgradeForMachine(logger);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected PENDING): ", remoteSoftwareUpgradeDeplotmant);
            ztsRobustness.sendSystemStatuses(false);
            ztsRobustness.sendWaitingRemoteSoftwareUpgrade();
            logger.log("RSU test WAITING -> FAILED");
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected WAITING): ", remoteSoftwareUpgradeDeplotmant);

            ztsRobustness.sendErrorSZT(DataGenerator.remoteConfigurationKind);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected WAITING): ", remoteSoftwareUpgradeDeplotmant);
            ztsRobustness.sendErrorSZT(DataGenerator.remoteSoftwareUpgradeKind);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected FAILED): ", remoteSoftwareUpgradeDeplotmant);

            //corner cases - SENT -> DONE if CCoD sends no WAITING but have CURRENT applied
            logger.log("RSU SENT -> DONE if CCoD sends no WAITING but have CURRENT applied");
            ztsRobustness.initRemoteSoftwareUpgradeForMachine(logger);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected PENDING): ", remoteSoftwareUpgradeDeplotmant);
            ztsRobustness.sendSystemStatuses(false);
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected SENT): ", remoteSoftwareUpgradeDeplotmant);
            ztsRobustness.sendDoneRemoteUpgrade();
            remoteSoftwareUpgradeDeplotmant = ztsRobustness.getMachineStateRemoteSoftwareUpgrade(tenant);
            logger.log("GET RSU DEPLOYMENT STATE (Expected DONE): ", remoteSoftwareUpgradeDeplotmant);

            logger.log("###ZTS REMOTE SOFTWARE UPGRADE TESTING DONE");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ZTSException e) {
            logger.log(e.getMessage());
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void log(String message) throws IOException {
        logger.log(message);
    }

    private void testRemoteConfiguration() throws IOException {
        try {

            RemoteConfigurationCCC remoteConfigurationDeployment = null;

            logger.log("### REMOTE CONFIGURATION");
            ztsRobustness.initRemoteConfigurationForMachine(logger);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected PENDING): ",remoteConfigurationDeployment);
            main.minusSecondsRmEvent = 305;
            logger.log("RC - Cathing Up - no package info expected");
            ztsRobustness.sendSystemStatuses(true);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected PENDING): ",remoteConfigurationDeployment);
            main.minusSecondsRmEvent = 0;

            logger.log("RC - caught up - package info expected");
            ztsRobustness.sendSystemStatuses(true);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected SENT): ",remoteConfigurationDeployment);

            logger.log("RC WAITING SetSoftwareUpgrade - expected no change");
            ztsRobustness.sendWaitingRemoteSoftwareUpgrade();
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected SENT): ",remoteConfigurationDeployment);


            ztsRobustness.sendWaitingRemoteConfiguration();
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected WAITING): ",remoteConfigurationDeployment);

            logger.log("RC DONE SetSoftwareUpgrade - expected no change");
            ztsRobustness.sendDoneRemoteUpgrade();
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected WAITING): ",remoteConfigurationDeployment);

            ztsRobustness.sendDoneRemoteConfiguration();
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected DONE): ",remoteConfigurationDeployment);

            //failed state
            logger.log("RC test FAILED state");
            ztsRobustness.initRemoteConfigurationForMachine(logger);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected PENDING): ",remoteConfigurationDeployment);

            ztsRobustness.sendSystemStatuses(false);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected SENT): ",remoteConfigurationDeployment);
            logger.log("RC test SENT -> FAILED");
            ztsRobustness.sendErrorSZT(DataGenerator.remoteSoftwareUpgradeKind);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected SENT): ",remoteConfigurationDeployment);
            ztsRobustness.sendErrorSZT(DataGenerator.remoteConfigurationKind);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected FAILED): ",remoteConfigurationDeployment);

            ztsRobustness.initRemoteConfigurationForMachine(logger);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected PENDING): ",remoteConfigurationDeployment);
            ztsRobustness.sendSystemStatuses(false);
            ztsRobustness.sendWaitingRemoteConfiguration();
            logger.log("RC test WAITING -> FAILED");
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected WAITING): ",remoteConfigurationDeployment);

            ztsRobustness.sendErrorSZT(DataGenerator.remoteSoftwareUpgradeKind);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected WAITING): ",remoteConfigurationDeployment);
            ztsRobustness.sendErrorSZT(DataGenerator.remoteConfigurationKind);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected FAILED): ",remoteConfigurationDeployment);

            logger.log("RC SENT -> DONE if CCoD sends no WAITING but have CURRENT applied");
            ztsRobustness.initRemoteConfigurationForMachine(logger);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected PENDING): ",remoteConfigurationDeployment);
            ztsRobustness.sendSystemStatuses(false);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected SENT): ",remoteConfigurationDeployment);
            ztsRobustness.sendDoneRemoteConfiguration();
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected DONE): ",remoteConfigurationDeployment);

            logger.log("RC PENDING -> DONE if CCoD sends no WAITING but have CURRENT applied");
            ztsRobustness.initRemoteConfigurationForMachine(logger);
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected PENDING): ",remoteConfigurationDeployment);
            ztsRobustness.sendDoneRemoteConfiguration();
            remoteConfigurationDeployment = ztsRobustness.getMachineStateRemoteContifuration(tenant);
            logger.log("GET RC DEPLOYMENT STATE (Expected DONE): ",remoteConfigurationDeployment);

            logger.log("###ZTS REMOTE CONFIGURATION TESTING DONE");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ZTSException e) {
            logger.log(e.getMessage());
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

    private void sendUserUpdateRequest(UserUpdateType updateType, String toVersion) throws IOException, InterruptedException, MqttException {

        //Initial sync when CCoD is started - full sync
        if(updateType==BOOT && toVersion==null){

            //Send UpdateRequest w/o users
            RmUserUpdateRequest uurEmpty = RmHandler.generateUserUpdateRequest(machine.getMachineUUID(),Utilities.getCurrentDateStampIncludedCatchingUp(),timeZoneOffset,"","",sessionData.messageSequence,null);
            if(sendMqttEvent(USER_UPDATE,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(uurEmpty))!=null || !waitForRMReply)
                sessionData.incrementSequence(USER_UPDATE);

            //Send systemstatus to get fileVersion
            RmSystemStatus ss = RmHandler.generateSystemStatusMessage(machine.getMachineUUID(),"Idle","Idle",Utilities.getCurrentDateStampIncludedCatchingUp(),timeZoneOffset,sessionData.messageSequence,"0");
            String ssResponse = sendMqttEvent(SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(), RmHandler.parseRmToString(ss));
            if(!waitForRMReply)
                sessionData.incrementSequence(SYSTEM_STATUS);
            if(ssResponse!=null && !ssResponse.startsWith("failed")){
                sessionData.incrementSequence(SYSTEM_STATUS);
                RmServerReply rmServerReply = RmHandler.parseStringToRmServerReply(ssResponse);

                if(rmServerReply.FILE_VERSIONS == null){
                    logger.log("Did not get FILE_VERSIONS in reply, MuM is turned off for the tenant, not sending more user update requests",0,null,null,null);
                    sendUserUpdateRequest = false;
                    return;
                }

                String fileVersion = rmServerReply.FILE_VERSIONS.users;

                //Send UpdateRequest to get updates fromV=0 to latestVersion (fileVersion)
                RmUserUpdateRequest userUpdateRequest1 = RmHandler.generateUserUpdateRequest(machine.getMachineUUID(),Utilities.getCurrentDateStampIncludedCatchingUp(),timeZoneOffset,"0",fileVersion,sessionData.messageSequence,null);
                String userUpdateReply = sendMqttEvent(USER_UPDATE,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(userUpdateRequest1));
                if(userUpdateReply!=null || !userUpdateReply.startsWith("failed") || !waitForRMReply){
                    sessionData.incrementSequence(USER_UPDATE);
                    RmUserUpdateReply reply = RmHandler.parseStringToRmUserMumReply(userUpdateReply);
                    updateMachineUserSyncState(reply,null);

                }
            }

        }

        //send update request to get diff between old and new file version
        else if(updateType==BOOT && toVersion!=null){
            RmUserUpdateRequest request = RmHandler.generateUserUpdateRequest(machine.getMachineUUID(),Utilities.getCurrentDateStampIncludedCatchingUp(),timeZoneOffset,machine.getMachineUserSyncState().getFileVersion(),toVersion,sessionData.messageSequence,null);
            String response = sendMqttEvent(USER_UPDATE,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(request));
            if(response!= null || !response.startsWith("failed") || !waitForRMReply){
                sessionData.incrementSequence(USER_UPDATE);
                RmUserUpdateReply reply = RmHandler.parseStringToRmUserMumReply(response);
                updateMachineUserSyncState(reply,null);
            }
        }

        //upon session end - delete all authorized users for this machine
        else if (updateType==DELETE){
            int maxSizePerRequest=1000;
            Map<String,RmUserMuM> authorizedUsers = machine.getMachineUserSyncState().getAuthorizedUsers();
            List<String> keys = new LinkedList<>(authorizedUsers.keySet());
            List<List<String>> partitionedKeys = Lists.partition(keys,maxSizePerRequest);
            for(List<String> keysInParitions : partitionedKeys){
                List<RmUserChange> usersList = new LinkedList<>();
                for(String k : keysInParitions){
                    RmUserChange userChange = new RmUserChange();
                    userChange.KIND=DELETE.name();
                    userChange.USER = authorizedUsers.get(k);
                    usersList.add(userChange);
                }
                RmUserUpdateRequest updateRequest = RmHandler.generateUserUpdateRequest(machine.getMachineUUID(),Utilities.getCurrentDateStampIncludedCatchingUp(),timeZoneOffset,null,null,sessionData.messageSequence,usersList);
                String response = sendMqttEvent(USER_UPDATE_DELETE,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(updateRequest));
                if(response!=null && !response.startsWith("failed") || !waitForRMReply){
                    sessionData.incrementSequence(USER_UPDATE);
                    RmUserUpdateReply reply = RmHandler.parseStringToRmUserMumReply(response);
                    updateMachineUserSyncState(reply,usersList);

                }
                if(debugMode)
                    logger.log(logMachineUserState("On Stop Session"),0,null,null,null);
            }

        }

        //Normal Sync - selects a random type then generates a message with random amount of users to update
        else if (updateType==RANDOM){

            Random rng = new Random();
            Map<String, RmUserMuM> authorizedUsers = machine.getMachineUserSyncState().getAuthorizedUsers();
            UserUpdateType[] userUpdateTypes = getValidUpdateTypes(authorizedUsers,machineUsersMuMPool);
            if(userUpdateTypes==null)
                return;

            //UPDATE & DELETE uses users that are authorized for the machine
            //ADD uses userPool assigned to the machine (not authorized)
            UserUpdateType type = userUpdateTypes[rng.nextInt(userUpdateTypes.length)];
            List<RmUserChange> usersToSync = new LinkedList<>();
            int usersAmount=0;
            if(type==ADD){

                usersAmount = rng.nextInt(machineUsersMuMPool.size());

                //limit amount doe to kafka msg size
                if(usersAmount>1000)
                    usersAmount=rng.nextInt(1000);

                for(int i = 0; i<usersAmount;i++){
                    usersToSync.add(new RmUserChange(machineUsersMuMPool.get(i),type.name()));
                }
            }
            else if(type==UPDATE || type==DELETE){
                Map<String, RmUserMuM> authorizedUsersMachine = machine.getMachineUserSyncState().getAuthorizedUsers();
                Set<String> keys = authorizedUsersMachine.keySet();
                usersAmount = rng.nextInt(keys.size()+1);
                //limit amount doe to kafka msg size
                if(usersAmount>1000)
                    usersAmount=1000;

                int i = 0;
                for(String key : keys){
                    if(i>=usersAmount)
                        break;

                    RmUserMuM u;
                    if(type==UPDATE)
                        u = DataGenerator.randomizeUserUpdateVals(authorizedUsersMachine.get(key),machine.getMachineUUID(),sessionData.messageSequence);
                    else
                        u = authorizedUsersMachine.get(key);

                    usersToSync.add(new RmUserChange(u,type.name()));
                    i++;
                }

            }

            EventType eventType= USER_UPDATE;
            switch (type){
                case DELETE:
                    eventType = USER_UPDATE_DELETE;
                    break;
                case UPDATE:
                    eventType = USER_UPDATE_UPDATE;
                    break;
                case ADD:
                    eventType = USER_UPDATE_ADD;
                    break;
            }

            RmUserUpdateRequest updateRequest = RmHandler.generateUserUpdateRequest(machine.getMachineUUID(),Utilities.getCurrentDateStampIncludedCatchingUp(),timeZoneOffset,null,null,sessionData.messageSequence,usersToSync);
            String updateReply = sendMqttEvent(eventType,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(updateRequest));
            if(!waitForRMReply)
                sessionData.incrementSequence(USER_UPDATE);
            if(updateReply!=null && !updateReply.startsWith("failed")){
                sessionData.incrementSequence(USER_UPDATE);
                RmUserUpdateReply reply = RmHandler.parseStringToRmUserMumReply(updateReply);
                if(reply.MESSAGE_TYPE.equals("UserUpdateReply"))
                    updateMachineUserSyncState(reply,usersToSync);

                else
                    logger.log("UserUpdateRequest failed",0,null,null,"Did not get UserUpdateReply for the request, failing to update machineUserSyncState");


            }

        }

    }

    private synchronized UserUpdateType[] getValidUpdateTypes(Map<String, RmUserMuM> authorizedUsers, List<RmUserMuM> machineUsersMuMPool) throws IOException {
        try{

            UserUpdateType[] userUpdateTypes=null;
            if((authorizedUsers==null || authorizedUsers.isEmpty()) && (machineUsersMuMPool!=null && !machineUsersMuMPool.isEmpty())) {
                userUpdateTypes = new UserUpdateType[]{ADD};
            }
            else if((authorizedUsers!=null && !authorizedUsers.isEmpty()) && (machineUsersMuMPool==null || machineUsersMuMPool.isEmpty())){
                userUpdateTypes = new UserUpdateType[]{UPDATE, DELETE};
            }
            else if((authorizedUsers!=null && !authorizedUsers.isEmpty()) && (machineUsersMuMPool!=null && !machineUsersMuMPool.isEmpty())){
                userUpdateTypes = new UserUpdateType[]{ADD, UPDATE, DELETE};
            }
            else {
                userUpdateTypes = null;
                logger.log("MuM",0,null,null,"Authorized USR arr and machineUSersMuMPool empty, unable to create USerUpdateRequest");
            }
            return userUpdateTypes;
        }catch (ConcurrentModificationException e){
            System.out.println(e.getMessage());
            return null;
        }
    }

    private void updateMachineUserSyncState(RmUserUpdateReply reply, List<RmUserChange> usersList) {
        machine.getMachineUserSyncState().setFileVersion(reply.TO_VERSION);
        List<RmUserChange> usersToUpdate;
        if(reply.USERS!=null)
            usersToUpdate = reply.USERS;
        else
            usersToUpdate = usersList;

        Map<String,RmUserMuM> usersOnMachine = machine.getMachineUserSyncState().getAuthorizedUsers();
        for(RmUserChange u : usersToUpdate){
            switch (u.KIND){
                case "ADD":
                    usersOnMachine.put(u.USER.USER_ID,u.USER);
                    machineUsersMuMPool.remove(u.USER);
                    break;
                case "UPDATE":
                    updateUserOnMachine(u.USER);
                    break;
                case "DELETE":
                    usersOnMachine.remove(u.USER.USER_ID);
                    break;
            }
        }
    }

    private void updateUserOnMachine(RmUserMuM user) {
        machine.getMachineUserSyncState().getAuthorizedUsers().put(user.USER_ID, Utilities.cloneMachineUser(user));
    }

    public void initMuMUserPool() throws IOException {
        List<RmUserMuM> pool = DataGenerator.getMachineUsers(machineNbr);
        if (pool ==null || pool.isEmpty()){
            logger.log("ccodUsersMuM.xml", 0, null,null, "Session has no machineUsers partitioned, setting sendUserUpdate to false for this session");
            sendUserUpdateRequest=false;
        }else{
            machineUsersMuMPool = Collections.synchronizedList(new LinkedList<>());
            for(RmUserMuM u : pool){
                machineUsersMuMPool.add(u);
            }
        }

    }

    private void sendFullState() throws IOException, InterruptedException, MqttException {

        String dateStamp = Utilities.getCurrentDateStampIncludedCatchingUp();
         RmSystemStatus rm = RmHandler.generateFullStateMessage(
                 RmHandler.generateSystemStatusMessage(machine.getMachineUUID(), "Idle","Unknown",dateStamp,main.timeZoneOffset,sessionData.messageSequence, machine.getMachineUserSyncState().getFileVersion()),
                 machine);
         rm.VERSION = DefaultStrings.ccodVersionBoot;
         if(sendMqttEvent(EventType.SYSTEM_STATUS,dateStamp,RmHandler.parseRmToString(rm))!=null || !waitForRMReply)
            sessionData.incrementSequence(SYSTEM_STATUS);

    }

    private void sendClearAlert() throws IOException, InterruptedException, MqttException {

        List<RmError> remainingAlerts= new LinkedList<>();
        for(RmError alert : unclearedAlerts){
            String[] limitStartTime = alert.ERROR_ID.split("-");
            long limit = Long.parseLong(limitStartTime[0]);
            long startTime = Long.parseLong(limitStartTime[1]);
            long timeNow = System.currentTimeMillis();
            int compare = Long.compare((timeNow-startTime),limit);
            alert.CLEARED=true;
            alert.MACHINE_ID = alert.UUID = machine.getMachineUUID();
            alert.MESSAGE_TYPE= "Error";
            alert.DATE_TIME = Utilities.getCurrentDateStampIncludedCatchingUp();
            alert.RECEIVER="Server";
            alert.MESSAGE_SEQUENCE=sessionData.messageSequence;
            alert.SEQUENCE = sessionData.alertSequence;

            if(compare>=0 || alert.ERROR_ID.startsWith("0")){
                if(sendMqttEvent(ERROR,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(alert))==null){
                    remainingAlerts.add(alert);
                }

            }else
                remainingAlerts.add(alert);
        }

        unclearedAlerts=remainingAlerts;
    }

    private void sendErrors(List<RmError> errors, boolean sendAlertInOrder) throws InterruptedException, IOException, MqttException {
        String[] severities = DataGenerator.getErrorSeverities();
        String[] locations = DataGenerator.getAlertLocations();
        String[] systemLocations = DataGenerator.getErrorLocations();
        String[] errorIds = DataGenerator.getErrorIds();
        List<RmBoxContent> rmBox = machine.getBoxContents();

        Random r = new Random();
        for(RmError error : errors){
            error.DATE_TIME = Utilities.getCurrentDateStampIncludedCatchingUp();
            error.TIME_ZONE_UTC_OFFSET=timeZoneOffset;
            error.VERSION = machine.getSfVersion();
            error.MACHINE_ID =error.UUID =machine.getMachineUUID();
            error.RECEIVER="Server";
            error.MESSAGE_SEQUENCE=sessionData.messageSequence;
            error.SEQUENCE = sessionData.alertSequence;
            error.MESSAGE_TYPE="Error";

            if(error.KIND!=null && (error.KIND.equals("ThresholdFull")||error.KIND.equals("ThresholdEmpty"))){
                error.SEVERITY=severities[1];
                if(error.KIND.equals("ThresholdFull"))
                    error.INFORMATION="Containers reaching threshold, Purge to low level";
                if(error.KIND.equals("ThresholdEmpty"))
                    error.INFORMATION="Containers reaching low level, Refill containers";
                error.LOCATION= locations[r.nextInt(locations.length)];
            }
            else{

                error.CLEARED=false;
                error.SEVERITY = severities[r.nextInt(severities.length)];
                error.LOCATION=systemLocations[r.nextInt(systemLocations.length)];

                if(r.nextInt(100)<75)
                    error.INFORMATION=DataGenerator.getErrortInformation(error.SEVERITY);
            }

            int waitToClear = r.nextInt(60000*3)+120000;
            long currentTimeMilis = System.currentTimeMillis();
            error.ERROR_ID = waitToClear+"-"+currentTimeMilis;

            //optionals
            if(r.nextBoolean())
                error.DEVICE_ID = rmBox.get(r.nextInt(rmBox.size())).DEVICE_ID;
            if(r.nextBoolean())
                error.DEVICE_ERROR_CODE = "dec-"+sessionData.messageSequence;
            if(r.nextBoolean())
                error.ERROR_KEY="ek-"+sessionData.messageSequence;
            if(r.nextBoolean())
                error.JXFS_ERROR_CODE=(int)sessionData.messageSequence;

            if(r.nextBoolean()){
                int size =r.nextInt(10)+1;
                List<String> errorArgs = new LinkedList<>();
                for(int i = 0 ; i<size;i++){
                    errorArgs.add(systemLocations[r.nextInt(systemLocations.length)]+" "+error.INFORMATION+", "+error.SEVERITY);
                }
                error.ERROR_ARGS = errorArgs;
            }
        }


        if(sendAlertInOrder){
            for(RmError alert : errors){
                alert.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
                if(sendMqttEvent(ERROR,alert.DATE_TIME,RmHandler.parseRmToString(alert))!=null || !waitForRMReply){
                    addToUnclearAlerts(alert);
                    sessionData.incrementSequence(ERROR);
                }
            }
        }
        else{
            RmError alert = errors.get(r.nextInt(errors.size()));
            alert.DATE_TIME = Utilities.getCurrentDateStampIncludedCatchingUp();
            if(sendMqttEvent(ERROR,alert.DATE_TIME,RmHandler.parseRmToString(alert))!=null || !waitForRMReply){
                addToUnclearAlerts(alert);
                sessionData.incrementSequence(ERROR);
            }

        }
    }

    private void addToUnclearAlerts(RmError alert) {
        if(unclearedAlerts!=null)
            unclearedAlerts.add(alert);
    }

    private void sendBoxContent(int transactionSequence) throws IOException, InterruptedException, MqttException {
        List<RmBoxContent> devices = machine.getBoxContents();

        for(RmBoxContent device : devices){

            String dateStamp = Utilities.getCurrentDateStampIncludedCatchingUp();
            RmBoxContent rm = RmHandler.generateBoxContent(
                    dateStamp,
                    timeZoneOffset,
                    machine.getMachineUUID(),
                    transactionSequence,
                    sessionData.messageSequence,
                    device.DEVICE,
                    device.DEVICE_ID,
                    device.DEVICE_TYPE,
                    device.VALUE_TYPE,
                    device.BOX_CONTENTS
            );

            String payload = RmHandler.parseRmToString(rm);
            if(sendMqttEvent(BOX_CONTENT,dateStamp,payload)!=null || !waitForRMReply){
                sessionData.incrementSequence(BOX_CONTENT);
            }
        }

    }

    private SystemStatusStatus oldSystemStatus=null;

    private String sendSystemStatus(List<SystemStatusStatus> statuses) throws InterruptedException, IOException, MqttException {
        List<SystemStatusStatus> systemStatuses;
        int startIndex,endIndex;
        if(statuses == null){
            systemStatuses = this.statuses;
            startIndex = 1;
            endIndex = 4;
        }
        else{
            systemStatuses = statuses;
            startIndex = 0;
            endIndex = statuses.size();
        }

        String fileVersionDiffer = null;

        for(int i = startIndex; i<endIndex;i++){
            String dateStamp = Utilities.getCurrentDateStampIncludedCatchingUp();
            SystemStatusStatus oldStatus;
            if(oldSystemStatus==null)
                if(i<=0)
                    oldStatus = Idle;
                else
                    oldStatus = systemStatuses.get(i - 1);
            else
                oldStatus = oldSystemStatus;

            RmSystemStatus rm;
            rm = RmHandler.generateSystemStatusMessage(machine.getMachineUUID(), systemStatuses.get(i).name(),oldStatus.name(),dateStamp,main.timeZoneOffset,sessionData.messageSequence, machine.getMachineUserSyncState().getFileVersion());

            String payload = RmHandler.parseRmToString(rm);
            String response = sendMqttEvent(EventType.SYSTEM_STATUS,dateStamp,payload);
            if(!waitForRMReply)
                sessionData.incrementSequence(SYSTEM_STATUS);
            if(response!=null && !response.startsWith("failed")){
                sessionData.incrementSequence(SYSTEM_STATUS);
                oldSystemStatus = systemStatuses.get(i);
                RmServerReply reply = RmHandler.parseStringToRmServerReply(response);
                if(reply.FILE_VERSIONS== null || reply.FILE_VERSIONS.users==null)
                    logger.log("FILE_VERSIONS is null in SystemStatus reply - check for timteouts in RM proxy",0,null,null,response);
                else if (!reply.FILE_VERSIONS.users.equalsIgnoreCase(machine.getMachineUserSyncState().getFileVersion()))
                    fileVersionDiffer = reply.FILE_VERSIONS.users;


            }

        }
        return fileVersionDiffer;

    }

    private String payloadTransaction;

    @Override
    public String toString(){
        return machine.toString();
    }

    public void sendTransactionBoxContent(RmUserCommon rmUser) throws InterruptedException, IOException, MqttException {

        //TODO implement counterfeis/suspect notes: c2 -c4 transactions

        String dateTime;
        RmTransaction rmTransaction;
        try{
            if(sendTransactionInOrder){
                for (TransactionType transactionType : transactionTypes){
                    if(nextTransactionType!=null){
                        transactionType = nextTransactionType;
                        if(main.debugMode || lastSession)
                            System.out.println(nextTransactionType);
                    }
                    dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();

                    rmTransaction = DataGenerator.getTransactionRow(
                            transactionType,
                            machine,
                            rmUser,
                            dateTime,
                            timeZoneOffset,
                            sessionData.transactionSequence,
                            sessionData.messageSequence,
                            sessionData.sequennce);


                    if(sendBoxContents && (rmTransaction!=null)){
                        sendBoxContent(sessionData.transactionSequence);
                        rmTransaction.MESSAGE_SEQUENCE=sessionData.messageSequence;
                        dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();
                        rmTransaction.DATE_TIME=dateTime;
                    }

                    payloadTransaction = RmHandler.parseRmToString(rmTransaction);

                    if(sendMqttEvent(EventType.TRANSACTION,dateTime,payloadTransaction)!=null || !waitForRMReply){
                        nextTransactionType=null;
                        sessionData.incrementSequence(TRANSACTION);
                    }
                }
            }else{

                TransactionType transactionType;
                if(nextTransactionType==null)
                    transactionType= transactionTypes.get(new Random().nextInt(transactionTypes.size()));
                else{
                    transactionType=nextTransactionType;
                    if(main.debugMode || lastSession)
                        System.out.println(nextTransactionType);
                }

                dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();
                RmTransaction transaction = DataGenerator.getTransactionRow(
                        transactionType,
                        machine,
                        rmUser,
                        dateTime,
                        timeZoneOffset,
                        sessionData.transactionSequence,
                        sessionData.messageSequence,
                        sessionData.sequennce);

                if(sendBoxContents){
                    sendBoxContent(sessionData.transactionSequence);
                    transaction.MESSAGE_SEQUENCE=sessionData.messageSequence;
                    transaction.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
                }
                payloadTransaction = RmHandler.parseRmToString(transaction);
                if(sendMqttEvent(EventType.TRANSACTION,dateTime,payloadTransaction)!=null || !waitForRMReply)
                    sessionData.incrementSequence(TRANSACTION);
            }
            nextTransactionType = null;
        }

        catch (BoxContentEmptyExcception e) {
            e.printStackTrace();
            //MANUAL REFFIL for COINS or REFILL for NOTES
            String message = e.getMessage();
            String[] devicesForRefillType = message.split("!>");

            if(devicesForRefillType[0].equals(NOTE.name()))
                nextTransactionType =TransactionType.REFILL;
            if(devicesForRefillType[0].equals(COIN.name()))
                nextTransactionType = TransactionType.MANUAL_REFILL;

            message+="\n\tNext transactions set to"+nextTransactionType.name();
            logger.log(message, 0, null,null, null);
            if(main.debugMode || lastSession)
                System.out.println(machine.getMachineUUID()+" - "+ message);
        }
        catch (BoxContentFullException e) {
           // e.printStackTrace();
            nextTransactionType=TransactionType.PURGE;
            String message=e.getMessage()+"\n\tNext transactions set to"+nextTransactionType.name();
            logger.log(message, 0, null,null, null);
            RmError alert = new RmError();
            alert.KIND="ThresholdFull";
            alert.INFORMATION=message;
            List<RmError> alerts = new LinkedList<>();
            alerts.add(alert);
            sendErrors(alerts,true);
            if(main.debugMode ||lastSession)
                System.out.println(machine.getMachineUUID()+" - "+ message);
        }
        catch (EmptiableBoxesFullException e) {
           // e.printStackTrace();
            nextTransactionType=TransactionType.EMPTY_OUT;

            String message=e.getMessage()+"\n\tNext transactions set to"+nextTransactionType.name();
            logger.log(message, 0, null,null, null);
            RmError alert = new RmError();
            alert.KIND="ThresholdFull";
            alert.INFORMATION=message;
            List<RmError> alerts = new LinkedList<>();
            alerts.add(alert);
            sendErrors(alerts,true);
            if(main.debugMode||lastSession)
                System.out.println(machine.getMachineUUID()+" - "+ message);
        }
        catch (DropSafeFullException e){
            nextTransactionType=TransactionType.EXCHANGE;
            machine.setEmptyDropSafe(true);
            String message=e.getMessage()+"\n\tNext transactions set to"+nextTransactionType.name();
            logger.log(message, 0, null,null, null);
            if(main.debugMode||lastSession)
                System.out.println(machine.getMachineUUID()+" - "+ message);
        }
        catch (JsonProcessingException e) {
           // e.printStackTrace();
            logger.log(e.getMessage(), 0, null,null, null);
        }

    }

    public RmUserCommon selectUser() {
        Random r = new Random();
        //generate workunits
        String workUnitParent, workUnitName;
        List<String> parents = new LinkedList<>();
        parents.addAll(workUnits.keySet());
        workUnitParent = parents.get(r.nextInt(parents.size()));
        String[] wu = workUnits.get(workUnitParent);
        workUnitName = wu[r.nextInt(wu.length)];


        //only set workunits sometimes
//        if(r.nextInt(100)>25){
//            workUnitParent = "";
//            if(r.nextBoolean()==true)
//                workUnitName="";
//        }
//

        //generate user, set originating user for some transactions
        Cashier machineUser = machineUsersCommon.get(r.nextInt(machineUsersCommon.size()));
        RmUserCommon originatingUserRm = null;
        int includeOriginatingUser = r.nextInt(100);
        if(includeOriginatingUser<25){
            int neighbour = machineUsersCommon.indexOf(machineUser);
            if(neighbour ==0) {
                neighbour++;
            }
            else if(neighbour == machineUsersCommon.size()-1) {
                neighbour--;
            }
            else {
                neighbour++;
            }
            Cashier originatingUser = null;
          //  originatingUser = machineUsersCommon.get(neighbour);
          //  originatingUserRm = RmHandler.generateUser(originatingUser.id,originatingUser.name,originatingUser.role,workUnitName,workUnitParent,null,originatingUser.accounts);
        }
        return RmHandler.generateUser(machineUser.id,machineUser.name,machineUser.role,workUnitName,workUnitParent,originatingUserRm,machineUser.accounts);
    }

    public void connectToMqtt() throws MqttException, IOException {
        mqtt = new MqTTHandler(brokerHost,tenant,machine.getMachineUUID(),mqttUserName,mqttPassWord);
        logger.log("Mqtt initiated", 0, null,null, null);
        long startTime = System.currentTimeMillis();
        mqtt.setWaitForResponse(waitForRMReply);
        mqtt.setQos(mqttQos);
        mqtt.connect();
        long duration = System.currentTimeMillis()-startTime;
        logger.log("Mqtt connected: "+brokerHost+", "+tenant+","+machine.getMachineUUID(), duration, null,null, null);

        keepRunning= true;
    }

    public void stopSession() throws IOException, MqttException, InterruptedException{
        logger.log(sessionName+" reached time to run for, stopping session", 0, null,null, null);
        keepRunning = false;

        mqtt.disconnect();
        logger.log("Mqtt client disconnected", 0, null,null, null);

        main.putSessionData(sessionStoreKey,sessionData);

        if(failedEventsAmount>10){
            logger.log("Machine terminated due to 10 failed mqtt responses in a row",0,null,null,null);
            main.incrementFailedMachines();
        }
        logger.log(" Session data saved: "+sessionData.toString(), 0, null,null, null);
        logger.log(logMachineContentInCurrentState(), 0, null,null, null);
        logger.log(logMachineUserState("Session End(after deletion of all authorized users"),0,null,null,null);
        logger.closeLoggers();
    }


    private boolean runTimeReached() throws IOException {

        if((!brokerHost.equalsIgnoreCase("localhost") && !brokerHost.equalsIgnoreCase("127.0.0.1")))
            if(failedEventsAmount>10 )
                return true;

        //run indifinetly when runtimeMin <0
        if(Long.compare(runTimeMin,0)<0)
            return false;

        long timeNow = System.currentTimeMillis();
        long runFor = (runTimeMin*60*1000)+(machineNbr*main.machineRampUpMs);
        long runningFor = timeNow-sessionStartTime;

        String message= sessionName+" entered a new event loop, running for: "+runningFor+" / "+runFor;
        logger.log(message, 0, null,null, null);

        if(lastSession)
            System.out.println(message);

        int i = Long.compare(runningFor,runFor);
        if(i >= 1)
            return true;
        else
            return false;
    }

    public String sendMqttEvent(EventType type, String dateTime, String payload) throws MqttException, InterruptedException, IOException {

        if(type!=BOX_CONTENT)
            Thread.sleep(eventDelayMs);


        if(payload==null){
            logger.log("Mqtt description not sent, payload is null, "+type.name(), 0, null,null, null);
            return null;
        }

        if(Main.debugMode || lastSession)
            System.out.print("CcodSession.sendMqttEvent(): "+machine.getMachineUUID()+" - "+type+"\t\tmqtt response: ");

        long startTime = System.currentTimeMillis();
        mqtt.publishMessage(payload);

        String mqttResponse=null;
        long duration=-1;

        if(waitForRMReply==true){
            mqtt.waitForProcessingToFinish(mqttTimeoutLimit);
            long endTime = System.currentTimeMillis();
            duration = endTime-startTime;
            mqttResponse = mqtt.getResponsedata();
            if(mqttResponse==null){
                failedEventsAmount++;
                mqttResponse = "failed_"+type.name();
            }
            else{
                failedEventsAmount=0;
            }
        }


        if(Main.debugMode || lastSession)
            System.out.println(mqttResponse);

        logger.log(payload, duration, type,dateTime, mqttResponse);

        return mqttResponse;
    }

    public void setStatuses(List<SystemStatusStatus> statuses) {
        this.statuses = statuses;
    }

    public void setErrorWarnings(List<ErrorWarning> errorWarnings) {
        this.errorWarnings = errorWarnings;
    }

    public void setTransactionTypes(List<TransactionType> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }

    public void setTransacionsInOrder(boolean orderOrRandom) {
        this.sendTransactionInOrder = orderOrRandom;
    }

    public void isLastSession(boolean lastSession) {
        this.lastSession=lastSession;
    }

    public void setSendErrorsInOrder(boolean sendAlertsInOrder) {
        this.sendAlertsInOrder = sendAlertsInOrder;
    }

    public void setSendLaneAllocations(boolean sendLaneAllocations) {
        this.sendLaneAllocations = sendLaneAllocations;
    }

    public void setSendLaneAllocationDaybreak(boolean sendLaneAllocationDayBreak) {
        this.sendLaneAllocationDayBreak=sendLaneAllocationDayBreak;
    }

    public void setSendDLSTransactions(boolean sendDLSTransactions) {
        this.sendDLSTransactions=sendDLSTransactions;
    }

    public void setSyncWorkunits(boolean syncWorkUnits) {
        this.syncWorkunits=syncWorkUnits;
    }

    public void setWaitForRmReply(boolean waitForRMReply) {
        this.waitForRMReply=waitForRMReply;
    }

    public void setMqttQos(int mqttQos) {
        this.mqttQos=mqttQos;
    }


    public class SessionLogger {

        //private HashMap<EventType, FileWriter> eventWriters = new HashMap();
        private HashMap<EventType, Writer> eventWriters = new HashMap();
        //private FileWriter logWriter;
        private Writer logWriter;
        private String fileNameSuffix;

        private SessionLogger(String sessionName){
            fileNameSuffix =sessionName;

        }

        public void initLoggers() throws IOException {

            String path = System.getProperty("user.dir")+pathSeparator+"sessionData"+pathSeparator;
            String logHeader = "timeStamp"+csvDelimeter+"eventType"+csvDelimeter+"responseTimeMs"+csvDelimeter+"payload"+csvDelimeter+"response";

            File parent = new File(path);
            if(!parent.exists())
                parent.mkdir();

          //  logWriter = new FileWriter(new File(parent,fileNameSuffix+"_log.txt"));
            logWriter = new OutputStreamWriter(new FileOutputStream(new File(parent,fileNameSuffix+"_log.txt")), StandardCharsets.UTF_8);


            eventWriters = new HashMap<>();

            if(!demoMode){

                List<EventType> eventTypes = EventType.getEventTypes();

                for(EventType eventType : eventTypes){
                    String pathSuffix="";
                    switch (eventType){
                        case ERROR:
                            pathSuffix = "rmAlerts"+pathSeparator;
                            break;
                        case BOX_CONTENT:
                            pathSuffix="rmBoxContents"+pathSeparator;
                            break;
                        case TRANSACTION:
                            pathSuffix="rmTransactions"+pathSeparator;
                            break;
                        case USER_UPDATE:
                            pathSuffix="rmUserUpdates"+pathSeparator;
                            break;
                        case SYSTEM_STATUS:
                            pathSuffix="rmSystemStatuses"+pathSeparator;
                            break;
                    }

                    parent = new File(path);
                    File directory = new File(parent,pathSuffix);
                    if(!directory.exists())
                        directory.mkdir();

                    //eventWriters.put(eventType, new FileWriter(path+pathSuffix+fileNameSuffix+".csv"));

                    eventWriters.put(eventType, new OutputStreamWriter(new FileOutputStream(new File(path+pathSuffix+fileNameSuffix+".csv")), StandardCharsets.UTF_8));
                    logLine(eventWriters.get(eventType),logHeader);
                }
            }


        }
//FileWriter
        private void logLine(Writer fileWriter, String line) throws IOException {

            fileWriter.write(line+"\n");
            fileWriter.flush();

        }

        public void log(String message, Object remoteConfigurationDeployment) throws IOException {
            log(message+new Gson().toJson(remoteConfigurationDeployment));
        }

        public void log(String message) throws IOException {
            log(message,0,null,null,null);
        }

        private void log(String payload, long duration, EventType type, String dateTime, String mqttResponse) throws IOException {

            StringBuilder row = new StringBuilder();
            if(dateTime==null)
                dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();

            row .append(Utilities.getCurrentDateStampLog()+"\t");
            if(type!=null)
                row.append(type.name()+"\t");

            int compare = Long.compare(duration,0);
            if(compare>0)
                row.append(duration+"\t");

            row.append(payload+"\t");

            if(mqttResponse!=null)
                row.append(mqttResponse);

            logLine(logWriter, row.toString());

            if(type!=null && !demoMode){
                StringBuilder eventLine = new StringBuilder();
                eventLine.append(Utilities.getCurrentDateStampLog()+csvDelimeter);
                eventLine.append(type.name()+csvDelimeter);
                eventLine.append(duration+csvDelimeter);
                eventLine.append(payload+csvDelimeter);
                eventLine.append(mqttResponse);
                Writer eventWriter = eventWriters.get(type);
                if(eventWriter!=null)
                    logLine(eventWriter,eventLine.toString());
                else
                    logLine(logWriter,type.name()+" Not found, unable to log: "+eventLine.toString());

            }
        }

        private void closeLoggers() throws IOException {

            if(logWriter != null)
                logWriter.close();

            Set<EventType> keys = eventWriters.keySet();

            for(EventType key : keys)
                if(eventWriters.get(key)!=null)
                    eventWriters.get(key).close();
        }



    }

}
