package ccodSession;

import CCoD.CCoDMachine;
import com.fasterxml.jackson.core.JsonProcessingException;
import rmObjects.RmError;
import rmObjects.RmHandler;

import java.io.Serializable;
import java.util.List;

public class SessionData implements Serializable {

    private static final long serialVersionUID = 1L;
    private String rmLicenseId, rmMachineId;


    public int transactionSequence;
    public long messageSequence;
    public long sequennce;
    protected long boxContentSequence;
    public long alertSequence;
    protected long systemStatusSequence;
    protected long userUpdateSequence;

    private String fileName;
    protected CCoDMachine machine;
    public List<RmError> unclearedAlerts;

    public SessionData(String sessionStoreKey, CCoDMachine machine, int machineNbr, String rmLicenseId) {
        fileName = sessionStoreKey;
        this.rmLicenseId=rmLicenseId;
        this.machine=machine;

        if(this.rmLicenseId==null)
            this.rmLicenseId="";

        rmMachineId = Integer.toString(machineNbr);
        if(machineNbr<10) {
            rmMachineId = "000"+machineNbr;
        }
        if(machineNbr<100) {
            rmMachineId = "00"+machineNbr;
        }
        if(machineNbr<1000) {
            rmMachineId = "0"+machineNbr;
        }

    }

    public String toString(){

        StringBuilder sb = new StringBuilder();
        sb.append("Session Store key:"+fileName+", ");
        sb.append("Message sequence: "+transactionSequence+", ");
        sb.append("Sequence: "+transactionSequence+", ");
        sb.append("BoxContent sequence: "+transactionSequence+", ");
        sb.append("Alert sequence: "+transactionSequence+", ");
        sb.append("SystemStatus sequence: "+transactionSequence+", ");
        sb.append("UserUpdate sequence: "+transactionSequence+", ");
        sb.append("RM MACHINE_ID: "+rmMachineId+", ");
        sb.append("RM LICENSE_ID: "+rmLicenseId);
        try {
            sb.append("\n\tBoxContent: "+RmHandler.parseRmBoxContentToString(machine.getBoxContents()));
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
        return sb.toString();
    }

    public void incrementSequence(EventType eventType) {
        messageSequence++;
        switch (eventType){
            case SYSTEM_STATUS:
                systemStatusSequence++;
                break;
            case TRANSACTION:
                transactionSequence++;
                sequennce++;
                break;

            case BOX_CONTENT:
                boxContentSequence++;
                break;

            case ERROR:
                alertSequence++;
                break;

            case USER_UPDATE:
                userUpdateSequence++;
                break;

        }
    }


}
