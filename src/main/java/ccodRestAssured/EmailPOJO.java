package ccodRestAssured;

public class EmailPOJO implements Comparable<EmailPOJO> {

    public String emailTitle;
    public String emailContent;
    public String date;

    public EmailPOJO(String title, String content,String date) {
        this.emailTitle = title;
        this.emailContent = content;
        this.date=date;
    }

    @Override
    public int compareTo(EmailPOJO o){
        return this.date.compareTo(o.date);
    }

}
