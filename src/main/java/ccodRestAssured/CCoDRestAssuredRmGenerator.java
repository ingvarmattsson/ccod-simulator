package ccodRestAssured;

import CCoD.Currency;
import CCoD.TransactionType;
import cccObjects.DeviceCCC;
import cccObjects.SiteCCC;
import ccodSession.CCoDSession;
import ccodSession.Environments;
import com.fasterxml.jackson.core.JsonProcessingException;
import createCbData.ZReportGenerator;
import data.DataGenerator;
import data.DefaultStrings;
import data.Utilities;
import exceptions.BoxContentEmptyExcception;
import exceptions.BoxContentFullException;
import exceptions.DropSafeFullException;
import exceptions.EmptiableBoxesFullException;
import org.eclipse.paho.client.mqttv3.MqttException;
import rmObjects.*;
import rmReplay.RmReplay;
import ztsAutomation.DLSRobustnessZTS;

import java.io.IOException;
import java.util.*;

import static CCoD.TransactionType.*;

public class CCoDRestAssuredRmGenerator extends DLSRobustnessZTS {


    public RmError generateRmError(CCoDSession ccodSession) {
        RmError error = new RmError();
        Random r = new Random();
        error.DATE_TIME = Utilities.getCurrentDateStampIncludedCatchingUp();
        error.TIME_ZONE_UTC_OFFSET=timeZoneOffset;
        error.VERSION = ccodSession.getMachine().getSfVersion();
        error.MACHINE_ID =error.UUID =ccodSession.getMachine().getMachineUUID();
        error.RECEIVER="Server";
        error.MESSAGE_SEQUENCE=ccodSession.sessionData.messageSequence;
        error.SEQUENCE = ccodSession.sessionData.alertSequence;
        error.MESSAGE_TYPE="Error";
        error.ERROR_ID="id-ra-"+error.MACHINE_ID+"-"+error.MESSAGE_SEQUENCE;
        error.CLEARED = false;
        error.KIND = DataGenerator.getErrorKinds()[r.nextInt(DataGenerator.getErrorKinds().length)];
        error.SEVERITY = DataGenerator.getErrorSeverities()[r.nextInt(DataGenerator.getErrorSeverities().length)];
        error.INFORMATION = DataGenerator.getErrortInformation(error.SEVERITY);
        error.LOCATION = DataGenerator.getErrorLocations()[r.nextInt(DataGenerator.getErrorLocations().length)];

        ccodSession.sessionData.messageSequence++;
        ccodSession.sessionData.alertSequence++;
        return error;
    }


    public RmSystemStatus generateRmSystemStatus(CCoDSession ccodSession) {
        Random r = new Random();
        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(
                ccodSession.getMachine().getMachineUUID(),
                DataGenerator.getSystemStatusStatuses().get(r.nextInt(DataGenerator.getSystemStatusStatuses().size())).name(),
                DataGenerator.getSystemStatusStatuses().get(r.nextInt(DataGenerator.getSystemStatusStatuses().size())).name(),
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                ccodSession.sessionData.messageSequence++,
                "0"

        );
        return ss;
    }

    public RmTransaction generateRmTransaction(CCoDSession ccodSession, TransactionType transactionType) throws BoxContentEmptyExcception, EmptiableBoxesFullException, DropSafeFullException, BoxContentFullException, JsonProcessingException {
        return DataGenerator.getTransactionRow(
                transactionType,
                ccodSession.getMachine(),
                ccodSession.selectUser(),
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                ccodSession.sessionData.transactionSequence++,
                ccodSession.sessionData.messageSequence++,
                ccodSession.sessionData.sequennce++
        );
    }

    public RmBoxContent generateRmBoxContent(CCoDSession ccodSession, RmBoxContent rmBoxContent) {
        RmBoxContent rm = RmHandler.generateBoxContent(
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                ccodSession.getMachine().getMachineUUID(),
                ccodSession.sessionData.transactionSequence++,
                ccodSession.sessionData.messageSequence++,
                rmBoxContent.DEVICE,
                rmBoxContent.DEVICE_ID,
                rmBoxContent.DEVICE_TYPE,
                rmBoxContent.VALUE_TYPE,
                rmBoxContent.BOX_CONTENTS
        );
        rm.SEQUENCE = ccodSession.sessionData.sequennce++;
        return  rm;
    }

    public void generateDailyRmData(Set<DeviceCCC> deviceCCC, Environments env, String tenant, int runTimeMin, Currency currency) throws IOException, MqttException, InterruptedException {
        System.out.println("#### SENDING DAILY RM DATA");

        List<Thread> ccodSessions = new LinkedList<>();
        List<DeviceCCC> devices = new LinkedList<>();
        devices.addAll(deviceCCC);
        for(int i = 0; i < devices.size(); i ++){
            CCoDSession ccod = initCCodSimulator(
                    devices.get(i).hardwareId,
                    "3.15.0-ra-scheduledReportGenerator",
                    currency,
                    env,
                    tenant,
                    runTimeMin
            );
            ccod.setSendTransactions(true);
            ccod.setSendBoxContents(true);
            ccod.setSendErrors(true);
            ccod.setSendSystemStatuses(true);
            ccod.setSendFullState(true);

            LinkedList<TransactionType> transactionTypes = new LinkedList<>();
            transactionTypes.add(DISPENSE);
            transactionTypes.add(DEPOSIT);
            transactionTypes.add(MANUAL_DEPOSIT);
            transactionTypes.add(UNFINISHED_TRANSACTION);
            transactionTypes.add(POSSIBLE_TAMPERING);
            ccod.setTransactionTypes(transactionTypes);
            ccod.setTransacionsInOrder(false);
            ccodSessions.add(new Thread(ccod));
        }
        for(Thread ccodSession : ccodSessions){
            ccodSession.start();
        }
        for(Thread ccodSession : ccodSessions){
            ccodSession.join();
        }
        System.out.println("#### SEND DAILY RM DATA DONE");


    }

    public void generateZReports(Set<DeviceCCC> devicesCCC, Environments env, String tenant, int numUsersZreport, String fromDateZReport, String toDateZReport, String timeZoneOffset, int endOfDayTime, Currency currency) throws IOException {
        System.out.println("### GENERATING ZREPORT FILE");
        List<DeviceCCC> devices = new LinkedList<>();
        devices.addAll(devicesCCC);
        List<String> fileNames = new LinkedList<>();

        for(DeviceCCC device : devices){
            fileNames.add(new ZReportGenerator(
                    device.hardwareId,
                    device.siteToken,
                    numUsersZreport,
                    fromDateZReport,
                    toDateZReport,
                    timeZoneOffset,
                    endOfDayTime,
                    currency.name(),
                    env,
                    tenant
            ).generateCBData());
        }
        System.out.println("### REPLAYING ZREPORT FILE");
        for(String fileName : fileNames){
            String[] machineIdFileName = fileName.split("<>");
            RmReplay r = new RmReplay(
                    DefaultStrings.getBrokerHost(env),
                    tenant,
                    machineIdFileName[0],
                    DefaultStrings.mqttUserName,
                    DefaultStrings.mqttPassWord,
                    machineIdFileName[1]
            );

            r.sendMessagesFromFile();
        }



    }
}
