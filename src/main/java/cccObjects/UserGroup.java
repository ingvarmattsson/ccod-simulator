package cccObjects;

import java.util.List;

public class UserGroup {
    public String token;
    public String name;
    public List<String> assignedMachines;
    public List<String> assignedUsers;
}
