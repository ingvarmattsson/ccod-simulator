package cccObjects;

import java.util.List;

public class DispenseLimitSettingsWrapperCCCResponse {
    public Boolean canExceedInitial,enabled,everEnabled;
    public int dayBreakHour;
    public String machineGroupToken;
    public List<DispenseLimitSettingsCCC> dispenseLimits;
    public List<String> receivedLatestConfigMachineUuids;
    public TTALWrapper transactionTypeAffectingLimits;



}
