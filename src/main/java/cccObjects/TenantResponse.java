package cccObjects;

import tenantAuthorization.SiteWhereTenant;

import java.util.List;

public class TenantResponse {
    public String numResults;
    public List<SiteWhereTenant> results;
}
