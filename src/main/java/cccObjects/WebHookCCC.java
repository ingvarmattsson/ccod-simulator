package cccObjects;

import java.util.Map;

public class WebHookCCC {
    public String token;
    public String name;
    public String triggerType;
    public String destination;
    public Map<String,String> headers;
    public Boolean allowNonHttps;
}
