package cccObjects;

public class ValueCCC {

    public long total;
    public int count;
    public int decimals;
    public String currency;
    public int denomination;
    public int pieceValue; // RCS >= 2.10
    public int exchangeRate; // optional
    public int exchangeRateDecimals; // optional
    public String reference; // optional
    public String type;
    public String claimedValueType; // RCS >= 2.0.1
    public String customType; // optional
    public int c2Count; // optional
    public int c3Count; // optional
    public int c4BCount; // optional
    public boolean creditCat2; // optional
    public boolean creditCat3; // optional

}
