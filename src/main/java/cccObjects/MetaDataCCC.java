package cccObjects;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MetaDataCCC implements Serializable {
    private static final long serialVersionUID = 1L;

    public Map<String,String> mdVars;
    public String alias;
    public String centerLatitude;
    public String centerLongitude;
    public String ipaddress;
    public String serialNo;
    public String zoomLevel;

    public MetaDataCCC(){
//        mdVars = new HashMap<>();
    }

    public void add(String key, String value){
        if(mdVars==null)
            mdVars = new HashMap<>();

        mdVars.put(key,value);
    }
}
