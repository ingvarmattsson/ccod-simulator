package cccObjects;

import java.util.List;

public class BoxMovementCCC {
    public String boxId;
    public String containerId;
    public List<ValueBagCCC> movement;
    public List<ValueBagCCC> resultingContent;
}
