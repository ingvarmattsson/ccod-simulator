package cccObjects;

import java.util.List;

public class SiteGroup {
    public List<String> cities;
    public List<String> containedSites;
    public List<String> countries;
    public List<String> sites;
    public String token;
    public String name;
}
