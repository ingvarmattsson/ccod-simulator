package cccObjects;

import java.util.List;

public class IdentPayloadDLS {

    public String ident;
    public List<String> idents;
    public String type;
    public Integer resetHour;
    public List<IdentDLS> dispenseAmounts;

}
