package cccObjects;

import java.util.List;

public class MachinegroupCCCResponse {

    public int numResults;
    public List<MachineGroupCCC> results;
}
