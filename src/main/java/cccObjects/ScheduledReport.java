package cccObjects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScheduledReport {

    public Integer attempts;
    public String culture;
    public List<String> dates;
    public List<Integer> everyMonthly;
    public List<Integer> everyWeekly;
    public String name;
    public Integer sequentialAttempts;
    public String time;
    public String timeZone;
    public String token;
    public List<Recipient> recipients;
    public List<Report> reports;

    public static class Recipient {
        public String recipient;
        public String type;
        public Params params;
    }

    public static class Params {
        public String emailTitle;
        public String timestamp;
        public String date;
        public String machineIds;
        public String fromAccountingDate;
        public String toAccountingDate;
        public String fromDate;
        public String toDate;
        public String endOfDayTime;
    }

    public static class Report {
        public String displayName;
        public String fileType;
        public String report;
        public String subClasstitle;
        public Map<String,String> headerMapping;
//        public Params params;
        public Map<String,String> params = new HashMap<>();
    }

}
