package cccObjects;

import java.util.List;

public class UserMachine {
    public List<String> accessedMachines;
    public List<String> assignedGroups;
    public List<String> assignedMachines;
    public List<String> doors;
    public Boolean deactivated;
    public Boolean noPin;
    public Boolean noPinBio;
    public Boolean noPinCard;
    public Boolean noPinDoor;
    public Boolean noPinUserId;
    public Boolean changePinOnLogin;
    public Integer nbrLogins;
    public String pin;
    public String roleName;
    public String userId;
    public String cardIdentifier;
    public String validFrom;
    public String validUntil;

}
