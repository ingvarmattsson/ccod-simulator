package cccObjects;

import java.util.Map;

public class MachineDeviceInfo {
    public String hardware;
    public Map<String, Object> firmware;
}
