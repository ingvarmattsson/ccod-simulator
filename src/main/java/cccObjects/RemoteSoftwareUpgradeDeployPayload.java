package cccObjects;

import com.google.gson.JsonElement;

import java.util.LinkedList;
import java.util.List;

public class RemoteSoftwareUpgradeDeployPayload {
    public List<String> machineIds;
    public String packageId;
    public String scheduleInfo;
    public RemoteSoftwareUpgradeDeployPayload(String machineUUID, String scheduleInfo, String packageId) {
        machineIds = new LinkedList<>();
        machineIds.add(machineUUID);
        this.scheduleInfo = scheduleInfo;
        this.packageId=packageId;
    }
}
