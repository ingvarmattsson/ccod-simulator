package cccObjects;

import java.util.Map;

public class MachineDevice {
    public String id;
    public Map<String, String[]> capabilities;
    public MachineDeviceInfo deviceInfo;
    public MachineDeviceInfo sensorInfo;

}
