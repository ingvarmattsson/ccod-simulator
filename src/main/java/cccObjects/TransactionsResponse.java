package cccObjects;

import cccObjects.TransactionCCC;

import java.util.List;

public class TransactionsResponse {

    public int numResults;
    public List<TransactionCCC> results;
}
