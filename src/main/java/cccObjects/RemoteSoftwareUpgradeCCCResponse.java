package cccObjects;

import java.util.List;

public class RemoteSoftwareUpgradeCCCResponse {

    public int numResults;
    public List<RemoteSoftwareUpgradeCCC> results;
}
