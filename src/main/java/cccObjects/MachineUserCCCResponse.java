package cccObjects;

import java.util.List;

public class MachineUserCCCResponse {
    public int numResults;
    public List<MachineUserCCC> results;
}
