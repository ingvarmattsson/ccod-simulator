package cccObjects;

import java.util.List;

public class MachineDetailCCC {
    public String accountingDate;
    public String activeDate;
    public String alias;
    public String ccodVersion;
    public String dateTime;
    public String machineId;
    public String site;
    public String timeZoneUtcOffset;
    public String type;
    public String uuid;
    public List<MachineDevice> devices;

}
