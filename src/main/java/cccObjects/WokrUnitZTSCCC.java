package cccObjects;

import java.util.List;

public class WokrUnitZTSCCC {
    public String NAME;
    public String DISPLAY_NAME;
    public Boolean IGNORE_ALLOCATION_DAY_BREAK;
    public List<String> MONEY_MIXES;
    public List<String> ROLE_IDS;
    public List<WokrUnitZTSCCC> CHILDREN;
}
