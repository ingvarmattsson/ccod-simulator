package cccObjects;

public class Metadata {
    public String customerReference;
    public String countryCode;
    public String email;
    public String phone;
    public String siteGroups;
    public Boolean autoRefreshEnabled;
}
