package cccObjects;

import java.util.List;

public class POSTransaction {

    public class POSCash{
        public int amount;
        public int decimal;
        public String currency;
        public String type;

    }

    public class User{
        public String roleName;
        public String userId;
        public String userName;
    }

    public String accountingDate;
    public List<POSCash> cashTotals;
    public List<POSCash> cashValues;
    public String machineId;
    public String messageType;
    public List<POSCash> nonCashTotals;
    public List<POSCash> nonCashValues;
    public List<POSCash> openingAmount;
    public String outlet;
    public String posAdapterId;
    public String shiftDateTimeUtc;
    public String shiftLocalTime;
    public String shiftLocalTimeUtcOffset;
    public String siteId;
    public String token;
    public Boolean useInitalBank;

}
