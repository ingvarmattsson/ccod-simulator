package cccObjects;

import java.util.List;
import java.util.Map;

public class TransactionCCC {
    public long totalAmount; // not suitable for multiple currencies
    public List<TotalAmountCCC> totalAmounts;
    public String accountingDate, dateTime,receivedDateTime;
    public long commission; // not suitable for multiple currencies
    public String currency; // not suitable for multiple currencies
    public int decimals;
    public String reference; // optional
    public String machineUserId;
    public TransactionUserCCC machineUser;
    public String depositKind; // optional
    public List<ValueBagCCC> valueBags;
    public String checksum; // optional
    public long transactionSequence; // RCS >= 2.5.0
    public String type;
    public String subType; // optional
    public String mixName; // optional
    public String mixDisplayName; // optional
    public Boolean mixEdited; // optional
    public List<String> strapSeals; // optional
    public List<Mix> subMixes; // optional
    public Map<String, String> customData; // optional
    public List<DeviceMovementCCC> deviceMovements; // RCS >= 2.15.7
    public String siteToken;
    public String machineId;
    public boolean isCorrection;


}
