package cccObjects;

import java.util.List;

public class Alert {
    public String name;
    public List<ScheduledReport.Recipient> recipients;
    public String token;
    public Trigger trigger;

    public static class Trigger {
        public Trigger child;
        public String messageTypeFilter;
        public String type;
        public String typeClass;
        public String fromTime;
        public String toTime;
        public List<String> filter;
    }
}
