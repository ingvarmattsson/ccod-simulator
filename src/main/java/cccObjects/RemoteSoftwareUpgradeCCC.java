package cccObjects;

import java.util.List;

public class RemoteSoftwareUpgradeCCC {

    public boolean common;
    public String createdBy;
    public String createdDate;
    public String description;
    public String file;
    public String id;
    public String label;
    public String name;
    public String node;
    public String packageId;
    public List<String> parts;
    public String productName;
    public String revision;
    public String version;
    public String installationId;
    public String machineUuid;
    public String state;
    public String stateChangedDate;
}
