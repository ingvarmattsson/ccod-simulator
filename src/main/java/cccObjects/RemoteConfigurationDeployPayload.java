package cccObjects;

import java.util.LinkedList;
import java.util.List;

public class RemoteConfigurationDeployPayload {

    public List<String> machineUuids;
    public String scheduleInfo;
    public String templateUuid;
    public boolean writePermissions;

    public RemoteConfigurationDeployPayload(String machineUuid, String scheduleInfo, String templateUuid, boolean writePermissions) {
        this.machineUuids = new LinkedList<>();
        machineUuids.add(machineUuid);
        this.scheduleInfo = scheduleInfo;
        this.templateUuid = templateUuid;
        this.writePermissions = writePermissions;
    }
}
