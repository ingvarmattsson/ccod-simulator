package cccObjects;

import java.util.List;

public class MachineDetailCCCResponse {
    public int numResults;
    public List<MachineDetailCCC> results;
}
