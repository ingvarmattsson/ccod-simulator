package cccObjects;

import java.util.List;

public class DispenseLimit {

    public static class DispenseAmount{
        public int amount;
        public int initial;
        public int decimals;
        public String currency;
    }

    public class DispenseLimitIdent{
        public List<DispenseAmount> dispenseAmounts;
        public String ident;
        public int resetHour;
        public String type;
    }

    public Boolean canExceedInitial;
    public int dayBreakHour;
    public Boolean enabled;
    public Boolean everEnabled;
    public String machineGroupToken;
    public List<String> receivedLatestConfigMachineUuids;
    public List<DispenseLimitIdent> dispenseLimits;
}
