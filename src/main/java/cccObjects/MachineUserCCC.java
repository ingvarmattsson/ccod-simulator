package cccObjects;

import java.util.List;

public class MachineUserCCC {
    public List<String> assignedMachines;
    public List<String> accessedMachines;
    public List<String> assignedGroups;
    public List<String> doors;
    public String alias;
    public String cardIdentifier;
    public String fingerprints;
    public String name;
    public String roleName;
    public String userId;

    public Boolean changePinOnLogin;
    public Boolean deactivated;
    public Boolean list;
    public Boolean noPin;
    public Boolean noPinBio;
    public Boolean noPinCard;
    public Boolean noPinDoor;
    public Boolean noPinUserId;

    public Integer nbrBadPin;
    public Integer nbrLogins;
}
