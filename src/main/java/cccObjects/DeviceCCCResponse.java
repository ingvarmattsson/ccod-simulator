package cccObjects;

import java.util.List;

public class DeviceCCCResponse {

    public int numResults;
    public List<DeviceCCC> results;
}
