package cccObjects;

import java.util.List;

public class DeviceCCC {
    public String assignmentToken,createdBy,createdDate,hardwareId,siteToken,specificationToken,status,updatedBy,
            updatedDate;
    public Boolean deleted;
    public List<String> deviceElementMappings;
    public MetaDataCCC metadata;
}
