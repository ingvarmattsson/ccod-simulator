package cccObjects;

import java.util.List;

public class RemoteConfigurationCCC {

    public String uuid;
    public String name;
    public String version;
    public List<String> parts;
    public String packageName;
    public String description;
    public String machineUuid;
    public String templateUuid;
    public String scheduleInfo;
    public String state;
}
