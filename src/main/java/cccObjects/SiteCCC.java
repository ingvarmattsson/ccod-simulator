package cccObjects;

import java.util.List;

public class SiteCCC {
    public String name;
    public String imageUrl="https://connect.cashcomplete.com/assets/img/favicon/favicon.ico";
    public String description;
    public Map map = new Map();
    public Metadata metadata = new Metadata();
    public String token;

    public class Map {
        public String type="openstreetmap";
        public Metadata metadata = new Metadata();
    }
}
