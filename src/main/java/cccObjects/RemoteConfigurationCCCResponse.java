package cccObjects;

import java.util.List;

public class RemoteConfigurationCCCResponse {

    public int numResults;
    public List<RemoteConfigurationCCC> results;
}
