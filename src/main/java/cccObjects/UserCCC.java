package cccObjects;

public class UserCCC {
    public String countryCode;
    public String email;
    public String firstName;
    public String lastName;
    public String password;
    public String phone;
    public String role;
    public String status;
    public String username;

    public Metadata metadata;
}
