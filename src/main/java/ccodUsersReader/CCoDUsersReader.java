package ccodUsersReader;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

public class CCoDUsersReader {
    /*not sent
    * b8c5176b-28db-4d93-980d-0ca33df3dc26
    * c4fa7802-abf1-4194-a83a-1c03420d11ee
    *d4e82fb3-6c8f-41c7-89d4-dd228e41798c
    * e783600b-ac7f-495c-84d9-8e91f2af12f3
    *f2ddf35e-ce38-4ae4-9314-1c4af9a68a03
    *
    *
    * */

    private static String uuid="03621fb7-2a7b-4c2f-886b-c4b793693f42";

    private static String[] uuids={
            "03621fb7-2a7b-4c2f-886b-c4b793693f42",
            "056ef50a-1931-4a2e-ba05-5e3303ea96e0",
            "2fde249f-b14a-46b9-8a6c-dbf303bb95d6",
            "5fb178bb-a2b7-4d10-bbe5-9c11d5957181",
            "613bbc0c-d811-499e-8be2-7f9c6d5c7dfc",
            "7528af95-5318-433c-898b-b1248fb825ef",
            "8c830873-9784-4b5c-8363-aa047e1b3c4d",
            "93250d79-458f-4bcd-ab2b-8bc45c26fbc7",
            "ad1b77d6-e394-423e-ab84-4dc49e0e8a72",
            "b1045cb7-7a64-4ff6-87e4-12637fe61255",
            "b9b72e5c-bb4b-4a5a-be2f-20e71101e650",
            "bffc65c6-734f-414a-86a5-c687c7b010fe",
            "c052e5ab-3f8e-43d7-8e70-8d9758c335a6",
            "cd8c0e9c-7d6c-4cf5-be57-e3e1158bb4b1",
            "d39bf820-310d-4bdb-a1b3-0a4b9288ff6b",
            "e23a22bf-d592-4a72-8fa0-e1d6927c6a96",
            "fcdcb581-1464-4555-a576-e30450ffc3cc",
            "ff0c78d0-1800-4d3a-a756-3b68b21849c9"


    };

    public static void main(String[] args) {
        CCoDUsersReader ur = new CCoDUsersReader();
        System.out.println("BRICOMARTES");
        for(int i = 0; i< uuids.length;i++){
            ur.run(uuids[i]);
        }

    }

    private void run(String uuid) {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("D:\\devEnv\\workspace\\ccod-simulator\\rmDocuments\\brinksemea\\"+uuid+".json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray employeeList = (JSONArray) obj;
          //  System.out.println(employeeList);

            //Iterate over employee array

            System.out.println("\n\n### Machine uuid: "+uuid+" ###");

            employeeList.forEach( emp -> parseEmployeeObject( (JSONObject) emp ));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
    }

    private static void parseEmployeeObject(JSONObject employee)
    {
        //Get employee object within list
        JSONObject employeeObject = (JSONObject) employee.get("USER");

        //Get employee first name
        String userID = (String) employeeObject.get("USER_ID");
        String name = (String) employeeObject.get("NAME");
        String role = (String) employeeObject.get("ROLE_NAME");
        String pin = (String) employeeObject.get("PIN");

        StringBuilder sb = new StringBuilder();
        sb.append("USER_ID: "+userID+",\t");
        sb.append("NAME: "+name+",\t");
        sb.append("ROLE_NAME: "+role+",\t");
        sb.append("PIN: "+pin+",\t");

        System.out.println("\t\t"+sb.toString());

    }
}
