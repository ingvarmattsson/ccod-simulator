package DLStransactionGenerator;

import ccodSession.Environments;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.DefaultStrings;
import data.Utilities;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import rmObjects.*;
import rmReplay.RmReplay;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.LinkedList;
import java.util.List;

import static ccodSession.Environments.DEV;
import static ccodSession.Environments.UAT;


/*USAGE
* Requirements:
*   ccodUsers.xml in xml folder (run xml.MachineUsersGenerator to generate users)
*   workUnits.xml in xml folder
*   dlsTransactionTemplate in rmDocuments folder (RM Transaction)
*
* Running this class will create a document with rm messages including:
*   For each user in ccodUsers.xml will create
*   - 1 SystemStatus Identify message for user
*    - 1 SystemStatus Identify for workunit
*    - 1 Transaction (according to transactionTemplate) read
*git
* The document is saved and passed as parameter to RmReplay which in turn sends messages to CCC
*
* */
public class DLSTransactionGenerator {


    private static String machineUUID ="JM-05"; // null keeps transactionTemplates UUID
    private static Environments env = UAT;
    private static String tenant="testautomation";
    private static boolean replyAfterGeneration = true;

    private String fileTransactionTemplate = System.getProperty("user.dir")+"\\rmDocuments\\dlsTransactionTemplates\\dls-50-EUR-DISPENSE.rm";
    private String fileUsersXML = System.getProperty("user.dir")+"\\xml\\ccodUsersStaticDLS.xml";
    private String fileWorkUnitsXML = System.getProperty("user.dir")+"\\xml\\workUnitsDLS.xml";
    private String transactionsFileName = "100usr50eurDispense.rm";


    private static RmTransaction transactionTemplate;

    public DLSTransactionGenerator(String machineUUID){
        this.machineUUID=machineUUID;

    }
    public DLSTransactionGenerator(){

    }

    public static void main(String[] args)  {
        DLSTransactionGenerator dls = new DLSTransactionGenerator();
        try {
            transactionTemplate = dls.readTransactionTemplate();
            dls.generateDDLMessages(transactionTemplate, dls.readUsersXml(), dls.readWorkUnitsXml(),true);
            if(replyAfterGeneration)
                dls.replyMessages();
        }

        catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void replyMessages() {
        System.out.println("RmReply, env: "+env+", tenant: "+tenant);
        RmReplay r = new RmReplay(DefaultStrings.getBrokerHost(env),
                tenant,
                transactionTemplate.MACHINE_ID,
                DefaultStrings.mqttUserName,
                DefaultStrings.mqttPassWord,
                transactionsFileName
                );

        r.sendMessagesFromFile();
    }

    public List<String> generateDDLMessages(RmTransaction transactionTemplate, List<RmUserCommon> users, List<RmWorkUnit> workunits, boolean returnOnly) throws IOException {

        List<String> rmMessages = new LinkedList<>();
        long transactionSequenceStart = transactionTemplate.TRANSACTION_SEQ;
        long messageSequenceStart = transactionTemplate.MESSAGE_SEQUENCE;

        if (machineUUID != null)
            transactionTemplate.MACHINE_ID = transactionTemplate.UUID = machineUUID;

        System.out.print("Generating transactions... ");
        int wuIndex = 0;
        for (int i = 0; i<users.size(); i++) {
            RmUserCommon user = users.get(i);
            if (wuIndex >= workunits.size())
                wuIndex = 0;

            RmSystemStatus systemStatusIdentify = RmHandler.generateSystemStatusMessage(
                    transactionTemplate.MACHINE_ID,
                    "Identify",
                    "Idle",
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    transactionTemplate.TIME_ZONE_UTC_OFFSET,
                    ++messageSequenceStart, null);

            rmMessages.add(RmHandler.parseRmToString(systemStatusIdentify));

            RmIdentify identifyUser = RmHandler.generateIdentifyMessage(
                    transactionTemplate.MACHINE_ID,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    transactionTemplate.TIME_ZONE_UTC_OFFSET,
                    ++messageSequenceStart,
                    user,
                    null,
                    transactionTemplate.MACHINE_TYPE
            );

            rmMessages.add(RmHandler.parseRmToString(identifyUser));

            RmIdentify identifyWorkUnit = RmHandler.generateIdentifyMessage(
                    transactionTemplate.MACHINE_ID,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    transactionTemplate.TIME_ZONE_UTC_OFFSET,
                    ++messageSequenceStart,
                    user,
                    workunits.get(wuIndex),
                    transactionTemplate.MACHINE_TYPE);

            rmMessages.add(RmHandler.parseRmToString(identifyWorkUnit));

            RmTransaction transaction = clone(transactionTemplate);
            user.WORK_UNIT = workunits.get(wuIndex++);
            user.WORK_UNIT_NAME = user.WORK_UNIT.NAME;
            transaction.TRANSACTION_SEQ = ++transactionSequenceStart;
            transaction.MESSAGE_SEQUENCE = transaction.SEQUENCE = ++messageSequenceStart;
            transaction.USER = user;
            transaction.DATE_TIME = Utilities.getCurrentDateStampIncludedCatchingUp();

            rmMessages.add(RmHandler.parseRmToString(transaction));

        }

        System.out.println("Done");
        generateRmFile(rmMessages, (machineUUID  == null  ? transactionTemplate.MACHINE_ID : machineUUID));
        return rmMessages;
    }

    public void generateRmFile(List<String> rmMessages, String uuid) throws IOException {
        String path = System.getProperty("user.dir")+"\\rmDocuments\\"+transactionsFileName;
        System.out.println("Generating rm file: "+path);
        BufferedWriter writer  = new BufferedWriter(new FileWriter(new File(path)));
        for(String t : rmMessages)
            writer.write(t+"\n");

        writer.flush();
        writer.close();
        System.out.println("Done. \nMachine UUID: "+uuid);

    }

    public RmTransaction clone(RmTransaction transactionTemplate) {
        RmTransaction t = new RmTransaction();
        t.DATE_TIME=transactionTemplate.DATE_TIME;
        t.USER=transactionTemplate.USER;
        t.MESSAGE_SEQUENCE=transactionTemplate.MESSAGE_SEQUENCE;
        t.TRANSACTION_SEQ=transactionTemplate.TRANSACTION_SEQ;
        t.TYPE=transactionTemplate.TYPE;
        t.TOTAL_AMOUNT=transactionTemplate.TOTAL_AMOUNT;
        t.CURRENCY=transactionTemplate.CURRENCY;
        t.DECIMALS=transactionTemplate.DECIMALS;
        t.ACCOUNTING_DATE=transactionTemplate.ACCOUNTING_DATE;
        t.SUB_TYPE=transactionTemplate.SUB_TYPE;
        t.DEVICE_MOVEMENTS=transactionTemplate.DEVICE_MOVEMENTS;
        t.MIX_NAME=transactionTemplate.MIX_NAME;
        t.VALUE_BAGS=transactionTemplate.VALUE_BAGS;
        t.CHECKSUM=transactionTemplate.CHECKSUM;
        t.COMMISION=transactionTemplate.COMMISION;
        t.CUSTOM_DATA=transactionTemplate.CUSTOM_DATA;
        t.DEPOSIT_KIND=transactionTemplate.DEPOSIT_KIND;
        t.EXPECTED_AMOUNTS=transactionTemplate.EXPECTED_AMOUNTS;
        t.IS_CORRECTION=transactionTemplate.IS_CORRECTION;
        t.MIX_DISPLAY_NAME=transactionTemplate.MIX_DISPLAY_NAME;
        t.MIX_EDITED= transactionTemplate.MIX_EDITED;
        t.REFERENCE=transactionTemplate.REFERENCE;
        t.REFERENCE_UUID=transactionTemplate.REFERENCE_UUID;
        t.REQUESTED_AMOUNTS=transactionTemplate.REQUESTED_AMOUNTS;
        t.STRAP_SEALS=transactionTemplate.STRAP_SEALS;
        t.SUB_MIXES=transactionTemplate.SUB_MIXES;
        t.TRANSACTION_COMMISSIONS=transactionTemplate.TRANSACTION_COMMISSIONS;
        t.TRANSACTION_UUID=transactionTemplate.TRANSACTION_UUID;
        t.BOX_DEPOSITS=transactionTemplate.BOX_DEPOSITS;
        t.CARD_BALANCE=transactionTemplate.CARD_BALANCE;
        t.CARD_LOADED=transactionTemplate.CARD_LOADED;
        t.NOTES_TO_BOX=transactionTemplate.NOTES_TO_BOX;
        t.MESSAGE_TYPE=transactionTemplate.MESSAGE_TYPE;
        t.LICENSE_ID=transactionTemplate.LICENSE_ID;
        t.MACHINE_ID=transactionTemplate.MACHINE_ID;
        t.MACHINE_TYPE=transactionTemplate.MACHINE_TYPE;
        t.MD5=transactionTemplate.MD5;
        t.RECEIVER=transactionTemplate.RECEIVER;
        t.SEQUENCE=transactionTemplate.SEQUENCE;
        t.TIME_ZONE_UTC_OFFSET=transactionTemplate.TIME_ZONE_UTC_OFFSET;
        t.UUID=transactionTemplate.UUID;
        t.VERSION=transactionTemplate.VERSION;

        return t;
    }

    public RmTransaction readTransactionTemplate() throws IOException {
        System.out.println("Reading transaction template: "+fileTransactionTemplate);
        return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(new File(fileTransactionTemplate), RmTransaction.class);

    }

    public List<RmWorkUnit> readWorkUnitsXml() throws IOException, SAXException, ParserConfigurationException {
        System.out.println("Reading workunits: "+fileWorkUnitsXML);
        Document document  = Utilities.getXmlRoot(fileWorkUnitsXML);
        document.getDocumentElement().normalize();

        NodeList groups = document.getElementsByTagName("unitgroup");
        List<RmWorkUnit> workUnits = new LinkedList<>();
        for(int i =0; i<groups.getLength();i++){
            Node unitGroup = groups.item(i);

            if(unitGroup.getNodeType()==Node.ELEMENT_NODE){
                Element eElement = (Element) unitGroup;
                String groupName = eElement.getAttribute("name");
                NodeList units = eElement.getElementsByTagName("unit");

                for(int j = 0; j< units.getLength();j++){
                    Node unit =units.item(j);
                    if(unit.getNodeType()==Node.ELEMENT_NODE){

                        Element eElement2 = (Element) unit;
                        RmWorkUnit wu = new RmWorkUnit();
                        wu.PARENT = new RmWorkUnitParent();
                        wu.PARENT.NAME=groupName;
                        wu.NAME= eElement2.getAttribute("name");
                        workUnits.add(wu);
                    }

                }

            }

        }

        return workUnits;
    }

    public List<RmUserCommon> readUsersXml() throws ParserConfigurationException, IOException, SAXException {
        System.out.println("Reading users: "+fileUsersXML);
        Document document  = Utilities.getXmlRoot(fileUsersXML);
        document.getDocumentElement().normalize();

        Element root = document.getDocumentElement();
        NodeList nList = document.getElementsByTagName("user");
        List<RmUserCommon> users = new LinkedList<>();

        for(int i =0; i<nList.getLength();i++){
            Node node = nList.item(i);
            if(node.getNodeType()==Node.ELEMENT_NODE){
                Element eElement = (Element) node;
                RmUserCommon u  = new RmUserCommon();
                u.NAME=eElement.getAttribute("name");
                u.USER_ID=eElement.getAttribute("identifier");
                u.ROLE_NAME=eElement.getAttribute("role");;
                u.ACCOUNTS = new LinkedList<RmAccount>();
                RmAccount acc = new RmAccount();
                acc.ACCOUNT_NUMBER = u.USER_ID;
                acc.TYPE="Account1";
                u.ACCOUNTS.add(acc);

                users.add(u);
            }

        }
        return users;
    }
}
