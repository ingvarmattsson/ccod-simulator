package exceptions;

public class BoxContentEmptyExcception extends Exception {
    public BoxContentEmptyExcception(String message){
        super(message);
    }
}
