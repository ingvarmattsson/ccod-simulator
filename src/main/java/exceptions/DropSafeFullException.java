package exceptions;

public class DropSafeFullException extends Exception {
    public DropSafeFullException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
