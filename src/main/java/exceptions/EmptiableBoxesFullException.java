package exceptions;

public class EmptiableBoxesFullException extends Exception {

    public EmptiableBoxesFullException(String message){
        super( message);
    }
}
