package exceptions;

public class ZTSException extends Exception {
    public ZTSException(String message) {
        super(message);
    }
}
