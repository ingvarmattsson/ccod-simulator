package exceptions;

public class BoxContentFullException extends Exception {

    public BoxContentFullException(String message){
        super(message);
    }
}
