package siteCreator;

import cccObjects.DeviceAssignmentCCC;
import cccObjects.DeviceCCC;
import cccObjects.MetaDataCCC;
import cccObjects.SiteCCC;
import ccodSession.Environments;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import rest.RestHandler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class DeviceCreator {

    private Environments env=Environments.UAT;
    private String tenant = "katalonbasic";
    private String specificationToken="299d8cfd-ad6d-40b0-87f5-4c1430410491";
    private String assignmentAssetId="rcs500";
    private String assignmentassetModuleId="suzohappPC";

    public static void main(String[] args) throws IOException, URISyntaxException {
        new DeviceCreator().run();
    }

    private void run() throws IOException, URISyntaxException {

        RestHandler rest = new RestHandler(env);
        rest.getTokenAuth();
        List<SiteCCC> sites = rest.getSites(tenant);

        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        for (SiteCCC s : sites){
            DeviceCCC deviceCCC = new DeviceCCC();
            deviceCCC.hardwareId = "JM-A-"+s.name.split(" ")[0];
            deviceCCC.metadata = new MetaDataCCC();
            deviceCCC.metadata.centerLatitude="";
            deviceCCC.metadata.centerLongitude="";
            deviceCCC.metadata.ipaddress="";
            deviceCCC.metadata.zoomLevel="4";
            deviceCCC.siteToken=s.token;
            deviceCCC.specificationToken = specificationToken;
            deviceCCC.status="Active";
            String payloadDevice = om.writeValueAsString(deviceCCC);

            DeviceAssignmentCCC assignmentCCC = new DeviceAssignmentCCC();
            assignmentCCC.assetId = assignmentAssetId;
            assignmentCCC.assetModuleId = assignmentassetModuleId;
            assignmentCCC.assignmentType="Associated";
            assignmentCCC.deviceHardwareId=deviceCCC.hardwareId;
            assignmentCCC.metadata = new MetaDataCCC();
            String payloadAssignment = om.writeValueAsString(assignmentCCC);

            System.out.println(payloadDevice);
            System.out.println(payloadAssignment);
            registerDevice(payloadAssignment,payloadDevice);

        }


    }

    private void registerDevice(String payloadAssignment, String payloadDevice) throws IOException, URISyntaxException {
        RestHandler rest = new RestHandler(env);
        rest.getTokenAuth();
        int responseDevice = rest.postDevice(tenant,payloadDevice);
        System.out.println("POST Device - "+responseDevice);
        if(responseDevice==200) {
            rest = new RestHandler(env);
            rest.getTokenAuth();
            int responseAssignment = rest.postDeviceAssignment(tenant,payloadAssignment);
            System.out.println("POST Assignment - "+responseAssignment);
        }

    }
}
