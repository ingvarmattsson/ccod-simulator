package siteCreator;

import cccObjects.SiteCCC;
import ccodSession.Environments;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import rest.RestHandler;

import java.io.IOException;
import java.net.URISyntaxException;

public class SiteCreator {

    private String tenant ="katalonbasic";
    private Environments env =  Environments.UAT;
    private int numberOfSites = 10;
    private int sitePrefix = 1000;


    public static void main(String[] args) throws IOException, URISyntaxException {
        new SiteCreator().run();
    }

    private void run() throws IOException, URISyntaxException {
        String siteSuffix = " - Site";
        int siteIndexStart = sitePrefix;
        while(sitePrefix<(siteIndexStart+numberOfSites)){
            SiteCCC s = new SiteCCC();
            s.name = sitePrefix+siteSuffix;
            s.imageUrl="https://connect.cashcomplete.com/assets/img/favicon/favicon.ico";
            s.description="";

            String payload = new ObjectMapper().writeValueAsString(s);
            RestHandler rest = new RestHandler(env);
            rest.getTokenAuth();
            int responseCode = rest.postSite(tenant,payload);
            System.out.println(s.name+" --> "+responseCode);
            sitePrefix++;
        }

    }
}
