package rmObjects;

import java.util.HashMap;
import java.util.Map;

public class RmDeviceInfo {

    public Map<String,RmFirmware> Firmware;
    public String Hardware;
    public String Protocol;
    public String SerialNumber;
    public String ACCType;
    public String Name;
    public String Version;

    public RmDeviceInfo(){

    }

}
