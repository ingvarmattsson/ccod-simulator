
package rmObjects;

import java.io.Serializable;

public class RmValue implements Serializable {
    private static final long serialVersionUID = 1L;

    public boolean C2_CREDITED;
    public long TOTAL;
    public int COUNT;
    public int DECIMALS;
    public String CURRENCY;
    public int DENOMINATION;
    public int PIECE_VALUE;
    public int EXCHANGE_RATE;
    public int EXCHANGE_RATE_DECIMALS;
    public String REFERENCE;
    public String TYPE;
    public String CLAIMED_VALUE_TYPE;
    public String CUSTOM_TYPE;
    public int C2_COUNT;
    public int C3_COUNT;
    public int C4B_COUNT;
    public boolean CREDIT_CAT_2;
    public boolean CREDIT_CAT_3;



}

