package rmObjects;

import java.util.List;

public class RmUserUpdateReply extends RmServerReply{

    public String TO_VERSION;
    public String FROM_VERSION;
    public List<RmUserChange> USERS;

}
