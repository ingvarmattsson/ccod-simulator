package rmObjects;

import ztsAutomation.ZTSPAckageObjectCommon;

import java.util.LinkedList;
import java.util.List;

public class RmSettings {

    public ZTSPAckageObjectCommon SetConfigurationUpdate;
    public ZTSPAckageObjectCommon SetSoftwareUpgrade;
    public ZTSPAckageObjectCommon SetDayBreak;
    public RmDispenseLimitSettingsOldProtocol SetDispenseLimitSettings;
    public List<RmWorkUnitParent> SetWokrUnits;

    public RmSettings(){

    }
}
