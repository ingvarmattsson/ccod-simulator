package rmObjects;

import java.util.List;

public class RmWorkUnitParent {

    public Boolean IGNORE_ALLOCATION_DAY_BREAK;
    public Boolean IS_MONEY_MOVER;
    public String NAME;
    public String DISPLAY_NAME;
    public List<String> ROLE_IDS;
    public List<RmWorkUnit> CHILDREN;
    public List<String> MONEY_MIXES;

    public RmWorkUnitParent(String parentName){
        NAME=parentName;
    }

    public RmWorkUnitParent(){

    }
}
