package rmObjects;

import java.util.List;

public class RmWorkUnit {

    public Boolean IS_MONEY_MOVER;
    public RmWorkUnitParent PARENT;
    public String NAME;
    public String DISPLAY_NAME;
    public Boolean IGNORE_ALLOCATION_DAY_BREAK;
    public List<String> ROLE_IDS;
    public List<String> MONEY_MIXES;

    public RmWorkUnit(String workUnitName, String parentName){
        PARENT = new RmWorkUnitParent(parentName);
        NAME = workUnitName;

    }

    public RmWorkUnit() {
        PARENT = new RmWorkUnitParent();
    }
}
