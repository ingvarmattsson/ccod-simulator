package rmObjects;

public class RmServerReply extends RmMessageHeader{
    public int REPLY_CODE;
    public String KEEP_ALIVE;
    public String PASSWD_FORMAT;
    public RmFileVersion FILE_VERSIONS;
    public RmSettings SETTINGS;
    public RmDispenseLimitSettingsOldProtocol DISPENSE_LIMIT_SETTINGS;

}
