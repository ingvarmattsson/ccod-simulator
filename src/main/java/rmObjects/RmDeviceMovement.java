package rmObjects;

import java.util.LinkedList;
import java.util.List;

public class RmDeviceMovement {

    public String DEVICE_ID;
    public List<RmBoxMovement> BOX_MOVEMENTS;

    public RmDeviceMovement(){
        BOX_MOVEMENTS = new LinkedList<>();
    }

}
