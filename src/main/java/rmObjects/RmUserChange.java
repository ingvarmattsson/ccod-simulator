package rmObjects;

public class RmUserChange {
    public String KIND; //ADD, UPDATE, DELETE
    public RmUserMuM USER;

    public RmUserChange(){
        USER = new RmUserMuM();
    }
    public RmUserChange(RmUserMuM user, String updateType){
        USER = user;
        KIND = updateType;
    }
}
