package rmObjects;

public class RmAccount {
    public String ACCOUNT_NUMBER;
    public String TYPE;

    public RmAccount( String accountNbr, String type )
    {
        ACCOUNT_NUMBER = accountNbr;
        TYPE = type;
    }

    public RmAccount() {

    }
}
