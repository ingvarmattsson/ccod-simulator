package rmObjects;

public class RmDispenseLimitSettingsOldProtocol {

    public RmDailyDispenseLimitOldProtocol DAILY_DISPENSE_LIMITS;
    public RmTransactionTypeAffectingLimitsOldProtocol TRANSACTION_TYPES_AFFECTING_LIMITS;
    public Boolean CAN_EXCEED_INITIAL;
    public Integer DAY_BREAK_HOUR;
}
