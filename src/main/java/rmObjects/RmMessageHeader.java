package rmObjects;

public class RmMessageHeader {

    public String DATE_TIME;
    public String TIME_ZONE_UTC_OFFSET; // RCS >= 2.15.10
    public String VERSION;
    public String MACHINE_ID;
    public String UUID; // RCS >= 2.15.10
    public String RECEIVER;
    public String LICENSE_ID; // optional
    public Long MESSAGE_SEQUENCE;
    public Long SEQUENCE;
    public String MESSAGE_TYPE;
    public String MD5; // optional
    public String MACHINE_TYPE; // optional
   // public RmFileVersion FILE_VERSIONS;



}
