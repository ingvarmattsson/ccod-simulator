package rmObjects;

import java.util.List;

public class RmSystemStatus extends RmMessageHeader{

    public RmDispenseLimitSettingsOldProtocol DISPENSE_LIMIT_SETTINGS;
    public String STATUS;
    public String OLD_STATUS;
    public RmFileVersion FILE_VERSIONS;

    public List<RmDevice> DEVICES;
    public List<RmDoor> DOORS;

    public RmSettings SETTINGS;

    public RmSystemStatus(){
        FILE_VERSIONS = new RmFileVersion(null);
    }
}
