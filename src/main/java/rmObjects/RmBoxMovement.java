package rmObjects;

import java.util.List;

public class RmBoxMovement {
    public String BOX_ID;
    public String CONTAINER_ID;
    public List<RmValueBag> MOVEMENT;
    public List<RmValueBag> RESULTING_CONTENT;
}
