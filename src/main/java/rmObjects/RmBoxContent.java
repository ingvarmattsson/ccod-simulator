package rmObjects;

import java.io.Serializable;
import java.util.List;

public class RmBoxContent extends RmMessageHeader implements Serializable{
    private static final long serialVersionUID = 1L;

    public String DEVICE;
    public String DEVICE_ID;
    public String DEVICE_TYPE;
    public String VALUE_TYPE;
    public int TRANSACTION_SEQ;
    public List<RmBox> BOX_CONTENTS;
    public boolean HIDE_CONTENTS;

    public RmBoxContent(){
        HIDE_CONTENTS=false;
    }
}
