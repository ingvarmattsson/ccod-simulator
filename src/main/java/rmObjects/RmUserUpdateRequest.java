package rmObjects;

import java.util.List;

public class RmUserUpdateRequest extends RmMessageHeader {
    public String FROM_VERSION;
    public String TO_VERSION;
    public List<RmUserChange> USERS;
}
