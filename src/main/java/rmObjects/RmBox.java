package rmObjects;

import java.io.Serializable;
import java.util.List;

public class RmBox implements Serializable {
    private static final long serialVersionUID = 1L;

    public String BOX_ID;
    public String CONTAINER_ID;
    public List<RmValue> VALUES;
    public String TYPE;
    public String NAME;
    public int DEFAULT_LEVEL;
    public int MAXIMUM_LEVEL;
    public int MINIMUM_LEVEL;
    public int HIGH_LIMIT_LEVEL;
    public int LOW_LIMIT_LEVEL;
    public String FILL_STATUS; //OK MISSING ERROR
    public List<String> CAPABILITIES; //DEPOSIT DISPENSE EMPTY
    public List<String> ERROR_STATUS; // OK MISSING ERROR
}
