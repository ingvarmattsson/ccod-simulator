package rmObjects;

import java.io.Serializable;
import java.util.List;

public class RmUserMuM implements Serializable {
    private static final long serialVersionUID = 1L;

    public String USER_ID; //login
    public String ALIAS; //alias
    public String CARD_IDENTIFIER; //identifier
    public String NAME; //name
    public String PIN; //password
    public String ROLE_NAME; //role
    public Boolean DEACTIVATED; //deactivated
    public Boolean CHANGE_PIN_ON_LOGIN; //changePinOnLogin
    public Boolean NO_PIN; //noPin
    public Boolean NO_PIN_BIO; //noPinBio
    public Boolean NO_PIN_CARD; //noPinCard
    public Boolean NO_PIN_DOOR; //noPinDoor
    public Boolean NO_PIN_USER_ID; //noPinLogin
    public Integer NBR_OF_BAD_PIN; //nbrBadPIN
    public Integer NBR_OF_LOGINS; //nbrOfLogins
    public String VALID_FROM; //validFrom
    public String VALID_UNTIL; //validUntil
    public String WORK_UNIT_NAME; //??
    public List<String> DOORS ; //doors
    public String FINGERPRINTS; //(biometrics != null) ? biometrics.fingerprints : null
    public Boolean LIST; //list

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("USER_ID: "+USER_ID);
        if(ALIAS!=null)
            sb.append(", ALIAS: "+ALIAS);
        if(CARD_IDENTIFIER!=null)
            sb.append(", CARD_IDENTIFIER: "+CARD_IDENTIFIER);
        if(NAME!=null)
            sb.append(", NAME: "+NAME);
        if(PIN!=null)
            sb.append(", PIN: "+PIN);
        if(ROLE_NAME!=null)
            sb.append(", ROLE_NAME: "+ROLE_NAME);
        if(DEACTIVATED!=null)
            sb.append(", DEACTIVATED: "+DEACTIVATED);
        if(CHANGE_PIN_ON_LOGIN!=null)
            sb.append(",CHANGE_PIN_ON_LOGIN : "+CHANGE_PIN_ON_LOGIN);
        if(NO_PIN!=null)
            sb.append(", NO_PIN: "+NO_PIN);
        if(NO_PIN_BIO!=null)
            sb.append(", NO_PIN_BIO: "+NO_PIN_BIO);
        if(NO_PIN_CARD!=null)
            sb.append(", NO_PIN_CARD: "+NO_PIN_CARD);
        if(NO_PIN_DOOR!=null)
            sb.append(", NO_PIN_DOOR: "+NO_PIN_DOOR);
        if(NO_PIN_USER_ID!=null)
            sb.append(", NO_PIN_USER_ID: "+NO_PIN_USER_ID);
        if(NBR_OF_BAD_PIN!=null)
            sb.append(", NBR_OF_BAD_PIN: "+NBR_OF_BAD_PIN);
        if(NBR_OF_LOGINS!=null)
            sb.append(", : NBR_OF_LOGINS"+NBR_OF_LOGINS);
        if(VALID_FROM!=null)
            sb.append(", VALID_FROM: "+VALID_FROM);
        if(VALID_UNTIL!=null)
            sb.append(", VALID_UNTIL: "+VALID_UNTIL);
        if(FINGERPRINTS!=null)
            sb.append(", FINGERPRINTS: "+FINGERPRINTS);
        if(LIST!=null)
            sb.append(", LIST: "+LIST);
        if(DOORS!=null && !DOORS.isEmpty()){
            String doorsVal="";
            for(String door : DOORS)
                doorsVal+=door+",";
            sb.append(", DOORS: "+doorsVal.substring(0,doorsVal.length()-1));
        }

        return sb.toString();

    }
}
