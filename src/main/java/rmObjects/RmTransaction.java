package rmObjects;

import java.util.List;
import java.util.Map;

public class RmTransaction extends RmMessageHeader {

    public double TOTAL_AMOUNT;
    public String ACCOUNTING_DATE;
    public long COMMISION;
    public String CURRENCY;
    public int DECIMALS;
    public String REFERENCE;
    public RmUserCommon USER;
    public String DEPOSIT_KIND;
    public long BOX_DEPOSITS;
    public long CARD_BALANCE;
    public long CARD_LOADED;
    public List<RmValueBag> VALUE_BAGS;
    public String CHECKSUM;
    public int NOTES_TO_BOX;
    public long TRANSACTION_SEQ;
    public String TYPE;
    public String SUB_TYPE;
    public String MIX_NAME;
    public String MIX_DISPLAY_NAME;
    public boolean MIX_EDITED;
    public List<String> STRAP_SEALS;
    public List<RmMix> SUB_MIXES;
    public Map<String, String> CUSTOM_DATA;
    public List<RmDeviceMovement> DEVICE_MOVEMENTS;
    public boolean IS_CORRECTION;
    public List<RmAmount> EXPECTED_AMOUNTS;
    public List<RmAmount> REQUESTED_AMOUNTS;
    public String TRANSACTION_UUID;
    public String REFERENCE_UUID;
    public List<RmTransactionCommission> TRANSACTION_COMMISSIONS;
    public RmLaneAllocation ALLOCATION_ADDED;
    public RmLaneAllocation ALLOCATION_REMOVED;
    public Integer COMMISSION;

    public RmTransaction()
    {
        MESSAGE_TYPE = "Transaction";
        IS_CORRECTION = false;
    }


}
