package rmObjects;

import java.io.Serializable;
import java.util.List;

public class RmError extends RmMessageHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    public String INFORMATION;
    public String KIND;
    public String LOCATION;
    public String SEVERITY;
    public String ERROR_ID;
    public String DEVICE_ID;
    public String DEVICE_ERROR_CODE;
    public Integer JXFS_ERROR_CODE;
    public String ERROR_KEY;
    public List<String> ERROR_ARGS;
    public boolean CLEARED;
}
