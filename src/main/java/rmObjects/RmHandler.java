package rmObjects;

import CCoD.CCoDMachine;
import CCoD.TransactionType;
import ccodSession.Main;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import data.DataGenerator;
import data.Utilities;

import java.util.*;

import static CCoD.TransactionType.*;

public class RmHandler {

    private static String version="3.7.0";
    private static String server="Server";

    public static List<RmDeviceMovement> generateDeviceMovements(List<RmBox> machineBoxesFrom, List<RmBox> machineBoxesTo) {
        return null;
    }

    public static List<RmValueBag> generateValueBags(List<RmBox> machineBoxesFrom, List<RmBox> machineBoxesTo, TransactionType type) {
        return new LinkedList<>();
    }

    public static String parseDeviceMovementToString(List<RmDeviceMovement> deviceMovements) {
        return "";
    }


    public static RmSystemStatus generateSystemStatusMessage(String machineUUID, String status, String oldStatus,
                                                             String dateTime, String timeZoneOffset, Long seq, String fileVersion) {

        RmSystemStatus rm = new RmSystemStatus();
        rm.RECEIVER = server;
        rm.SEQUENCE = seq;
        rm.MESSAGE_SEQUENCE = seq;
        rm.MESSAGE_TYPE="SystemStatus";
        rm.STATUS=status;
        rm.OLD_STATUS=oldStatus;
        rm.MACHINE_ID =rm.UUID =machineUUID;
        rm.DATE_TIME = dateTime;
        rm.TIME_ZONE_UTC_OFFSET = timeZoneOffset;
        rm.VERSION=version;
        rm.FILE_VERSIONS.users = fileVersion;

        return rm;
    }

    public static RmError generateAlertMessage(String machineUUID, boolean cleared, String kind, String location, String severity, String information,
                                               String dateTime, String timeZoneOffset, Long seq){
        RmError rm = new RmError();
        rm.RECEIVER = server;
        rm.SEQUENCE = seq;
        rm.MESSAGE_SEQUENCE = seq;
        rm.MESSAGE_TYPE="Error";
        rm.MACHINE_ID =rm.UUID =machineUUID;
        rm.DATE_TIME = dateTime;
        rm.TIME_ZONE_UTC_OFFSET = timeZoneOffset;
        rm.VERSION=version;
        rm.CLEARED = cleared;
        rm.KIND=kind;
        rm.LOCATION=location;
        rm.SEVERITY = severity;
        rm.INFORMATION=information;


        return rm;
    }


    public static RmSystemStatus generateFullStateMessage(RmSystemStatus systemStatus, CCoDMachine machine) {

        List<RmDevice> devices = new LinkedList<>();

        //Soundcard
        RmDevice deviceSoundCard = new RmDevice();
        deviceSoundCard.ID  ="SOUND."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        RmDeviceInfo info = new RmDeviceInfo();
        info.Hardware="ccod-simulator."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info.ACCType="NONE";
        info.Version="V_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info.SerialNumber="SN_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info.Protocol="PROTOCOL_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info.Name="NAME_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        deviceSoundCard.DEVICE_INFO = info;//deviceInfos;

        //Keyboard
        RmDevice deviceKeyBoard = new RmDevice();
        deviceKeyBoard.ID="KEYBOARD."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info = new RmDeviceInfo();
        info.Name="KB_INFO_NAME."+machine.getMachineUUID();
        info.ACCType="NONE";
        info.Protocol="KB_PROTOCOL"+machine.getMachineUUID();
        info.SerialNumber="KB_SN."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info.Version="KB_VERSION."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;


        deviceKeyBoard.SENSOR_INFO = info;

        //RECIEPTPRINTER
        RmDevice deviceRP = new RmDevice();
        deviceRP.ID="RECIEPTPRINTER."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;

        RmFirmware fw = new RmFirmware();
        fw.Sum="RB_SUM_1_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        fw.Type="RB_TYPE_1_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        fw.Version="RB_VERSION_1_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;

        RmFirmware fw2 = new RmFirmware();
        fw2.Sum="RB_SUM_2_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        fw2.Type="RB_TYPE_2_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        fw2.Version="RB_VERSION_2_"+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;

        RmDeviceInfo info2 = new RmDeviceInfo();
        info2.Name="RB_INFO_NAME_2."+machine.getMachineUUID();
        info2.Protocol="RB_PROTOCOL_2"+machine.getMachineUUID();
        info2.SerialNumber="RB_SN_2."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;
        info2.Version="RB_VERSION_2."+machine.getMachineUUID()+"."+systemStatus.MESSAGE_SEQUENCE;

        info2.Firmware = new HashMap<>();
        info2.Firmware.put("RB_FW_1",fw);
        info2.Firmware.put("RB_FW_2",fw2);

        RmDeviceInfo info3 = new RmDeviceInfo();

        info3.Firmware = new HashMap<>();
        info3.Firmware.put("RB_FW_0",fw);

        deviceRP.SENSOR_INFO=info3;//deviceInfos;
        deviceRP.DEVICE_INFO=info2;

        //add devices
        devices.add(deviceSoundCard);
        devices.add(deviceKeyBoard);
        devices.add(deviceRP);

        List<RmDoor> doors = new LinkedList<>();
        doors.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        doors.add(new RmDoor("CLOSED","NOTE_ACCEPTOR_BOTTOM"));
        doors.add(new RmDoor("CLOSED","NOTE_ACCEPTOR_TOP"));
        doors.add(new RmDoor("CLOSED","NOTE_DISPENSER"));

        systemStatus.DOORS = doors;
        systemStatus.DEVICES = devices;
        return systemStatus;

    }


    public static RmUserCommon generateUser(String machineUserId, String machineUserName, String machineUserRole, String workUnitName, String workUnitGroup, RmUserCommon originatingUser, List<String> accounts) {


        RmUserCommon user = new RmUserCommon();
        RmWorkUnit unit = new RmWorkUnit(workUnitName,workUnitGroup);
        user.WORK_UNIT = unit;
        user.NAME = machineUserName;
        user.USER_ID = machineUserId;
        user.ROLE_NAME= machineUserRole;
        user.WORK_UNIT_NAME = workUnitName;
        user.ACCOUNTS = new LinkedList<>();

        if(accounts!=null){
            for(String acc : accounts){
                String[] nbrType = acc.split("-");
                user.ACCOUNTS.add(new RmAccount(nbrType[0],nbrType[1]));

            }
        }

        if(originatingUser!=null){
            int idFullBoth = new Random().nextInt(3);
            switch (idFullBoth){
                //only id
                case 0:
                    user.ORIGINATING_USER=originatingUser.USER_ID;
                    break;
                //Only full
                case 1:
                    user.ORIGINATING_USER_FULL=originatingUser;
                    break;
                case 2:
                    user.ORIGINATING_USER=originatingUser.USER_ID;
                    user.ORIGINATING_USER_FULL=originatingUser;
                    break;

            }
        }

        return user;
    }

    public static String parseRmToString(RmMessageHeader rmMessage) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
       // objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        return objectMapper.writeValueAsString(rmMessage);

    }

    public static String parseValueBagToString(List<RmValueBag> valueBag) throws JsonProcessingException {

        if(valueBag.isEmpty())
            return "";

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(RmValueBag v : valueBag)
            sb.append(objectMapper.writeValueAsString(v)+",");
        String vb = sb.toString();
        vb = vb.substring(0,vb.length()-1);
        vb+="]";

        return vb;

    }

    public static String parseRmBoxesToString(List<RmBox> boxes) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(RmBox box : boxes)
            sb.append(objectMapper.writeValueAsString(box)+",");
        String vb = sb.toString();
        vb = vb.substring(0,vb.length()-1);
        vb+="]";

        return vb;
    }


    public static String parseRmBoxContentToString(List<RmBoxContent> machineContent) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(RmBoxContent box : machineContent)
            sb.append(objectMapper.writeValueAsString(box)+",");
        String vb = sb.toString();
        vb = vb.substring(0,vb.length()-1);
        vb+="]";

        return vb;
    }


    public static RmBoxContent generateBoxContent(String dateNow, String timeZoneOffset, String machineUUID, int transactionSequence, long messageSequence, String device, String deviceID, String deviceType, String valueType, List<RmBox> boxContent) {
        RmBoxContent rm = new RmBoxContent();
        //header
        rm.DATE_TIME=dateNow;
        rm.TIME_ZONE_UTC_OFFSET=timeZoneOffset;
        rm.VERSION=version;
        rm.MACHINE_ID =rm.UUID =machineUUID;
        rm.RECEIVER= server;
        rm.MESSAGE_SEQUENCE =  messageSequence;
        rm.SEQUENCE =0L;
        rm.MESSAGE_TYPE = "BoxContent";


        //boxcontent
        rm.DEVICE=device;
        rm.DEVICE_ID=deviceID;
        rm.DEVICE_TYPE=deviceType;
        rm.VALUE_TYPE=valueType;
        if(transactionSequence>=0)
            rm.TRANSACTION_SEQ= transactionSequence;
        rm.BOX_CONTENTS = boxContent;


        return rm;

    }

    public static RmTransaction generateTransaction(String dateNow, String machineId, RmUserCommon rmUser,
                                                     long transactionSequence, long messageSequence, long sequence,double totalAmount,
                                                     String currency, TransactionType type, String subType,
                                                    String timeZoneOffset, List<RmValueBag> valueBags, List<RmDeviceMovement> deviceMovements) {


        if(type==MANUAL_DEPOSIT){
            type = DEPOSIT;

        }

        RmTransaction rm = new RmTransaction();
        rm.USER = rmUser;
        if(valueBags==null)
            rm.VALUE_BAGS = new LinkedList<>();
        else
            rm.VALUE_BAGS = valueBags;

        if(deviceMovements==null)
            rm.DEVICE_MOVEMENTS = new LinkedList<>();
        else
            rm.DEVICE_MOVEMENTS = deviceMovements;

        rm.MACHINE_ID = rm.UUID = machineId;
        rm.RECEIVER = server;
        rm.TYPE = type.name();
        if(!subType.isEmpty())
            rm.SUB_TYPE=subType;
        rm.TIME_ZONE_UTC_OFFSET = timeZoneOffset;
        rm.DATE_TIME = dateNow;
        rm.VERSION = version;
        rm.TRANSACTION_SEQ = transactionSequence;
        rm.MESSAGE_SEQUENCE = messageSequence;
        rm.SEQUENCE = sequence;
        rm.TRANSACTION_UUID= DataGenerator.getTransactionUUID();

        rm.TOTAL_AMOUNT = totalAmount;
        rm.ACCOUNTING_DATE = rm.DATE_TIME;
        rm.COMMISION = 0;
        rm.CURRENCY = currency;
        rm.DECIMALS = 2;
        rm.REFERENCE = "Reference";

        rm.MESSAGE_TYPE = "Transaction";

        if(Main.getAccountingDateOffset()!=null) {
            rm.ACCOUNTING_DATE= Utilities.changeDateTime(rm.DATE_TIME,Main.getAccountingDateOffset());
        }

        //optional fields
        Random r = new Random();
        if(Main.excludeAccountingDate == true || r.nextInt(100)>75)
            rm.ACCOUNTING_DATE=null;

        if(Main.includeOptionalFieldsRm == true || r.nextInt(100)>75)
            rm.DEPOSIT_KIND = DataGenerator.getDepositKind(true);

        if(Main.includeOptionalFieldsRm == true || r.nextInt(100)>75)
            rm.CHECKSUM = rm.TYPE+":"+rm.TRANSACTION_SEQ;

        if(Main.includeOptionalFieldsRm == true || r.nextInt(100)>75){
            rm.MIX_EDITED = r.nextBoolean();
            rm.MIX_NAME = DataGenerator.getMixName();
            rm.MIX_DISPLAY_NAME = rm.MIX_NAME+":"+rm.USER.WORK_UNIT_NAME;

            if(r.nextBoolean())
                rm.SUB_MIXES = DataGenerator.getSubMixes();
        }

        if((Main.includeOptionalFieldsRm == true && rm.TYPE.equals(EXCHANGE.name())) || (rm.TYPE.equals(EXCHANGE.name()) && (r.nextBoolean()==true)))
            rm.STRAP_SEALS = DataGenerator.getStrapSeals(rm.TRANSACTION_SEQ,rm.MACHINE_ID);

        if(Main.includeOptionalFieldsRm == true || r.nextInt(100)>75)
            rm.CUSTOM_DATA = DataGenerator.getCustomData(rm.TRANSACTION_SEQ,rm.MESSAGE_SEQUENCE);

        if((Main.includeOptionalFieldsRm == true && rm.TYPE.equals(DEPOSIT.name()) )|| (rm.TYPE.equals(DEPOSIT.name()) && (r.nextInt(100)>75))){
            rm.SUB_TYPE="EXPECTED_AMOUNT";
            rm.EXPECTED_AMOUNTS = DataGenerator.getExpectedAmounts(rm.CURRENCY,rm.DECIMALS,rm.TOTAL_AMOUNT);
        }

        if((Main.includeOptionalFieldsRm == true && rm.TYPE.equals(DISPENSE.name())) || (r.nextInt(100)>75) && rm.TYPE.equals(DISPENSE.name())){
            rm.SUB_TYPE="DISPENSE_SURPLUS";
            rm.REQUESTED_AMOUNTS = DataGenerator.getExpectedAmounts(rm.CURRENCY,rm.DECIMALS,rm.TOTAL_AMOUNT);
        }

        if(Main.includeOptionalFieldsRm == true || r.nextInt(100)>75)
            rm.REFERENCE_UUID = DataGenerator.getTransactionUUID();

        if((Main.includeOptionalFieldsRm == true && rm.TYPE==DEPOSIT.name()) || (rm.TYPE==DEPOSIT.name() && r.nextInt(100)>75))
            rm.TRANSACTION_COMMISSIONS = DataGenerator.getTransactionCommissions(rm.TOTAL_AMOUNT, rm.DECIMALS,rm.CURRENCY, r.nextBoolean());




        return rm;
    }

    private static RmValueBag[] parseStringValueBagToRm(String transactionValueBag) {
        Gson gson = new Gson();
        return gson.fromJson(transactionValueBag,RmValueBag[].class);
    }


    private static List<RmBox> parseStringBoxContentToRm(String boxContent) {
        return Arrays.asList(new Gson().fromJson(boxContent, RmBox[].class));
    }

    public static RmTransaction parseStringToRmTransaction(String rmTransaction) {
        return new Gson().fromJson(rmTransaction,RmTransaction.class);
    }

    public static RmIdentify generateIdentifyMessage(String machineId, String dateTime, String timeZoneUtcOffset, long sequence, RmUserCommon user, RmWorkUnit workUnit, String machineType) {
        RmIdentify rm = new RmIdentify();
        rm.ALARM = true;
        rm.PASSED ="1";
        rm.USER = user;
        if(workUnit!=null){
            rm.USER.WORK_UNIT_NAME = workUnit.NAME;
            rm.USER.WORK_UNIT = workUnit;
        }
        rm.MESSAGE_TYPE="Identify";
        rm.DATE_TIME = dateTime;
        rm.TIME_ZONE_UTC_OFFSET = timeZoneUtcOffset;
        rm.LICENSE_ID="";
        rm.MACHINE_ID = rm.UUID=machineId;
        rm.MESSAGE_SEQUENCE = rm.SEQUENCE = sequence;
        rm.MACHINE_TYPE=machineType;
        rm.VERSION = version;
        rm.RECEIVER = server;

        return rm;
    }

    public static RmUserUpdateRequest generateUserUpdateRequest(String machineUUID, String currentDateStamp, String timeZoneOffset,String fromVersion, String toVersion, long sequence, List<RmUserChange> usersToUpdate) {
        RmUserUpdateRequest rm = new RmUserUpdateRequest();
        rm.MESSAGE_TYPE="UserUpdateRequest";
        rm.MACHINE_ID= rm.UUID =machineUUID;
        rm.DATE_TIME=currentDateStamp;
        rm.MESSAGE_SEQUENCE = rm.SEQUENCE = sequence;
        rm.RECEIVER=server;
        rm.VERSION=version;
        rm.TIME_ZONE_UTC_OFFSET=timeZoneOffset;

        rm.FROM_VERSION=fromVersion;
        rm.TO_VERSION=toVersion;
        rm.USERS = usersToUpdate;
        return rm;
    }

    public static RmServerReply parseStringToRmServerReply(String message) {
        return new Gson().fromJson(message,RmServerReply.class);

    }

    public static RmUserUpdateReply parseStringToRmUserMumReply(String message){
        return new Gson().fromJson(message, RmUserUpdateReply.class);
    }

    public static RmMessageHeader parseToRmMessageHeader(String dlsMessage) {
        return new Gson().fromJson(dlsMessage, RmMessageHeader.class);
    }

    public static RmSystemStatus parseStringToRmSystemStatus(String message) {
         return new Gson().fromJson(message, RmSystemStatus.class);
    }
}
