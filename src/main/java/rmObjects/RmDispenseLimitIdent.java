package rmObjects;

import java.util.List;

public class RmDispenseLimitIdent {

    public String IDENT;
    public Integer DAY_BREAK_HOUR;
    public List<RmDispenseLimitIdentAmount> AMOUNT_LIST;
}
