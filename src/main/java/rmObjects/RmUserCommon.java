package rmObjects;

import java.util.List;

public class RmUserCommon {

    public List<RmAccount> ACCOUNTS;
    public String CARD_TRACK;
    public String CARD_IDENTIFIER;
    public String USER_ID;
    public String ORIGINATING_USER;
    public RmUserCommon ORIGINATING_USER_FULL;
    public String NAME;
    public List<Integer> PIN;
    public String WORK_UNIT_NAME;
    public RmWorkUnit WORK_UNIT;
    public String ROLE_NAME;
    public String DEACTIVATE_REASON;

    public RmUserCommon(){
        WORK_UNIT = new RmWorkUnit();
    }
}


