package rmObjects;

import java.util.LinkedList;
import java.util.List;

public class RmValueBag {
    public double TOTAL;
    public List<RmValue> VALUES;
    public String TYPE;
    public int REJECTS;

    public RmValueBag(){
        VALUES = new LinkedList<>();
    }
}
