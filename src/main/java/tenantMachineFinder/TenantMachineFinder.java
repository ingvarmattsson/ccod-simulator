package tenantMachineFinder;

import cccObjects.DeviceCCC;
import ccodSession.Environments;
import rest.RestHandler;
import tenantAuthorization.SiteWhereTenant;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class TenantMachineFinder {

    private String targetMachineUUID ="911a2c77-4d9a-4b38-b401-dea16ed2ec68";

    private Environments env=Environments.DE;

    public static void main(String[] args) {
        new TenantMachineFinder().run();

    }

    private void run() {
        List<SiteWhereTenant> tenants = null;
        List<String> tenantsWithMachine = new LinkedList<>();
        try {
            tenants = getTenants(env);
            for(SiteWhereTenant t : tenants){
                List<DeviceCCC> machines = getMachines(env,t.authenticationToken);
                if(machines == null)
                    continue;

                int count =0 ;
                for(DeviceCCC m:machines){
                    if(m.hardwareId==null)
                        continue;

                    count++;
                    System.out.println("\t"+count+"\t"+m.hardwareId);
                    if(m.hardwareId.equalsIgnoreCase(targetMachineUUID)){
                        System.out.println("\t\t\tTarget hardware found");
                        tenantsWithMachine.add(t.id);
                        break;
                    }
                }


            }
            System.out.println(printResults(tenantsWithMachine));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    private String printResults(List<String> tenantsWithMachine) {
        if(tenantsWithMachine.isEmpty())
            return targetMachineUUID +" not found on node "+env.name();

        StringBuilder sb = new StringBuilder();
        sb.append(targetMachineUUID +" found on tenant: "+"\n");
        for(String t: tenantsWithMachine)
            sb.append("\t"+t+"\n");

        return sb.toString();

    }

    private List<DeviceCCC> getMachines(Environments env, String tenant) throws IOException, URISyntaxException {
        RestHandler rest = new RestHandler(env);
        rest.getToken();
        return rest.getMachines(tenant);
    }

    public List<SiteWhereTenant> getTenants(Environments env) throws IOException, URISyntaxException {
        RestHandler rest = new RestHandler(env);
        rest.getToken();
        return rest.getTenants("");
    }
}
