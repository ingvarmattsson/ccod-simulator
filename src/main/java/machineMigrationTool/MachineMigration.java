package machineMigrationTool;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MachineMigration {

    private static List<String> availableMachines;
    private static List<String> machinesToMigrate;
    private static List<String> eligibleMachines;
    private static String pathToFiles =System.getProperty("user.dir")+"/machineMigrationTool/";
    private static int cutoff = 300;

    public static void main(String[] args) {

        try {
            readAvailableMachines();
            readMachinesToMigrate();
            getEligibleMachines();
            printMachineGroupPayLoad();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printMachineGroupPayLoad() {
        int amount = (eligibleMachines.size() < 300 ? eligibleMachines.size() : cutoff);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i<amount;i++)
            sb.append("\""+eligibleMachines.get(i)+"\", ");

        System.out.println(sb.toString().substring(0,sb.toString().length()-2));
    }

    private static void getEligibleMachines() {
        System.out.println("#### GETTING ELIGIBLE MACHINES");
        eligibleMachines = new LinkedList<>();

        for (String available : availableMachines){
            for (String migrate : machinesToMigrate){
                if(available.equalsIgnoreCase(migrate)){
                    System.out.println(available);
                    eligibleMachines.add(available);
                }
            }
        }

    }

    private static void readMachinesToMigrate() throws IOException {
        System.out.println("#### READING MACHINES TO MIGRATE ####");
        BufferedReader br = new BufferedReader(new FileReader(pathToFiles+"BrinksMachinesToMigrate.csv"));
        machinesToMigrate = new LinkedList<>();
        try {
            String line = br.readLine();
            int i =1;

            while (line != null) {
                String[] uuidSubstring = line.split(",");
                System.out.println(i+" "+uuidSubstring[1]);
                machinesToMigrate.add(uuidSubstring[1].trim());
                line = br.readLine();
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }
    }

    private static void readAvailableMachines() throws IOException {
        System.out.println("#### READING AVAILABLE MACHINES ####");

        BufferedReader br = new BufferedReader(new FileReader(pathToFiles+"brinksAvailableMachines.txt"));
        availableMachines = new LinkedList<>();
        try {
            String line = br.readLine();
            int i =1;

            while (line != null) {
                String[] uuidPrefix = line.split(":");
                System.out.println(i+" "+uuidPrefix[0]);
                String uuid = uuidPrefix[0].substring(1,uuidPrefix[0].length()-1).trim();
                availableMachines.add(uuid);
                line = br.readLine();
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }

    }
}
