package scripts;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TransactionLookup {

    static String result = "{\"numResults\":15,\"results\":[{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:31:04.031Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:31:04.031Z\",\"receivedDateTime\":\"2019-04-26T15:31:06.545Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:30:41.030Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:30:41.030Z\",\"receivedDateTime\":\"2019-04-26T15:30:43.343Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:30:09.030Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:30:09.030Z\",\"receivedDateTime\":\"2019-04-26T15:30:11.458Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:29:22.029Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:29:22.029Z\",\"receivedDateTime\":\"2019-04-26T15:29:24.516Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:28:02.028Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:28:02.028Z\",\"receivedDateTime\":\"2019-04-26T15:28:05.094Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:21:53.021Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:21:53.021Z\",\"receivedDateTime\":\"2019-04-26T15:21:55.574Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:21:18.021Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:21:18.021Z\",\"receivedDateTime\":\"2019-04-26T15:21:20.939Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:13:57.013Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:13:57.013Z\",\"receivedDateTime\":\"2019-04-26T15:13:59.504Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:07:01.007Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:07:01.007Z\",\"receivedDateTime\":\"2019-04-26T15:07:03.412Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:05:17.005Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:05:17.005Z\",\"receivedDateTime\":\"2019-04-26T15:05:19.861Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:03:50.003Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:03:50.003Z\",\"receivedDateTime\":\"2019-04-26T15:03:52.459Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:02:38.002Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:02:38.002Z\",\"receivedDateTime\":\"2019-04-26T15:02:40.462Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:02:04.002Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:02:04.002Z\",\"receivedDateTime\":\"2019-04-26T15:02:06.678Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:01:30.001Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:01:30.001Z\",\"receivedDateTime\":\"2019-04-26T15:01:32.053Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"},{\"totalAmount\":19649,\"totalAmounts\":[{\"amount\":19649,\"decimals\":2,\"currency\":\"EUR\",\"type\":\"TOTAL\"}],\"accountingDate\":\"2019-04-26T16:00:47Z\",\"commission\":0,\"currency\":\"EUR\",\"decimals\":2,\"reference\":\"Reference\",\"machineUserId\":\"0001\",\"machineUser\":{\"userId\":\"0001\",\"name\":\"Cashier TA-01\",\"workUnitName\":\"WU-JM-01\",\"roleName\":\"administrator\",\"workUnit\":{\"name\":\"WU-JM-01\",\"parent\":{\"name\":\"Madrid\"}}},\"valueBags\":[{\"totalAmount\":0.0,\"values\":[{\"total\":19649,\"count\":0,\"decimals\":2,\"currency\":\"EUR\",\"denomination\":0,\"pieceValue\":0,\"exchangeRate\":0,\"exchangeRateDecimals\":0,\"c2Count\":0,\"c3Count\":0,\"c4BCount\":0,\"creditCat2\":false,\"creditCat3\":false}],\"rejects\":0}],\"transactionSequence\":1,\"type\":\"DEPOSIT\",\"mixEdited\":false,\"siteToken\":\"3991b2e3-a841-4841-9078-11ec0531174f\",\"isCorrection\":false,\"dateTime\":\"2019-04-26T16:00:47Z\",\"receivedDateTime\":\"2019-04-26T15:00:49.395Z\",\"timeZoneUtcOffset\":\"+01:00\",\"version\":\"3.1.2\",\"machineId\":\"JM-01\",\"uuid\":\"JM-01\",\"receiver\":\"Server\",\"messageSequence\":1,\"messageType\":\"Transaction\"}]}";



    static class Transaction{
        String totalAmount;
        String accountingDate;
        String uuid;
        String machineUserId;

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getAccountingDate() {
            return accountingDate;
        }

        public void setAccountingDate(String accountingDate) {
            this.accountingDate = accountingDate;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getMachineUserId() {
            return machineUserId;
        }

        public void setMachineUserId(String machineUserId) {
            this.machineUserId = machineUserId;
        }

        public Transaction(){

        }
    }
    public static void main(String[] args) throws IOException, ParseException {

        String machineUserId="0003";
        String totalAmount="-12392";
        String accountingDate="2019-04-26T12:18:20Z";

        new TransactionLookup().searchForTransaction(machineUserId,totalAmount,accountingDate,result);
    }

    public String searchForTransaction(String machineUserId, String totalAmount, String accountingDate, String jsonResponse) throws IOException, ParseException {

        //put results in array
        ObjectMapper om = new ObjectMapper();
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        int indexToStartOfArray = jsonResponse.indexOf('[');
        String results = jsonResponse.substring(indexToStartOfArray,jsonResponse.length()-1);
        Transaction[] transactions = om.readValue(results, Transaction[].class);

        boolean transationFound = false;
        long startTime = System.currentTimeMillis(); //move to preprocessor
        long endTime = 0;


       // String machineUserId = "0003"; //${}
        //String totalAmount = "6989"; //${}
        int amount = Integer.parseInt(totalAmount);
      //  String accountingDate = "2019-04-26T12:18:31.018Z"; //${}
        String[] accDate = accountingDate.split("T");

        if(accountingDate.contains(".")){
            accountingDate = accDate[0]+" "+accDate[1].substring(0,accDate[1].indexOf("."));

        }else{
            accountingDate = accDate[0]+" "+accDate[1].substring(0,accDate[1].length()-1);
        }

        Date thisDate =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(accountingDate);
        Date thatDate;

        String foundTransaction = null;
        for(int i =0;i<transactions.length;i++){

            Transaction t = transactions[i];
            String tAccountingDate = t.accountingDate;
            String[] tAcc = tAccountingDate.split("T");

            if(tAccountingDate.contains(".")){
                tAccountingDate = tAcc[0]+" "+tAcc[1].substring(0,tAcc[1].indexOf("."));
            }else{
                tAccountingDate = tAcc[0]+" "+tAcc[1].substring(0,tAcc[1].length()-1);
            }

            int tAmount = Integer.parseInt(t.totalAmount);

            System.out.println(tAccountingDate+"  "+t.machineUserId+"  "+t.totalAmount+" i="+i);
            thatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tAccountingDate);

            if (thisDate.equals(thatDate) && machineUserId.equalsIgnoreCase(t.machineUserId) && amount==tAmount){
                transationFound = true;
                foundTransaction=t.accountingDate+t.machineUserId+totalAmount;
                break;
            }
        }

        endTime = System.currentTimeMillis();
        long timeSearching = endTime-startTime;
        System.out.println(timeSearching);
        return foundTransaction;
        //set foundTransaction for Jmeter loop controller
    }
}
