package allTenants;

import ccodSession.Environments;
import data.DefaultStrings;
import rest.RestHandler;
import tenantAuthorization.SiteWhereTenant;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static ccodSession.Environments.*;

public class AllTenantsPrinter {

    private Environments[] environments = { US_EAST, US_WEST, DE, ASIA, CH, LAT,BRINKS_US};
    private RestHandler restHandler;

    public static void main(String[] args) throws IOException, URISyntaxException {
        new AllTenantsPrinter().run();
    }

    private void run() throws IOException, URISyntaxException {
        for (int i = 0; i < environments.length; i++){
            restHandler = new RestHandler(environments[i]);
            restHandler.getToken();
            List<SiteWhereTenant> tenants =  restHandler.getTenants("");
            printTenants(environments[i], tenants);
        }

    }

    private void printTenants(Environments environment, List<SiteWhereTenant> tenants) {
        System.out.println("\n### "+environment.name()+" ("+ DefaultStrings.getBaseURL(environment)+") ###");
        String format = "\t%1$-50s| %2$-10s\n";

        for(SiteWhereTenant t : tenants){
            String name = t.name;
            String edition = t.metadata.get("edition");
            if (edition==null)
                edition="basic";

            System.out.format(format, name,edition);

        }

    }
}
