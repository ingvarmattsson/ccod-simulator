package siteFinder;

import cccObjects.SiteCCC;
import ccodSession.Environments;
import rest.RestHandler;
import tenantAuthorization.SiteWhereTenant;
import tenantMachineFinder.TenantMachineFinder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import static ccodSession.Environments.*;

public class SiteFinder {

    public static Environments env = US_EAST;
    private static String siteNameToFind ="Atlantis";


    public static void main(String[] args) {

        try {
            List<SiteWhereTenant> tenants = new TenantMachineFinder().getTenants(env);
            List<String> tenantsWithSite = findTenantsWithSite(tenants);
            printTenantsWithSite(tenantsWithSite);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private static void printTenantsWithSite(List<String> tenantsWithSite) {
        System.out.println("\n Tenants with site "+siteNameToFind+":");
        for (String t : tenantsWithSite)
            System.out.println("\t"+t);
    }

    private static List<String> findTenantsWithSite(List<SiteWhereTenant> tenants) throws IOException, URISyntaxException {
        List<SiteCCC> sites = new LinkedList<>();
        List<String> tenantsWithSite = new LinkedList<>();
        for(SiteWhereTenant t : tenants){
            if(t == null)
                continue;
            RestHandler rest = new RestHandler(env);
            rest.getTokenAuth();
            sites= rest.getSites(t.authenticationToken);
            if (sites==null)
                continue;
            if (t.authenticationToken.equalsIgnoreCase("idaho"))
                System.out.println();
            for (SiteCCC s : sites){
                if( s == null)
                    continue;
                System.out.println("\t"+s.name);
                if(s.name.equalsIgnoreCase(siteNameToFind))
                    tenantsWithSite.add(t.name);
            }
            System.out.println("\n");
        }
        return tenantsWithSite;
    }
}
