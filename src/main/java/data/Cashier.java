package data;

import java.util.List;

public  class Cashier{

    public String name, id, role;
    public List<String> accounts;

    public Cashier(String name, String id, String role, List<String> accounts){
        this.name = name;
        this.id=id;
        this.role=role;
        this.accounts = accounts;

    }
}