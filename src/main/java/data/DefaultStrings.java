package data;

import CCoD.Currency;
import ccodSession.Environments;
import rest.RestHandler;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import static CCoD.Currency.EUR;
import static CCoD.Currency.USD;
import static ccodSession.Environments.UAT;
import static ccodSession.Environments.US_EAST;

public class DefaultStrings {

    public static final String apiUsername ="suzohappwebapp";// "suzohappwebapp";
    public static final String apiPassword ="yqosY8wGjUZZ8j8gCfs4";// "yqosY8wGjUZZ8j8gCfs4";
    public static final String cccUserName="esbuser";//""esbuser";
    public static final String cccUserPassword="r-28UzJE2#e25ach";//"r-28UzJE2#e25ach";
    public static final String apiUserConsumerKey="Letfo7zmkwWmKDFgnfYqkflSYmka";//"Letfo7zmkwWmKDFgnfYqkflSYmka";
    public static final String apiUserConsumerSecret="S3_eysJbe5_Atm35EfP7KF4iQ5sa";//"S3_eysJbe5_Atm35EfP7KF4iQ5sa";

    public static final String mqttUserName ="suzohapp";
    public static final String mqttPassWord ="suzohapp";
    public static final String csvDelimeter=";";
    public static final String resultTenantIndexer="<$>";
    public static final String pathSeparator = File.separator;
    public static final String dateFormatPatternMachine ="yyyyMMdd HH:mm:ss.SSS";
    public static final String dateFormatPatternAPI ="yyyy-MM-dd HH:mm:ss.SSS";
    public static final String dateFormatPatternCbDataGenerator ="yyyy-MM-dd'T'HH:mm:s";
    public static String tokenScope ="alerts2_read_access_subscriber alerts2_write_access_subscriber alerts_read_access_subscriber alerts_write_access_subscriber assignments_read_access_subscriber assignments_write_access_subscriber ccodlicense_read_access_subscriber ccodlicense_write_access_subscriber correction_read_access_subscriber correction_write_access_subscriber device_1575379048777 devices_read_access_subscriber devices_write_access_subscriber forecasting_read_access_subscriber forecasting_write_access_subscriber geocoding_read_access_subscriber machines_read_access_subscriber machines_write_access_subscriber machineusers_read_access_subscriber machineusers_write_access_subscriber onboarding_read_access_subscriber onboarding_write_access_subscriber pointofsale_read_access_subscriber pointofsale_write_access_subscriber report_read_access_subscriber report_write_access_subscriber sitegroups_read_access_subscriber sitegroups_write_access_subscriber sites_read_access_subscriber sites_write_access_subscriber specifications_read_access_subscriber tenants_read_access_subscriber tenants_write_access_subscriber transactions_read_access_subscriber users_read_access_subscriber users_write_access_subscriber";
    public static String ccodVersionBoot ="3.14.0-ccodSimulator";
    public static String usernameAuth ="esbuser";
    public static String hashedPasswordAuth="a779ecb6ccae0f5620fba3a420fe15f5d6c2209b";
    public static boolean useOldApimToGetToken = false;

    public static String tenantRestAssured ="testautomation";
    public static String xAuth="Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo";
    public static Environments environmentForRestAssured = UAT;
    public static String yahooUserName ="suzohappta1@yahoo.com";
    public static String yahooPassword ="pqnbnorvhnhlkwsj"; // "131@Z!kal" yahooapppassword:pqnbnorvhnhlkwsj


    public static List<String> getTenants(Environments environment) {

        List tenants = new LinkedList();

        switch (environment){
            case ASIA:
            case US_EAST:
            case DE:
            case CH:
                String te="salesdemotransport";
                if(environment== US_EAST) {
                    te ="salesdemotransportation";
                }
                tenants.add("salesdemoretail");
                tenants.add("salesdemobasic");
                tenants.add("salesdemobusiness");
                tenants.add(te);
                break;
            case UAT:
                tenants.add("testautomation");
                tenants.add("alabama");
                tenants.add("alaska");
                tenants.add("arizona");
                tenants.add("arkansas");
                /*tenants.add("colorado");
                tenants.add("california");
                tenants.add("connecticut");
                tenants.add("delaware");*/
                break;
            case DEV:
                tenants.add("suzohappretail");
                tenants.add("suzohappbasic");
                tenants.add("suzohappbusiness");
                tenants.add("suzohapptransport");
                break;
        }
        return tenants;
    }

    public static String getBrokerHost(Environments environment) {
        String brokerHost = null;

        switch (environment){
            case UAT:
                brokerHost="device-test.cashcomplete.com";
                break;
            case CH:
                brokerHost="device.cashcomplete.ch";
                break;
            case DE:
                brokerHost="device.cashcomplete.de";
                break;
            case US_EAST:
                brokerHost="device.cashcomplete.com";
                break;
            case ASIA:
                brokerHost="device.cashcomplete.asia";
                break;
            case DEV:
                brokerHost="device-dev.cashcomplete.com";
                break;
            case LAT:
                brokerHost="device.cashcomplete.lat";
                break;
            case RU:
                brokerHost="device.cashcomplete.ru";
                break;
            case BRINKS_US:
                brokerHost="device.brinksccc.com";
                break;
            case US_WEST:
                brokerHost="device-us-west.cashcomplete.com";
                break;
        }
        return brokerHost;
    }

    public static String getBaseURL(Environments environment) {
        String prefix=null,suffix=null;
        switch (environment){
            case UAT:
                prefix="test-api.";
                suffix=".com";
                break;
            case CH:
                prefix="api.";
                suffix=".ch";
                break;
            case DE:
                prefix="api.";
                suffix=".de";
                break;
            case US_EAST:
                prefix="api.";
                suffix=".com";
                break;
            case ASIA:
                prefix="api.";
                suffix=".asia";
                break;
            case DEV:
                prefix="dev-api.";
                suffix=".com";
                break;
            case LAT:
                prefix="api.";
                suffix=".lat";
                break;

            case RU:
                prefix="api.";
                suffix=".ru";
                break;
            case BRINKS_US:
                return "https://api.brinksccc.com";
            case US_WEST:
                return "https://api-us-west.cashcomplete.com";
            case WEBHOOK:
                return "https://webhook.site";
        }

        return "https://"+prefix+"cashcomplete"+suffix;
    }

    public static String getAPIReferer(Environments environment) {
        String referer = null;
        switch (environment){
            case DEV:
                referer="connect-dev";
                break;
            case UAT:
                referer="connect-test";
                break;
            case ASIA:
            case US_EAST:
            case DE:
            case CH:
            case RU:
                referer="connect";
                break;
            case BRINKS_US:
                return "https://api.brinksccc.com/";

        }

        return "https://"+referer+".cashcomplete.com/";
    }

    public static String getUsersXmlPath() {
        return System.getProperty("user.dir")+pathSeparator+"xml"+pathSeparator+"ccodUsers.xml";
    }

    public static String getXmlPath(){
        return System.getProperty("user.dir")+pathSeparator+"xml";
    }

    public static String getCCCURL(Environments env) {
        String domain = "";
        switch (env) {

            case UAT:
                domain = "connect-test.cashcomplete.com";
                break;

            case US_EAST:
                domain = "connect.cashcomplete.com";
                break;

            case DE:
                domain = "connect.cashcomplete.de";
                break;

            case CH:
                domain = "connect.cashcomplete.ch";
                break;

            case ASIA:
                domain = "connect.cashcomplete.asia";
                break;

            case LAT:
                domain = "connect.cashcomplete.lat";
                break;

            case UK:
                domain = "connect.cashcomplete.uk";
                break;

            case RU:
                domain = "connect.cashcomplete.ru";
                break;

        }

        return domain;
    }

    public static String[] getMachinesForDLS() {
        return new String[]{"JM-02", "JM-03", "JM-04", "JM-11", "JM-12", "JM-13", "JM-USD-01"};
    }

    public static Currency[] getMachineCurrencyForDLS() {
        return new Currency[]{EUR,EUR,EUR,EUR,EUR,EUR,USD};
    }
}
