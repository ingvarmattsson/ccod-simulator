package data;

import rmObjects.RmDeviceMovement;
import rmObjects.RmValueBag;

import java.util.List;

public class GeneratedTransaction {
    public List<RmValueBag> valueBags;
    public List<RmDeviceMovement> deviceMovements;
}
