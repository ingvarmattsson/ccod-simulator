package data;

import ccodSession.Environments;
import ccodSession.Main;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import rmObjects.RmUserMuM;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static ccodSession.Main.*;
import static data.DefaultStrings.*;

public class Utilities {

    public static void writeToFile(String rmMessage) {
        String fileName = "transaction.rm";
        String path = System.getProperty("user.dir")+"\\transactions\\";

        try {
            FileWriter fw = new FileWriter(path+fileName);
            fw.write(rmMessage);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static synchronized String getCurrentDateStampIncludedCatchingUp() {

        if(Long.compare(minusSecondsRmEvent,0) > 0)
            return Instant.now().atOffset(ZoneOffset.of(timeZoneOffset)).minusSeconds(Main.minusSecondsRmEvent).format(DateTimeFormatter.ofPattern(dateFormatPatternMachine));

        return Instant.now().atOffset(ZoneOffset.of(timeZoneOffset)).format(DateTimeFormatter.ofPattern(dateFormatPatternMachine));
    }

    public static synchronized String getCurrentDateStampLog(){
        return Instant.now().atOffset(ZoneOffset.of(timeZoneOffset)).format(DateTimeFormatter.ofPattern(dateFormatPatternMachine));
    }

    public static String getDateAsMachinePattern(String date){
        return Instant.parse(date).atOffset(ZoneOffset.of(timeZoneOffset)).format(DateTimeFormatter.ofPattern(dateFormatPatternMachine));
    }

    public static String getCurrentTimeHHmm(int plusMinutes) {
        return Instant.now().atOffset(ZoneOffset.of(timeZoneOffset)).plusMinutes(plusMinutes).format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    public static String getDateStampCBDataGenerator(long minusDays) {
        return Instant.now().atOffset(ZoneOffset.of(timeZoneOffset)).minusDays(minusDays).format(DateTimeFormatter.ofPattern(dateFormatPatternCbDataGenerator))+"Z";
    }

    public static String parseDateToQueryStringParam(String date){
        String[] dateTime = date.split(" ");

        String parsedDate= null;
        String year = dateTime[0].substring(0,4);
        String month = dateTime[0].substring(4,6);
        String day = dateTime[0].substring(6,8);

        parsedDate = year+"-"+month+"-"+day+"T"+dateTime[1]+"Z";

        return  parsedDate;
    }

    public static String[] incrDateTime(String dateTime, int offset){
        String[]date=dateTime.split("T");
        String lowBound=":00:00.000Z";
        String highBound=":59:59.000Z";
        String hour=date[1].substring(0,2);

        int h = Integer.parseInt(hour);

        String[] yyyyMMdd = date[0].split("-");
        int yyyy = Integer.parseInt(yyyyMMdd[0]);
        int mm = Integer.parseInt(yyyyMMdd[1]);
        int dd = Integer.parseInt(yyyyMMdd[2]);

        h+=offset;
        if(h>23){
            h=0;
            dd++;

            //31 days
            int daysInMonth=30;
            if(mm==1 || mm==3 || mm==5 || mm==7 || mm==8 ||mm==10 ||mm==12){
                daysInMonth=31;
            }
            //leap year
            else if ( mm==2 ){
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR,yyyy);
                boolean isLeapYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;

                daysInMonth = (isLeapYear ? 29 : 28);
            }

            if(dd>daysInMonth){
                dd=1;
                mm++;

                if(mm>12){
                    mm=1;
                    yyyy++;
                }

            }

        }

        hour = (h<10 ? "0"+h : Integer.toString(h));
        yyyyMMdd[2] = (dd<10 ? "0"+dd : Integer.toString(dd));
        yyyyMMdd[1] = (mm<10 ? "0"+mm : Integer.toString(mm));
        yyyyMMdd[0] = Integer.toString(yyyy);

        dateTime=yyyyMMdd[0]+"-"+yyyyMMdd[1]+"-"+yyyyMMdd[2]+"T"+hour;

        String[] dates ={dateTime+lowBound,dateTime+highBound} ;
        return dates;
    }


    public static void main(String[] args) {
        new Utilities().test();

    }

    public static String changeDateTime(String dateTime,AccountingDateOffset offset){


        SimpleDateFormat df = new SimpleDateFormat(dateFormatPatternMachine);
        String changedDateTime=dateTime;
        try {
            Date d = df.parse(dateTime);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            c.add(Calendar.HOUR, offset.hour);
            c.add(Calendar.MINUTE, offset.minute);
            c.add(Calendar.DAY_OF_MONTH, offset.day);
            changedDateTime =df.format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return changedDateTime;

    }


    public static String parseToAPIDate(String dateTime) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat(dateFormatPatternMachine);

        //"+02:00"
        Date d = df.parse(dateTime);
        int hour = Integer.parseInt(timeZoneOffset.substring(1,3));
        int minute = Integer.parseInt(timeZoneOffset.substring(4));
        if(timeZoneOffset.startsWith("+")){
            hour = -hour;
            minute = - minute;
        }
        Calendar gc = new GregorianCalendar();
        gc.setTime(d);
        gc.add(Calendar.HOUR, hour);
        gc.add(Calendar.MINUTE, minute);

        String s = new SimpleDateFormat(dateFormatPatternAPI).format(gc.getTime());
        return s.replace(" ","T")+"Z";
    }

    public static LinkedList<String> readExpectedResult(String fileName) throws IOException {
        String path = System.getProperty("user.dir")+pathSeparator+"expectedResultsCRUD"+pathSeparator+fileName;
        LinkedList<String> expectedAsString = new LinkedList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        String line;
        while ((line = reader.readLine())!=null)
            expectedAsString.add(line);
        reader.close();
        //remove headers
        expectedAsString.removeFirst();
        return expectedAsString;
    }

    public static Document getXmlRoot(String fileUsersXML) throws ParserConfigurationException, IOException, SAXException {
        File file = new File(fileUsersXML);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        return documentBuilder.parse(file);
    }

    public static String generatePassword(int length) {

        return new Random().ints((length<8 == true || length>20 ? 12 : length), 48, 90).collect(StringBuilder::new,
                StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static RmUserMuM cloneMachineUser(RmUserMuM user) {
        RmUserMuM clone = new RmUserMuM();
        clone.USER_ID=user.USER_ID;
        clone.ALIAS=user.ALIAS;
        clone.CARD_IDENTIFIER=user.CARD_IDENTIFIER;
        clone.NAME=user.NAME;
        clone.PIN=user.PIN;
        clone.ROLE_NAME=user.ROLE_NAME;
        clone.DEACTIVATED=user.DEACTIVATED;
        clone.CHANGE_PIN_ON_LOGIN=user.CHANGE_PIN_ON_LOGIN;
        clone.NO_PIN=user.NO_PIN;
        clone.NO_PIN_BIO=user.NO_PIN_BIO;
        clone.NO_PIN_CARD=user.NO_PIN_CARD;
        clone.NO_PIN_DOOR=user.NO_PIN_DOOR;
        clone.NO_PIN_USER_ID=user.NO_PIN_USER_ID;
        clone.NBR_OF_BAD_PIN=user.NBR_OF_BAD_PIN;
        clone.NBR_OF_LOGINS=user.NBR_OF_LOGINS;
        clone.VALID_FROM=user.VALID_FROM;
        clone.VALID_UNTIL=user.VALID_UNTIL;
        clone.DOORS=user.DOORS;
        clone.FINGERPRINTS=user.FINGERPRINTS;
        clone.LIST=user.LIST;
        return clone;
    }

    public static String printEnvironments() {
        StringBuilder sb = new StringBuilder();
        Environments[] envs = Environments.values();
        for(int i = 0; i< envs.length; i++)
            sb.append(envs[i]+" ");
        return sb.toString();
    }

    public static BufferedReader getFileReader(String fileName) throws FileNotFoundException {
        return new BufferedReader(new FileReader(new File(fileName)));
    }

    public static String[] getNames(String nameType) {
        switch (nameType){
            case "BoyNames":
                return new String[]{
                        "Alexander"
                        ,"Albert"
                        ,"Adam"
                        ,"Agnus"
                        ,"Bert"
                        ,"Beril"
                        ,"Charles"
                        ,"Ceaser"
                        ,"Calle"
                        ,"David"
                        ,"Dirk"
                        ,"Dave"
                        ,"Dennis"
                        ,"Eric"
                        ,"Eugine"
                        ,"Filip"
                        ,"Frans"
                        ,"Gustaf"
                        ,"Henrik"
                        ,"Ingmar"
                        ,"Johan"
                        ,"John"
                        ,"Jonas"
                        ,"Kasper"
                        ,"Lucas"
                        ,"Leif"
                        ,"Max"
                        ,"Nori"
                        ,"Nick"
                        ,"Oscar"
                        ,"Patrik"
                        ,"Qvintus"
                        ,"Rasmus"
                        ,"Roger"
                        ,"Steve"
                        ,"Stefan"
                        ,"Tobias"
                        ,"Troed"
                        ,"Uwe"
                        ,"Victor"
                        ,"Walter"
                        ,"Yusuf"
                        ,"Zaltser"
                };
            case "GirlNames":
                return new String[]{
                        "Alexandra"
                        ,"Ava"
                        ,"Bea"
                        ,"Beatrice"
                        ,"Cornelia"
                        ,"Denisé"
                        ,"Erika"
                        ,"Filippa"
                        ,"Gustava"
                        ,"Gina"
                        ,"Hope"
                        ,"Grace"
                        ,"Ingela"
                        ,"Joan"
                        ,"Klara"
                        ,"Ludmilla"
                        ,"Laura"
                        ,"Maja"
                        ,"Margret"
                        ,"Nicole"
                        ,"Orfelia"
                        ,"Patricia"
                        ,"Queen"
                        ,"Rapunsel"
                        ,"Robin"
                        ,"Steffany"
                        ,"Tina"
                        ,"Ulla"
                        ,"Victoria"
                        ,"Winona"
                        ,"Yogini"
                        ,"Zandra"
                };
            case "Surnames":
                return new String[]{
                        "Anderson"
                        ,"BegnesBailey"
                        ,"Cole"
                        ,"Cox"
                        ,"Dixon"
                        ,"Dean"
                        ,"Davis"
                        ,"Evans"
                        ,"Edwards"
                        ,"Foster"
                        ,"Fox"
                        ,"Green"
                        ,"Griffits"
                        ,"Gibson"
                        ,"George"
                        ,"Harper"
                        ,"Harris"
                        ,"Hunt"
                        ,"Icking"
                        ,"Isengard"
                        ,"Jackman"
                        ,"Johnson"
                        ,"Jongman"
                        ,"Jonker"
                        ,"Kaal"
                        ,"Ketels"
                        ,"Kalb"
                        ,"Kalf"
                        ,"Kalste"
                        ,"Kloek"
                        ,"Lambert"
                        ,"Loeff"
                        ,"Locken"
                        ,"Lohman"
                        ,"Louret"
                        ,"Lubbers"
                        ,"Mann"
                        ,"Minks"
                        ,"Miers"
                        ,"Metzer"
                        ,"Möser"
                        ,"Neckers"
                        ,"Nobel"
                        ,"Nobis"
                        ,"Nonhoff"
                        ,"O'Connor"
                        ,"Oberink"
                        ,"Ollewick"
                        ,"Oonk"
                        ,"Onsta"
                        ,"Papn"
                        ,"Philips"
                        ,"Pontman"
                        ,"Pierks"
                        ,"Piper"
                        ,"Poppelman"
                        ,"Quinn"
                        ,"Qwitinck"
                        ,"Raey"
                        ,"Reker"
                        ,"Roer"
                        ,"roth"
                        ,"Rottmans"
                        ,"Rosier"
                        ,"Ros"
                        ,"Salman"
                        ,"Sickink"
                        ,"Soule"
                        ,"Spankler"
                        ,"Sanders"

                };
        }
        return null;
    }


    public void test(){
        int i = 0;
        while (i<10){
            try {
                Thread.sleep(50);
                //  System.out.println(Utilities.getCurrentDateStampIncludedCatchingUp());
                System.out.println("\t"+ getCurrentDateStampIncludedCatchingUp());
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



    public static String getDateOnly(String currentDateStamp) {
        String[] prefixTime = currentDateStamp.split(" ");
        return prefixTime[0];
/*        StringBuilder sb = new StringBuilder();
        sb.append(prefixTime[0]+"_");


        if(prefixTime[1].contains(".")){
            int index = prefixTime[1].indexOf(".");
            prefixTime[1] = prefixTime[1].substring(0,index);
        }
        String[] time = prefixTime[1].split(":");
        for(int i = 0;i<time.length;i++){
            sb.append(time[i]+"_");
        }
        String formatted = sb.toString();
        formatted = formatted.substring(0,formatted.length()-1);
        return formatted;*/
    }

}
