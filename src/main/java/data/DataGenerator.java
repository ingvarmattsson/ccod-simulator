package data;

import CCoD.*;
import CCoD.Currency;
import ccodSession.Main;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import exceptions.BoxContentEmptyExcception;
import exceptions.BoxContentFullException;
import exceptions.DropSafeFullException;
import exceptions.EmptiableBoxesFullException;
import org.xml.sax.SAXException;
import rmObjects.*;
import xml.MachineUsersGenerator;
import xml.WorkUnitReader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static CCoD.Currency.*;
import static CCoD.MachineModels.*;
import static CCoD.TransactionType.*;
import static CCoD.ValueType.COIN;
import static CCoD.ValueType.NOTE;
import static data.FillStatus.*;


public class DataGenerator {

    public static String remoteSoftwareUpgradeKind="RemoteUpgrade";
    public static String remoteConfigurationKind="RemoteUpdate";
    private static boolean datestampRealTime = true;
    private static String startDate="20190101 00:00:00.000";
    private static String csvDelimeter =";", sfVersion="3.7.0";
    private static Currency currency=USD;

    private static double sumTotalAmount(List<RmValue> values) {
        double totalAmount=0;
        for(RmValue v : values)
            totalAmount += (double) v.TOTAL;

        return totalAmount;
    }

    public static List<ErrorWarning> getErrorWarnings() {
        return null;
    }

    public static List<TransactionType> getTransactionTypes(boolean allTypes) {
        if(allTypes)
            return TRANSACTION_TYPE_LIST;
        else{
            List<TransactionType> list = new LinkedList<>();
            list.add(DEPOSIT);
            list.add(DISPENSE);
//            list.add(END_OF_SHIFT);
            list.add(POSSIBLE_TAMPERING);
            list.add(UNFINISHED_TRANSACTION);
            list.add(MANUAL_DEPOSIT);
            return list;
        }
    }

    private static LinkedList<String> generateMachineBoxContents(CCoDMachine machine) throws JsonProcessingException {
        LinkedList<String> list = new LinkedList<>();
        //list.add(headerBoxContents);
        String dateTime="";

        if(!datestampRealTime)
            dateTime=startDate;

        //"machineId"+csvDelimeter+"dateTime"+csvDelimeter+"sfVersion"+csvDelimeter+"device"+csvDelimeter+"deviceId"+csvDelimeter+"deviceTye"+csvDelimeter+"valueType"+csvDelimeter+"transactionSequence"+csvDelimeter+"boxContent";


        LinkedList<RmBoxContent> deviceContents = machine.getBoxContents();
        for (RmBoxContent device : deviceContents){
            StringBuilder sb = new StringBuilder();

            String boxContents = RmHandler.parseRmBoxesToString(device.BOX_CONTENTS);
            sb.append(machine.getMachineUUID()+csvDelimeter
                    +dateTime+csvDelimeter
                    +machine.getSfVersion()+csvDelimeter
                    +device.DEVICE+csvDelimeter
                    +device.DEVICE_ID+csvDelimeter
                    +device.DEVICE_TYPE+csvDelimeter
                    +device.VALUE_TYPE+csvDelimeter
                    +boxContents);
            list.add(sb.toString());

        }

        return list;
    }


    private static LinkedList<String> generateParametersForMachinecontendEndpoint(LinkedList<CCoDMachine> machines) {
        LinkedList<String> params = new LinkedList();
        String header = "machineGroup";
        params.add(header);

        machines.add(new CCoDMachine(RCS_500,sfVersion,"Sel-01",currency));
        machines.add(new CCoDMachine(RCS_500,sfVersion,"Sel-02",currency));
        machines.add(new CCoDMachine(RCS_500,sfVersion,"4f1151b4-3774-434e-a919-1d914502099f",currency));
        machines.add(new CCoDMachine(RCS_500,sfVersion,"bbec51ed-7f1f-4bcb-beec-a4f768a9435c",currency));

        int i =1;

        StringBuilder row = new StringBuilder();
        for(CCoDMachine machine : machines){
            row.append(machine.getMachineUUID()+",");
            if(i%10==0){
                String machineRow = row.toString().substring(0,row.length()-1);
                params.add(machineRow);
                row = new StringBuilder();

            }
            i++;

        }

        String remaining = row.toString();
        if(!remaining.isEmpty()){
            remaining = remaining.substring(0,remaining.length()-1);
            params.add(remaining);
        }

        return params;
    }

    public static List<SystemStatusStatus> getSystemStatusStatuses(){
        return  SystemStatusStatus.SYSTEM_STATUS_TYPE_LIST;
    }

    public static RmTransaction getTransactionRow(TransactionType type, CCoDMachine machine, RmUserCommon rmUser, String dateTime,
                                                  String timeZoneOffset, long transactionSequence, long messageSequence,
                                                  long sequence) throws JsonProcessingException, BoxContentFullException, BoxContentEmptyExcception, EmptiableBoxesFullException, DropSafeFullException {


        boolean transactionNegative = isTransactionNegative(type);

        List<RmBox> eligibleForTransaction;
        GeneratedTransaction transaction = null;
        Random r = new Random();
        int coinsNotesBoth = r.nextInt(3);

        if (type!=REFILL && type!=MANUAL_REFILL && type!=PURGE && type!=EMPTY_OUT && type!=EXCHANGE){
            String  exceptionMessage;

            //check drop safe needs emptying
            if((exceptionMessage = checkDropSafeContent(machine))!=null)
                throw new DropSafeFullException(exceptionMessage);

            //ckeck if any emptiable devices need to be emptied
            if((exceptionMessage = checkNeedForEmptyOut(machine))!=null)
                throw new EmptiableBoxesFullException(exceptionMessage);

            //check if any potential devices fill level is ok
            if(transactionNegative){
                if((exceptionMessage = checkMachineContents(machine,type, transactionNegative))!=null)
                    throw new BoxContentEmptyExcception(exceptionMessage);
            }else
                if((exceptionMessage = checkMachineContents(machine,type, transactionNegative))!=null) {

                    throw new BoxContentFullException(exceptionMessage);
                }

            if(type!=MANUAL_DEPOSIT){
                eligibleForTransaction = machine.getBoxesByFillStatus((transactionNegative ? Capability.DISPENSE : Capability.DEPOSIT));
                transaction = performTransaction(eligibleForTransaction,type,coinsNotesBoth,transactionNegative,currency,machine);

            }else{
                transaction = performManualDepositTransaction(machine,transactionSequence);

            }
        }

        else if(type==PURGE || type==REFILL || type == MANUAL_REFILL ||type==EMPTY_OUT){
            //purge dispense coins to transport box
            //purge dispense notes to note recycler bag
            //refill notes/coins
            Map<RmBox, LinkedList<RmBox>> boxToMoveBoxesFrom = machine.getBoxesByFillStatus(type);
            transaction = purgeRefillEmpty(boxToMoveBoxesFrom,type);


        }

        else if(type==EXCHANGE && machine.getEmptyDropSafe()){
            //Create BoxExchange
            //Create BoxContent
            //Create Transaction EXCHANGE OUT (empty all dropsafe contents)
            transaction = exchangeDropSafe(machine);

        }

        if(transaction==null)
            return null;

        List<RmValueBag> rmValueBags=transaction.valueBags;
        List<RmDeviceMovement> deviceMovements=transaction.deviceMovements;
        String subType = "";
        if(type==EXCHANGE) {
            subType="OUT";
        }

        double totalAmount=0;
        for(RmValueBag vb : rmValueBags)
            totalAmount+=vb.TOTAL;

        if(Main.debugMode)
            System.out.println(machine.getMachineUUID()+" Transaction: "+type);

        return RmHandler.generateTransaction(
                dateTime,
                machine.getMachineUUID(),
                rmUser,
                transactionSequence,
                messageSequence,
                sequence,
                totalAmount,
                rmValueBags.get(0).VALUES.get(0).CURRENCY,
                type,
                subType,
                timeZoneOffset,
                rmValueBags,
                deviceMovements

        );
    }

    private static GeneratedTransaction exchangeDropSafe(CCoDMachine machine) {
        GeneratedTransaction transaction = new GeneratedTransaction();
        RmBoxContent dropSafe = machine.getDropSafe();


        List<RmValueBag> valueBags = new LinkedList<>();
        List<RmValue> values = dropSafe.BOX_CONTENTS.get(0).VALUES;

        for (RmValue v : values){
            RmValueBag valueBag = new RmValueBag();

            RmValue vAdd = new RmValue();
            vAdd.DENOMINATION = v.DENOMINATION;
            vAdd.COUNT = v.COUNT;
            vAdd.TOTAL = - v.TOTAL;
            vAdd.TYPE = v.TYPE;
            vAdd.CLAIMED_VALUE_TYPE = v.CLAIMED_VALUE_TYPE;
            vAdd.PIECE_VALUE = v.PIECE_VALUE;
            vAdd.CURRENCY = v.CURRENCY;
            vAdd.REFERENCE = v.REFERENCE;
            vAdd.DECIMALS = v.DECIMALS;
            vAdd.C2_CREDITED = v.C2_CREDITED;
            vAdd.C2_COUNT = v.C2_COUNT;
            vAdd.C4B_COUNT = v.C4B_COUNT;
            vAdd.CUSTOM_TYPE = v.CUSTOM_TYPE;
            vAdd.EXCHANGE_RATE = v.EXCHANGE_RATE;
            vAdd.EXCHANGE_RATE_DECIMALS = v.EXCHANGE_RATE_DECIMALS;
            vAdd.C3_COUNT = v.C3_COUNT;
            vAdd.CREDIT_CAT_2 = v.CREDIT_CAT_2;
            vAdd.CREDIT_CAT_3 = v.CREDIT_CAT_3;

            valueBag.VALUES.add(vAdd);
            valueBag.REJECTS=0;
            calculateValueBagTotalAmount(valueBag,true);
            valueBag.TYPE=dropSafe.BOX_CONTENTS.get(0).TYPE;

            valueBags.add(valueBag);
        }

        dropSafe.BOX_CONTENTS.get(0).VALUES = new LinkedList<>();
        dropSafe.BOX_CONTENTS.get(0).FILL_STATUS="Ok";
        machine.setEmptyDropSafe(false);
        transaction.valueBags = valueBags;
        transaction.deviceMovements=generateDeviceMovementsManualDeposits(valueBags,dropSafe);
        return transaction;

    }

    private static String checkDropSafeContent(CCoDMachine machine) {
        String exceptionMessage=null;
        RmBoxContent dropSafe = machine.getDropSafe();

        if(dropSafe.BOX_CONTENTS.get(0).VALUES.size()>dropSafe.BOX_CONTENTS.get(0).HIGH_LIMIT_LEVEL){
            exceptionMessage="Drop Safe reaches capacity. Count: "+dropSafe.BOX_CONTENTS.get(0).VALUES.size()+" / "+dropSafe.BOX_CONTENTS.get(0).HIGH_LIMIT_LEVEL;
            dropSafe.BOX_CONTENTS.get(0).FILL_STATUS="Full";

        }

        return exceptionMessage;
    }

    private static GeneratedTransaction performManualDepositTransaction(CCoDMachine machine, long transactionReference) {

        RmBoxContent dropSafe= machine.getDropSafe();
        if(dropSafe == null)
            return null;

        Random rng = new Random();
        GeneratedTransaction t = new GeneratedTransaction();

        List<RmValueBag> valueBags = new LinkedList<>();
        String type = dropSafe.BOX_CONTENTS.get(0).TYPE;

        int amountOfBags = rng.nextInt(10)+1;
        for(int i = 0 ; i< amountOfBags;i++){

            RmValue value = new RmValue();
            value.CURRENCY=Currency.getRandomCurrency().name();
            value.TOTAL = value.DENOMINATION = value.PIECE_VALUE = rng.nextInt(5000)+1000;
            value.REFERENCE = "barCode-"+transactionReference+"-"+i;
            value.DECIMALS=2;
            value.COUNT=1;
            value.TYPE = type;
            value.CLAIMED_VALUE_TYPE = ClaimedValueTypes.getRandomClaimedValueType().name();

            RmValueBag valueBag = new RmValueBag();
            valueBag.VALUES = new LinkedList<>();
            valueBag.VALUES.add(value);
            calculateValueBagTotalAmount(valueBag,true);
            valueBag.REJECTS =0;
            valueBag.TYPE = type;
            valueBags.add(valueBag);

            dropSafe.BOX_CONTENTS.get(0).VALUES.add(value);

        }

        t.valueBags = valueBags;
        t.deviceMovements = generateDeviceMovementsManualDeposits(valueBags,dropSafe);


        return t;
    }

    private static List<RmDeviceMovement> generateDeviceMovementsManualDeposits(List<RmValueBag> valueBags, RmBoxContent dropSafe) {
        List<RmDeviceMovement> movements = new LinkedList<>();
        RmDeviceMovement movement = new RmDeviceMovement();
        movement.DEVICE_ID = dropSafe.DEVICE_ID;
        RmBoxMovement boxMovement = new RmBoxMovement();
        boxMovement.BOX_ID = dropSafe.BOX_CONTENTS.get(0).BOX_ID;
        boxMovement.MOVEMENT = valueBags;
        boxMovement.RESULTING_CONTENT = generateValueBags(dropSafe);
        movement.BOX_MOVEMENTS = new LinkedList<>();
        movement.BOX_MOVEMENTS.add(boxMovement);
        movements.add(movement);
        return movements;
    }

    private static List<RmValueBag> generateValueBags(RmBoxContent dropSafe) {
        List<RmValueBag> valueBags = new LinkedList<>();

        for(RmValue v : dropSafe.BOX_CONTENTS.get(0).VALUES){
            RmValueBag valueBag = new RmValueBag();
            valueBag.VALUES.add(v);
            valueBag.TYPE = v.TYPE;
            valueBag.REJECTS=0;
            calculateValueBagTotalAmount(valueBag,true);
            valueBags.add(valueBag);
        }
        return valueBags;
    }

    private static GeneratedTransaction purgeRefillEmpty(Map<RmBox, LinkedList<RmBox>> boxToMoveBoxesFrom, TransactionType type) {

        GeneratedTransaction transaction = new GeneratedTransaction();
        List<RmBox> keys = new LinkedList<>(boxToMoveBoxesFrom.keySet());
        LinkedList<RmValueBag> valueBags = new LinkedList<>();

        for(RmBox key : keys){

            RmValueBag vb = new RmValueBag();
            vb.TYPE = key.TYPE;
            vb.VALUES = new LinkedList<>();

            LinkedList<RmBox> contents = boxToMoveBoxesFrom.get(key);

            for(RmBox box : contents){
                for(RmValue value : box.VALUES){

                    RmValue tVal = setTransactionValueFields(value);

                    int difference=0;
                    if(type==PURGE || type==EMPTY_OUT){
                        difference=value.COUNT-box.DEFAULT_LEVEL;
                        difference = -difference;
                    }
                    if(type==MANUAL_REFILL || type==REFILL){
                        difference= box.DEFAULT_LEVEL-value.COUNT;
                    }
                    if(difference!=0){
                        calculateRmValue(tVal,difference);
                        vb.VALUES.add(tVal);
                    }

                    value.COUNT = box.DEFAULT_LEVEL;

                    //update destination containers (BAG TransportBox)
                    if(type==PURGE){
                        difference = -difference;
                        for(RmValue vUpdate : key.VALUES){
                            if (vUpdate.DENOMINATION==tVal.DENOMINATION && vUpdate.TYPE.equals(tVal.TYPE))
                                calculateRmValue(vUpdate,vUpdate.COUNT+difference);

                        }
                    }
                }



            }

            if(!vb.VALUES.isEmpty()){
                calculateValueBagTotalAmount(vb,true);
                valueBags.add(vb);
            }

        }

        transaction.valueBags=valueBags;
        return transaction;

    }

    private static String checkNeedForEmptyOut(CCoDMachine machine) {

        List<RmBox> boxes = machine.getBoxesByFillStatus(Capability.EMPTY);

        for(RmBox box : boxes){
            int max = Integer.MIN_VALUE;
            for(RmValue value : box.VALUES){
                int currentMax = Integer.MIN_VALUE;
                if(value.COUNT>=box.HIGH_LIMIT_LEVEL){
                    currentMax = value.COUNT;
                    if(currentMax>=max)
                        max = currentMax;
                }
            }

            box.FILL_STATUS= OK.name();
            if(max>=box.HIGH_LIMIT_LEVEL)
                box.FILL_STATUS= HIGH_LIMIT_LEVEL.name();
            if(max>= box.MAXIMUM_LEVEL)
                box.FILL_STATUS= MAXIMUM_LEVEL.name();

        }

        String message="";
        for(RmBox box : boxes){
            if(!box.FILL_STATUS.equals(OK.name())){

                message+=box.NAME+"-"+box.TYPE+"!>";
            }
        }

        if(message.isEmpty())
            return null;

        return message+machine.getMachineUUID();
    }


    private static String checkMachineContents(CCoDMachine machine, TransactionType type, boolean transactionNegative) {

        LinkedList<RmBox> boxes;
        if(transactionNegative){
            boxes= machine.getBoxesByFillStatus(Capability.DISPENSE);
        }else {
            boxes= machine.getBoxesByFillStatus(Capability.DEPOSIT);
        }

        for(RmBox box : boxes){

            if(box.BOX_ID.equals("Bag")
                    || box.BOX_ID.equals("Drop Safe")
                    || box.BOX_ID.equals("C2-01")
                    || box.BOX_ID.equals("SVAULT")
                    || box.BOX_ID.equals("TransportBox.1"))
                continue;

            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;

            for(RmValue value : box.VALUES){
                int currentMin = Integer.MAX_VALUE;
                int currentMax= Integer.MIN_VALUE;

                if (transactionNegative){
                    if(value.COUNT<= box.LOW_LIMIT_LEVEL){
                        currentMin = value.COUNT;
                        if(currentMin<=min)
                            min = currentMin;
                    }
                    if(value.COUNT<= box.MINIMUM_LEVEL){
                        currentMin = value.COUNT;
                        if(currentMin<=min)
                            min = currentMin;
                    }

                }

                else {
                    if(value.COUNT>= box.HIGH_LIMIT_LEVEL){
                        currentMax = value.COUNT;
                        if(currentMax>=max)
                            max = currentMax;
                    }
                    if(value.COUNT>= box.MAXIMUM_LEVEL){
                        currentMax = value.COUNT;
                        if(currentMax>=max)
                            max = currentMax;
                    }
                }

            }


            box.FILL_STATUS = OK.name();
            if(transactionNegative){
                if(min<=box.LOW_LIMIT_LEVEL)
                    box.FILL_STATUS=LOW_LIMIT_LEVEL.name() ;
                if(min<= box.MINIMUM_LEVEL)
                    box.FILL_STATUS=MINIMUM_LEVEL.name();

            }
            else{

                if(max>=box.HIGH_LIMIT_LEVEL)
                    box.FILL_STATUS= HIGH_LIMIT_LEVEL.name();
                if(max>= box.MAXIMUM_LEVEL)
                    box.FILL_STATUS= MAXIMUM_LEVEL.name();

            }
        }

        FillStatus evaluationStatus=OK;

        if(transactionNegative)
            evaluationStatus=MINIMUM_LEVEL;
        else
            evaluationStatus=MAXIMUM_LEVEL;

        int coinsCount = 0, notesCount = 0;
        String message="";
        for(RmBox box : boxes){
            if(box.FILL_STATUS.equals(evaluationStatus.name())){
                if(box.TYPE.equals(COIN.name()))
                    coinsCount++;
                if(box.TYPE.equals(NOTE.name()))
                    notesCount++;
                message+="!>"+box.NAME+"!>"+box.TYPE;
            }
        }
        String prefix = (coinsCount>notesCount) ? COIN.name() : NOTE.name();
        if(message.isEmpty())
            return null;

        return prefix+message+"!>"+machine.getMachineUUID();
    }

    private static GeneratedTransaction performTransaction(List<RmBox> eligibleForTransaction, TransactionType transactionType, int coinsNotesBoth, boolean transactionNegative, Currency currency, CCoDMachine machine) {

        List<RmBox> transactionRmBoxes = new LinkedList<>();
        Map<String,List<RmValue>> tVals = new HashMap<>();

        //randomize which boxes should be touched by transaction

        //Split to NOTES and COINS boxes, interact with at least 1 from each
        if (coinsNotesBoth==2){
            List<RmBox> coins = new LinkedList<>();
            List<RmBox> notes = new LinkedList<>();
            for(RmBox box : eligibleForTransaction){
                if(box.TYPE.equals(NOTE.name()))
                    notes.add(box);
                if(box.TYPE.equals(COIN.name()))
                    coins.add(box);
            }

            transactionRmBoxes.addAll(getRandomRmBoxes(coins));
            tVals.put(COIN.name(), new LinkedList<RmValue>());

            transactionRmBoxes.addAll(getRandomRmBoxes(notes));
            tVals.put(NOTE.name(), new LinkedList<RmValue>());

        }
        //coins only
        else if (coinsNotesBoth==0){
            List<RmBox> coins = new LinkedList<>();
            for(RmBox box : eligibleForTransaction){
                if(box.TYPE.equals(COIN.name()))
                    coins.add(box);
            }

            transactionRmBoxes.addAll(getRandomRmBoxes(coins));
            tVals.put(COIN.name(), new LinkedList<RmValue>());


            //notes only
        }else if(coinsNotesBoth==1){
            List<RmBox> notes = new LinkedList<>();
            for(RmBox box : eligibleForTransaction){
                if(box.TYPE.equals(NOTE.name()))
                    notes.add(box);
            }

            transactionRmBoxes.addAll(getRandomRmBoxes(notes));
            tVals.put(NOTE.name(), new LinkedList<RmValue>());

        }

        //for each box randomize counts, update RmBoxes and create value bags
        int maxRngNotes = 10;
        int maxRngCoins= 50;
        Random r = new Random();


        Map<String,List<RmBoxMovement>> deviceMovements = new HashMap<>();

        for(RmBox box : transactionRmBoxes){
            int min = box.MINIMUM_LEVEL;
            int max = box.MAXIMUM_LEVEL;
            List<RmValue> values = box.VALUES;

            String parentDeviceId = machine.getParentBoxContentDeviceId(box);
            List<RmBoxMovement> boxMovements =deviceMovements.get(parentDeviceId);
            if(boxMovements == null)
                boxMovements = new LinkedList<>();

            RmBoxMovement bm = new RmBoxMovement();
            bm.BOX_ID=box.BOX_ID;
            bm.CONTAINER_ID=box.CONTAINER_ID;
            bm.MOVEMENT = new LinkedList<>();
            bm.RESULTING_CONTENT = new LinkedList<>();

            RmValueBag vb = new RmValueBag();
            vb.TYPE = box.TYPE;
            vb.VALUES = new LinkedList<>();
            vb.REJECTS=0;

            for(RmValue value : values){
                int count = value.COUNT;
                int transactionCount = 0;
                if(transactionNegative){
                    if(count>=min){

                        if(value.TYPE.equals(COIN.name()))
                            transactionCount = r.nextInt(maxRngCoins)+1;
                        if(value.TYPE.equals(NOTE.name()))
                            transactionCount = r.nextInt(maxRngNotes)+1;

                        transactionCount=-transactionCount;

                        //prevent exceeding min limit
                        if(count+transactionCount<min)
                            transactionCount = count-min;

                    }

                }else{
                    if(count<=max){

                        if(value.TYPE.equals(COIN.name()))
                            transactionCount = r.nextInt(maxRngCoins);
                        if(value.TYPE.equals(NOTE.name()))
                            transactionCount = r.nextInt(maxRngNotes);

                        //prevent exceeding max limit
                        if(transactionCount+count>max)
                            transactionCount = max-count;

                    }

                }

                value.COUNT+=transactionCount;
                value.TOTAL = value.COUNT*value.DENOMINATION;

                //create transaction value bag
                RmValue tVal = setTransactionValueFields(value);
                calculateRmValue(tVal,transactionCount);



                vb.VALUES.add(tVal);
                calculateValueBagTotalAmount(vb,false);
                bm.MOVEMENT.add(vb);

                tVals.get(tVal.TYPE).add(tVal);

            }
            boxMovements.add(bm);
            deviceMovements.put(parentDeviceId,boxMovements);

        }


        GeneratedTransaction transaction = new GeneratedTransaction();

        LinkedList<RmValueBag> valueBags = new LinkedList<>();
        Set<String> keys = tVals.keySet();
        for(String key : keys){
            RmValueBag vb = new RmValueBag();
            vb.TYPE = key;
            vb.REJECTS=0;
            vb.VALUES = tVals.get(key);

            calculateValueBagTotalAmount(vb,false);
            valueBags.add(vb);
        }


        transaction.valueBags =valueBags;
        transaction.deviceMovements = generateDeviceMovements(transactionRmBoxes,deviceMovements);

        return transaction;
    }

    private static List<RmDeviceMovement> generateDeviceMovements(List<RmBox> transactionRmBoxes, Map<String, List<RmBoxMovement>> groupedBoxMovements) {
        List<String> keys = new LinkedList<>(groupedBoxMovements.keySet());
        List<RmDeviceMovement> deviceMovements = new LinkedList<>();

        for(String key : keys){
            List<RmBoxMovement> boxMovements = groupedBoxMovements.get(key);
            for (RmBoxMovement bm : boxMovements){
                List<RmValueBag> valueBags = new LinkedList<>();
                for(RmBox transactionBox : transactionRmBoxes){
                    if(bm.CONTAINER_ID.equals(transactionBox.CONTAINER_ID)){
                        RmValueBag vb = new RmValueBag();
                        vb.TYPE = transactionBox.TYPE;
                        vb.REJECTS=0;
                        vb.VALUES = transactionBox.VALUES;
                        calculateValueBagTotalAmount(vb,false);
                        valueBags.add(vb);
                    }
                }

                bm.RESULTING_CONTENT = valueBags;

            }
            RmDeviceMovement dm = new RmDeviceMovement();
            dm.DEVICE_ID=key;
            dm.BOX_MOVEMENTS = boxMovements;
            deviceMovements.add(dm);

        }
        return deviceMovements;
    }

    private static void calculateValueBagTotalAmount(RmValueBag vb, boolean withDecimals) {
        double totalAmount =0;
        for(RmValue val : vb.VALUES){
            if(withDecimals){
                StringBuilder sb = new StringBuilder();
                sb.append("1");
                for(int i = 0; i< val.DECIMALS;i++)
                    sb.append("0");
                double denominator = Double.parseDouble(sb.toString());
                totalAmount+=(val.TOTAL/denominator);
            }
            else{
                totalAmount+=val.TOTAL;
            }

        }
        if(withDecimals)
            vb.TOTAL=Utilities.round(totalAmount,2);
        else{
            int tot = (int) totalAmount;
            vb.TOTAL= tot;
        }

    }


    private static void calculateRmValue(RmValue value, int count){
        value.COUNT=count;
        value.TOTAL = value.DENOMINATION*value.COUNT;
    }

    private static RmValue setTransactionValueFields(RmValue value) {
        RmValue tVal =  new RmValue();
        tVal.DENOMINATION = tVal.PIECE_VALUE= value.DENOMINATION;
        tVal.COUNT=0;
        tVal.TOTAL = tVal.COUNT*tVal.DENOMINATION;
        tVal.TYPE = value.TYPE;
        tVal.DECIMALS = value.DECIMALS;
        tVal.CURRENCY = value.CURRENCY;
        tVal.C2_CREDITED = value.C2_CREDITED;
        tVal.EXCHANGE_RATE = value.EXCHANGE_RATE;
        tVal.EXCHANGE_RATE_DECIMALS = value.EXCHANGE_RATE_DECIMALS;
        tVal.REFERENCE = value.REFERENCE;
        tVal.CLAIMED_VALUE_TYPE = value.CLAIMED_VALUE_TYPE;
        tVal.CUSTOM_TYPE = value.CUSTOM_TYPE;

        return tVal;
    }

    private static List<RmBox> getRandomRmBoxes(List<RmBox> boxes) {
        LinkedList<RmBox> transactionRmBoxes = new LinkedList<>();

        int amountOfDenominationsToUse = new Random().nextInt(boxes.size()-1)+1; //at least 1
        List<Integer> usedIndexes = new LinkedList<>();
        for(int i = 0; i < amountOfDenominationsToUse; i++){
            int index = new Random().nextInt(amountOfDenominationsToUse);
            if(!usedIndexes.contains(index)){
                transactionRmBoxes.add(boxes.get(index));
                usedIndexes.add(index);
            }

        }

        return transactionRmBoxes;
    }

    private static String incrementDateBySeconds(String startDate, int incrementDateBySeconds) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.mmm");
        try {
            Date date = dateFormat.parse(startDate);
            long seconds = date.getTime()/1000;
            seconds+=incrementDateBySeconds;
            date = new Date(seconds*1000);
            String d = dateFormat.format(date);
            return d;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean dateToReached(String startDate, String endDate) {

        if(startDate == null || endDate == null)
            return true;

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.mmm");
        Date dateFrom, dateTo;
        try {
            dateFrom = dateFormat.parse(startDate);
            dateTo = dateFormat.parse(endDate);
            int i = dateFrom.compareTo(dateTo);
            if(i >=0)
                return true;
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }

    }

    private static String getRandomPossibleTamperingDeviceName() {
        String[] name = {"CAM.1","PERCONTA.1","PERCONTA.2","HCM.1"};
        return name[new Random().nextInt(name.length)];
    }


    private static boolean isTransactionNegative(TransactionType type) {
        int nbr = new Random().nextInt(100);
        switch (type){
            case DISPENSE:
            case EMPTY_OUT:
            case EXCHANGE:
            case PURGE:
            case MOVE:
                return true;

            //can be either
            case POSSIBLE_TAMPERING:
                if(nbr<25)
                    return true;
                return false;
            case UNFINISHED_TRANSACTION:
                return new Random().nextBoolean();

            case END_OF_SHIFT:
            case REFILL:
            case MANUAL_REFILL:
                return false;
        }
        return false;
    }

    public static void generateCsV(String fileName, LinkedList<String> data) {

        String suffixPath = "";


        if (fileName.startsWith("systemStatus"))
            suffixPath="\\systemStatuses\\";
        if(fileName.startsWith("transaction"))
            suffixPath="\\transactions\\";
        if(fileName.startsWith("machineContent") || fileName.startsWith("boxContent"))
            suffixPath="\\machineContent\\";
        if(fileName.startsWith("transactionSequence"))
            suffixPath="\\transactionSequences\\";
        if(fileName.startsWith("boxContentBoot"))
            suffixPath="\\machineContent\\whenBooting\\";
        if(fileName.startsWith("boxContentSequence"))
            suffixPath="\\boxContentSequences\\";



        String path = System.getProperty("user.dir")+"\\Jmeter\\bin\\ccc\\testData"+suffixPath;

        try {

            FileWriter fw = new FileWriter(path+fileName);

            for(String row : data)
                fw.write(row+"\n");

            fw.flush();
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static LinkedList<Cashier> getCashiers() {
        LinkedList<Cashier> cashiers = new LinkedList<Cashier>();

        String[] accountType={"Account1","Account2","Card","Barcode","Numeric"};
        List<String> accounts = new LinkedList<>();

        //same accnbr as user id
        accounts.add("1001-"+accountType[0]);
        cashiers.add(new Cashier("Sara Madison","1001","administrator",accounts));

        //different accnbr user id
        accounts = new LinkedList<>();
        accounts.add("10021002-"+accountType[1]);
        cashiers.add(new Cashier("Kosmas Leitner","1002","administrator", accounts));

        //several accounts
        accounts = new LinkedList<>();
        accounts.add(":<>1003557-"+accountType[2]);
        accounts.add("<:>1003558-"+accountType[3]);
        cashiers.add(new Cashier("Paramonos Benetton","1003","administrator", accounts));

        //all accountTypes
        accounts = new LinkedList<>();
        accounts.add("1004-"+accountType[0]);
        accounts.add("10041004-"+accountType[1]);
        accounts.add(":<>1004557-"+accountType[2]);
        accounts.add("<:>1004558-"+accountType[3]);
        accounts.add("10040404-"+accountType[4]);
        cashiers.add(new Cashier("Sharif Westcott","1004","cashier", accounts));

        //no accounts
        accounts = null;
        cashiers.add(new Cashier("Silvius Zahradník","1005","cashier", accounts));
        cashiers.add(new Cashier("Fionola Pál","1006","cashier", accounts));
        cashiers.add(new Cashier("Gwenneg Capello","1007","maintenance", accounts));
        cashiers.add(new Cashier("Mìcheal Prebensen","1008","maintenance", accounts));
        cashiers.add(new Cashier("Hevel Schreier","1009","cit", accounts));
        cashiers.add(new Cashier("Adila Engman","1010","cit2", accounts));

        // not from mum
        cashiers.add(new Cashier("Deana NM Pamplin","1011","cashierNotMum", accounts));
        cashiers.add(new Cashier("Charlotte NM Liao","1012","cashier NotMum", accounts));
        cashiers.add(new Cashier("Darren NM Strawster","1013","cashier Not Mum", accounts));
        cashiers.add(new Cashier("Daria NM Blossom","1014","administrator", accounts));
        return cashiers;
    }

    public static HashMap<String,String[]> getWorkUnits(boolean namedNumbersOnly) {

        HashMap<String,String[]> map = new HashMap<String, String[]>();

        String[] workUnitGroup1;
        String[] workUnitGroup2;
        String[] workUnitGroup3;
        String[] workUnitGroup4;
        String[] workUnitGroup5;
        String[] workUnitGroup6 = null;

        if(namedNumbersOnly){
            workUnitGroup1 = new String[]{"10", "11", "12", "15"};
            workUnitGroup2 = new String[]{"2147483647", "2147483648", "-2147483648", "-2147483649"};
            workUnitGroup3 = new String[]{"105", "123", "145", "300"};
            workUnitGroup4 = new String[]{"99", "100", "101", "0"};
            workUnitGroup5 = new String[]{"1", "-1", "399", "400"};
        }else{

            workUnitGroup1 = new String[]{"JBM-01"/*, "JBM-02", "JBM-03", "JBM-04"*/};
            workUnitGroup2 = new String[]{"Store-01", "Store-02", "Store-03", "Store-04"};
            workUnitGroup3 = new String[]{"MID-01", "MID-02", "MID-03", "MID-04"};
            workUnitGroup4 = new String[]{"HR-01", "HR-02", "HR-03", "HR-04"};
            workUnitGroup5 = new String[]{"SIN-01", "SIN-02", "SIN-03", "SIN-04"};
        //    workUnitGroup6 = new String[]{"WU-LONG-1", "WU-LONG-2-LONGER", "WU-LONG-3-LONGERASDA"};
        }


        map.put("Madrid",workUnitGroup1);
        map.put("Barcelona",workUnitGroup2);
        map.put("Paris",workUnitGroup3);
        map.put("London",workUnitGroup4);
        map.put("Singapore",workUnitGroup5);
        if(workUnitGroup6!=null)
            map.put("Long Workunit Values",workUnitGroup6);

        return map;
    }


    public static List<RmError> getErrors() {

     String[] errorKinds ={   "Configuration","Jam","Communication","Other","IO", "Unknown", "Malfunction", "Busy", "Discrepancy","Power","Reminder","Statistics"};

     List<RmError> alertsToReturn = new LinkedList<>();
     for(int i =0;i<errorKinds.length;i++){

         RmError alert = new RmError();
         alert.CLEARED = false;
         alert.KIND=errorKinds[i];
         alertsToReturn.add(alert);

     }
        return alertsToReturn;
    }
    public static String[] getErrorSeverities() {
        return new String[]{"Error","Warning","Unknown"};
    }

    public static String[] getErrorKinds() {
        String[] errorKinds = {
                "Configuration",
                "Jam",
                "Communication",
                "Other",
                "IO",
                "Unknown",
                "Malfunction",
                "Busy",
                "Discrepancy",
                "Power",
                "Reminder",
                "Statistics",
                "RemoteUpdate",
                "RemoteUpgrade"
        };
        return errorKinds;
    }

    public static String[] getErrorLocations() {
        return new String[]{
                "CoinDeposit",
                "NoteDeposit",
                "CoinDispense",
                "NoteDispense",
                "Cardreader",
                "Barcode",
                "Keypad",
                "Sensor",
                "Network",
                "Alarm",
                "Unknown",
                "Display",
                "DropSafe",
                "OnLine",
                "Software",
                "Database",
                "NoteRecycler",
                "TillDrawer",
                "TransportBox",
                "UPS",
                "Door",
                "Safe"
        };
    }

    public static String getErrortInformation(String severity) {

        List<String> list = new LinkedList();
        switch (severity){
            case "Error":
                list.add("URT.1: Note Recycler: Problems with dispenser Room 3A (EUR 20.00)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 2A (EUR 10.00)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 1A (EUR 5.00)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 4A (EUR 50.00)");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1A (EUR 0.05) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1B (EUR 0.25) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1C (EUR 0.05) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1D (EUR 0.25) is not in place.");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2A (EUR 2.00) is not in place.");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2B (EUR 1.00) is not in place.");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2C (EUR 2.00) is not in place.");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2D (EUR 1.00) is not in place.");
                list.add("URT.1: Reset operation is required [[RCS700 Reset Operation Required]]");
                list.add("URT.1: Lower module out of place [[RCS700 ContactSupport]]");
                list.add("URT.1: UTCS out of place [[RCS700 ContactSupport]]");
                list.add("URT.1: TCS cover open [[TCS CoverOpen]]");
                list.add("URT.1: Unit position error");
                list.add("ICX.1: The machine safety interlock is disabled [[RCSActive ContactSupport]]");
                list.add("URT.1: Note Recycler: Dispenser Room 3A (EUR 20.00) is not in place.");
                list.add("URT.1: Note Recycler: Dispenser Room 2A (EUR 10.00) is not in place.");
                list.add("URT.1: Note Recycler: Dispenser Room 1A (EUR 5.00) is not in place.");
                list.add("URT.1: Note Recycler: Dispenser Room 4A (EUR 50.00) is not in place.");
                list.add("URT.1: Note Recycler: Cashbox (stacker) missing (URT)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 4A (EUR 10.00)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 3A (EUR 20.00)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 2A (EUR 5.00)");
                list.add("URT.1: Note Recycler: Problems with dispenser Room 1A (EUR 1.00)");
                list.add("URT.1: Note Recycler: Inlet tray contains money.");
                list.add("URT.1: LT unit out of place [[RCS700 ContactSupport]]");
                list.add("URT.1: Upper module out of place [[RCS700 ContactSupport]]");
                list.add("URT.1: Transportation check error");
                list.add("URT.1: Note Recycler: Dispenser Room 4A (EUR 10.00) is not in place.");
                list.add("URT.1: BV open [[RCS700 ContactSupport]]");
                list.add("ICX.1: Belt Motor Overcurrent. Check to see if there is a jam in the hopper. Check under the height gauge. [72]");
                list.add("URT.1: Jam in UTCS [[Note Jam]]");
                list.add("URT.1: Jam in upper module [[Note Jam]]");
                list.add("URT.1: The operation failed or device service is not initialized. (5688041)");
                list.add("URT.1: Bills detected in Upper unit or UTCS [57A1202]");
                list.add("URT.1: Bills detected in Upper unit or UTCS [57A1203]");
                list.add("URT.1: Transportation jam  [5669081] [[Note Jam]]");
                list.add("URT.1: Bills detected in Upper unit or UTCS [57A1201]");
                list.add("URT.1: Unexpected transportation  [5590051]");
                list.add("URT.1: Transportation jam  [5678041] [[Note Jam]]");
                list.add("Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK");
                list.add("Software License: Your software license does not support your current hardware configuration.");
                list.add("Software License: Your software license does not support online features (RM). Get a new license or disable RM");
                list.add("PERCONTA.1: Errors during an input or output operation [1020]");
                list.add("PERCONTA.2: Errors during an input or output operation [1020]");
                list.add("LJINTSH.1: Errors during an input or output operation [1020]");
                list.add("RCSCONTROLBOARD.1: RCS Control Board: Error opening communication port: COM40");
                list.add("URT.1: Transportation jam  [5660051] [[Note Jam]]");
                list.add("URT.1: Transportation jam  [5668081] [[Note Jam]]");
                list.add("ICX.1: Lift Motor Overcurrent.Check to see if there is a jam in the lift mechanism or main hopper. [70]");
                list.add("URT.1: Unexpected transportation  [551130A]");
                list.add("URT.1: The operation failed or device service is not initialized. (5480110)");
                list.add("ICX.1: The device is switched offline [1012]");
                list.add("PERCONTA.1: The device is switched offline [1012]");
                list.add("PERCONTA.2: The device is switched offline [1012]");
                list.add("LJINTSH.1: The device is switched offline [1012]");
                list.add("null: RCS Control Board: The device is switched offline");
                list.add("ICX.1: Diameter reading too low [[45-14 Coin Sensor]]");
                list.add("URT.1: Errors during an input or output operation [1020]");
                list.add("URT.1: USB Driver comm error. Hard reset needed [F3F60000]");
                list.add("URT.1: The operation failed or device service is not initialized. (55B8069)");
                list.add("ICX.1: Machine Stopped because the Machine lid was opened  [[Coin Jam Upper]]");
                list.add("URT.1: The operation failed or device service is not initialized. [1015]");
                list.add("URT.1: Command error [570210C] [[RCS700 ContactSupport]]");
                list.add("URT.1: BV error [574FE62] [[RCS700 ContactSupport]]");
                list.add("URT.1: BV error [574FE03] [[RCS700 ContactSupport]]");
                list.add("URT.1: The operation failed or device service is not initialized. (55C1301)");
                list.add("URT.1: Transportation jam  [5668061] [[Note Jam]]");
                list.add("ICX.1: Errors during an input or output operation [1020]");
                list.add("URT.1: Transportation jam  [5668083] [[Note Jam]]");
                list.add("URT.1: The device is switched offline [1012]");
                list.add("URT.1: Transportation motor error [5964500]");
                list.add("Software License: The license manager unexpectedly stopped");
                list.add("URT.1: The operation failed or device service is not initialized. (55C8103)");
                list.add("SMARTLED.1: Device still closed");
                list.add("SMARTLED.1: Errors during an input or output operation [1020]");
                list.add("RCSCONTROLBOARD.1: RCS Control Board: Error opening communication port: COM12");
                list.add("URT.1: The operation failed or device service is not initialized. (568005F)");
                list.add("URT.1: The operation failed or device service is not initialized. (5A71A70)");
                list.add("URT.1: Note Recycler: Dispenser Room 3A (EUR 20.00) is not in place.");
                list.add("URT.1: Note Recycler: Dispenser Room 2A (EUR 5.00) is not in place.");
                list.add("URT.1: Note Recycler: Dispenser Room 1A (EUR 1.00) is not in place.");
                list.add("URT.1: The operation failed or device service is not initialized. (5A51A20)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1A (EUR 0.05)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1B (EUR 0.25)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1C (EUR 0.05)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1D (EUR 0.25)");
                list.add("URT.1: Unexpected transportation  [564009F]");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1A (EUR 0.01)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1B (EUR 0.05)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1C (EUR 0.01)");
                list.add("PERCONTA.1: Coin dispenser 1: Problems with dispenser 1D (EUR 0.05)");
                list.add("URT.1: Transportation jam  [5558061] [[Note Jam]]");
                list.add("URT.1: The operation failed or device service is not initialized. (5A15920)");
                list.add("URT.1: Transportation jam  [5670081] [[Note Jam]]");
                list.add("URT.1: The operation failed or device service is not initialized. (5A14BC1)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2B (EUR 0.10)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2A (EUR 2.00)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2B (EUR 1.00)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2C (EUR 2.00)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2D (EUR 1.00)");
                list.add("URT.1: The operation failed or device service is not initialized. (55A0099)");
                list.add("URT.1: Unexpected transportation  [5647021]");
                list.add("RCSCONTROLBOARD.1: RCS Control Board: Error opening communication port: COM30");
                list.add("URT.1: Recycleur de billets: Problems with dispenser Room 2A (EUR 5.00)");
                list.add("URT.1: Recycleur de billets: Problems with dispenser Room 1A (EUR 1.00)");
                list.add("URT.1: Erreur de position module");
                list.add("URT.1: Recycleur de billets: Dispenser Room 4A (EUR 10.00) is not in place.");
                list.add("URT.1: Recycleur de billets: Dispenser Room 3A (EUR 20.00) is not in place.");
                list.add("URT.1: Recycleur de billets: Dispenser Room 2A (EUR 5.00) is not in place.");
                list.add("URT.1: Recycleur de billets: Dispenser Room 1A (EUR 1.00) is not in place.");
                list.add("URT.1: Recycleur de billets: Cashbox (stacker) missing (URT)");
                list.add("URT.1: Recycleur de billets: Problems with dispenser Room 4A (EUR 10.00)");
                list.add("URT.1: Recycleur de billets: Problems with dispenser Room 3A (EUR 20.00)");
                list.add("URT.1: Capot case de dépôt (TCS) ouvert");
                list.add("URT.1: Une opération de réinitialisation est nécessaire");
                list.add("URT.1: Le module bas n'est pas en place");
                list.add("ICX.1: Device still closed");
                list.add("URT.1: Transportation jam  [5556061] [[Note Jam]]");
                list.add("URT.1: The operation failed or device service is not initialized. (568FFF1)");
                list.add("URT.1: Reset operation is required [[RCS700 ContactSupport]]");
                list.add("URT.1: The operation failed or device service is not initialized. (55C8041)");
                list.add("URT.1: Transportation jam  [5668413] [[Note Jam]]");
                list.add("RECEIPTPRINTER.1: Receipt printer: Unable to print receipt");
                list.add("ICX.1: Starting Error - No encoder signal [[RCSActive ContactSupport]]");
                list.add("ICX.1: Upper Deck Open.The upper deck mechanism is not closed. Check that it is latched properly. [71]");
                list.add("URT.1: The operation failed or device service is not initialized. (5480130)");
                list.add("RCSCONTROLBOARD.1: RCS Control Board: Error opening communication port: COM37");
                list.add("ICX.1: Missing/faulty board [[RCSActive ContactSupport]]");
                list.add("ICX.1: RCS Active CAM: An unknown error occurred during CAM/dispenser configuration.");
                list.add("ICX.1: RCS Active CAM: CAM box configuration wrong: EUR 0.25 does not match dispenser denomination EUR 0.05");
                list.add("Software License: The license is not valid for this machine");
                list.add("ICX.1: RCS Active CAM: CAM box configuration wrong: EUR 0.50 does not match dispenser denomination EUR 0.05");
                list.add("ICX.1: RCS Active CAM: CAM box configuration wrong: EUR 0.05 does not match dispenser denomination EUR 0.25");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1A (EUR 0.25) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1B (EUR 0.05) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1C (EUR 0.25) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1D (EUR 0.10) is not in place.");
                list.add("Software License: License file is missing");
                list.add("com.scancoin.common.statemachine.internal.StateMachine: Internal Software Error");
                list.add("URT.1: Transportation jam  [5665081] [[Note Jam]]");
                list.add("URT.1: Sensor dark check error  [4100C08] [[RCS700 ContactSupport]] ");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2A (EUR 0.25)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2C (EUR 0.25)");
                list.add("PERCONTA.2: Coin dispenser 2: Problems with dispenser 2D (EUR 0.10)");
                list.add("RECEIPTPRINTER.1: Receipt printer: Error constructing XML from layout template: file:/C:/OrchardJ/resources/configurable/i18n/receipts/receipt-printer_en_US_CUST.xml");
                list.add("URT.1: Command error [5721291] [[RCS700 ContactSupport]]");
                list.add("ICX.1: RCS Active CAM: The transaction could not be ended properly.");
                list.add("ICX.1: RCS Active CAM: Could not set the hopper configuration. Check perconta boards and restart.");
                list.add("PERCONTA.1: Device still closed");
                list.add("URT.1: Command error [5721206] [[RCS700 ContactSupport]]");
                list.add("ICX.1: RCS Active CAM: CAM box configuration wrong: EUR 2.00 does not match dispenser denomination EUR 0.25");
                list.add("URT.1: The device is in an exchange state. [6076]");
                list.add("URT.1: Transportation jam  [5665011] [[Note Jam]]");
                list.add("ICX.1: The operation failed or device service is not initialized. [1015]");
                list.add("URT.1: Transportation jam  [5669061] [[Note Jam]]");
                list.add("URT.1: BV error [5930400] [[RCS700 ContactSupport]]");
                list.add("URT.1: The operation failed or device service is not initialized. (55B9069)");
                list.add("ICX.1: Optical Diameter Sensor is dirty [[Coin Jam Upper]]");
                list.add("URT.1: The operation failed or device service is not initialized. (55B5029)");
                list.add("URT.1: The operation failed or device service is not initialized. (5A417E2)");
                list.add("RECEIPTPRINTER.1: Receipt printer: Receipt printer communication error (offline or not connected?)");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2B (EUR 0.10) is not in place.");
                list.add("CIMA.1: Note Unit: Dispenser Roll 3 (EUR 10.00) is not in place.");
                list.add("CIMA.1: Note Unit: Dispenser Roll 4 (EUR 10.00) is not in place.");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2C (SEK 0.05) is not in place.");
                list.add("PERCONTA.2: Coin dispenser 2: Dispenser 2D (EUR 0.01) is not in place.");
                list.add("CIMA.1: Note Unit: Dispenser Roll 2 (EUR 5.00) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1D (SEK 0.02) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1C (SEK 0.02) is not in place.");
                list.add("CAM.1: Coin acceptor: Cashbox (stacker) missing (OTHERCAM)");
                list.add("RECEIPTPRINTER.1: Receipt printer: Error reading template: file:/C:/devEnv/CCoD/370%20beta145/OrchardJ/resources/configurable/i18n/receipts/receipt-printer_en.xml");
                list.add("CIMA.1: Machine error without details [99997]");
                list.add("MEI.1: Note acceptor: Cashbox (stacker) missing (MEI)");
                list.add("CAM.1: Plug & Play error (500)");
                list.add("CAM.1: The device is switched offline [1012]");
                list.add("CAM.1: The size of the Escrow is bigger than the coin box. (441)");
                list.add("CIMA.1: Note Unit: Device error occured during transaction. Please run error resolver.");
                list.add("CIMA.1: cashInStart was not called. (6089)");
                list.add("CIMA.1: Note Unit: Cashbox information corrupt on CIMA");
                list.add("Coin Dispenser 2: The device is switched offline [1012]");
                list.add("Coin Dispenser 1: The device is switched offline [1012]");
                list.add("Software License: Your software license does not support Smart Vaults.");
                list.add("TRANSPORTBOX.1: Transport box: CashBox data/TransportBox is full");
                list.add("MEI.1: The cash box is not correctly inserted (CashboxRemoved). []");
                list.add("MEI.1: Note acceptor: MEI: Bag value exceeded - deposit not available.");
                list.add("Note Acceptor: Note acceptor: Cashbox (stacker) missing (MEI)");
                list.add("Note Acceptor: The cash box is not correctly inserted (CashboxRemoved). []");
                list.add("URT.1: Reset operation is required");
                list.add("Receipt Printer: Receipt printer: Receipt printer communication error (offline or not connected?)");
                list.add("SDK: InputOutputDevice Not connected");
                list.add("SDK: CarWashType1 Not connected");
                list.add("SDK: Xenteo Not connected");
                list.add("SDK: EKNext Not connected");
                list.add("Coin Dispenser 3: Errors during an input or output operation [1020]");
                list.add("Bill Validator: Errors during an input or output operation [1020]");
                list.add("Coin Dispenser 1: Errors during an input or output operation [1020]");
                list.add("Coin Dispenser 2: Errors during an input or output operation [1020]");
                list.add("Coin Recycler: The device is switched offline [1012]");
                list.add("SDK: ObjectDispenser Not connected");
                list.add("SDK: POS Not connected");
                list.add("BarcodeReader: Barcode reader: Error opening communication port: /dev/ttyACM0");
                list.add("SDK: POS Failure");
                list.add("SDK: EKNext Failure");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1B (DKK 1.00) is not in place.");
                list.add("PERCONTA.1: Coin dispenser 1: Dispenser 1A (EUR 2.00) is not in place.");
                list.add("com.scancoin.db.eventhandler.internal.EventMapper: Internal Database Error. Can't store data 'com.scancoin.db.common.api.DbError'");
                list.add("com.scancoin.common.display.internal.VelocityDisplayContentGenerator: Internal Software Error. cannot create template file SwingHtmlPage");
                list.add("Coin Acceptor: The device is switched offline [1012]");
                list.add("Note Acceptor: Errors during an input or output operation [1020]");
                list.add("POSGenerator board CDS: Mainboard: Error opening communication port: COM10");
                list.add("SDK: Xenteo Failure");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 4B (EUR 5.00)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 4A (EUR 200.00)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 3A (EUR 50.00)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 2A (EUR 20.00)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 1A (EUR 10.00)");
                list.add("Note Recycler: Reset operation is required");
                list.add("Receipt Printer: Receipt printer: Receipt printer out of paper");
                list.add("TransportBox: Transport box: CashBox data/TransportBox is full");
                list.add("Coin Dispenser 2: Error occurred that is device related [1025]");
                list.add("Note Recycler: Note Recycler: Cashbox (stacker) missing (URT)");
                list.add("Note Recycler: Note Recycler: Dispenser Room 4A (EUR 200.00) is not in place.");
                list.add("Note Recycler: Note Recycler: Dispenser Room 4B (EUR 5.00) is not in place.");
                list.add("Note Recycler: Lower module out of place");
                list.add("Service Printer: Service printer: Receipt printer communication error (offline or not connected?)");
                list.add("Customer Printer: Customer printer: Receipt printer communication error (offline or not connected?)");
                list.add("CDS9R Control board: The device is switched offline [1012]");
                list.add("Coin Dispenser 2: Coin dispenser 2: Dispenser 2C (SEK 0.05) is not in place.");
                list.add("Coin Dispenser 1: Coin dispenser 1: Dispenser 1A (EUR 2.00) is not in place.");
                list.add("Coin Dispenser 1: Coin dispenser 1: Dispenser 1B (DKK 1.00) is not in place.");
                list.add("Coin Dispenser 1: Coin dispenser 1: Dispenser 1D (SEK 0.02) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 5 (EUR 20.00) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 7 (EUR 50.00) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 6 (EUR 20.00) is not in place.");
                list.add("Coin Acceptor: Coin acceptor: An Article 6 box is needed for the current configuration.");
                list.add("Coin Dispenser 1: Coin dispenser 1: Problems with dispenser 1A (EUR 2.00)");
                list.add("Coin Dispenser 1: Coin dispenser 1: Problems with dispenser 1B (DKK 1.00)");
                list.add("Coin Dispenser 1: Coin dispenser 1: Problems with dispenser 1C (SEK 0.02)");
                list.add("Coin Dispenser 1: Coin dispenser 1: Problems with dispenser 1D (SEK 0.02)");
                list.add("Note Recycler: Note Unit: Dispenser Roll 9 (EUR 500.00) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 4 (EUR 10.00) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 3 (EUR 10.00) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 2 (EUR 5.00) is not in place.");
                list.add("Note Recycler: Note Unit: Dispenser Roll 1 (EUR 5.00) is not in place.");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 1A (CLP 1)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 2A (CLP 2)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 3A (CLP 5)");
                list.add("Note Recycler: Note Recycler: Problems with dispenser Room 4A (CLP 10)");
                list.add("null: Caja fuerte: Door Open");
                list.add("Note Recycler: Unexpected transportation  [5525083]");
                list.add("Note Recycler: TCS cover open");
                list.add("Coin Dispenser 2: Dispensador de monedas 2: Dispenser 2D (CLP 100) is not in place.");
                list.add("Coin Dispenser 2: Dispensador de monedas 2: Dispenser 2C (CLP 500) is not in place.");
                list.add("Coin Dispenser 2: Missing hopper #1 (001)");
                list.add("Coin Dispenser 2: Missing hopper #2 (002)");
                list.add("Coin Dispenser 2: Missing hopper #3 (003)");
                list.add("Coin Dispenser 2: Missing hopper #4 (004)");
                list.add("Coin Dispenser 1: Dispensador de monedas 1: Dispenser 1C (CLP 50) is not in place.");
                list.add("Coin Dispenser 1: Missing hopper #4 (004)");
                list.add("Coin Dispenser 1: Missing hopper #3 (003)");
                list.add("Coin Dispenser 1: Missing hopper #2 (002)");
                list.add("Coin Dispenser 1: Dispositivo fue cambiado a fuera de linea [1012]");
                list.add("Coin Dispenser 2: Dispositivo fue cambiado a fuera de linea [1012]");
                list.add("Coin Dispenser 2: Dispensador de monedas 2: Problems with dispenser 2D (CLP 100)");
                list.add("Coin Dispenser 2: Dispensador de monedas 2: Problems with dispenser 2C (CLP 500)");
                list.add("Coin Dispenser 1: Dispensador de monedas 1: Problems with dispenser 1C (CLP 50)");
                list.add("Coin Dispenser 1: Dispensador de monedas 1: Dispenser 1D (CLP 10) is not in place.");
                list.add("Coin Dispenser 1: Missing hopper #1 (001)");
                list.add("ICX active CAM: Dispositivo fue cambiado a fuera de linea [1012]");
                list.add("Note Recycler: Note Recycler: Inesperado eliminación de caja (URT)");
                list.add("Note Recycler: Unit position error");
                list.add("Note Recycler: LT unit out of place");
                list.add("Note Recycler: BV open");
                list.add("Note Recycler: Upper module out of place");
                list.add("Note Recycler: Bills detected in Upper unit or UTCS [57A1201]");
                list.add("Note Recycler: UTCS out of place");
                list.add("Note Recycler: Jam in upper module");
                list.add("Note Recycler: Jam in UTCS");
                list.add("Note Recycler: Transportation jam  [5669061]");
                list.add("Note Recycler: Note Recycler: Bandeja de entrada contiene dinero");
                list.add("Note Recycler: La operacion fallo o el servicio del dispositivo no esta inicializado (55C8103) ");
                list.add("Note Recycler: BV fan error");
                list.add("Note Recycler: Bills detected in Upper unit or UTCS [57A1203]");
                list.add("Note Recycler: La operacion fallo o el servicio del dispositivo no esta inicializado (5686061) ");
                list.add("Note Recycler: Transportation jam  [5559061]");
                list.add("Note Recycler: Transportation jam  [5557061]");
                list.add("Note Recycler: Bills detected in Upper unit or UTCS [57A1202]");
                list.add("Note Recycler: Transportation jam  [566EEE1]");
                list.add("ICX active CAM: La operacion fallo o el servicio del dispositivo no esta inicializado (Coin belt wear limit reached. Change the belts on the machine and clear the count in the Belts menu.) ");
                list.add("Note Recycler: Transportation jam  [5660051]");
                list.add("Note Recycler: Unexpected transportation  [564FFF1]");
                list.add("Note Recycler: HCM Front door is open [S5110200]");
                list.add("Coin Acceptor: Hopper missing (136)");
                list.add("Coin Dispenser 1: Coin dispenser 1: Dispenser 1C (EUR 0.10) is not in place.");
                list.add("Coin Dispenser 1: Coin dispenser 1: Dispenser 1B (EUR 0.05) is not in place.");
                list.add("Coin Dispenser 1: Coin dispenser 1: Dispenser 1D (EUR 1.00) is not in place.");
                list.add("RCS Control Board: RCS Control Board: Error opening communication port: COM11");
                list.add("Note Recycler: Errors during an input or output operation [1020]");
                list.add("Note Recycler: LF(front door) open [E511021B]");
                list.add("Note Recycler: Note Recycler: Reset operation is required.");
                list.add("Note Recycler: HCM Upper unit is not in position [S5110700]");
                list.add("Note Recycler: HCM URJB door is open [S5110900]");
                list.add("Note Recycler: Note remain in transportation route [E5DC0800]");
                list.add("Note Recycler: Note Recycler: Inlet tray contains money.");
                list.add("Note Recycler: Note Recycler: CashBox HCM is full");
                list.add("Note Recycler: Note Recycler: Cashbox (stacker) missing (HCM)");
                list.add("Note Recycler: Note Transportation jam [E5343304]");
                list.add("Note Recycler: Unexpected transportation [E597255B]");
                list.add("Note Recycler: cashInStart was not called. [6089]");
                list.add("Note Recycler: Command error [E571FF03]");
                list.add("Note Recycler: Note Recycler: Device error occured during transaction. Please run error resolver.");
                list.add("Note Recycler: Unexpected transportation [E5320B65]");
                list.add("Coin Dispenser 2: Coin dispenser 2: Problems with dispenser 2D (EUR 0.25)");
                list.add("Coin Dispenser 2: Coin dispenser 2: Problems with dispenser 2C (EUR 2.00)");
                list.add("Coin Dispenser 2: Coin dispenser 2: Dispenser 2C (EUR 2.00) is not in place.");
                list.add("Coin Dispenser 2: Coin dispenser 2: Dispenser 2D (EUR 0.25) is not in place.");
                list.add("Note Recycler: The operation failed or device service is not initialized. (E5350665)");
                break;
            case "Warning":
                list.add("URT.1: Note Recycler: Box Room 4A (EUR 50.00) was detected empty");
                list.add("PERCONTA.2: Coin dispenser 2: Box 2C (EUR 0.25) was detected empty");
                list.add("PERCONTA.2: Coin dispenser 2: Box 2A (EUR 0.25) was detected empty");
                list.add("URT.1: Note Recycler: Box Room 3A (EUR 20.00) was detected empty");
                list.add("URT.1: Note Recycler: Box Room 4A (EUR 10.00) was detected empty");
                list.add("URT.1: Note Recycler: Box Room 2A (EUR 5.00) was detected empty");
                list.add("URT.1: Note Recycler: Box Room 1A (EUR 1.00) was detected empty");
                list.add("ICX.1: RCS Active CAM: The content of ICX.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 677.20");
                list.add("PERCONTA.2: Coin dispenser 2: Box 2A (EUR 2.00) was detected empty");
                list.add("PERCONTA.2: Coin dispenser 2: Box 2B (EUR 1.00) was detected empty");
                list.add("PERCONTA.2: Coin dispenser 2: Box 2D (EUR 1.00) was detected empty");
                list.add("PERCONTA.1: Coin dispenser 1: Box 1C (EUR 0.25) was detected empty");
                list.add("PERCONTA.2: Coin dispenser 2: Box 2D (EUR 0.10) was detected empty");
                list.add("PERCONTA.1: Coin dispenser 1: Box 1C (EUR 0.01) was detected empty");
                list.add("ICX.1: RCS Active CAM: The content of ICX.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 0.10");
                list.add("ICX.1: Unable to find file {0} [[RCSActive ContactSupport]]");
                list.add("ICX.1: An outlet has batched [[RCSActive ContactSupport]]");
                list.add("ICX.1: RCS Active CAM: The content of ICX.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 6.90");
                list.add("ICX.1: RCS Active CAM: The content of ICX.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 5.10");
                list.add("ICX.1: RCS Active CAM: The content of ICX.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 4.60");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -250.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 35.00");
                list.add("PERCONTA.1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 2.00");
                list.add("PERCONTA.2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 0.10");
                list.add("Startup: Errors occurred during startup");
                list.add("CAM.1: Escrow full (440)");
                list.add("CAM.1: Coins are having trouble leaving the hopper (138)");
                list.add("Coin Dispenser 2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 3.00 EUR 0.04 SEK 0.50");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00 SEK 0.40 DKK 5.00");
                list.add("Note Recycler: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -25.00 NOK -100.00");
                list.add("Coin Acceptor: Coin acceptor: The content of CAM.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -5.98");
                list.add("PERCONTA.1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: DKK 10.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -80.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -110.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -50.00");
                list.add("PERCONTA.2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -25.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -400.00");
                list.add("PERCONTA.1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 265.00");
                list.add("PERCONTA.2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 1.00");
                list.add("PERCONTA.1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 100.00");
                list.add("Coin Dispenser 2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 3.00 SEK 0.50");
                list.add("Note Recycler: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -200.00 NOK -750.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -20.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -100.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -700.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -650.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 10.00");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00 SEK 0.40 DKK 10.00");
                list.add("Coin Dispenser 2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 3.00 EUR 0.10 SEK 0.50");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00 SEK 0.40");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00 SEK 0.20 DKK 5.00");
                list.add("Coin Recycler: Coin Recycler: The content of C2 was changed while RCS800 Software was not running. A transaction has been added with the difference: EUR 7.50");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -15.00");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -525.00");
                list.add("PERCONTA.1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00 SEK 0.40 DKK 10.00");
                list.add("Space: Low on disk space (96Mb remaining)");
                list.add("CIMA.1: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -105.00");
                list.add("Coin Dispenser 2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -78.00 EUR -2.30 SEK -11.50");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -460.00 SEK -9.20 DKK -230.00");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -80.00 SEK -1.00");
                list.add("Coin Dispenser 2: Coin dispenser 2: The content of PERCONTA.2 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -20.00 EUR -0.50");
                list.add("Coin Dispenser 2: Hopper2: Box Hopper2 (TOK 1.00) was detected empty");
                list.add("Note Recycler: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -100.00");
                list.add("Note Recycler: Note Unit: The bag is missing. Notes cannot be moved to the bag.");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 20.00 DKK 5.00");
                list.add("Note Recycler: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -75.00");
                list.add("Coin Dispenser 1: Coin dispenser 1: The content of PERCONTA.1 was changed while CCoD software was not running. A transaction has been added with the difference: DKK 10.00");
                list.add("Note Recycler: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR -150.00");
                list.add("Note Recycler: Note Unit: The content of CIMA.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 360.00");
                break;
            case "Unknown":
                list.add("Something went wrong");
                list.add("A issue occured");
                list.add("Device malfunctioned");
                list.add("Connection Lost to the device");
                break;
        }
        return list.get(new Random().nextInt(list.size()));

    }

    public static String[] getErrorIds() {
        String[] errorIds = new String[100];
        for(int i = 0 ;i<errorIds.length;i++){
            errorIds[i]="10"+i;
        }
        return errorIds;
    }

    public static String getDepositKind(boolean randomKind) {
        String[] depositKinds = {"A","B","AA","AB","BA","BB"};
        return (randomKind == false ? depositKinds[0]: depositKinds[new Random().nextInt(depositKinds.length)]);
    }

    public static String getMixName() {
        String[] mixNames = {"Mix A","Mix B","Mix AA","Mix AB","Mix BA","Mix BB"};
        return mixNames[new Random().nextInt(mixNames.length)];
    }

    public static List<String> getStrapSeals(long TRANSACTION_SEQ, String MACHINE_ID) {
        List<String> list = new LinkedList<>();
        for(int i =0 ;i< new Random().nextInt(10)+1;i++)
            list.add(i+"-"+TRANSACTION_SEQ+":"+MACHINE_ID);
        return list;
    }

    public static Map<String, String> getCustomData(long TRANSACTION_SEQ, Long MESSAGE_SEQUENCE) {
        Map<String,String> customData = new HashMap<>();
        for(int i =0;i<new Random().nextInt(10);i++)
            customData.put("k-"+i+"-"+TRANSACTION_SEQ,"v"+"-"+i+"-"+MESSAGE_SEQUENCE);

        return customData;

    }

    public static List<RmAmount> getExpectedAmounts(String currency, int decimals, double total_amount) {
        List<RmAmount> amounts = new LinkedList<>();
        int size = new Random().nextInt(10)+1;
        for (int i =0; i<size;i++){
            RmAmount a = new RmAmount();
            a.DECIMALS=decimals;
            a.CURRENCY=currency;
            a.TOTAL = (int) total_amount/size;
            amounts.add(a);
        }
        return amounts;
    }

    public static String getTransactionUUID() {
        return UUID.randomUUID().toString();
    }

    public static List<RmMix> getSubMixes() {
        List<RmMix> list = new LinkedList<>();
        for (int i =0; i<new Random().nextInt(10)+1;i++){
            RmMix mix = new RmMix();
            mix.MIX_NAME  =getMixName();
            mix.MIX_DISPLAY_NAME = "D:"+mix.MIX_NAME;
            mix.MIX_EDITED = new Random().nextBoolean();
            list.add(mix);

        }
        return list;
    }

    public static List<RmTransactionCommission> getTransactionCommissions(double totalAmount, double decimals,String currency, boolean oneCommission) {
        List<RmTransactionCommission> list = new LinkedList<>();
        String types[] = {"MAX","MIN","OTHER"};
        int percentages[] = {100,300,500,600,700,800};
        int size = (oneCommission == true ? 1 : (new Random().nextInt(10)+1));
        Random r = new Random();
        for(int i =0; i<size;i++){
            RmTransactionCommission c = new RmTransactionCommission();
            c.CURRENCY=currency;
            c.DECIMALS = (int) decimals;
            c.PERC_DECIMALS =2;
            c.PERCENTAGE = percentages[r.nextInt(percentages.length)];
            c.TOTAL = (long) (totalAmount*((double)c.PERCENTAGE/10000));
            c.TYPE = types[r.nextInt(types.length)];
            list.add(c);

        }

        return list;
    }

    public static String[] getAlertLocations() {
        return new String[]{"CoinDeposit", "NoteDeposit","CoinDispense","NoteDispense"};
    }


    private static List<List<RmUserMuM>> rmUsersPartitioned;
    /**Uses xml/ccodUsersMuM.xml file to read users and generate a pool of mahineUsers to distribute over the ccod sessions.
     * Returns false if the file is missing.
     *
     * If the file is missing, run MachineUsersGenerator in xml package to generate ccodUsers.xml, copy the content to ccodUsersMum.xml
     * */
    public static boolean initMachineUserPool(int amountOfMachines) {

        try {
            List<RmUserMuM> rmUsersPool = MachineUsersGenerator.readUsersFromFile("ccodUsersMum.xml");
            rmUsersPartitioned =  Lists.partition(rmUsersPool,rmUsersPool.size()/amountOfMachines);

        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
            System.out.println("disabling machine user sync");
            return false;
        }
        return true;
    }

    public static List<RmUserMuM> getMachineUsers(int machineNbr) {

        if(rmUsersPartitioned==null || machineNbr<1 || machineNbr>rmUsersPartitioned.size())
            return null;

        return rmUsersPartitioned.get(machineNbr-1);
    }

    public static RmUserMuM randomizeUserUpdateVals(RmUserMuM userToEdit, String machineUUID, long sequennce) {
        Random rng = new Random();
        int decision = 0; //0=null, 1 = edit; 2 = keep as is
        RmUserMuM user = Utilities.cloneMachineUser(userToEdit);
        decision = rng.nextInt(3);
        if(decision==0)
            user.ALIAS=null;
        if(decision==1)
            user.ALIAS="-editAlias-"+machineUUID+"-"+sequennce;

        decision = rng.nextInt(3);
        if(decision==0)
            user.CARD_IDENTIFIER=null;
        if(decision==1)
            user.CARD_IDENTIFIER="-editCardIdentifier-"+machineUUID+"-"+sequennce;

        decision = rng.nextInt(3);
        if(decision==0)
            user.NAME=null;
        if(decision==1)
            user.NAME="-editName-"+machineUUID+"-"+sequennce;

        decision = rng.nextInt(3);
        if(decision==0)
            user.PIN=null;
        if(decision==1)
            user.PIN="-editPin-"+machineUUID+"-"+sequennce;

        decision = rng.nextInt(3);
        if(decision==0)
            user.ROLE_NAME=null;
        if(decision==1)
            user.ROLE_NAME="-editRoleName-"+machineUUID+"-"+sequennce;

        decision = rng.nextInt(3);
        if(decision==0)
            user.CHANGE_PIN_ON_LOGIN=null;
        if(decision==1)
            user.CHANGE_PIN_ON_LOGIN=rng.nextBoolean();

        decision = rng.nextInt(3);
        if(decision==0)
            user.NO_PIN=null;
        if(decision==1)
            user.NO_PIN=rng.nextBoolean();

        decision = rng.nextInt(3);
        if(decision==0)
            user.NO_PIN_BIO=null;
        if(decision==1)
            user.NO_PIN_BIO=rng.nextBoolean();

        decision = rng.nextInt(3);
        if(decision==0)
            user.NO_PIN_CARD=null;
        if(decision==1)
            user.NO_PIN_CARD=rng.nextBoolean();

        decision = rng.nextInt(3);
        if(decision==0)
            user.NO_PIN_DOOR=null;
        if(decision==1)
            user.NO_PIN_DOOR=rng.nextBoolean();

        decision = rng.nextInt(3);
        if(decision==0)
            user.NO_PIN_USER_ID=null;
        if(decision==1)
            user.NO_PIN_USER_ID=rng.nextBoolean();

        decision = rng.nextInt(3);
        if(decision==0)
            user.NBR_OF_BAD_PIN=null;
        if(decision==1)
            user.NBR_OF_BAD_PIN=rng.nextInt(10)-1;

        decision = rng.nextInt(3);
        if(decision==0)
            user.NBR_OF_LOGINS=null;
        if(decision==1)
            user.NBR_OF_LOGINS=rng.nextInt(10)-1;

        decision = rng.nextInt(3);
        if(decision==0)
            user.VALID_FROM=null;
        if(decision==1)
            user.VALID_FROM="20210101 00:00:00.000";

        decision = rng.nextInt(3);
        if(decision==0)
            user.VALID_UNTIL=null;
        if(decision==1)
            user.VALID_UNTIL="20200731 00:00:00.000";

        decision = rng.nextInt(3);
        if(decision==0)
            user.DOORS=null;
        if(decision==1)
            user.DOORS=DataGenerator.getUserMuMDoors();

        decision = rng.nextInt(3);
        if(decision==0)
            user.LIST=null;
        if(decision==1)
            user.LIST=rng.nextBoolean();

        return user;
    }

    private static List<String> getUserMuMDoors() {
        String[] doorsArr = {"CIMA.1.COVER","CIMA.1.SAFE","COIN_DISPENSER","NOTE_ACCEPTOR_BOTTOM","NOTE_ACCEPTOR_TOP","NOTE_DISPENSER","TILL_BOX","TILL_DRAWER","TRANSPORTBOX_DRAWER"};
        List<String> doors = new LinkedList<>();
        int amount = new Random().nextInt(doorsArr.length);
        if(amount==0)
            amount = 1;
        for(int i = 0; i< amount;i++){

            doors.add(doorsArr[i]);
        }
        return doors;
    }

    public static LinkedList<Cashier> getCashiersDLS() throws IOException, SAXException, ParserConfigurationException {
        LinkedList<Cashier> cashiers  = getCashiers();
        List <RmUserMuM> rmusers = MachineUsersGenerator.readUsersFromFile("ccodUsersStaticDLS.xml");

        if (rmusers==null ||rmusers.isEmpty())
            return cashiers;
        else{
            cashiers = new LinkedList<>();
            for(RmUserMuM rmuser : rmusers){
                List account = new LinkedList();
                account.add(rmuser.USER_ID+"-DLS");
                cashiers.add(new Cashier(rmuser.NAME,rmuser.USER_ID,rmuser.ROLE_NAME, account));
            }
        }
        return cashiers;
    }

    public static HashMap<String, String[]> getWorkUnitsDLS() throws IOException, SAXException, ParserConfigurationException {


        List<RmWorkUnit> wus = WorkUnitReader.readWorkUnits("workUnitsDLS.xml");

        if(wus==null ||wus.isEmpty())
            return getWorkUnits(true);
        else{

            HashMap<String,String[]> workunits = workunits = new HashMap();
            List<String> wuArr = new LinkedList<>();
            String wuName="";
            String key="";
            for(int i = 0; i<wus.size();i++){
                if(wuName.isEmpty()){
                    wuArr.add(wus.get(i).NAME);
                    wuName = wus.get(i).NAME;
                    key = wus.get(i).PARENT.NAME;
                }
                if(i>0){
                    //    p = wus.get(i).PARENT.NAME;
                    String previousPrefix = wuArr.get(wuArr.size()-1).substring(0,3);
                    String currentPrefix = wus.get(i).NAME.substring(0,3);
                    if(currentPrefix!=null &&  previousPrefix.equalsIgnoreCase(currentPrefix) && i != wus.size()-1 ){
                        wuArr.add(wus.get(i).NAME);
                    }else {
                        String[] payload =wuArr.toArray(new String[0]);
                        workunits.put(key, payload);
                        wuArr = new LinkedList<>();
                        wuArr.add(wus.get(i).NAME);
                        key = wus.get(i).PARENT.NAME;
                    }

                }
            }
            return workunits;
        }
    }

}
