package data;

public class AccountingDateOffset {
    int day, hour, minute;

    public AccountingDateOffset(int day, int hour, int minute){
        this.day=day;
        this.hour=hour;
        this.minute=minute;
    }
}
