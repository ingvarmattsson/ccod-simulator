package createCbData;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);
	private static final String DATABASE_NAME_SUFFIX = "-data";
	private static final String RM_DATE_FORMAT = "yyyyMMdd HH:mm:ss.SSS";

	private Utils() {
	}

	public static <T1, T2> List<T2> transformList(List<T1> rmList, Class<T2> apiType) {
		if (rmList == null) {
			return null;
		} else {
			try {
				List<T2> list = new ArrayList<>();
				for (T1 rmObject : rmList) {
					list.add(apiType.getConstructor(rmObject.getClass()).newInstance(rmObject));
				}
				return list;
			} catch (Exception e) {
				throw new IllegalArgumentException(e);
			}
		}
	}

	public static <T1, T2> List<T2> mapList(List<T1> list, Function<T1, T2> mapper) {
		return nullSafeList(list).stream().map(mapper).collect(Collectors.toList());
	}

	public static <T> T useIfPresent(T replacement, T fallback) {
		if (replacement != null)
			return replacement;
		return fallback;
	}

	public static <T, E> E ifPresent(T base, Function<T, E> function) {
		if (base != null)
			return function.apply(base);
		return null;
	}

	public static <T, E> E ifPresent(T base, Function<T, E> function, E fallback) {
		E value = ifPresent(base, function);
		if (value != null)
			return value;
		return fallback;
	}

	public static <T> List<T> nullSafeList(List<T> list) {
		return Optional.ofNullable(list).orElse(Collections.emptyList());
	}

	public static <T> List<T> nullSafeList(T[] in) {
		return Optional.ofNullable(in).map(Arrays::asList).orElse(Collections.emptyList());
	}

	public static String rmDateTimeToUtc(String rmDateTime, ZoneOffset utcOffset) {
		if (rmDateTime == null || rmDateTime.isEmpty()) {
			return rmDateTime;
		}
		try {
			return LocalDateTime.parse(rmDateTime, DateTimeFormatter.ofPattern(RM_DATE_FORMAT)).toInstant(utcOffset)
					.toString();
		} catch (Exception e) {
			logger.error("Failed to convert date time to UTC: " + rmDateTime, e);
		}
		return null;
	}

	public static String utcToRmDateTime(String utcDateTime, ZoneOffset utcOffset) {
		if (utcDateTime == null || utcDateTime.isEmpty()) {
			return utcDateTime;
		}
		try {
			return Instant.parse(utcDateTime).atOffset(utcOffset).toLocalDateTime()
					.format(DateTimeFormatter.ofPattern(RM_DATE_FORMAT));
		} catch (Exception e) {
			logger.error("Failed to convert UTC to RM date time: " + utcDateTime, e);
		}
		return null;
	}

	public static ZoneOffset parseOffset(String offsetString) {
		ZoneOffset zoneOffset = ZoneOffset.UTC;
		try {
			if (offsetString == null || offsetString.isEmpty()) {
//				logger.warn("No UTC offset specified, using UTC");
			} else {
				zoneOffset = ZoneOffset.of(offsetString);
			}
		} catch (Exception e) {
			logger.error("Failed to parse time zone offset: " + offsetString, e);
		}
		return zoneOffset;
	}

	public static boolean isInteger(String s) {
		if (s == null || s.isEmpty()) {
			return false;
		}
		for (int index = 0; index < s.length(); index++) {
			if (!Character.isDigit(s.charAt(index)))
				return false;
		}
		return true;
	}





	public static List<String> splitListParam(String param) {
		if (param != null) {
			if (param.startsWith("(")) {
				param = param.substring(1);
			}
			if (param.endsWith(")")) {
				param = param.substring(0, param.length() - 1);
			}
			return Arrays.asList(param.split("[ ]"));
		}
		return null;
	}

	public static <T> Predicate<T> distinctByProperty(Function<? super T, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	public static <T> List<T> onlyInFirst(List<T> a, List<T> b) {
		List<T> onlyA = new ArrayList<>(a);
		onlyA.removeAll(b);
		return onlyA;
	}

	public static <T> List<T> onlyInFirst(List<T> a, List<T> b, Comparator<T> comp) {
		List<T> result = new ArrayList<>(a);
		for (T t : a) {
			if (containsByComparator(b, t, comp)) {
				result.remove(t);
			}
		}
		return result;
	}

	public static <T> boolean containsByComparator(List<T> l, T e, Comparator<T> c) {
		return l.stream().anyMatch(t -> c.compare(e, t) == 0);
	}

	public static <T> List<T> intersection(List<T> a, List<T> b, Comparator<T> c) {
		return a.stream().filter(a1 -> b.stream().anyMatch(b1 -> c.compare(a1, b1) == 0)).collect(Collectors.toList());
	}

	@SafeVarargs
//	@SuppressWarnings("unchecked")
	public static <T> List<T> mergeList(List<T>... lists) {
		return Arrays.asList(lists).stream().flatMap(List::stream).collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> toOneList(Object... element) {
		return Arrays.asList(element).stream().flatMap(e -> {
			if (e instanceof List) {
				return (Stream<T>) ((List<T>) e).stream();
			}
			return Stream.of((T) e);
		}).collect(Collectors.toList());
	}

	public static JsonObject filter(JsonObject je, String fl) {
		if (fl == null) {
			return je;
		}
		JsonObject result = new JsonObject();
		String[] fields = fl.split("[ ]");
		for (String f : fields) {
			String[] fs = f.split("[.]");
			if (fs.length > 1) {
				result.add(fs[0], filter(je.get(fs[0]).getAsJsonObject(), f.substring(fs[0].length() + 1)));
			} else {
				result.add(f, je.getAsJsonObject().get(fs[0]));
			}
		}
		return result;
	}



	public static <T> JsonElement filterList(JsonArray list, String fl) {
		list.forEach(je -> Utils.filter2(je.getAsJsonObject(), fl));
		return list;
	}



	public static JsonObject filterCollectionResponse(JsonObject collectionResponse, String fl) {
		collectionResponse.get("results").getAsJsonArray().forEach(je -> Utils.filter2(je.getAsJsonObject(), fl));
		return collectionResponse;
	}

	public static JsonElement filter2(JsonElement in, String fl) {
		if (fl != null) {
			filterR(in, mapList(Arrays.asList(fl.split("[ ]")), s -> Arrays.asList(s.split("[.]"))));
		}
		return in;
	}

	private static JsonElement filterR(JsonElement in, List<List<String>> fl) {
		if (in.isJsonObject()) {
			for (String k : in.getAsJsonObject().entrySet().stream().map(e -> e.getKey()).collect(Collectors.toSet())
					.toArray(new String[0])) {
				boolean keep = false;
				for (List<String> f : fl) {
					if (k.equals(f.get(0))) {
						keep = true;
						if (f.size() > 1) {
							filterR(in.getAsJsonObject().get(k),
									fl.stream().filter(l -> l.get(0).equals(f.get(0))).map(l -> l.subList(1, l.size()))
											.filter(l -> !l.isEmpty()).collect(Collectors.toList()));
						}
					}
				}
				if (!keep) {
					in.getAsJsonObject().remove(k);
				}
			}
			return in;
		} else if (in.isJsonArray()) {
			in.getAsJsonArray().forEach(je -> filterR(je, fl));
			return in;
		}
		throw new IllegalArgumentException("Can only filter arrays and objects");
	}

	public static <T> List<T> paginate(List<T> list, int pageSize, int page) {
		int from = page(page) * pageSize - pageSize;
		int to = page(page) * pageSize;

		if (from > list.size()) {
			return Collections.emptyList();
		}

		if (list.size() < to) {
			to = list.size();
		}
		return list.subList(from, to);
	}

	public static int page(Integer page) {
		return Optional.ofNullable(page).filter(i -> i > 0).orElse(1);
	}

	public static String preferredJson(String json, boolean moreQuotation) {
		return moreQuotation ? json.replace("\"", "\\\"") : json.replace("\\\"", "\"");
	}
}
