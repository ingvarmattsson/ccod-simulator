package createCbData;


import java.util.*;
import java.util.stream.Collectors;

public class CashAmountList extends ArrayDeque<CashAmount> {
	private static final long serialVersionUID = 1L;

	public CashAmountList(Collection<CashAmount> amounts) {
		super(amounts != null ? amounts : Collections.emptyList());
	}

	public CashAmountList() {
		super();
	}

	public CashAmountList(List<RmCashAmount> in) {
		// Inlined "if" because otherwise it gets funky with having call to super as
		// first statement in constructor
		super(in != null ? in.stream().map(CashAmount::new).collect(Collectors.toList()) : Collections.emptyList());
	}

	public CashAmountList(CashAmount... amounts) {
		super(Arrays.asList(amounts));
	}

	public static CashAmountList fromList(List<CashAmount> cas) {
		return new CashAmountList(
				Utils.nullSafeList(cas).toArray(new CashAmount[Optional.ofNullable(cas).map(l -> l.size()).orElse(0)]));
	}

	public CashAmountList(long amount, int decimals, String currency) {
		this(new CashAmount(amount, decimals, currency));
	}

	public CashAmountList(ValueList collect) {
		super(collect != null ? collect.stream().map(CashAmount::new).collect(Collectors.toList())
				: Collections.emptyList());
	}

	public CashAmount addReturns(CashAmount ca) {
		return super.add(ca) ? ca : null;
	}

	public CashAmount get(String currency) {
		for (CashAmount ca : this) {
			if (ca.currency.equals(currency)) {
				return ca;
			}
		}
		throw new IllegalArgumentException();
	}

	public CashAmount getType(String type) {
		for (CashAmount ca : this) {
			if (ca.type.equals(type)) {
				return ca;
			}
		}
		return new CashAmount(0, 0, "", type);
	}

	public boolean hasType(String type) {
		return this.stream().anyMatch(ca -> ca.type.equals(type));
	}

	/**
	 * Adds cash amount to existing cash amount or adds a copy of it if no matching
	 * currency was found.
	 */
	public void append(CashAmount ca) {
		this.stream().filter(ca::canOperate).findFirst().orElseGet(() -> addReturns(ca.copy().zero())).addIfCan(ca);
	}

	public void appendType(CashAmount ca) {
		this.stream().filter(ca::canOperateSameType).findFirst().orElseGet(() -> addReturns(ca.copy().zero()))
				.addIfCan(ca);
	}

	public void appendType(CashAmountList cal) {
		cal.forEach(this::appendType);
	}

	public void append(Value v) {
		append(new CashAmount(v));
	}

	public void append(CashAmountList cal) {
		cal.forEach(this::append);
	}

	public void append(ValueList vList) {
		vList.forEach(this::append);
	}

	public void append(List<Value> vList) {
		vList.forEach(this::append);
	}

	public void append(Collection<Value> values) {
		values.forEach(this::append);
	}

	/**
	 * Performs @this - cal
	 */
	public CashAmountList subtractAll(CashAmountList cal) {
		cal.forEach(this::subtract);
		return this;
	}

	public void subtract(CashAmount ca) {
		for (CashAmount ownca : this) {
			if (ownca.canOperate(ca)) {
				ownca.subtractIfCan(ca);
				return;
			}
		}
		add(ca.copy().invertAmount());
	}

	public List<RmCashAmount> toRmCashAmountList() {
		List<RmCashAmount> rmlist = new LinkedList<>();
		this.forEach(ca -> rmlist.add(ca.toRmCashAmount()));
		return rmlist;
	}

	public static CashAmountList getListWithZeroes(CashAmountList cal) {
		CashAmountList r = new CashAmountList();
		cal.forEach(ca -> r.add(new CashAmount(0, ca.decimals, ca.currency, ca.type)));
		return r;
	}

	public CashAmountList copyToInvertedValues() {
		CashAmountList inverted = new CashAmountList();
		this.forEach(ca -> inverted.add(ca.copy().invertAmount()));
		return inverted;
	}

	public CashAmountList deepCopy() {
		CashAmountList copy = new CashAmountList();
		this.forEach(ca -> copy.add(ca.copy()));
		return copy;
	}

	// Returns first object with currency
	public CashAmount getCurrency(String currency) {
		return stream().filter(ca -> ca.currency.equals(currency)).findFirst()
				.orElseGet(() -> new CashAmount(0, 0, currency, null));
	}

	public CashAmount getCurrencyType(String currency, String type) {
		return stream().filter(ca -> ca.currency.equals(currency)).filter(ca -> ca.type.equals(type)).findFirst()
				.orElseGet(() -> new CashAmount(0, 0, currency, type));
	}

	public CashAmount getFirstOrDefault(CashAmount def) {
		if (!isEmpty()) {
			return getFirst();
		}
		return def;
	}

	public CashAmount getFirstOrEmpty() {
		return getFirstOrDefault(new CashAmount(0, 0, "", ""));
	}

	public boolean nullSafeAddAll(Collection<CashAmount> cac) {
		if (cac != null) {
			super.addAll(cac);
			return true;
		}
		return false;
	}

	public boolean hasCurrency(String currency) {
		return stream().anyMatch(ca -> currency.equals(ca.currency));
	}

	public CashAmount get(int index) {
		Optional<CashAmount> op = stream().skip(index).findFirst();
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}

	public String toStringWithCurrency() {
		return this.stream().map(CashAmount::toCanonicalWithCurrency).collect(Collectors.joining(", "));
	}

	public int hashCode() {
		return super.hashCode();
	}

}
