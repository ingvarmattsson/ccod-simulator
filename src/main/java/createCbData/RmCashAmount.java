package createCbData;

import rmObjects.RmValue;

public class RmCashAmount extends RmValue implements Rm {
	public long AMOUNT;
	public int DECIMALS;
	public String CURRENCY;
	public String TYPE;
}
