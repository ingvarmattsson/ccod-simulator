package createCbData;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import data.DataGenerator;
import data.Utilities;
import org.xml.sax.SAXException;
import rmObjects.*;
import xml.MachineUsersGenerator;

import javax.rmi.CORBA.Util;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class CbDataGenerator {
	static int numusers = 10;

	static String machineId = "JM-01";
	static String siteId = "75e442e6-470e-489a-a859-7d2e97680e07";
	static String currency = "EUR";

	static String beginShiftPeriod = "2021-06-27T00:00:00Z";
	static String endShiftPeriod = "2021-06-30T00:00:00Z";
	static String timeZone = "+01:00";
	static int endOfDaySeconds = 7200; // "+02:00"

	static File outputFile = new File(
			"D:\\devEnv\\workspace\\ccod-simulator\\rmDocuments\\cbSalesDemoEu1.rm");
	static String fileNameSuffix;

	static String[] boynames;
	static String[] girlnames;
	static String[] surnames;
	// Weighted
	static String[] userRoles = {"Manager", "Cashier", "Cashier", "Cashier",
			"Cashier", "Cashier"};
	static String[] workunitsName = {"Food court", "The Store",
			"Store entrance", "Store second entrance", "Help desk",
			"The Store 2", "The Store 3 ", "The Store 4"};
	static String[] workunits = {"0001", "0002", "0003", "0004", "0005", "0002",
			"0002 ", "0002"};

	static long messageSequence = 0;
	static long transSeq = 0;
	static boolean first = true;

	static ObjectMapper om;
	static JsonParser parser = new JsonParser();

	static Random r = new Random();

	private static List<RmUserCommon> users;
	private static String fileNameMachineUsers="ccodUsersZReport.xml";

	public CbDataGenerator(String hardwareId, String siteToken, int numUsersZreport, String fromDateZReport, String toDateZReport, String timeZoneOffset, int endOfDayTime, String currency, String fileName) {
		machineId=hardwareId;
		siteId=siteToken;
		numusers=numUsersZreport;
		this.currency=currency;
		beginShiftPeriod=fromDateZReport;
		endShiftPeriod=toDateZReport;
		timeZone=timeZoneOffset;
		endOfDaySeconds=endOfDayTime;
		outputFile = new File(
				"D:\\devEnv\\workspace\\ccod-simulator\\rmDocuments\\"+fileName);
		fileNameSuffix = fileName;

	}

	public String generateZReport() throws IOException {
		om = new ObjectMapper();
		om.setSerializationInclusion(Include.NON_NULL);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		boynames = Utilities.getNames("BoyNames");
		girlnames = Utilities.getNames("GirlNames");
		surnames = Utilities.getNames("Surnames");

		LinkedList<RmMessageHeader> rmMessages = new LinkedList<>();
		if(users == null || users.isEmpty())
			users = MachineUsersGenerator.readMachineUsers(fileNameMachineUsers,numusers);

		Instant current = Instant.parse(beginShiftPeriod);
		int days = 0;
		while (current.isBefore(Instant.parse(endShiftPeriod))) {
			generateShifts(users, rmMessages, current.toString());
			current = current.plusSeconds(24 * 3600);
			days++;
		}

		Collections.sort(rmMessages, new Comparator<RmMessageHeader>() {
			@Override
			public int compare(RmMessageHeader o1, RmMessageHeader o2) {
				return o1.DATE_TIME.compareTo(o2.DATE_TIME);
			}
		});

		FileWriter fw = new FileWriter(outputFile);

		for (RmMessageHeader rm : rmMessages) {
			rm.MESSAGE_SEQUENCE = messageSequence++;
			String message;
			if (rm instanceof RmTransaction) {
				((RmTransaction) rm).TRANSACTION_SEQ = transSeq++;
				message = om.writeValueAsString(rm);
			} else {
				message = om.writeValueAsString(rm);
				JsonObject jo = parser.parse(message).getAsJsonObject();
				jo.remove("VERSION_NUMBER"); // Important!
				message = jo.toString();
			}
			fw.write(message);
			fw.write('\n');
			System.out.println(message);
		}
		fw.close();
		System.out.println(days * 3 * numusers);
		return machineId+"<>"+fileNameSuffix;
	}

	public static void main(String... args) throws IOException {
		om = new ObjectMapper();
		om.setSerializationInclusion(Include.NON_NULL);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		boynames = Utilities.getNames("BoyNames");
		girlnames = Utilities.getNames("GirlNames");
		surnames = Utilities.getNames("Surnames");

		LinkedList<RmUserCommon> users = new LinkedList<>();
		LinkedList<RmMessageHeader> rmMessages = new LinkedList<>();
		generateUser(users);

		Instant current = Instant.parse(beginShiftPeriod);
		int days = 0;
		while (current.isBefore(Instant.parse(endShiftPeriod))) {
			generateShifts(users, rmMessages, current.toString());
			current = current.plusSeconds(24 * 3600);
			days++;
		}

		Collections.sort(rmMessages, new Comparator<RmMessageHeader>() {
			@Override
			public int compare(RmMessageHeader o1, RmMessageHeader o2) {
				return o1.DATE_TIME.compareTo(o2.DATE_TIME);
			}
		});

		FileWriter fw = new FileWriter(outputFile);

		for (RmMessageHeader rm : rmMessages) {
			rm.MESSAGE_SEQUENCE = messageSequence++;
			String message;
			if (rm instanceof RmTransaction) {
				((RmTransaction) rm).TRANSACTION_SEQ = transSeq++;
				message = om.writeValueAsString(rm);
			} else {
				message = om.writeValueAsString(rm);
				JsonObject jo = parser.parse(message).getAsJsonObject();
				jo.remove("VERSION_NUMBER"); // Important!
				message = jo.toString();
			}
			fw.write(message);
			fw.write('\n');
			System.out.println(message);
		}
		fw.close();
		System.out.println(days * 3 * numusers);
	}

	private static void generateUser(LinkedList<RmUserCommon> users) {
		HashSet<String> names = new HashSet<>();

		while (users.size() < numusers) {
			String name = generateName();
			if (names.contains(name)) {
				continue;
			}
			names.add(name);
			RmUserCommon ru = new RmUserCommon();
			ru.NAME = name;
			ru.USER_ID = String.format("%7s", "" + users.size()).replace(' ',
					'0');
			ru.ROLE_NAME = userRoles[r.nextInt(userRoles.length)];
			users.add(ru);
		}
	}

	private static void generateShifts(List<RmUserCommon> users,
                                       LinkedList<RmMessageHeader> rmMessages, String day) {
		Instant dayStart = Instant.parse(day).plusSeconds(endOfDaySeconds)
				.minusSeconds(ZoneOffset.of(timeZone).getTotalSeconds());

		int count = 0;

		for (RmUserCommon ru : users) {
			if (count % 2 == 0) {
				rmMessages.addAll(generateShift(ru,
						dayStart.plusSeconds(8 * 3600),
						dayStart.plusSeconds(13 * 3600), Instant.parse(day)));
			} else {
				rmMessages.addAll(generateShift(ru,
						dayStart.plusSeconds(12 * 3600),
						dayStart.plusSeconds(17 * 3600), Instant.parse(day)));
			}
			count++;
		}

	}

	private static List<RmMessageHeader> generateShift(RmUserCommon ru,
                                                       Instant startOfShift, Instant endOfShift, Instant accountingDate) {
		int startOfShiftFuzzing = (int) Math
				.round(r.nextGaussian() * 3600 * 1.5);
		int endOfShiftFuzzing = (int) Math.round(r.nextGaussian() * 3600 * 1.5);

		startOfShift = startOfShift.plusSeconds(startOfShiftFuzzing);
		endOfShift = endOfShift.plusSeconds(endOfShiftFuzzing);

		if (endOfShift.isBefore(startOfShift)) {
			endOfShift = startOfShift.plus(20, ChronoUnit.MINUTES);
		}

		int diff;

		if (r.nextInt(2) == 0) {
			diff = (int) Math.round(r.nextGaussian() * 50);
		} else {
			diff = 0;
		}
		int sale = (int) Math.round(Math.abs(r.nextGaussian() * 50000));

		int[] mixs = {-29000, -26500, -20000};

		int dispense = mixs[r.nextInt(mixs.length)];

		RmTransaction dispenseTrans = generateTransaction(startOfShift, ru,
				"DISPENSE", dispense, 2, currency, timeZone);

		int returnAmount = (int) Math
				.round(Math.abs(r.nextGaussian() * 0.33) * sale);

		CashAmount returns = new CashAmount(-1 * returnAmount, 2, currency,
				"CASH_RETURNS");
		CashAmount sales = new CashAmount(sale + returnAmount, 2, currency,
				"CASH_SALES");

		String workunit = "None";

		int workunitindex = r.nextInt(workunitsName.length);
		ru.WORK_UNIT_NAME = workunitsName[workunitindex];
		workunit = workunits[workunitindex];

		RmZReport zr = generateZReport(ru, accountingDate.toString(),
				endOfShift.plusSeconds(180).toString(), timeZone, workunit,
				sales, returns);

		RmTransaction eosTrans = generateTransaction(endOfShift, ru,
				"END_OF_SHIFT", -1 * dispense + sale + diff, 2, currency,
				timeZone);

		LinkedList<RmMessageHeader> list = new LinkedList<>();
		list.add(dispenseTrans);
		list.add(eosTrans);
		list.add(zr);

		long eos = -1 * dispense + sale + diff;
		long total = (dispense + eos) - sale - diff;

		if (total != 0L) {
			System.out.println(total + "---------------");
			System.out.println("diff:" + diff);
			System.out.println("sale: " + sale);
			System.out.println("actual: " + (eos + dispense));

			System.exit(1);
		}
		return list;
	}

	public static RmTransaction generateTransaction(Instant dateTime,
                                                    RmUserCommon user, String type, long totalAmount, int decimals,
                                                    String currency, String timeZone) {
		RmTransaction next = new RmTransaction();
		next.USER = user;
		next.VALUE_BAGS = new LinkedList<>();
		next.MACHINE_ID = next.UUID = machineId;
		next.RECEIVER = "Server";
		RmValue v = new RmValue();
		v.TOTAL = totalAmount;
		v.DECIMALS = decimals;
		v.CURRENCY = currency;
		RmValueBag vb = new RmValueBag();
		vb.VALUES = new LinkedList<>();
		vb.VALUES.add(v);
		next.VALUE_BAGS.add(vb);
		next.TYPE = type;
		next.TIME_ZONE_UTC_OFFSET = timeZone;
		next.DATE_TIME = Utils.utcToRmDateTime(dateTime.toString(),
				Utils.parseOffset(timeZone));
		next.VERSION = "3.1.2";

		next.TOTAL_AMOUNT = ((double) totalAmount) / 100;
		next.ACCOUNTING_DATE = next.DATE_TIME;
		next.COMMISSION = 0;
		next.CURRENCY = currency;
		next.DECIMALS = decimals;
		next.REFERENCE = "Reference";

		next.MESSAGE_TYPE = "Transaction";
		return next;
	}

	static RmZReport generateZReport(RmUserCommon user, String accountingDate,
                                     String shiftDateTime, String timeZone, String workunit,
                                     CashAmount... cashAmounts) {
		ZReport zr = new ZReport();
		zr.user = new PointOfSaleUser(user.USER_ID, user.NAME);
		zr.dateTime = zr.accountingDate = accountingDate;
		zr.shiftDateTimeUtc = shiftDateTime;
		zr.cashValues = new CashAmountList(cashAmounts);
		zr.workUnitName = user.WORK_UNIT_NAME;
		zr.workUnit = workunit;
		zr.posAdapterId = zr.machineId = machineId;
		zr.shiftLocalTimeUtcOffset = zr.timeZoneUtcOffset = timeZone;
		zr.outlet = "The store";
		zr.siteId = siteId;
		zr.openingAmount = null;
		zr.token = zr.user.userId + "-" + zr.workUnit + "-"
				+ shiftDateTime.replace(":", "");
		return zr.toRmZReport();
	}

	private static String generateName() {
		StringBuilder name = new StringBuilder();
		if (r.nextInt(2) == 0) {
			name.append(boynames[r.nextInt(boynames.length)]);
		} else {
			name.append(girlnames[r.nextInt(girlnames.length)]);
		}
		name.append(" ");

		name.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(r.nextInt(26)));

		name.append(". ");

		name.append(surnames[r.nextInt(surnames.length)]);
		if (first) {
			first = false;
			return "Tobias Ekholm";
		} else {
			return name.toString();
		}
	}

	public void setUsers(List<RmUserCommon> users) {
		this.users = users;
	}
}
