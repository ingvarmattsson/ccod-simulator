package createCbData;


import rmObjects.RmMessageHeader;

import java.time.ZoneOffset;

public class MessageHeader {
	public String dateTime;
	public String timeZoneUtcOffset; // RCS >= 2.15.10
	public String version;
	public String machineId;
	public String uuid; // RCS >= 2.15.10
	public String receiver;
	public Long messageSequence;
	public Long sequence;
	public String messageType;
	public String md5; // optional

	// not from ccod
	public String siteToken;
	public String receivedDateTime;
	public transient String messageFamily;

	public static final String SITE_TOKEN = "siteToken";

	public MessageHeader(MessageHeader mh) {
		this.dateTime = mh.dateTime;
		this.timeZoneUtcOffset = mh.timeZoneUtcOffset;
		this.version = mh.version;
		this.machineId = mh.machineId;
		this.uuid = mh.uuid;
		this.receiver = mh.receiver;
		this.messageSequence = mh.messageSequence;
		this.sequence = mh.sequence;
		this.messageType = mh.messageType;
		this.md5 = mh.md5;

		this.siteToken = mh.siteToken;
		this.receivedDateTime = mh.receivedDateTime;
		this.messageFamily = mh.messageFamily;
	}

	public MessageHeader(String dateTime, String timeZoneUtcOffset, String machineId, String uuid, Long messageSequence,
                         String messageType, String siteToken, String receivedDateTime) {
		this.dateTime = dateTime;
		this.timeZoneUtcOffset = timeZoneUtcOffset;
		this.machineId = machineId;
		this.uuid = uuid;
		this.messageSequence = messageSequence;
		this.messageType = messageType;
		this.siteToken = siteToken;
		this.receivedDateTime = receivedDateTime;
	}

	public MessageHeader(RmMessageHeader rmMessageHeader, ZoneOffset utcOffset, String siteToken,
						 String receivedDateTime) {
		this(Utils.rmDateTimeToUtc(rmMessageHeader.DATE_TIME, utcOffset != null ? utcOffset : ZoneOffset.of("+00:00")),
				rmMessageHeader.TIME_ZONE_UTC_OFFSET, rmMessageHeader.MACHINE_ID, rmMessageHeader.UUID,
				rmMessageHeader.MESSAGE_SEQUENCE, rmMessageHeader.MESSAGE_TYPE, siteToken, receivedDateTime);
		version = rmMessageHeader.VERSION;
		receiver = rmMessageHeader.RECEIVER;
		sequence = rmMessageHeader.SEQUENCE;
		md5 = rmMessageHeader.MD5;
	}

	public MessageHeader(RmMessageHeader rmMessageHeader, String siteToken, String receivedDate) {
		this(rmMessageHeader, ZoneOffset.of(rmMessageHeader.TIME_ZONE_UTC_OFFSET), siteToken, receivedDate);
	}

	public MessageHeader(RmMessageHeader rmMessageHeader, String siteToken) {
		this(rmMessageHeader, ZoneOffset.of(rmMessageHeader.TIME_ZONE_UTC_OFFSET), siteToken, null);
	}

	public MessageHeader(RmMessageHeader rmMessageHeader) {
		this(rmMessageHeader, ZoneOffset.of(rmMessageHeader.TIME_ZONE_UTC_OFFSET), null, null);
	}

	public MessageHeader(String uuid) {
		this.uuid = uuid;
	}

	public MessageHeader() {
	}

//	public RmMessageHeader toRm() {
//		RmMessageHeader rmh = new RmMessageHeader(Utils.utcToRmDateTime(dateTime, Utils.parseOffset(timeZoneUtcOffset)),
//				timeZoneUtcOffset, machineId, uuid);
//		rmh.VERSION = version;
//		rmh.RECEIVER = receiver;
//		rmh.MESSAGE_SEQUENCE = messageSequence;
//		rmh.SEQUENCE = sequence;
//		rmh.MESSAGE_TYPE = messageType;
//		rmh.MD5 = md5;
//		return rmh;
//	}

	public String getMessageFamily() {
		if (messageFamily != null) {
			return messageFamily;
		}
		return messageType;
	}

	public String getUuid() {
		return uuid;
	}

}
