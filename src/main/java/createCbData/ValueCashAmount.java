package createCbData;


import rmObjects.RmValue;

public abstract class ValueCashAmount<T> {
	public int decimals;
	public String currency;
	public String type;

	public abstract T copy();

	public abstract RmValue toRm();

	public String getType() {
		return type;
	};
}
