package createCbData;


import rmObjects.RmCashierInfo;
import rmObjects.RmMessageHeader;

import java.util.List;

public class RmZReport extends RmMessageHeader {
	public List<RmCashAmount> CASH_TOTALS;
	public RmCashierInfo CASHIER_INFO;
	public String REVENUE_CENTER;
	public String SITE_ID;
	public String WORK_UNIT_NAME;
	public String OUTLET;
	public String SHIFT_DATE_TIME_UTC;
	public String SHIFT_DATE_TIME;
	public String ACCOUNTING_DATE;
	public List<RmCashAmount> CASH_VALUES;
	public List<RmCashAmount> NON_CASH_VALUES;
	public List<RmCashAmount> OPENING_AMOUNT;
	public int VERSION_NUMBER;
	public String INDEXER;
	public String MACHINE_TYPE;
	public boolean USE_INITIAL_BANK;

	public RmZReport() {
		MESSAGE_TYPE = "ZReport";
	}
}
