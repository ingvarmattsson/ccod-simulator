package createCbData;


import rmObjects.RmValue;

public class Value extends ValueCashAmount<Value> {
	public static final String NOTE = "NOTE";
	public static final String COIN = "COIN";
	public long total;
	public int count;
	public int denomination;
	public int pieceValue; // RCS >= 2.10
	public int exchangeRate; // optional
	public int exchangeRateDecimals; // optional
	public String reference; // optional
	public String claimedValueType; // RCS >= 2.0.1
	public String customType; // optional
	public int c2Count; // optional
	public int c3Count; // optional
	public int c4BCount; // optional
	public boolean creditCat2; // optional
	public boolean creditCat3; // optional

	public Value() {
	}

	public static Value blank() {
		Value v = new Value();
		
		return v.zero();
	}
	
	public Value(RmValue rmValue) {
		total = rmValue.TOTAL;
		count = rmValue.COUNT;
		decimals = rmValue.DECIMALS;
		currency = rmValue.CURRENCY;
		denomination = rmValue.DENOMINATION;
		pieceValue = rmValue.PIECE_VALUE;
		exchangeRate = rmValue.EXCHANGE_RATE;
		exchangeRateDecimals = rmValue.EXCHANGE_RATE_DECIMALS;
		reference = rmValue.REFERENCE;
		type = rmValue.TYPE;
		claimedValueType = rmValue.CLAIMED_VALUE_TYPE;
		customType = rmValue.CUSTOM_TYPE;
		c2Count = rmValue.C2_COUNT;
		c3Count = rmValue.C3_COUNT;
		c4BCount = rmValue.C4B_COUNT;
		creditCat2 = rmValue.CREDIT_CAT_2;
		creditCat3 = rmValue.CREDIT_CAT_3;
	}

	public Value(long count, int decimals, String currency, int denomination) {
		this.total = count * denomination;
		this.count = (int) count;
		this.decimals = decimals;
		this.currency = currency;
		this.denomination = denomination;
	}

	public Value(long count, int decimals, String currency, int denomination, String type) {
		this.total = count * denomination;
		this.count = (int) count;
		this.decimals = decimals;
		this.currency = currency;
		this.denomination = denomination;
		this.type = type;
	}

	public Value(long total, int decimals, String currency) {
		this(total, decimals, currency, 1);
	}

	public Value(long total, int decimals, String currency, String type) {
		this(total, decimals, currency, 1, type);
	}

	public Value(CashAmount ca) {
		this(ca.amount, ca.decimals, ca.currency);
	}

	public static Value sub(Value v1, Value v2) {
		return v1.copy().sub(v2);
	}

	public static Value add(Value v1, Value v2) {
		return v1.copy().add(v2);
	}

	@Override
	public RmValue toRm() {
		RmValue rv = new RmValue();

		rv.TOTAL = total;
		rv.COUNT = count;
		rv.DECIMALS = decimals;
		rv.CURRENCY = currency;
		rv.DENOMINATION = denomination;
		rv.PIECE_VALUE = pieceValue;
		rv.EXCHANGE_RATE = exchangeRate;
		rv.TYPE = type;
		rv.C2_COUNT = c2Count;
		rv.C3_COUNT = c3Count;
		rv.C4B_COUNT = c4BCount;
		rv.CREDIT_CAT_2 = creditCat2;
		rv.CREDIT_CAT_3 = creditCat3;

		return rv;
	}

	@Override
	public Value copy() {
		return new Value(this.toRm());
	}

	public String denominationToCanonical() {
		return new CashAmount(denomination, decimals, currency, type).toCanonical();
	}

	public String totalToCanonical() {
		return new CashAmount(total, decimals, currency, type).toCanonical();
	}

	public boolean sameType(Value value) {
		return decimals == value.decimals && currency.equals(value.currency) && denomination == value.denomination
				&& pieceValue == value.pieceValue && this.getType().equals(value.getType());
	}

	public boolean sameTypeAndCurrency(Value value) {
		return currency.equals(value.currency) && this.getType().equals(value.getType());
	}

	public String getType() {

		if (type == null) {
			return "";
		}

		if (!type.equals("CLAIMED_VALUE")) {
			return type;
		}

		if (claimedValueType == null) {
			return "";
		}

		if (!claimedValueType.equals("CUSTOM")) {
			return claimedValueType;
		}

		return customType;
	}

	public Value invert() {
		total *= -1;
		count *= -1;
		c2Count *= -1;
		c3Count *= -1;
		c4BCount *= -1;
		return this;
	}

	public Value zero() {
		total = 0;
		count = 0;
		c2Count = 0;
		c3Count = 0;
		c4BCount = 0;
		return this;
	}

	public boolean isZero() {
		return total == 0 && count == 0 && c2Count == 0 && c3Count == 0 && c4BCount == 0;
	}

	public String keyString() {
		return currency + '-' + denomination + '-' + decimals + '-' + getType();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("total:" + total);
		sb.append(",decimals:" + decimals);
		sb.append(",currency: " + currency);
		sb.append(",type: " + type);
		sb.append("}");
		return sb.toString();
	}

	public Value add(Value v) {
		total += v.total;
		count += v.count;
		c2Count += v.c2Count;
		c3Count += v.c3Count;
		c4BCount += v.c4BCount;
		return this;
	}

	public Value addAndAdjustDecimals(Value v) {
		decimals = v.decimals > decimals ? v.decimals : decimals;
		if (currency == null || currency.length() == 0) {
			currency = v.currency;
		}
		add(v);

		return this;
	}

	public Value addTotal(Value v) {
		total += v.total;
		return this;
	}

	public Value sub(Value v) {
		return this.add(v.invert());
	}

}
