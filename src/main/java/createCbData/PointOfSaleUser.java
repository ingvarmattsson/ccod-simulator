package createCbData;

import rmObjects.RmCashierInfo;

public class PointOfSaleUser {
	public String userId;
	public String userName;
	public String roleName;

	public PointOfSaleUser() {
		userId = "NO USER ID FOUND";
		userName = "NO USER NAME FOUND";
		roleName = "UNKNOWN";
	}

	public PointOfSaleUser(RmCashierInfo rmPosu) {
		userId = rmPosu.CASHIER_ID;
		userName = rmPosu.CASHIER_NAME;
		roleName = rmPosu.ROLE_NAME;
	}

	public PointOfSaleUser(String userId, String userName, String roleName) {
		this.userId = userId;
		this.userName = userName;
		this.roleName = roleName;
	}

	public PointOfSaleUser(String userId, String userName) {
		this.userId = userId;
		this.userName = userName;
		this.roleName = "";
	}

	public RmCashierInfo toRmCashierInfo() {
		RmCashierInfo rci = new RmCashierInfo();
		rci.CASHIER_ID = userId;
		rci.CASHIER_NAME = userName;
		rci.ROLE_NAME = roleName;
		return rci;
	}
}
