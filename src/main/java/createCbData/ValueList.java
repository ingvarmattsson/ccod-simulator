package createCbData;

import rmObjects.RmValue;

import java.util.*;
import java.util.stream.Collectors;

public class ValueList extends ArrayList<Value> {
	private static final long serialVersionUID = 1L;

	public ValueList(List<Value> in) {
		super(in != null ? in : Collections.emptyList());
	}

	public ValueList() {
		super();
	}

	public ValueList(Value... values) {
		this(Arrays.asList(values));
	}

	public static ValueList ofRm(List<RmValue> rmValues) {
		return new ValueList(Utils.mapList(rmValues, Value::new));
	}

	/**
	 * Returns the resulting value modified by this operation
	 */
	public Value append(Value v) {
		return this.stream().filter(v::sameType).findFirst().orElseGet(() -> addReturns(v.copy().zero())).add(v);
	}

	public void append(Collection<Value> values) {
		values.stream().forEachOrdered(this::append);
	}

	public Value addReturns(Value v) {
		return super.add(v) ? v : null;
	}

	public Value getDenomination(int denomination) {
		return this.stream().filter(v -> v.denomination == denomination).findFirst()
				.orElse(new Value(0, 0, "", denomination));
	}

	public void subtract(Value v) {
		Optional<Value> opt = this.stream().filter(v::sameType).findFirst();
		if (opt.isPresent()) {
			opt.get().sub(v);
		} else {
			add(v.copy().invert());
		}
	}
	
	public void subtractFromTotal(Value v) {
		Optional<Value> opt = this.stream().filter(v::sameTypeAndCurrency).findFirst();
		if (opt.isPresent()) {
			opt.get().sub(v);
		} else {
			add(v.copy().invert());
		}
	}
	
	public void subtractAll(ValueList vs) {
		vs.forEach(this::subtract);
	}

	public ValueList subtract(ValueList vs) {
		for (Value v : vs) {
			subtract(v);
		}
		return this;
	}
	
	public ValueList subtractFromTotal(ValueList vs) {
		for (Value v : vs) {
			subtractFromTotal(v);
		}
		return this;
	}
	
	public ValueList invertAll() {
		this.forEach(Value::invert);
		return this;
	}

	public ValueList copy() {
		return new ValueList(Utils.mapList(this, Value::copy));
	}

	public ValueList purgeZeroes() {
		Iterator<Value> it = iterator();
		while (it.hasNext()) {
			if (it.next().isZero()) { it.remove(); }
		}
		return this;
	}

	public Value getFirst() {
		return get(0);
	}

	public Value removeFirst() {
		return remove(0);
	}

	public Value getLast() {
		return get(size() - 1);
	}

	@Override
	public String toString() {
		return stream().map(Value::totalToCanonical).collect(Collectors.joining(", "));
	}
}
