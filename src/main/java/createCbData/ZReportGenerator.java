package createCbData;

import cccObjects.DeviceCCC;
import ccodSession.Environments;
import com.google.common.collect.Lists;
import data.DefaultStrings;
import rest.RestHandler;
import rmObjects.RmUserCommon;
import rmReplay.RmReplay;
import xml.MachineUsersGenerator;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import static ccodSession.Environments.UAT;

public class ZReportGenerator {

    private int numUsersZreport = 100;
    private int numDevices = 20;
    private int endOfDayTime = 7200;
    private String fromDateZReport = "2021-07-01T00:00:00Z";
    private String toDateZReport = "2021-08-01T00:00:00Z";
    private String timeZoneOffset = "+01:00";
    private String currency = "EUR";

    private Environments env = UAT;
    private String tenant = "katalonretail";
    CbDataGenerator generator;
    private boolean executeInParallell = true;
    private boolean partitionUsers = true;

    public ZReportGenerator(String hardwareId, String siteToken, int numUsersZreport, String fromDateZReport, String toDateZReport, String timeZoneOffset, int endOfDayTime, String currency, Environments env, String tenant) {
        this.env = env;
        this.tenant = tenant;
        String fileNameZReport = env+"_"+tenant+"_"+"_ZREPORT_"+hardwareId+"_"+fromDateZReport.replaceAll(":","-")+"_"+toDateZReport.replaceAll(":","-")+".rm";
        generator= new CbDataGenerator(
                hardwareId,
                siteToken,
                numUsersZreport,
                fromDateZReport,
                toDateZReport,
                timeZoneOffset,
                endOfDayTime,
                currency,
                fileNameZReport
        );
    }

    public ZReportGenerator(){
    }

    public String generateCBData() throws IOException {
        return generator.generateZReport();
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        new ZReportGenerator().run();
    }

    private void run() throws IOException, URISyntaxException {
        List<DeviceCCC> devices = getMachines();
        List<String> fileNames = generateZReportDocuments(devices);;

        if (executeInParallell)
            executeInParallell(fileNames);
        else
            executeSequentially(fileNames);


    }

    private void executeInParallell(List<String> fileNames) {
        List<Thread> rmReplays = new LinkedList<>();

        System.out.println("### INITIATING RM REPLAY THREADS ###");
        for (int i = 0 ; i < fileNames.size(); i++){
            String[] machineIdFileName = fileNames.get(i).split("<>");
            rmReplays.add(new Thread(
                    new RmReplay(
                            DefaultStrings.getBrokerHost(env),
                            tenant,
                            machineIdFileName[0],
                            DefaultStrings.mqttUserName,
                            DefaultStrings.mqttPassWord,
                            machineIdFileName[1]
                    )
            ));
        }

        System.out.println("### STARTING THREADS ###");
        for(Thread t : rmReplays)
            t.start();

    }

    private void executeSequentially(List<String> fileNames) {
        for (String file : fileNames){
            System.out.println("\n\n### REPLAYING FILE: "+file);

            String[] machineIdFileName = file.split("<>");
            RmReplay r = new RmReplay(
                    DefaultStrings.getBrokerHost(env),
                    tenant,
                    machineIdFileName[0],
                    DefaultStrings.mqttUserName,
                    DefaultStrings.mqttPassWord,
                    machineIdFileName[1]
            );

            r.sendMessagesFromFile();
        }
    }

    private List<String> generateZReportDocuments(List<DeviceCCC> devices) throws IOException {
        List<String> fileNames = new LinkedList<>();
        int numMachines= devices.size()-numDevices;
        int indexToUsers = 0;
        List<List<RmUserCommon>> usersPatritioned = null;
        if(partitionUsers)
            usersPatritioned =  loadPartitionedUsers();

        for (int i = devices.size()-1 ; i >= numMachines; i--){
            String fileNameZReport = env+"_"+tenant+"_"+"_ZREPORT_"+devices.get(i).hardwareId+"_"+fromDateZReport.replaceAll(":","-")+"_"+toDateZReport.replaceAll(":","-")+".rm";
            System.out.println("\n\n### GENERATING ZREPORT: "+fileNameZReport);

            CbDataGenerator cbDataGenerator = new CbDataGenerator(
                    devices.get(i).hardwareId,
                    devices.get(i).siteToken,
                    numUsersZreport,
                    fromDateZReport,
                    toDateZReport,
                    timeZoneOffset,
                    endOfDayTime,
                    currency,
                    fileNameZReport
            );

            if(partitionUsers){
                cbDataGenerator.setUsers( usersPatritioned.get(indexToUsers));
                indexToUsers++;
            }

            fileNames.add(cbDataGenerator.generateZReport());

            System.out.println();
        }
        return fileNames;
    }

    private List<List<RmUserCommon>> loadPartitionedUsers() {
        LinkedList<RmUserCommon> allUsers = MachineUsersGenerator.readMachineUsers("ccodUsersZReport.xml",-1);
        List<List<RmUserCommon>> partitioned = Lists.partition(allUsers,numUsersZreport);
        return partitioned;

    }

    private List<DeviceCCC> getMachines() throws IOException, URISyntaxException {
        RestHandler rest = new RestHandler(env);
        rest.getTokenAuth();
        return rest.getMachines(tenant);
    }


}
