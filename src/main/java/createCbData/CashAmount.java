package createCbData;


import org.apache.commons.lang3.StringUtils;
import rmObjects.RmValue;

import java.io.Serializable;

import static org.apache.commons.lang3.StringUtils.leftPad;

@SuppressWarnings("serial")
public class CashAmount extends ValueCashAmount<CashAmount> implements Serializable, Comparable<CashAmount> {
	static String defaultDecimalSeparator = ".";
	public static final String TOTAL = "TOTAL";

	public long amount;

	public CashAmount() {
	}

	public CashAmount(long amount, int decimals, String currency, String type) {
		this.amount = amount;
		this.decimals = decimals;
		this.currency = currency;
		this.type = type;
	}

	public CashAmount(Value v) {
		this(v.total, v.decimals, v.currency, v.type);
	}

	public CashAmount(RmCashAmount rma) {
		this(rma.AMOUNT, rma.DECIMALS, rma.CURRENCY, rma.TYPE);
	}

	public CashAmount(long totalAmount, int decimals, String currency) {
		this(totalAmount, decimals, currency, null);
	}

	public RmCashAmount toRmCashAmount() {
		RmCashAmount rca = new RmCashAmount();
		rca.AMOUNT = amount;
		rca.CURRENCY = currency;
		rca.DECIMALS = decimals;
		rca.TYPE = type;
		return rca;
	}

	@Override
	public RmValue toRm() {
		return toRmCashAmount();
	}

	public boolean canOperate(CashAmount ca) {
		return ca != null && (currency != null && ca.currency != null && currency.equals(ca.currency))
				&& decimals == ca.decimals;
	}
	
	public boolean canOperateSameType(CashAmount ca) {
		return canOperate(ca) && StringUtils.equals(this.type, ca.type);
	}

	public CashAmount addIfCan(CashAmount ca) {
		if (canOperate(ca)) {
			amount += ca.amount;
		}
		type = TOTAL;
		return this;
	}
	
	public CashAmount addAndAdjustIfNecessary(CashAmount ca) {
		decimals = ca.decimals > decimals ? ca.decimals : decimals;
			
		if (canOperate(ca)) {
			amount += ca.amount;
		}
		type = TOTAL;
		return this;
	}
	
	public static CashAmount addIfCan(CashAmount c1, CashAmount c2) {
		if (c1 == null && c2 == null) {
			return null;
		}
		if (c1 == null) {
			c1 = new CashAmount(0L, c2.decimals, c2.currency, TOTAL);
		} else {
			c1 = c1.copy();
		}
		if (c2 == null) {
			c2 = new CashAmount(0L, c1.decimals, c1.currency, TOTAL);
		} else {
			c2 = c2.copy();
		}
		return c1.addIfCan(c2);
	}

	public static CashAmount subtractIfCan(CashAmount c1, CashAmount c2) {
		if (c1 == null && c2 == null) {
			return null;
		}
		if (c1 == null) {
			c1 = new CashAmount(0L, c2.decimals, c2.currency, TOTAL);
		} else {
			c1 = c1.copy();
		}
		if (c2 == null) {
			c2 = new CashAmount(0L, c1.decimals, c1.currency, TOTAL);
		} else {
			c2 = c2.copy();
		}
		return c1.subtractIfCan(c2);
	}

	public CashAmount subtractIfCan(CashAmount ca) {
		if (canOperate(ca)) {
			amount -= ca.amount;
		}
		type = TOTAL;
		return this;
	}

	@Override
	public CashAmount copy() {
		return new CashAmount((RmCashAmount) toRm());
	}

	public CashAmount invertAmount() {
		amount *= -1;
		return this;
	}

	public CashAmount zero() {
		amount = 0;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("amount:" + amount);
		sb.append(",decimals:" + decimals);
		sb.append(",currency: " + currency);
		sb.append(",type: " + type);
		sb.append("}");
		return sb.toString();
	}

	public String toCanonical() {
		return toCanonical(defaultDecimalSeparator);
	}

	public String toCanonical(String decSep) {
		return new StringBuilder().append(amount < 0 ? "-" : "").append(Long.toString(abs(amount / tenExp(decimals))))
				.append(decimals > 0 ? decSep : "").append(getDecimalString()).toString();
	}

	public String toCanonicalWithCurrency() {
		return toCanonical() + " " + currency;
	}

	private String getDecimalString() {
		if (decimals > 0)
			return leftPad(Long.toString(abs(amount % tenExp(decimals))), decimals).replace(' ', '0');
		else
			return "";
	}

	private long tenExp(long exponenet) {
		long base = 1;
		if (exponenet == 0) {
			return 1;
		}
		for (int i = 0; i < exponenet; i++) {
			base *= 10;
		}
		return base;
	}

	long abs(long num) {
		if (num < 0)
			return num * -1;
		return num;

	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof CashAmount) {
			CashAmount o = (CashAmount) other;
			return equalsNoType(o) && (type == o.type || type.equals(o.type));
		}
		return false;
	}

	public boolean equalsNoType(CashAmount o) {
		return o != null && o.amount == amount && o.decimals == decimals && o.currency.equals(currency);
	}

	@Override
	public int hashCode() {
		return Long.hashCode(amount) + Integer.hashCode(decimals) + currency.hashCode() + type.hashCode();
	}

	@Override
	public int compareTo(CashAmount ca) {
		if (!comparable(ca)) {
			throw new UnsupportedOperationException("Unsupported comarison: " + currency + " == " + ca.currency);
		}
		return compareAmountTo(ca);
	}

	public int compareAmountTo(CashAmount ca) {

		return Long.compare(amount, ca.amount);
	}

	public boolean comparable(CashAmount ca) {
		return currency.equals(ca.currency) && decimals == ca.decimals;
	}

	public CashAmount abs() {
		return new CashAmount(Math.abs(amount), decimals, currency, type);
	}
}
