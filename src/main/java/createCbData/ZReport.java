package createCbData;


import java.time.Instant;

public class ZReport extends MessageHeader {
	public String posAdapterId;
	public String token;
	public CashAmountList cashTotals;
	public CashAmountList nonCashTotals;
	public PointOfSaleUser user;
	public String workUnit;
	public String shiftDateTimeUtc;
	public String shiftLocalTime;
	public String shiftLocalTimeUtcOffset;
	public String accountingDate;
	public CashAmountList cashValues;
	public CashAmountList nonCashValues;
	public CashAmountList openingAmount;
	public String siteId;
	public String workUnitName;
	public String outlet;
	public String machineType;
	public boolean useInitalBank;
	public int versionNumber;

	public static final String ROLE_NAME = "user.roleName";
	public static final String SITE = "siteId";
	public static final String OUTLET = "outlet";
	public static final String ACCOUNTING_DATE = "accountingDate";

	public static final String MESSAGE_TYPE = "ZReport";

	public ZReport() {
		openingAmount = new CashAmountList();
		nonCashValues = new CashAmountList();
		messageType = MESSAGE_TYPE;
	}

	public ZReport(RmZReport rmzr, String receivedDate) {
		this();
		receivedDateTime = receivedDate;
		token = rmzr.INDEXER;
		machineId = posAdapterId = rmzr.MACHINE_ID;
		dateTime = rmzr.DATE_TIME;
		user = new PointOfSaleUser(rmzr.CASHIER_INFO);

		workUnit = rmzr.REVENUE_CENTER;
		shiftDateTimeUtc = Utils.rmDateTimeToUtc(rmzr.SHIFT_DATE_TIME, Utils.parseOffset(rmzr.TIME_ZONE_UTC_OFFSET));
		shiftLocalTime = rmzr.SHIFT_DATE_TIME;
		shiftLocalTimeUtcOffset = rmzr.TIME_ZONE_UTC_OFFSET;
		dateTime = accountingDate = Utils.rmDateTimeToUtc(rmzr.ACCOUNTING_DATE, Utils.parseOffset("+00:00"));
		cashValues = new CashAmountList(rmzr.CASH_VALUES);
		openingAmount = new CashAmountList(rmzr.OPENING_AMOUNT);
		if (rmzr.CASH_TOTALS != null) {
			cashTotals = new CashAmountList(rmzr.CASH_TOTALS);
		} else {
			calculateTotals();
		}
		nonCashValues = new CashAmountList(rmzr.NON_CASH_VALUES);
		siteId = rmzr.SITE_ID;
		workUnitName = rmzr.WORK_UNIT_NAME;
		outlet = rmzr.OUTLET;
		machineType = rmzr.MACHINE_TYPE;
		useInitalBank = rmzr.USE_INITIAL_BANK;

		versionNumber = rmzr.VERSION_NUMBER;
	}

	public RmZReport toRmZReport() {
		RmZReport rzr = new RmZReport();
		rzr.MACHINE_ID = posAdapterId;
		// rzr. = id;
		if (cashTotals != null) {
			rzr.CASH_TOTALS = cashTotals.toRmCashAmountList();
		}
		rzr.DATE_TIME = dateTime;
		if (user != null) {
			rzr.CASHIER_INFO = user.toRmCashierInfo();
		}
		rzr.REVENUE_CENTER = workUnit;
		rzr.WORK_UNIT_NAME = workUnitName;
		rzr.SITE_ID = siteId;
		rzr.TIME_ZONE_UTC_OFFSET = shiftLocalTimeUtcOffset;
		rzr.OUTLET = outlet;
		rzr.ACCOUNTING_DATE = Utils.utcToRmDateTime(accountingDate, Utils.parseOffset("+00:00"));

		rzr.DATE_TIME = rzr.SHIFT_DATE_TIME = Utils.utcToRmDateTime(shiftDateTimeUtc,
				Utils.parseOffset(shiftLocalTimeUtcOffset));
		rzr.CASH_VALUES = cashValues.toRmCashAmountList();
		rzr.NON_CASH_VALUES = nonCashValues.toRmCashAmountList();

		if (openingAmount != null) {
			rzr.OPENING_AMOUNT = openingAmount.toRmCashAmountList();
		}
		rzr.VERSION_NUMBER = versionNumber;
		rzr.INDEXER = token;

		rzr.MACHINE_TYPE = machineType;
		rzr.USE_INITIAL_BANK = useInitalBank;

		rzr.MESSAGE_TYPE = MESSAGE_TYPE;

		return rzr;
	}

	public ZReport calculateTotals() {
		cashTotals = new CashAmountList();
		nonCashTotals = new CashAmountList();
		cashTotals.append(cashValues);
		nonCashTotals.append(nonCashValues);
		return this;
	}

	public int getShiftLocalTimeUtcOffsetSeconds() {
		return java.time.ZoneOffset.of(shiftLocalTimeUtcOffset).getTotalSeconds();
	}

	public boolean hasOpeningAmount() {
		return openingAmount != null && !openingAmount.isEmpty();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("userId:" + user.userId);
		sb.append(",workUnitName:" + workUnitName);
		sb.append(",machineId: " + machineId);
		sb.append(",cashValues: " + cashValues);
		sb.append(",cashTotals: " + cashTotals);
		sb.append("}");
		return sb.toString();
	}

	public boolean hasShiftTime(String endOfDay) {
		if (shiftDateTimeUtc == null)
			return false;
		return !Instant.parse(shiftDateTimeUtc)
				.equals(Instant.parse(accountingDate).minusSeconds(getShiftLocalTimeUtcOffsetSeconds()))
				&& endOfDay != null && !endOfDay.equals("00:00");
	}

	public Instant getShiftTime(String endOfDay) {
		if (hasShiftTime(endOfDay)) {
			return Instant.parse(shiftDateTimeUtc);
		}
		return null;
	}

	public boolean hasShiftTime() {
		return shiftDateTimeUtc != null;
	}

	public Instant getDateTime() {
		return Instant.parse(dateTime);
	}

	public Instant getTime() {
		if (hasShiftTime()) {
			return Instant.parse(shiftDateTimeUtc);
		} else {
			return getDateTime();
		}
	}
}
