package xml;

import data.DefaultStrings;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import rmObjects.RmUserMuM;
import rmObjects.RmWorkUnit;
import rmObjects.RmWorkUnitParent;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class WorkUnitReader {

    public static List<RmWorkUnit> readWorkUnits(String fileName) throws ParserConfigurationException, IOException, SAXException {

        if(fileName==null)
           fileName="workUnitsLaneAllocations.xml";

        String filePath = DefaultStrings.getXmlPath()+DefaultStrings.pathSeparator+fileName;

        List<RmWorkUnit> workunits = Collections.synchronizedList(new LinkedList<>());
        File file = new File(filePath);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        document.getDocumentElement().normalize();
        Element root = document.getDocumentElement();
        System.out.println(root.getNodeName());

        NodeList unitGroup = document.getElementsByTagName("unitgroup");


        for(int i = 0; i<unitGroup.getLength();i++){
            Node nodeUnitGroup = unitGroup.item(i);
            if (nodeUnitGroup.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElementUnitGroup = (Element) nodeUnitGroup;
                RmWorkUnitParent parent = new RmWorkUnitParent();
                parent.NAME = eElementUnitGroup.getAttribute("name");
                String displayName = eElementUnitGroup.getAttribute("displayname");
                if(!displayName.isEmpty())
                    parent.DISPLAY_NAME = displayName;
                String bool = eElementUnitGroup.getAttribute("isMoneyMover");
                if(bool!= null && !bool.isEmpty())
                    parent.IS_MONEY_MOVER = Boolean.valueOf(bool);
                bool = eElementUnitGroup.getAttribute("ignoreAllocationDayBreak");
                if(bool!= null && !bool.isEmpty())
                    parent.IGNORE_ALLOCATION_DAY_BREAK = Boolean.valueOf(bool);
                String roles = eElementUnitGroup.getAttribute("roles");
                if(!roles.isEmpty())
                    parent.ROLE_IDS= Arrays.asList(roles.split(","));
                String moneymix = eElementUnitGroup.getAttribute("moneymix");
                if (!moneymix.isEmpty())
                    parent.MONEY_MIXES= Arrays.asList(moneymix.split(","));

                NodeList nworkUnits = eElementUnitGroup.getElementsByTagName("unit");

                for(int j =0; j< nworkUnits.getLength(); j++){
                    Node nodeWorkUnit = nworkUnits.item(j);
                    if (nodeWorkUnit.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElementWorkUnits = (Element) nodeWorkUnit;
                        RmWorkUnit workunit = new RmWorkUnit();
                        workunit.NAME = eElementWorkUnits.getAttribute("name");
                        displayName = eElementWorkUnits.getAttribute("displayname");
                        if(!displayName.isEmpty())
                            workunit.DISPLAY_NAME=displayName;
                        bool = eElementWorkUnits.getAttribute("isMoneyMover");
                        if(bool!= null && !bool.isEmpty())
                            workunit.IS_MONEY_MOVER = Boolean.valueOf(bool);

                        bool = eElementWorkUnits.getAttribute("ignoreAllocationDayBreak");
                        if(bool!= null && !bool.isEmpty())
                            workunit.IGNORE_ALLOCATION_DAY_BREAK = Boolean.valueOf(bool);
                        roles = eElementWorkUnits.getAttribute("roles");
                        if(!roles.isEmpty())
                            workunit.ROLE_IDS= Arrays.asList(roles.split(","));
                        moneymix = eElementWorkUnits.getAttribute("moneymix");
                        if(!moneymix.isEmpty())
                            workunit.MONEY_MIXES= Arrays.asList(moneymix.split(","));
                        workunit.PARENT = parent;
                        workunits.add(workunit);
                    }


                }

            }
        }

        return workunits;

    }

}
