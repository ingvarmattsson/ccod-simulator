package xml;

import com.github.javafaker.Faker;
import data.DefaultStrings;
import data.Utilities;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import rmObjects.RmUserCommon;
import rmObjects.RmUserMuM;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class MachineUsersGenerator {


    private static int numUsers = 5000;
    private static int loginIndexStart = 880000;
    private static String[] role ={"cashier","bartender","administrator", "cit", "manager"};
    private static String[] doorsArr = {"CIMA.1.COVER","CIMA.1.SAFE","COIN_DISPENSER","NOTE_ACCEPTOR_BOTTOM","NOTE_ACCEPTOR_TOP","NOTE_DISPENSER","TILL_BOX","TILL_DRAWER","TRANSPORTBOX_DRAWER"};
    private static String pathCcodUsersBaseFile = DefaultStrings.getUsersXmlPath();
    private static boolean alwaysIncludeAdditionalFields = true;
    private static boolean setValidDates= true;
    private static boolean setPasswords= false;
    private static boolean setBioMetrics =false;


    public static void main(String[] args) {

        Random rng = new Random();
        int rngThreshold = 50;
        if(alwaysIncludeAdditionalFields)
            rngThreshold = 0;




        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("users");
            document.appendChild(root);

            for (int i = 0; i<numUsers;i++){
                Element user = document.createElement("user");

                if(setBioMetrics && rng.nextInt(100)>=rngThreshold){

                    Element biometrics = document.createElement("biometrics");
                    Attr fingerprints = document.createAttribute("fingerprints");
                    fingerprints.setValue("value-fingerprints"+i);
                    biometrics.setAttributeNode(fingerprints);
                    user.appendChild(biometrics);

                }

               //login - only required field
                Attr attrLogin = document.createAttribute("login");
                attrLogin.setValue(Integer.toString(loginIndexStart +i));
                user.setAttributeNode(attrLogin);

                //password
                if(setPasswords && rng.nextInt(100)>=rngThreshold){
                    Attr attrPassword = document.createAttribute("password");
                    attrPassword.setValue(Utilities.generatePassword(rng.nextInt(10)+10));
                    user.setAttributeNode(attrPassword);
                }

                //name
                if(rng.nextInt(100)>=rngThreshold){
                    Attr attrName = document.createAttribute("name");
                    attrName.setValue((new Faker().name().fullName()));
                    user.setAttributeNode(attrName);
                }


                //identifier
                if(rng.nextInt(100)>=rngThreshold){
                    Attr attrIdentifier = document.createAttribute("identifier");
                    attrIdentifier.setValue(Integer.toString(loginIndexStart +i));
                    user.setAttributeNode(attrIdentifier);
                }



                //role
                if(rng.nextInt(100)>=rngThreshold){
                    Attr attrRole = document.createAttribute("role");
                    attrRole.setValue(role[new Random().nextInt(role.length)]);
                    user.setAttributeNode(attrRole);
                }

                //noPin
                if(rng.nextInt(100)>=rngThreshold){
                    Attr attrNoPin = document.createAttribute("noPin");
                    attrNoPin.setValue(String.valueOf(new Random().nextBoolean()));
                    user.setAttributeNode(attrNoPin);
                }

                //deactivated
                if(rng.nextInt(100)>=rngThreshold){
                    Attr attrDeactivated= document.createAttribute("deactivated");
                    attrDeactivated.setValue(String.valueOf(new Random().nextBoolean()));
                    user.setAttributeNode(attrDeactivated);
                }

                //nbrBadPIN
                if(rng.nextInt(100)>=rngThreshold){
                    Attr nbrBadPIN = document.createAttribute("nbrBadPIN");
                    nbrBadPIN.setValue(String.valueOf(rng.nextInt(10)+1));
                    user.setAttributeNode(nbrBadPIN);
                }


                //noPinBio
                if(rng.nextInt(100)>=rngThreshold){
                    Attr noPinBio = document.createAttribute("noPinBio");
                    noPinBio.setValue(String.valueOf(rng.nextBoolean()));
                    user.setAttributeNode(noPinBio);
                }
                //noPinLogin
                if(rng.nextInt(100)>=rngThreshold){
                    Attr noPinLogin = document.createAttribute("noPinLogin");
                    noPinLogin.setValue(String.valueOf(rng.nextBoolean()));
                    user.setAttributeNode(noPinLogin);
                }
                //noPinCard
                if(rng.nextInt(100)>=rngThreshold){
                    Attr noPinCard = document.createAttribute("noPinCard");
                    noPinCard.setValue(String.valueOf(rng.nextBoolean()));
                    user.setAttributeNode(noPinCard);
                }
                //noPinDoor
                if(rng.nextInt(100)>=rngThreshold){
                    Attr noPinDoor = document.createAttribute("noPinDoor");
                    noPinDoor.setValue(String.valueOf(rng.nextBoolean()));
                    user.setAttributeNode(noPinDoor);
                }
                //nbrOfLogins
                if(rng.nextInt(100)>=rngThreshold){
                    Attr nbrOfLogins = document.createAttribute("nbrOfLogins");
                    nbrOfLogins.setValue(Integer.toString(rng.nextInt(100)-1));
                    user.setAttributeNode(nbrOfLogins);
                }
                //validFrom validUntil
                if(setValidDates && rng.nextInt(100)>=rngThreshold){
                    Attr validFrom = document.createAttribute("validFrom");
                    validFrom.setValue("2020-06-01T00:00:00+02:00[Europe/Berlin]");
                    user.setAttributeNode(validFrom);

                    Attr validUntil = document.createAttribute("validUntil");
                    validUntil.setValue("2020-06-30T00:00:00+02:00[Europe/Berlin]");
                    user.setAttributeNode(validUntil);
                }

                //list
                if(rng.nextInt(100)>=rngThreshold){
                    Attr list = document.createAttribute("list");
                    list.setValue(String.valueOf(rng.nextBoolean()));
                    user.setAttributeNode(list);
                }
                //doors
                if(rng.nextInt(100)>=rngThreshold){

                    String doorsVal="";
                    int numDoors = new Random().nextInt(doorsArr.length)+1;
                    for(int j = 0;j<numDoors;j++)
                        doorsVal+=doorsArr[j]+",";
                    doorsVal = doorsVal.substring(0,doorsVal.length()-1);
                    Attr doors = document.createAttribute("doors");
                    doors.setValue(doorsVal);
                    user.setAttributeNode(doors);
                }

                System.out.println("Generating user: "+attrLogin.getValue());
                root.appendChild(user);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(pathCcodUsersBaseFile));

            transformer.transform(domSource, streamResult);
            System.out.println("Done creating XML File");


        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

    public static List<RmUserMuM> readUsersFromFile(String fileName) throws ParserConfigurationException, IOException, SAXException {

        String filePath = DefaultStrings.getXmlPath()+DefaultStrings.pathSeparator+fileName;

        List<RmUserMuM> users = Collections.synchronizedList(new LinkedList<>());
        File file = new File(filePath);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        document.getDocumentElement().normalize();
        Element root = document.getDocumentElement();
        System.out.println(root.getNodeName());

        NodeList nList = document.getElementsByTagName("user");

        for(int i = 0; i<nList.getLength();i++){
            Node node = nList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElement = (Element) node;
                RmUserMuM user = new RmUserMuM();
                user.USER_ID=parseStringValue(eElement.getAttribute("login"));
                user.ALIAS = parseStringValue(eElement.getAttribute("alias"));
                user.CARD_IDENTIFIER = parseStringValue(eElement.getAttribute("identifier"));
                user.NAME =parseStringValue(eElement.getAttribute("name"));
                user.PIN = parseStringValue(eElement.getAttribute("password"));
                user.ROLE_NAME=parseStringValue(eElement.getAttribute("role"));
                user.DEACTIVATED=parseBoolValue(eElement.getAttribute("deactivated"));
                user.CHANGE_PIN_ON_LOGIN= parseBoolValue(eElement.getAttribute("changePinOnLogin"));
                user.NO_PIN = parseBoolValue(eElement.getAttribute("noPin"));
                user.NO_PIN_BIO= parseBoolValue(eElement.getAttribute("noPinBio"));
                user.NO_PIN_CARD = parseBoolValue(eElement.getAttribute("noPinCard"));
                user.NO_PIN_DOOR = parseBoolValue(eElement.getAttribute("noPinDoor"));
                user.NO_PIN_USER_ID = parseBoolValue(eElement.getAttribute("noPinLogin"));
                user.NBR_OF_BAD_PIN= parseIntValue(eElement.getAttribute("nbrBadPIN"));
                user.NBR_OF_LOGINS= parseIntValue(eElement.getAttribute("nbrOfLogins"));
                user.VALID_FROM = convertToMachineDatePattern(parseStringValue(eElement.getAttribute("validFrom")));
                user.VALID_UNTIL = convertToMachineDatePattern(parseStringValue(eElement.getAttribute("validUntil")));
                String doors =  eElement.getAttribute("doors");
                if(doors!=null || doors.isEmpty())
                    user.DOORS = Arrays.asList(doors.split(","));
                //user.FINGERPRINTS = eElement.getAttribute("");
                user.LIST = parseBoolValue(eElement.getAttribute("list"));
                users.add(user);
            }
        }

        return users;

    }

    public static LinkedList<RmUserCommon> readMachineUsers(String filename, int numUsers) {
        LinkedList<RmUserCommon> users = new LinkedList<>();
        try {
            List<RmUserMuM> usersList = MachineUsersGenerator.readUsersFromFile("ccodUsersZReport.xml");

            if(numUsers<0)
                numUsers = usersList.size();

            for (int i = 0; i < numUsers; i++) {
                RmUserCommon user = new RmUserCommon();
                user.NAME = usersList.get(i).NAME;
                user.USER_ID = usersList.get(i).USER_ID;
                user.ROLE_NAME = usersList.get(i).ROLE_NAME;
                users.add(user);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return users;
    }

    private static Integer parseIntValue(String val){
        if(val==null || val.isEmpty())
            return null;
        return Integer.parseInt(val);
    }

    private static Boolean parseBoolValue(String val){
        if(val==null || val.isEmpty())
            return null;
        return Boolean.valueOf(val);
    }

    private static String parseStringValue(String val){
        if(val==null || val.isEmpty())
            return null;
        return val;
    }

    private static String convertToMachineDatePattern(String val){
        String date=null;
        if (val==null)
            return  null;
        date = val.substring(0,val.indexOf("+"))+"Z";
        return Utilities.getDateAsMachinePattern(date);
    }
}
