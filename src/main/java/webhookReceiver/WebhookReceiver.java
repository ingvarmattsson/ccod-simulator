package webhookReceiver;

import ccodSession.Environments;
import data.DefaultStrings;
import rest.RestHandler;

import java.io.IOException;
import java.net.URISyntaxException;

import static ccodSession.Environments.WEBHOOK;

public class WebhookReceiver {

    RestHandler restHandler;
    Environments env = WEBHOOK;
    private String webhookURL;
    WebhookToken token;

    public String createReceiver() throws IOException, URISyntaxException {
        restHandler = new RestHandler(env);
        token = restHandler.postWebhookRecipient();
        if(token==null)
            return null;
        webhookURL = DefaultStrings.getBaseURL(env)+"/"+token.uuid;
        return webhookURL;

    }


    public WebhookResponse getRequests(Webhooksorting sorting) throws IOException, URISyntaxException {
        restHandler = new RestHandler(env);
        return restHandler.getWebhookRequests(token.uuid,sorting.name(),100,1);
    }

    public WebhookData getRequestLatest() throws IOException, URISyntaxException {
        restHandler = new RestHandler(env);
        return restHandler.getWebhookRequestLatest(token.uuid);
    }

    public boolean deleteAllRequests() throws IOException, URISyntaxException {
        restHandler = new RestHandler(env);
        int statusCode = restHandler.deleteAllWebhookRequests(token.uuid);
        if (statusCode!=200)
            return false;
        return true;
    }


}
