package rest;

import cccObjects.*;
import ccodSession.Environments;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import tenantAuthorization.SiteWhereTenant;
import webhookReceiver.WebhookData;
import webhookReceiver.WebhookResponse;
import webhookReceiver.WebhookToken;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class RestHandler {

    private String baseURL;
    private String hAcceptAppJson ="application/json";
    private String hAcceptText ="test/plain";
    private String hAcceptAll ="*/*";
    private String hReferer;
    private String hUserAgent ="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36";
    private String hDateTime="dateTime";
    private String hSortOrder="desc";

    CloseableHttpClient httpClient;
    HttpHost host;
    Token token;
    String basicTokenForAccessToken;
    String basicTokenForXAuth;

    public RestHandler(Environments environment) {

        httpClient= HttpClientBuilder.create().build();

        baseURL = DefaultStrings.getBaseURL(environment);
        hReferer = DefaultStrings.getAPIReferer(environment);
        host = new HttpHost(baseURL,80,"https");
        basicTokenForAccessToken = "Basic "+ Base64.getEncoder().encodeToString((DefaultStrings.apiUserConsumerKey+":"+DefaultStrings.apiUserConsumerSecret).getBytes());
        basicTokenForXAuth = "Basic "+ Base64.getEncoder().encodeToString((DefaultStrings.cccUserName+":"+DefaultStrings.cccUserPassword).getBytes());


    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        RestHandler restHandler = new RestHandler(Environments.CH);
        restHandler.getToken();
        List<SiteCCC> s = restHandler.getSites("salesdemobusiness");
        System.out.println();
    }



    public Token getToken() throws IOException {
        if(DefaultStrings.useOldApimToGetToken)
            return getTokenOld();
        else
            return getTokenAuth();
    }

    private Token getTokenOld() throws IOException {
        String scope =DefaultStrings.tokenScope;
        HttpPost request = new HttpPost(baseURL+"/token");
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");

        List<NameValuePair> params = new ArrayList();
        params.add(new BasicNameValuePair("grant_type","password"));
        params.add(new BasicNameValuePair("username",DefaultStrings.apiUsername));
        params.add(new BasicNameValuePair("password",DefaultStrings.apiPassword));
        params.add(new BasicNameValuePair("scope",scope));

        request.setEntity(new UrlEncodedFormEntity(params));

        HttpResponse response = httpClient.execute(request);
        if(200 != response.getStatusLine().getStatusCode())
            return null;
        else{
            String responseBody = EntityUtils.toString(response.getEntity());
            token = new Gson().fromJson(responseBody, Token.class);
            return  token;

        }
    }

    public Token getTokenAuth() throws IOException {
        HttpPost request = new HttpPost(baseURL+"/api/v1/auth/token?username="+DefaultStrings.usernameAuth+"&hashedPassword="+DefaultStrings.hashedPasswordAuth);
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");

        List<NameValuePair> params = new ArrayList();
//        params.add(new BasicNameValuePair("username",DefaultStrings.usernameAuth));
//        params.add(new BasicNameValuePair("hashedPassword",DefaultStrings.hashedPasswordAuth));
//
//        request.setEntity(new UrlEncodedFormEntity(params));

        HttpResponse response = httpClient.execute(request);
        if(200 != response.getStatusLine().getStatusCode())
            return null;
        else{
            String responseBody = EntityUtils.toString(response.getEntity());
            token = new Gson().fromJson(responseBody, Token.class);
            return  token;

        }
    }

    public void setGeneralRequestHeaders(HttpRequestBase request, String contentTypeHeader) {
        request.setHeader(HttpHeaders.ACCEPT, hAcceptAppJson +", "+ hAcceptText +", "+ hAcceptAll);
        request.setHeader(HttpHeaders.AUTHORIZATION, basicTokenForAccessToken);
        request.setHeader(HttpHeaders.REFERER, hReferer);
        request.setHeader("Sec-Fetch-Mode", "cors");
        request.setHeader(HttpHeaders.USER_AGENT, hUserAgent);
        request.setHeader(HttpHeaders.ACCEPT, hAcceptAppJson );
        request.setHeader(HttpHeaders.CONTENT_TYPE, contentTypeHeader);
    }

    public void setAuthRequestHeaders(HttpRequestBase request, String tenant, Token token){
        request.setHeader("X-Authorization", basicTokenForXAuth);
        request.setHeader("X-CCC-Track-UserName","esbuser");
        request.setHeader("X-SiteWhere-Tenant",tenant);
        request.setHeader("X-CCC-Track-Client", "UI-prod-uat");
        request.setHeader("X-CCC-Track-Id", "4be3293d-5d0f-3a31-b032-11873d9136b1-226");
        if(token.token_type!=null && token.access_token != null)
            request.setHeader(HttpHeaders.AUTHORIZATION, token.token_type+" "+token.access_token);
        if(token.tokenType!=null && token.accessToken!=null)
            request.setHeader(HttpHeaders.AUTHORIZATION, token.tokenType+" "+token.accessToken);
    }

    public void setPostRequetHeaders(HttpRequestBase request, Integer contentLength) {
        request.setHeader(HttpHeaders.ACCEPT, hAcceptAppJson);
        request.setHeader(HttpHeaders.ACCEPT_ENCODING,"gzip, deflate, br");
        request.setHeader(HttpHeaders.ACCEPT_LANGUAGE,"en-US,en;q=0.9,sv;q=0.8,sv-SE;q=0.7");
        request.setHeader(HttpHeaders.CONNECTION, "keep-alive");
        if(contentLength!=null)
            request.setHeader(HttpHeaders.CONTENT_LENGTH,Integer.toString(contentLength));
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        request.setHeader(HttpHeaders.HOST, "test-api.cashcomplete.com");
        request.setHeader("Origin","htpps://connect-test.cashcomplete.com");
        request.setHeader(HttpHeaders.REFERER, "htpps://connect-test.cashcomplete.com");
        request.setHeader("Sec-Fetch-Mode", "cors");
        request.setHeader("Sec-Fetch-Site", "same-site");
        request.setHeader(HttpHeaders.USER_AGENT, hUserAgent);
    }






    public List<TransactionCCC> getActualTransactions(String tenant, String dateFrom, String dateTo) throws IOException, URISyntaxException {

        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/transactions");
        builder.setParameter("sortField", hDateTime);
        builder.setParameter("sortOrder",hSortOrder);
        builder.setParameter("includeDeviceMovements",Boolean.toString(true));
        builder.setParameter("filterOnType",Boolean.toString(true));
        builder.setParameter("pageSize",Integer.toString(10000));
        builder.setParameter("fromAccountingDate",dateFrom);
        builder.setParameter("toAccountingDate", dateTo);

        System.out.println("Fetching transactions for "+tenant+" on fromAccountingDate: "+dateFrom+
                ", dateTo: "+dateTo);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), TransactionsResponse.class).results;

    }

    public List<SiteWhereTenant> getTenants(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/tenants");
        builder.setParameter("pageSize",Integer.toString(1000));
        builder.setParameter("page",Integer.toString(1));
        System.out.println("Fetching all tenants");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        //TenantResponse tr= new Gson().fromJson(EntityUtils.toString(response.getEntity()), TenantResponse.class);
        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), TenantResponse.class).results;

    }

    public SiteWhereTenant getTenant(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/tenants/"+tenant);
        builder.setParameter("pageSize",Integer.toString(1000));
        builder.setParameter("page",Integer.toString(1));
        System.out.println("Fetching all tenants");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), SiteWhereTenant.class);
    }

    public HttpResponse executeRequest(HttpRequestBase request) throws IOException {
        return httpClient.execute(request);
    }

    public int authorizeUser(String tenantId, String userName) throws IOException, URISyntaxException {

        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/tenants/"+tenantId+"/authorize/"+userName);

        HttpPost request = new HttpPost(builder.build());

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenantId,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();

    }

    public int putTenant(String tenantPayLoad, String tenantId) throws IOException, URISyntaxException {

        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/tenants/"+tenantId);

        HttpPut request = new HttpPut(builder.build());
        request.setEntity(new StringEntity(tenantPayLoad));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenantId,token);

        HttpResponse response = httpClient.execute(request);

        int responseCode = response.getStatusLine().getStatusCode();
        return responseCode;
    }

    public int  postUser(String tenant, String userPayload) throws URISyntaxException, IOException {

        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/users");

        HttpPost request = new HttpPost(builder.build());
        request.setEntity(new StringEntity(userPayload));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();
    }

    public List<DeviceCCC> getMachines(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/devices");
        builder.setParameter("pageSize",Integer.toString(1000));
        builder.setParameter("includeAssignment",Boolean.toString(false));
        builder.setParameter("includeSpecification",Boolean.toString(false));
        System.out.println("Fetching machines for "+tenant);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), DeviceCCCResponse.class).results;

    }

    public MachineOnlineStatusHistoricResponseCCC getMachineOnlineStatusHistoric(Environments env, String tenant, String machineUUID, String fromDate, String toDate) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/machines/onlinestatus-history");
        builder.setParameter("fromDate",fromDate);
        builder.setParameter("toDate",toDate);
        builder.setParameter("machineId",machineUUID);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), MachineOnlineStatusHistoricResponseCCC.class);
    }

    public DeviceCCCResponse getMachinesPaged(String tenant, int page, int pageSize) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/devices");
        builder.setParameter("pageSize",Integer.toString(pageSize));
        builder.setParameter("page",Integer.toString(page));
        builder.setParameter("includeAssignment",Boolean.toString(true));
        builder.setParameter("includeSpecification",Boolean.toString(true));
        builder.setParameter("includeSite",Boolean.toString(true));

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), DeviceCCCResponse.class);
    }


    public MachineDetailCCCResponse getMachineDetailsPaged(String tenant, int page, int pageSize) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/machines/detaildata");
        builder.setParameter("includeLastError",Boolean.toString(false));
        builder.setParameter("includeLastTransaction",Boolean.toString(true));
        builder.setParameter("page",Integer.toString(page));
        builder.setParameter("pageSize",Integer.toString(pageSize));

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), MachineDetailCCCResponse.class);


    }

    public List<RemoteConfigurationCCC> getRemoteConfigurations(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remoteconfiguration/template");

        System.out.println("Fetching remote configuration templates for "+tenant);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        //TenantResponse tr= new Gson().fromJson(EntityUtils.toString(response.getEntity()), TenantResponse.class);
        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), RemoteConfigurationCCCResponse.class).results;

    }


    public List<RemoteConfigurationCCC> getRemoteConfigurationTemplates(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remoteconfiguration/template");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), RemoteConfigurationCCCResponse.class).results;
    }

    public List<RemoteSoftwareUpgradeCCC> getRemoteSoftwareInstallations(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remotesoftwareupgrade/packages");

        System.out.println("Fetching software installations for "+tenant);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;


        Type collectionType = new TypeToken<Collection<RemoteSoftwareUpgradeCCC>>(){}.getType();
        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), collectionType);
    }

    public RemoteSoftwareUpgradeCCCResponse postRemoteSoftwareUpgradeForMachine(String payload, String tenant) throws URISyntaxException, IOException {
        System.out.println("Posting Remote Software Upgrade payload\n"+payload);

        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remotesoftwareupgrade/installations");

        HttpPost request = new HttpPost(builder.build());
        request.setEntity(new StringEntity(payload));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(response.getStatusLine().getStatusCode()!=200)
            return null;

        HttpEntity responseEntity = response.getEntity();
        if(responseEntity==null)
            return null;

        InputStream is = response.getEntity().getContent();
        String responseContent = (String) IOUtils.toString(is, StandardCharsets.UTF_8);
        return new Gson().fromJson(responseContent, RemoteSoftwareUpgradeCCCResponse.class);
    }

    public RemoteConfigurationCCCResponse postRemoteConfigurationForMachine(String payload, String tenant) throws URISyntaxException, IOException {
        System.out.println("Posting Remote Software Upgrade payload\n"+payload);

        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remoteconfiguration/deploy");

        HttpPost request = new HttpPost(builder.build());
        request.setEntity(new StringEntity(payload));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(response.getStatusLine().getStatusCode()!=200)
            return null;

        HttpEntity responseEntity = response.getEntity();
        if(responseEntity==null)
            return null;

        InputStream is = response.getEntity().getContent();
        String responseContent = (String) IOUtils.toString(is, StandardCharsets.UTF_8);
        return new Gson().fromJson(responseContent, RemoteConfigurationCCCResponse.class);

    }

    public List<RemoteSoftwareUpgradeCCC> getRemoteSoftwareUpgradeStates(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remotesoftwareupgrade/installations");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), RemoteSoftwareUpgradeCCCResponse.class).results;
    }


    public List<RemoteConfigurationCCC> getRemoteConfigurationStates(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/remoteconfiguration/deploy");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), RemoteConfigurationCCCResponse.class).results;

    }

    public DispenseLimitSettingsCCC[] getDispenseLimitSettings(String tenant, String machineGrouptoken) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/configuration");
        builder.setParameter("groupToken",machineGrouptoken);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        try{
            HttpEntity res =response.getEntity();
            String re =EntityUtils.toString(res);
            return new Gson().fromJson(re, DispenseLimitSettingsCCC[].class);

        }catch (EOFException e){
            DispenseLimitSettingsCCC[] rEmpty = new DispenseLimitSettingsCCC[1];
            DispenseLimitSettingsCCC d = new DispenseLimitSettingsCCC();
            d.ident="-1";
            rEmpty[0] = d;
            return  rEmpty;
        }
    }

    public List<MachineGroupCCC> getMachineGroups(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/machinegroup");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), MachinegroupCCCResponse.class).results;
    }

    public int deleteMachineGroup(String machineGroupToken, String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/machinegroup");
        builder.setParameter("token",machineGroupToken);

        HttpDelete request = new HttpDelete(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();
    }

    public int deleteIdentFromGroup(String groupToken, String ident, String tenant, boolean deleteAll) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/configuration");
        builder.setParameter("groupToken",groupToken);
        builder.setParameter("id",ident);
        if(deleteAll)
            builder.setParameter("deleteAll",Boolean.toString(deleteAll));

        HttpDelete request = new HttpDelete(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();
    }


    public int putDLSettingIdent(String machineGroupToken, String tenant, String payload) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/configuration");
        builder.setParameter("groupToken",machineGroupToken);

        HttpPut request = new HttpPut(builder.build());
        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        request.setEntity(new StringEntity(payload));
        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();

    }

    public DispenseLimitSettingsWrapperCCCResponse postDLSSettings(String groupToken, String payload, String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/configuration");
        builder.setParameter("groupToken",groupToken);

        HttpPost request = new HttpPost(builder.build());
        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        request.setEntity(new StringEntity(payload));
        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;


        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), DispenseLimitSettingsWrapperCCCResponse.class);

    }

    public WokrUnitZTSCCC[] getWorkUnits(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/work-units");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), WokrUnitZTSCCC[].class);
    }

    public List<MachineUserCCC> getMachineUsers(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/machineusers");
        builder.setParameter("sortOrder","asc");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()),  MachineUserCCCResponse.class).results;
    }

    public MachineUserRoleCCC[] getMachineUserRoles(String tenant) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/roles");

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()),  MachineUserRoleCCC[].class);
    }

    public DispenseLimitSettingsWrapperCCCResponse getMachineGroupDLSettings(String tenant, String machineGroupToken) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/configuration/machinegroup/"+machineGroupToken);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()),  DispenseLimitSettingsWrapperCCCResponse.class);
    }

    public DispenseLimitSettingsWrapperCCCResponse putMachineGroupDLSettings(String tenant, String machineGroupToken, String payload) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/distributeddispenselimit/configuration/machinegroup/"+machineGroupToken);
    //    builder.setParameter("groupToken",machineGroupToken);

        HttpPut request = new HttpPut(builder.build());
        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        request.setEntity(new StringEntity(payload));
        HttpResponse response = httpClient.execute(request);
        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()),  DispenseLimitSettingsWrapperCCCResponse.class);
    }

    public MachineGroupCCC postMachineGroup(String tenant, String payload) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/machinegroup");

        HttpPost request = new HttpPost(builder.build());
        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        request.setEntity(new StringEntity(payload));
        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;


        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), MachineGroupCCC.class);

    }

    public List<SiteCCC> getSites(String tenantId) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/sites");
        builder.setParameter("pageSize",Integer.toString(1000));
        builder.setParameter("includeAssignments",Boolean.toString(false));
        builder.setParameter("includeDeleted",Boolean.toString(false));
        System.out.println("Fetching sites for "+tenantId);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenantId,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        //TenantResponse tr= new Gson().fromJson(EntityUtils.toString(response.getEntity()), TenantResponse.class);
        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), SitesResponse.class).results;
    }


    public SiteCCC getSite(String tenantId, String siteToken) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/sites/"+siteToken);
        builder.setParameter("pageSize",Integer.toString(1000));
        builder.setParameter("includeAssignments",Boolean.toString(false));
        builder.setParameter("includeDeleted",Boolean.toString(false));
        System.out.println("Fetching sites for "+tenantId);

        HttpGet request = new HttpGet(builder.build());
        setGeneralRequestHeaders(request,"application/x-www-form-urlencoded");
        setAuthRequestHeaders(request,tenantId,token);

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), SiteCCC.class);
    }

    public WebhookToken postWebhookRecipient() throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/token");
        HttpPost request = new HttpPost(builder.build());

        request.setEntity(new StringEntity("{\"timeout\":\"0\"}"));
        HttpResponse response = httpClient.execute(request);

        if(201 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), WebhookToken.class);

    }

    public WebhookResponse getWebhookRequests(String token, String sorting, int pageSize, int page) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(baseURL+"/token/"+token+"/requests");
        builder.setParameter("per_page",Integer.toString(pageSize));
        builder.setParameter("page",Integer.toString(page));
        builder.setParameter("sorting",sorting);

        HttpGet request = new HttpGet(builder.build());

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

//        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), Object.class);
        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), WebhookResponse.class);
    }

    public WebhookData getWebhookRequestLatest(String token) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(baseURL+"/token/"+token+"/request/latest");

        HttpGet request = new HttpGet(builder.build());

        HttpResponse response = httpClient.execute(request);

        if(200 != response.getStatusLine().getStatusCode())
            return null;

        return new Gson().fromJson(EntityUtils.toString(response.getEntity()), WebhookData.class);
    }

    public int deleteAllWebhookRequests(String token) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/token/"+token+"/request/");
        HttpDelete request = new HttpDelete(builder.build());
        HttpResponse response = httpClient.execute(request);
        return response.getStatusLine().getStatusCode();

    }

    public int postSite(String tenant, String payload) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/sites");

        HttpPost request = new HttpPost(builder.build());
        request.setEntity(new StringEntity(payload));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();

    }

    public int postDevice(String tenant, String payload) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/devices");

        HttpPost request = new HttpPost(builder.build());
        request.setEntity(new StringEntity(payload));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();

    }

    public int postDeviceAssignment(String tenant, String payload) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(baseURL+"/api/v1/assignments");

        HttpPost request = new HttpPost(builder.build());
        request.setEntity(new StringEntity(payload));

        setGeneralRequestHeaders(request,"application/json");
        setAuthRequestHeaders(request,tenant,token);

        HttpResponse response = httpClient.execute(request);

        return response.getStatusLine().getStatusCode();
    }


}
