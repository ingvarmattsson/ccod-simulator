package rest;

public class Token {

    //old apim
    public String access_token;
    public int expires_in;
    public String refresh_token;
    public String scope;
    public String token_type;

    //auth
    public String accessToken;
    public String expiresIn;
    public String refreshToken;
    public String tokenType;
}
