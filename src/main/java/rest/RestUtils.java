package rest;

import cccObjects.*;
import ccodSession.Environments;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static ccodSession.Environments.UAT;

public class RestUtils {

    public static Map<DeviceCCC, SiteCCC> getDevicesFromDifferentSites(Environments env, String tenant, int amountOfDevices) throws IOException, URISyntaxException {
        List<DeviceCCC> allDevices = RestUtils.getMachinesPagedAllMachines(env,tenant);
        Map<String, DeviceCCC> siteDevice = new HashMap<>();
        for(DeviceCCC d : allDevices){
            siteDevice.put(d.siteToken,d);
            if(siteDevice.keySet().size()>=amountOfDevices)
                break;
        }

        Map<DeviceCCC, SiteCCC> devicesSites = new HashMap<>();
        for(String siteToken : siteDevice.keySet()){

            RestHandler restHandler = new RestHandler(env);
            restHandler.getTokenAuth();
            SiteCCC site = restHandler.getSite(tenant,siteToken);
            devicesSites.put(siteDevice.get(siteToken),site);
        }

        return devicesSites;

    }

    public static List<DeviceCCC> getMachinesPagedAllMachines(Environments env, String tenant) throws IOException, URISyntaxException {
        List<DeviceCCC> devices = new LinkedList<>();
        RestHandler restHandler = new RestHandler(env);
        restHandler.getTokenAuth();
        DeviceCCCResponse response = restHandler.getMachinesPaged(tenant,1, 1);
        System.out.println("Getting machines for "+tenant);
        System.out.println("Amount machines: "+response.numResults);
        int returnedAmount = 0;
        int page = 1;
        while (returnedAmount<response.numResults){
            restHandler = new RestHandler(env);
            restHandler.getTokenAuth();
            DeviceCCCResponse res = restHandler.getMachinesPaged(tenant,page, 500);
            devices.addAll(res.results);
            returnedAmount+= res.results.size();
            System.out.println("Fetching page "+page+", numbers fetched: "+devices.size());
            page++;
        }
        return devices;
    }

    public static List<MachineDetailCCC> getMachineDetailsPagedAllMachines(Environments env, String tenant) throws IOException, URISyntaxException {
        List<MachineDetailCCC> machineDetails = new LinkedList<>();
        RestHandler restHandler = new RestHandler(env);
        restHandler.getTokenAuth();
        MachineDetailCCCResponse response = restHandler.getMachineDetailsPaged(tenant,7, 1);
        System.out.println("Getting machine details for "+tenant);
        System.out.println("Amount machines: "+response.numResults);
        int returnedAmount = 0;
        int page = 1;
        while (returnedAmount<response.numResults){
            restHandler = new RestHandler(env);
            restHandler.getTokenAuth();
            MachineDetailCCCResponse res = restHandler.getMachineDetailsPaged(tenant,page, 100);
            machineDetails.addAll(res.results);
            returnedAmount+= res.results.size();
            System.out.println("Fetching page "+page+", numbers fetched: "+machineDetails.size());
            page++;
        }

        return machineDetails;
    }

    public static void main(String[] args) {
        try {
            List<MachineDetailCCC> machineDetails = getMachineDetailsPagedAllMachines(UAT,"sdretail");
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }




}
