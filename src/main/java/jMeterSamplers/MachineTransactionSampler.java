package jMeterSamplers;

import com.fasterxml.jackson.core.JsonProcessingException;
import data.Utilities;
import mqtt.MqTTHandler;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.eclipse.paho.client.mqttv3.MqttException;
import rmObjects.RmHandler;
import rmObjects.RmTransaction;
import rmObjects.RmUserCommon;

import static data.DefaultStrings.mqttPassWord;
import static data.DefaultStrings.mqttUserName;

public class MachineTransactionSampler extends AbstractJavaSamplerClient {

    //RM Transaction variables
    private static String machineUUID ="JM-02"; // get machineID from JMeter csv file (each thread = 1 unique machine)
    private static String machineUserName="machineUserA"; // threadName JMeter?
    private static String machineUserId="0001";
    private static String machineUserRole ="administrator";
    private static String timeZoneOffset ="+02:00"; // "+01:00" CET
    private static double totalAmount = 50.50;
    private static int transactionSequence =1;
    private static String currency ="EUR";
    private static String type ="DEPOSIT";
    private static String subType ="";
    private static String workUnitName="WU-"+ machineUUID;
    private static String workUnitGroup="Barcelona";
    private static long rmTimeoutThreshold=30l;
    private static String dateNow= "";//"20190101 01:01:00.000";
    private static String transactionValueBag ="[{\"TOTAL\":29646.0,\"MACHINE_MODELS\":[{\"TOTAL\":34000,\"COUNT\":17,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":2000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":210000,\"COUNT\":42,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":5000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":190000,\"COUNT\":19,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":10000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":60000,\"COUNT\":3,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":20000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":4600,\"COUNT\":23,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":200,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":140000,\"COUNT\":28,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":5000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":350000,\"COUNT\":35,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":10000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":36000,\"COUNT\":18,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":2000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":140000,\"COUNT\":7,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":20000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":1800000,\"COUNT\":18,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":100000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"NOTE\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false}],\"MACHINE_MODEL_TYPE\":\"NOTE\",\"REJECTS\":0},{\"TOTAL\":3110.67,\"MACHINE_MODELS\":[{\"TOTAL\":67,\"COUNT\":67,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":1,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":470,\"COUNT\":94,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":5,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":1080,\"COUNT\":54,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":20,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":6100,\"COUNT\":122,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":50,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":38800,\"COUNT\":194,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":200,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":2700,\"COUNT\":108,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":25,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":150,\"COUNT\":3,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":50,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":30200,\"COUNT\":151,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":200,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":93500,\"COUNT\":187,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":500,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false},{\"TOTAL\":138000,\"COUNT\":138,\"DECIMALS\":2,\"CURRENCY\":\"EUR\",\"DENOMINATION\":1000,\"PIECE_VALUE\":0,\"EXCHANGE_RATE\":0,\"EXCHANGE_RATE_DECIMALS\":0,\"MACHINE_MODEL_TYPE\":\"COIN\",\"C2_COUNT\":0,\"C3_COUNT\":0,\"C4B_COUNT\":0,\"CREDIT_CAT_2\":false,\"CREDIT_CAT_3\":false}],\"MACHINE_MODEL_TYPE\":\"COIN\",\"REJECTS\":0}]";




    //MqTT variables
    private static String brokerHost ="device-test.cashcomplete.com";
    private static String tenant="testautomation";

    //Sampler parameter keys
    private static final String kBrokerHost="BROKER_HOST";
    private static final String kTenant="TENANT";
    private static final String kMachineUUID="MACHINE_UUID";
    private static final String kMachineUserId="MACHINE_USER_ID";
    private static final String kMachineUserName="MACHINE_USER_NAME";
    private static final String kMachineUserRole="MACHINE_USER_ROLE" ;
    private static final String kTransactionType="TRANSACTION_TYPE";
    private static final String kTransactionSubType="TRANSACTION_SUB_TYPE";
    private static final String kTransactionCurrency="TRANSACTION_CURRENCY";
    private static final String kTransactionTotals="TRANSACTION_TOTALS";
    private static final String kTransactionSequence="TRANSACTION_SEQUENCE";
    private static final String kWorkUnitName="WORKUNIT_NAME";
    private static final String kWorkUnitGroup="WORKUNIT_GROUP";
    private static final String kDateTime="DATE_TIME";
    private static final String kTimeZoneOffset="TIME_ZONE_OFFSET";
    private static final String kRMTimeOutThreshold="RM_TIMEOUT_THRESHOLD";
    private static final String kTransactionValueBag="TRANSACTION_VALUE_BAG";

    @Override
    public Arguments getDefaultParameters(){
        Arguments defaultParameters = new Arguments();
        defaultParameters.addArgument(kBrokerHost, brokerHost);
        defaultParameters.addArgument(kTenant,tenant);
        defaultParameters.addArgument(kMachineUUID, machineUUID);
        defaultParameters.addArgument(kMachineUserId, machineUserId);
        defaultParameters.addArgument(kMachineUserName, machineUserName);
        defaultParameters.addArgument(kMachineUserRole, machineUserRole);
        defaultParameters.addArgument(kTransactionType, type);
        defaultParameters.addArgument(kTransactionSubType, subType);
        defaultParameters.addArgument(kTransactionCurrency, currency);
        defaultParameters.addArgument(kTransactionTotals, Double.toString(totalAmount));
        defaultParameters.addArgument(kTransactionSequence, Integer.toString(transactionSequence));
        defaultParameters.addArgument(kTransactionValueBag, "");
        defaultParameters.addArgument(kWorkUnitName,workUnitName);
        defaultParameters.addArgument(kWorkUnitGroup, workUnitGroup);
        defaultParameters.addArgument(kDateTime, dateNow);
        defaultParameters.addArgument(kTimeZoneOffset, timeZoneOffset);
        defaultParameters.addArgument(kRMTimeOutThreshold, Long.toString(rmTimeoutThreshold));

        return defaultParameters;
    }

    @Override
    public SampleResult runTest(JavaSamplerContext context) {

        SampleResult sampleResult=null;

        //generate MachineUser
        brokerHost = context.getParameter(kBrokerHost);
        tenant = context.getParameter(kTenant);
        machineUserId = context.getParameter(kMachineUserId);
        machineUserName = context.getParameter(kMachineUserName);
        machineUserRole = context.getParameter(kMachineUserRole);
        workUnitName = context.getParameter(kWorkUnitName);
        workUnitGroup = context.getParameter(kWorkUnitGroup);
        machineUUID = context.getParameter(kMachineUUID);

        RmUserCommon rmUser = RmHandler.generateUser(machineUserId,machineUserName,machineUserRole,workUnitName, workUnitGroup, null, null);

        //generate RM-transaction
        dateNow = context.getParameter(kDateTime);
        if (dateNow.isEmpty())
            dateNow = Utilities.getCurrentDateStampIncludedCatchingUp();

        double totAmount = Double.parseDouble(context.getParameter(kTransactionTotals));
        String currency = context.getParameter(kTransactionCurrency);
        String type = context.getParameter(kTransactionType);
        String subType = context.getParameter(kTransactionSubType);
        transactionSequence = Integer.parseInt(context.getParameter(kTransactionSequence));
        String timeZoneOffset = context.getParameter(kTimeZoneOffset);
        transactionValueBag = context.getParameter(kTransactionValueBag);
        RmTransaction transaction = null;//RmHandler.generateTransaction(dateNow, machineUUID,rmUser,transactionSequence,totAmount,currency,type,subType,timeZoneOffset,transactionValueBag);

        //Send to MqTT
        sampleResult = new SampleResult();
        StringBuilder sb = new StringBuilder();
        sb.append("MachineUUID: "+ machineUUID +"\n");
        sb.append("User: "+machineUserId+" - "+machineUserName+"\n");
        sb.append("Workunit: "+workUnitName+"\n");

        try {
            String payload = RmHandler.parseRmToString(transaction);
            sb.append("Payload: "+payload);

            MqTTHandler mqtt = new MqTTHandler(brokerHost,tenant,machineUUID,mqttUserName,mqttPassWord);
            mqtt.connect();
            sampleResult.sampleStart();
            mqtt.publishMessage(payload);
            mqtt.waitForProcessingToFinish(rmTimeoutThreshold);
            sampleResult.sampleEnd(); // stop stopwatch
            String mqttResponse = mqtt.getResponsedata();
            mqtt.disconnect();


            if(mqttResponse!=null){

                sampleResult.setSuccessful( true );
                sampleResult.setResponseMessage( "Successfully performed transaction\n"+sb.toString());
                sampleResult.setResponseCodeOK(); // 200 code

                String dateMessageSent = "\"MESSAGE_DATE\":";
                String date = Utilities.parseDateToQueryStringParam(dateNow);
                dateMessageSent+="\""+date+"\"";
                String totalMessageSent = "\"MESSAGE_TOTAL\":\""+context.getParameter(kTransactionTotals)+"\"";
                String responseData=dateMessageSent+totalMessageSent+mqttResponse;
                sampleResult.setResponseData(responseData,"UTF-8");

            }else {

                sampleResult.setSuccessful( false );
                sampleResult.setResponseMessage( "Message not delivered\n"+sb.toString());
                sampleResult.setResponseCode("500"); // 200 code

            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e, sb.toString());

        } catch (MqttException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e, sb.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e, sb.toString());
        }

        return sampleResult;
    }


    public static void main(String[] args) throws JsonProcessingException {

        //Create RM description
        RmUserCommon rmUser = RmHandler.generateUser(machineUserId, machineUserName, machineUserRole, workUnitName, workUnitGroup, null, null);
        if (dateNow.isEmpty())
            dateNow = Utilities.getCurrentDateStampIncludedCatchingUp();

        transactionValueBag="";
        RmTransaction transaction = null;//RmHandler.generateTransaction(dateNow, machineUUID, rmUser, transactionSequence,totalAmount,currency,type,subType,timeZoneOffset,transactionValueBag);


        try {
            String payload = RmHandler.parseRmToString(transaction);

            MqTTHandler mqtt = new MqTTHandler(brokerHost,tenant,machineUUID, mqttUserName,mqttPassWord);
            mqtt.connect();
            mqtt.publishMessage(payload);
            mqtt.waitForProcessingToFinish(rmTimeoutThreshold);
            String mqttResponse = mqtt.getResponsedata();
            mqtt.disconnect();
            System.out.println("Mqtt response: "+mqttResponse);



        } catch (JsonProcessingException e) {
            e.printStackTrace();

        } catch (MqttException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



    }

}
