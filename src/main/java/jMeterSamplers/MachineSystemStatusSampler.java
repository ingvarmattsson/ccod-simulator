/*
public class MachineSystemStatusSampler extends AbstractJavaSamplerClient {

    private static String brokerHost ="device-test.cashcomplete.com", tenant="testautomation", machineUUID="JM-01",
            status="Idle", oldStatus="Disconnected", timeZoneOffset="+02:00", sequence="13";
    private static long rmTimeoutThreshold=30l;


    //Sampler parameter keys
    private static final String kBrokerHost="BROKER_HOST";
    private static final String kTenant="TENANT";
    private static final String kMachineUUID="MACHINE_UUID";
    private static final String kStatus="STATUS";
    private static final String kOldStatus="OLD_STATUS";
    private static final String kSequence="SEQUENCE";
    private static final String kTimeZoneOffset="OFFSET";
    private static final String kRMTimeOutThreshold="RM_TIMEOUT_THRESHOLD";

    @Override
    public SampleResult runTest(JavaSamplerContext context) {

        brokerHost = context.getParameter(kBrokerHost);
        tenant = context.getParameter(kTenant);
        machineUUID = context.getParameter(kMachineUUID);
        status = context.getParameter(kStatus);
        oldStatus = context.getParameter(kOldStatus);
        sequence = context.getParameter(kSequence);
        timeZoneOffset = context.getParameter(kTimeZoneOffset);

        String dateTime= Utilities.getCurrentDateStampIncludedCatchingUp();
        Long seq = Long.parseLong(sequence);
        //Create system status message, send to mqtt and get reply
        RmHandler handler = new RmHandler();
        RmSystemStatus rmStatus = handler.generateSystemStatusMessage(machineUUID,status,oldStatus,dateTime,timeZoneOffset,seq);
        SampleResult sampleResult = new SampleResult();
        String payLoad="";

        try {

            payLoad = RmHandler.parseRmToString(rmStatus);
            MqTTHandler mqtt = new MqTTHandler(brokerHost,tenant,machineUUID);
            mqtt.connect();
            sampleResult.sampleStart();
            mqtt.publishMessage(payLoad);
            mqtt.waitForProcessingToFinish(rmTimeoutThreshold);
            sampleResult.sampleEnd();
            String mqttResponse = mqtt.getResponsedata();
            mqtt.disconnect();

            if(mqttResponse!=null){

                sampleResult.setSuccessful( true );
                sampleResult.setResponseMessage( "Successfully performed transaction\n"+"Payload: "+payLoad);
                sampleResult.setResponseCodeOK(); // 200 code
                sampleResult.setResponseData(mqttResponse,"UTF-8");

            }else {

                sampleResult.setSuccessful( false );
                sampleResult.setResponseMessage( "Message not delivered\n"+"Payload: "+payLoad);
                sampleResult.setResponseCode("500"); // 200 code

            }



        } catch (JsonProcessingException | MqttException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e,"Payload: "+payLoad);
        } catch (InterruptedException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e,"Payload: "+payLoad);
        }


        return sampleResult;
    }

    @Override
    public Arguments getDefaultParameters() {
        Arguments defaultParameters = new Arguments();
        defaultParameters.addArgument(kBrokerHost, brokerHost);;
        defaultParameters.addArgument(kTenant, tenant);;
        defaultParameters.addArgument(kMachineUUID, machineUUID);;
        defaultParameters.addArgument(kStatus,status);;
        defaultParameters.addArgument(kOldStatus,oldStatus);
        defaultParameters.addArgument(kSequence,sequence);
        defaultParameters.addArgument(kRMTimeOutThreshold, Long.toString(rmTimeoutThreshold));
        return defaultParameters;
    }

    public static void main(String[] args) {
        String dateTime= Utilities.getCurrentDateStampIncludedCatchingUp();
        //Create system status message

        Long seq = Long.parseLong(sequence);
        RmHandler handler = new RmHandler();
        RmSystemStatus rmStatus = handler.generateSystemStatusMessage(machineUUID,status,oldStatus,dateTime,timeZoneOffset, seq);
        try {

            String payLoad = RmHandler.parseRmToString(rmStatus);
            MqTTHandler mqtt = new MqTTHandler(brokerHost,tenant,machineUUID);
            mqtt.connect();
            long startTime = System.currentTimeMillis();

            mqtt.publishMessage(payLoad);
            mqtt.waitForProcessingToFinish(rmTimeoutThreshold);
            long endTime = System.currentTimeMillis();
            String mqttResponse = mqtt.getResponsedata();

            mqtt.disconnect();
            System.out.println("response time: "+(endTime-startTime));
            System.out.println(mqttResponse);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}


*/