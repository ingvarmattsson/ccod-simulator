/*
public class BoxContentSampler extends AbstractJavaSamplerClient {

    private static String kBrokerHost="BROKER_HOST";
    private static String kTenant="TENANT";
    private static String kMachineId="MACHINE_UUID";
    private static String kDateTime="DATE_TIME";
    private static String kDevice="DEVICE";
    private static String kDeviceId="DEVICE_ID";
    private static String kDeviceType="DEVICE_TYPE";
    private static String kValueType="VALUE_TYPE";
    private static String kTransactionSequence="TRANSACTION_SEQUENCE";
    private static String kMessageSequence="MESSAGE_SEQUENCE";
    private static String kBoxContent="BOX_CONTENT";
    private static String kTimezoneOffset="UTC_OFFSET";
    private static String kSwVersion="SOFTWARE_VERSION";


    private long rmTimeoutThreshold = 30l;


    @Override
    public SampleResult runTest(JavaSamplerContext context) {

        SampleResult sampleResult = new SampleResult();

        String brokerHost = context.getParameter(kBrokerHost);
        String tenant = context.getParameter(kTenant);
        String machineId = context.getParameter(kMachineId);
        String dateTime = context.getParameter(kDateTime);
        String timeZoneOffset = context.getParameter(kTimezoneOffset);
        String device = context.getParameter(kDevice);
        String deviceId = context.getParameter(kDeviceId);
        String deviceType = context.getParameter(kDeviceType);
        String valueType = context.getParameter(kValueType);
        String transactionSequence = context.getParameter(kTransactionSequence);
        String messageSequence = context.getParameter(kMessageSequence);
        String boxContent = context.getParameter(kBoxContent);

        if(dateTime.isEmpty())
            dateTime = Utilities.getCurrentDateStampIncludedCatchingUp();

        int transSeq=-1;
        if(!transactionSequence.isEmpty())
            transSeq = Integer.parseInt(transactionSequence);

        RmBoxContent rm = RmHandler.generateBoxContent(
                dateTime,
                timeZoneOffset,
                machineId,
                transSeq,
                Long.parseLong(messageSequence),
                device,
                deviceId,
                deviceType,
                valueType,
                boxContent);

        try {
            String payload = RmHandler.parseRmToString(rm);

            MqTTHandler mqtt = new MqTTHandler(brokerHost,tenant,machineId);
            mqtt.connect();
            sampleResult.sampleStart();
            mqtt.publishMessage(payload);
            mqtt.waitForProcessingToFinish(rmTimeoutThreshold);
            sampleResult.sampleEnd(); // stop stopwatch
            String mqttResponse = mqtt.getResponsedata();
            mqtt.disconnect();

            if(mqttResponse!=null){

                sampleResult.setSuccessful( true );
                sampleResult.setResponseMessage( "Successfully performed boxContent message\nPayload: "+payload);
                sampleResult.setResponseCodeOK(); // 200 code
                sampleResult.setResponseData(mqttResponse,"UTF-8");

            }else {

                sampleResult.setSuccessful( false );
                sampleResult.setResponseMessage( "Message not delivered\nPayload: "+payload);
                sampleResult.setResponseCode("500"); // 200 code

            }

        }catch (JsonProcessingException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e, "Failed to parse to json");

        } catch (MqttException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e, "Mqtt");
        } catch (InterruptedException e) {
            e.printStackTrace();
            MqTTHandler.addErrorMessageToResult(sampleResult,e, "Wait interrupted");
        }

        return sampleResult;

    }

    @Override
    public Arguments getDefaultParameters(){
        Arguments defaultParameters = new Arguments();
        defaultParameters.addArgument(kBrokerHost, "");
        defaultParameters.addArgument(kTenant, "");
        defaultParameters.addArgument(kMachineId, "");
        defaultParameters.addArgument(kDateTime, "");
        defaultParameters.addArgument(kTimezoneOffset, "");
        defaultParameters.addArgument(kDevice, "");
        defaultParameters.addArgument(kDeviceId, "");
        defaultParameters.addArgument(kDeviceType, "");
        defaultParameters.addArgument(kValueType, "");
        defaultParameters.addArgument(kTransactionSequence, "");
        defaultParameters.addArgument(kMessageSequence, "");
        defaultParameters.addArgument(kBoxContent, "");
        defaultParameters.addArgument(kSwVersion, "");

        return defaultParameters;
    }

    public static void main(String[] args) {
        String machineUUID="JM-01";
        String dateNow="";
        String timeZoneOffset="+02:00";
        String brokerHost ="device-test.cashcomplete.com";
        String tenant="testautomation";
        int rmTimeoutThreshold=30;


        CCoDMachine m = new CCoDMachine(MachineModels.RCS_500,"3.7.0",machineUUID, Currency.EUR);
        List<RmBoxContent> boxes = m.getBoxContents();
        List<RmBoxContent> rmPayLoads = new LinkedList<>();



        try {

            int transactionSequence=10;
            for(RmBoxContent box : boxes){
                String bc = RmHandler.parseRmBoxesToString(box.BOX_CONTENTS);
                rmPayLoads.add(RmHandler.generateBoxContent(
                        dateNow,
                        timeZoneOffset,
                        machineUUID,
                        transactionSequence,
                        transactionSequence,
                        box.DEVICE,
                        box.DEVICE_ID,
                        box.DEVICE_TYPE,
                        box.VALUE_TYPE,
                        bc
                ));
                transactionSequence++;
            }

            String payload = null;

            MqTTHandler mqtt = new MqTTHandler(brokerHost,tenant,machineUUID);
            mqtt.connect();

            for(RmBoxContent rmPayload : rmPayLoads){
                long startTime = System.currentTimeMillis();

                payload = RmHandler.parseRmToString(rmPayload);
                mqtt.publishMessage(payload);
                mqtt.waitForProcessingToFinish(rmTimeoutThreshold);
                String mqttResponse = mqtt.getResponsedata();

                System.out.println("Duration: "+(System.currentTimeMillis()-startTime));
                System.out.println("Response message: "+ mqttResponse);

            }
            mqtt.disconnect();

        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        catch (MqttException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
*/