package emailHandler;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        EmailHandler h = new EmailHandler();
        try {
            h.connect();
            List<Message> messageList = h.getMessages();
            System.out.println("\n\n"+h.getTextFromMessage(messageList.get(0)));
            h.disconnect();

//            h.purgeInbox();
//            h.printMessages();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
