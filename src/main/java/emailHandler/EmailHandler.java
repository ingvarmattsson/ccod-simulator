package emailHandler;

import ccodRestAssured.EmailPOJO;
import data.DefaultStrings;

import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

public class EmailHandler {

    private String host = "pop.mail.yahoo.com";
    private String username = DefaultStrings.yahooUserName;
    private String password = DefaultStrings.yahooPassword;
    private Store store;
    private Folder emailFolder;
    private String attachmentSaveDirectory = System.getProperty("user.dir")+"\\scheduledReportAttachments";

    public String getEmailAttachmentSaveDirectory(){
        return attachmentSaveDirectory;
    }

    public void connect() throws MessagingException {
        System.out.print("EmailHandler - Connecting...");
        Properties properties = new Properties();
        properties.put("mail.pop3.host", host);
        properties.put("mail.pop3.port", "995");
        properties.put("mail.pop3.starttls.enable", "true");
        Session emailSession = Session.getDefaultInstance(properties);

        store = emailSession.getStore("pop3s");
        store.connect(host, username, password);

        emailFolder = store.getFolder("INBOX");
        emailFolder.open(Folder.READ_WRITE);
        System.out.println("Done");
    }

    public void disconnect() throws MessagingException {
        System.out.println("EmailHandler - Disconnecting");
        emailFolder.close(true);
        store.close();
    }

    public List<Message> getMessages() throws MessagingException {
        System.out.println("EmailHandler - Getting messages");
        return Arrays.asList(emailFolder.getMessages());

    }

    public void purgeInbox() throws MessagingException {
        System.out.println("EmailHandler - Purging inbox");
        Message[] messages = emailFolder.getMessages();
        for (int i =0; i<messages.length; i++){
            messages[i].setFlag(Flags.Flag.DELETED,true);
        }
    }

    public void printMessages() throws MessagingException, IOException {
        connect();
        System.out.println("EmailHandler - Printing messages in inbox");
        Message[] messages = emailFolder.getMessages();
        System.out.println("messages.length---" + messages.length);

        for (int i = 0, n = messages.length; i < n; i++) {
            Message message = messages[i];
            message.setFlag(Flags.Flag.SEEN,true);

            System.out.println("---------------------------------");
            System.out.println("Email Number " + (i + 1));
            System.out.println("Subject: " + message.getSubject());
            System.out.println("From: " + message.getFrom()[0]);
            System.out.println("Text: " + message.getContent().toString());
            System.out.println("Deleted: "+message.isSet(Flags.Flag.DELETED));

        }
        disconnect();

    }


    public String getTextFromMessage(Message message) throws MessagingException, IOException {
        String result = "";
        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart)  throws MessagingException, IOException{
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart){
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }

    public List<String> getEmailContent(List<Message> emailMessages) throws MessagingException, IOException {
        List<String> titleContent =  new LinkedList<>();
        for (Message m : emailMessages)
            titleContent.add(m.getSentDate()+"<>"+m.getSubject()+"#"+getTextFromMessage(m));
        return titleContent;
    }

    public List<EmailPOJO> extractEmailPojos(List<String> emailTitleContent) {
        StringBuilder sb = new StringBuilder();
        List<EmailPOJO> emailPOJOS = new LinkedList<>();
        sb.append("Amount of emails: "+emailTitleContent.size()+"\n");
        for(String s : emailTitleContent){
            String date = s.substring(0,s.indexOf("<>"));
            String title = s.substring(s.indexOf("<>"),s.indexOf("#"));
            String content = s.substring(s.indexOf("#")+1,s.length());

            emailPOJOS.add(new EmailPOJO(title,content,date));
            sb.append("Date: "+date+"\n");
            sb.append("Title: "+title+"\n");
            sb.append("Content: "+content+"\n");
            sb.append("------------------------------------");
        }
        System.out.println(sb.toString());
        Collections.sort(emailPOJOS);
        return emailPOJOS;
    }

    public String getEmailAttachment(Message message, String fileNamePrefix) throws IOException, MessagingException {
        String contentType = message.getContentType();
        String messageContent = "";
        // store attachment file name, separated by comma
        String attachFiles = "";
        String fileName = null;

        if (contentType.contains("multipart")) {
            // content may contain attachments
            Multipart multiPart = (Multipart) message.getContent();
            int numberOfParts = multiPart.getCount();
            for (int partCount = 0; partCount < numberOfParts; partCount++) {
                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                    // this part is attachment
                    fileName = part.getFileName();
                    fileName = fileName.replaceAll("\\:","-");
                    fileName = fileNamePrefix+fileName;
                    attachFiles += fileName + ", ";
                    part.saveFile(attachmentSaveDirectory + File.separator + fileName);
                } else {
                    // this part may be the message content
                    messageContent = part.getContent().toString();
                }
            }

            if (attachFiles.length() > 1) {
                attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
            }
        } else if (contentType.contains("text/plain")
                || contentType.contains("text/html")) {
            Object content = message.getContent();
            if (content != null) {
                messageContent = content.toString();
            }
        }

        return fileName;
    }
}