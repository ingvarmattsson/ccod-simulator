package capabilityModules;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class Capability {

    String[] CON_M_BASE={"WEB_PORTAL","FLEET_MANAGEMENT","DASHBOARD","REPORTS","CCC_USER_MANAGEMENT"};
    String[] CON_M_ENTERPRISE={"SG","ALR","ERE","EDA"};
    String[] CON_M_REMOTE_DEVICE_MANAGEMENT = {"RC","RSU", "RT"};
    String[] CON_M_CASH_ORCHESTRATION = {"DDL","LA"};
    String[] CON_M_R_20 =  {"RR","ROD"};
    String[] CON_M_R_100 =  {"RR","ROD"};
    String[] CON_M_R_500 =  {"RR","ROD"};
    String[] CON_M_R_1500 =  {"RR","ROD"};
    String[] CON_M_R_3000 =  {"RR","ROD"};
    String[] CON_M_R_3000_PLUS =  {"RR","ROD"};

    String[] CON_M_T_20 =  {"TR","TOD","AC"};
    String[] CON_M_T_100 =  {"TR","TOD","AC"};
    String[] CON_M_T_500 =  {"TR","TOD","AC"};
    String[] CON_M_T_1500 =  {"TR","TOD","AC"};
    String[] CON_M_T_3000 =  {"TR","TOD","AC"};
    String[] CON_M_T_3000_PLUS =  {"TR","TOD","AC"};

    String[] CON_M_VPN =  {"IPS_IPSEC"};
    String[] CON_M_INTEGRATION =  {"API","ADH","SEF"};
    String[] CON_M_SINGLE_SIGN_ON =  {"SML"};
    String[] CON_M_PRIVATE_HOSTING =  {"CPH","PPH"};



    //not yet implemented
    String[] COD_M_DEVELOPER_MICRO =  {"SDK","SCR","API"};
    String[] COD_M_DEVELOPER_LITE =  {"SDK","SCR","API"};
    String[] COD_M_DEVELOPER_FULL =  {"SDK","SCR","API"};
    String[] COD_M_VAULT_CONNECT =  {};
    String[] COD_M_XFS =  {};

    private class Payload{
        Map modules;
        boolean activate;
    }

    private void run() {
        Payload payload = new Payload();
        payload.activate=true;
        payload.modules = new HashMap();
//        payload.modules.put("CON_M_BASE", CON_M_BASE);
//        payload.modules.put("CON_M_ENTERPRISE", CON_M_ENTERPRISE);
//        payload.modules.put("CON_M_REMOTE_DEVICE_MANAGEMENT", CON_M_REMOTE_DEVICE_MANAGEMENT);
//        payload.modules.put("CON_M_CASH_ORCHESTRATION", CON_M_CASH_ORCHESTRATION);
        payload.modules.put("CON_M_R_20", CON_M_R_20);
//        payload.modules.put("CON_M_R_100", CON_M_R_100);
//        payload.modules.put("CON_M_R_500", CON_M_R_500);
//        payload.modules.put("CON_M_R_1500", CON_M_R_1500);
//        payload.modules.put("CON_M_R_3000", CON_M_R_3000);
//        payload.modules.put("CON_M_R_3000_PLUS", CON_M_R_3000_PLUS);
//        payload.modules.put("CON_M_T_20", CON_M_T_20);
//        payload.modules.put("CON_M_T_100", CON_M_T_100);
//        payload.modules.put("CON_M_T_500", CON_M_T_500);
//        payload.modules.put("CON_M_T_1500", CON_M_T_1500);
//        payload.modules.put("CON_M_T_3000", CON_M_T_3000);
//        payload.modules.put("CON_M_T_3000_PLUS", CON_M_T_3000_PLUS);
//        payload.modules.put("CON_M_VPN", CON_M_VPN);
//        payload.modules.put("CON_M_INTEGRATION", CON_M_INTEGRATION);
//        payload.modules.put("CON_M_SINGLE_SIGN_ON", CON_M_SINGLE_SIGN_ON);
//        payload.modules.put("CON_M_PRIVATE_HOSTING", CON_M_PRIVATE_HOSTING);




        System.out.println(new Gson().toJson(payload));
    }

    public static void main(String[] args) {
        new Capability().run();
    }
}
