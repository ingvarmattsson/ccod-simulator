package ccodTracereader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ResponseTimeHandler {

    private class ResponseTimeDateStamp{
        private int responseTime;
        private String dateStamp;

        private ResponseTimeDateStamp(int responseTime, String dateStamp){
            this.responseTime=responseTime;
            this.dateStamp = dateStamp;

        }

        @Override
        public String toString(){
            return responseTime+ "ms. Logline: "+dateStamp;
        }
    }

    private String file ="G:\\CCoD\\UAT\\CashComplete-Demo-3.14.0-beta17\\OrchardJ\\trace\\orchardj_online.log";

    public static void main(String[] args) {

        try {
            new ResponseTimeHandler().parseLog();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseLog() throws IOException, ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String replyIndicatior="Waited for ";

        ResponseTimeDateStamp lowest = new ResponseTimeDateStamp(Integer.MAX_VALUE," ");
        ResponseTimeDateStamp highest = new ResponseTimeDateStamp(Integer.MIN_VALUE," ");
        int nbrOfNullResponses = 0;
        List<ResponseTimeDateStamp> rtList = new LinkedList<>();


        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                List<String> lineList = Arrays.asList(line.split(" "));


                if(lineList.size()>1){
                    String logDateStamp = lineList.get(0)+" "+lineList.get(1);

                    if (line.contains(replyIndicatior)) {
                        int i = line.indexOf(replyIndicatior);

                        String responseTimeSuffix = line.substring(i + replyIndicatior.length());

                        String[] responseTimeArr = responseTimeSuffix.split(" ");
                        int responseTime = Integer.parseInt(responseTimeArr[0]);

                        ResponseTimeDateStamp current = new ResponseTimeDateStamp(responseTime, logDateStamp);
                        if (responseTime >= highest.responseTime)
                            highest = current;
                        if (responseTime <= lowest.responseTime)
                            lowest = current;
                        if (responseTime >= 30000)
                            nbrOfNullResponses++;

                        rtList.add(current);
                    }

                }
            }



            int mean=0;

            for(ResponseTimeDateStamp r : rtList){
                System.out.println(r.toString());
                mean+= r.responseTime;
            }
            System.out.println("\n###RESULTS###");
            System.out.println("lowest response time: "+lowest.toString());
            System.out.println("highest response time: "+highest.toString());
            System.out.println("Number of null responses: "+nbrOfNullResponses+"\n");
            System.out.println("Mean: "+mean / rtList.size());



        }

    }
}
