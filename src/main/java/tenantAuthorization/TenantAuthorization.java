package tenantAuthorization;

import ccodSession.Environments;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import data.DefaultStrings;
import data.Utilities;
import rest.RestHandler;
import rest.Token;

import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TenantAuthorization {

    private HashMap<String, SiteWhereTenant> userAuthorizedForTenants;
    private boolean userCreated, userAuthorizedForOtherTenants;

    private boolean loadUserRequest = false;

    //If the user (email )already exists in the environment then he will only be authorized for the tenants, user info will not be overwritten
    private Environments env =Environments.DE;

    //if null user will be authorized for all tenants
    private String[] tenantsToAuthorize = {

    };


    //if null automated mail will not be sent
    private String fromEmail = "patrik.lind@paycomplete.com";

    private RestHandler restHandler;
    private Token token;
    private UserPayLoad user;
    private List<SiteWhereTenant> allTenants;


    public static void main(String[] args) {

        new TenantAuthorization().run();

    }

    private void run() {

        try {
            init();
            allTenants = loadTenants();

            userAuthorizedForTenants = getTenantsUserIsAuthorizedFor();

            if(userAuthorizedForTenants.keySet().size()<=0){
                userAuthorizedForOtherTenants = false;
                System.out.println("User not present in node, creating new user");
                userCreated = createUser();
            }else {
                userAuthorizedForOtherTenants = true;
                System.out.print("User already authorized for: ");
                for (String t : userAuthorizedForTenants.keySet())
                    System.out.print(t+" ");
                System.out.println("\nNot creating new user");
            }

            if(userAuthorizedForOtherTenants || userCreated)
                sendMail(authorizeUser());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException | MessagingException e) {
            e.printStackTrace();
        }

    }

    private void sendMail(List<String> authorizedFor) throws MessagingException {

        StringBuilder mailContent = new StringBuilder();
        mailContent.append("Hi,"+"\n\n");
        mailContent.append("Your account "+user.username);

        if(!userAuthorizedForOtherTenants)
            mailContent.append(" \\ "+user.password);

        mailContent.append(" is authorized on "+DefaultStrings.getCCCURL(env));
        mailContent.append(" to access following tenants:\n");

        for(String tenant:authorizedFor)
            mailContent.append(tenant+"\n");


        mailContent.append("\nBest Regards,\n"+fromEmail);

        System.out.println("Sending email:\n\n"+mailContent.toString());

        /*
        String host = "localhost";//or IP address

        //Get the session object
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        //compose the message

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(fromEmail));
        message.addRecipient(Message.RecipientType.TO,new InternetAddress(user.email));
        message.addRecipient(Message.RecipientType.TO,new InternetAddress("patrik.lind@suzohapp.com"));
        message.setSubject("CCC - Tenant Authorization");


        message.setText(sb.toString());

        // Send message
        Transport.send(message);
        System.out.println("message sent successfully....");
        */

    }


    private HashMap<String, SiteWhereTenant> getTenantsUserIsAuthorizedFor() {
        HashMap<String, SiteWhereTenant> tenants = new HashMap<>();

        for (SiteWhereTenant tenant : allTenants)
            for (String tUser : tenant.authorizedUserIds)
                if(tUser != null && tUser.toLowerCase().equals(user.username))
                    tenants.put(tenant.authenticationToken,tenant);

        return tenants;
    }

    private List<String> authorizeUser() throws IOException, URISyntaxException, InterruptedException {

        System.out.println("Authorizing "+user.username+" for tenants: ");
        List<String> successFullAuthorizations = new LinkedList<>();

        List<String> tenantsToAuthorizeFor = new LinkedList<>();
        if( tenantsToAuthorize == null || tenantsToAuthorize.length<=0){
            for (SiteWhereTenant t : allTenants)
                tenantsToAuthorizeFor.add(t.id);
        }

        else{
            for(int i = 0;i<tenantsToAuthorize.length;i++)
            tenantsToAuthorizeFor.add(tenantsToAuthorize[i]);
        }

        for (String tenant : tenantsToAuthorizeFor){

            if(userAuthorizedForTenants.get(tenant)==null){

                System.out.print(tenant+" - ");

                restHandler = new RestHandler(env);
                //restHandler.getToken();
                restHandler.getTokenAuth();
                int responseCode = restHandler.authorizeUser(tenant,user.username);
                System.out.println(responseCode);

                if(responseCode==200)
                    successFullAuthorizations.add(tenant);
            }
        }

        System.out.println("DONE!");
        return successFullAuthorizations;

    }

    private boolean createUser() throws URISyntaxException, IOException {
        String tenant;
        if (tenantsToAuthorize!=null && tenantsToAuthorize.length>0){
            tenant = tenantsToAuthorize[0];
        }
        else{
            tenant = allTenants.get(0).authenticationToken;
        }

        System.out.print("Registering "+user.username+ " on tenant "+tenant);
        int responseCode = restHandler.postUser(tenant,new Gson().toJson(user));
        System.out.println(" - "+responseCode);

        if(responseCode==201){
//            userAuthorizedForTenants.put(tenant,new SiteWhereTenant());
            return true;
        }
        return false;
    }

    private List<SiteWhereTenant> excludeTenants() {
        return null;
    }


    private void init() throws IOException {
        if(!loadUserRequest)
            user = new UserPayLoad();

        else {
            UserRequest ur = new ObjectMapper().readValue(new File("userRequest.json"),UserRequest.class);
            env = Environments.valueOf(ur.node.toUpperCase());
            tenantsToAuthorize = ur.tenantsToAuthorizeFor;
            user = new UserPayLoad(ur.user.getUsername(),ur.user.getPassword(),ur.user.getFirstName(),ur.user.getLastName());

        }

        if(user.password==null || user.password.isEmpty())
            user.password = Utilities.generatePassword(12);
        restHandler = new RestHandler(env);
        token = restHandler.getToken();


    }

    private List<SiteWhereTenant> loadTenants() throws IOException, URISyntaxException {
        return restHandler.getTenants("suzohapp");
    }
}
