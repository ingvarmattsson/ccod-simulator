package tenantAuthorization;

import ccodSession.Environments;
import com.fasterxml.jackson.databind.ObjectMapper;
import rest.RestHandler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AuthorizeExistingUsersForTenant {

    private Environments env = Environments.US_EAST;
    private String tenant = "ctdooleys";

    private String[] us_east_west_br_toAuthorize = {
            "steven.mccann@suzohapp.com",
            "paula.rodriguez@suzohapp.com",
            "patrick.davila@suzohapp.com",
            "perry.byrd@suzohapp.com",
            "mbuyi.khazi@suzohapp.com",
            "marcie.dolezel@suzohapp.com",
            "johnathan.lopez@suzohapp.com",
            "carlos.carrillo@suzohapp.com",
            "sylvain.kouakou@suzohapp.com",
            "edwin.gerena@suzohapp.com",
            "alex.alejandro@suzohapp.com",
            "nik.winder@suzohapp.com",
            "cassandra.navarro@suzohapp.com"
    };

    private String[] allNodesToAuthorize = {
            "robert.selse@suzohapp.com",
            "salvatore.macri@suzohapp.com",
            "andre.deijlen@suzohapp.com",
            "tom.huser@suzohapp.com",
            "pierre.monin@suzohapp.com",
            "carel.portier@suzohapp.com",
            "piotr.korczak@suzohapp.com",
            "phil.blears@suzohapp.com",
            "gregg.wallace@paycomplete.com",
            "victor.wallman-carlsson@paycomplete.com",
            "erik.vierveijzer@paycomplete.com",
            "luca.lissemore@paycomplete.com"
    };

    private String[] asia_toAuthorize = {};
    private String[] ch_toAuthorize = {};
    private String[] de_toAuthorize = {};

    public static void main(String[] args) throws IOException, URISyntaxException {
        new AuthorizeExistingUsersForTenant().run();
    }

    private void run() throws IOException, URISyntaxException {
        List<String> userstoAdd = selectUsersToAuthorize();
        editTenant(userstoAdd);
    }

    private void editTenant(List<String> userstoAdd) throws IOException, URISyntaxException {
        RestHandler rest = new RestHandler(env);
        rest.getToken();
        SiteWhereTenant t = rest.getTenant(tenant);
        t.authorizedUserIds.addAll(userstoAdd);
        rest.putTenant(new ObjectMapper().writeValueAsString(t),tenant);
    }

    private List<String> selectUsersToAuthorize() {
        switch (env){
            case US_WEST:
            case US_EAST:
            case LAT:
                return mergeUsers(allNodesToAuthorize,us_east_west_br_toAuthorize);
            case ASIA:
                return mergeUsers(allNodesToAuthorize,asia_toAuthorize);
            case CH:
                return mergeUsers(allNodesToAuthorize,ch_toAuthorize);
            case DE:
                return mergeUsers(allNodesToAuthorize,de_toAuthorize);

        }
        return null;
    }

    private List<String> mergeUsers(String[] allNodesToAuthorize, String[] nodeSpecificUsers) {
        List<String> users = new LinkedList<>();
        Collections.addAll(users,allNodesToAuthorize);
        Collections.addAll(users,nodeSpecificUsers);
        return users;
    }


}
