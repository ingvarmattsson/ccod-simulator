package tenantAuthorization;

import java.util.HashMap;
import java.util.List;

public class SiteWhereTenant {

    public String id;
    public String authenticationToken;
    public String name;
    public String updatedDate;
    public String updatedBy;
    public String createdDate;
    public String createdBy;
    public List<String> authorizedUserIds;
    public boolean deleted;
    public HashMap<String,String> metadata;
}
