package tenantAuthorization;

import ccodSession.Environments;

import java.util.List;

public class UserRequest {

    public String fromEmail;
    public String node;
    public String[] tenantsToAuthorizeFor;
    public UserPayLoad user;

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String[] getTenantAuthTokens() {
        return tenantsToAuthorizeFor;
    }

    public void setTenantAuthTokens(String[] tenantAuthTokens) {
        this.tenantsToAuthorizeFor = tenantAuthTokens;
    }

    public UserPayLoad getUser() {
        return user;
    }

    public void setUser(UserPayLoad user) {
        this.user = user;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
}
