package tenantAuthorization;

public class UserPayLoad {

    public String status = "Active";
    public String countryCode="+1";
    public String role="Admin";
    public String phone="123456";
    public String username="piotr.korczak@paycomplete.com";
    public String password="XvnYw5mCMutP"; // if null or empty a random 12 length password will be generated
    public String firstName="Piotr";
    public String lastName="Korczak";

    public String email=username;

    public UserPayLoad() {
    }

    public UserPayLoad(String username, String password, String firstName, String lastName) {
        this.username = this.email = username;
        this.password = password;
        this. firstName = firstName;
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}