package ztsAutomation;

import CCoD.Currency;
import CCoD.MachineModels;
import CCoD.TransactionType;
import cccObjects.*;
import ccodSession.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import data.DefaultStrings;
import data.Utilities;
import exceptions.ZTSException;
import org.eclipse.paho.client.mqttv3.MqttException;
import rest.RestHandler;
import rmObjects.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import static CCoD.Currency.*;
import static ztsAutomation.DLSRobustnessZTS.EditIdentType.*;

public class DLSRobustnessZTS extends Main {

    enum EditIdentType{
        CHANGE_CURRENCY, ADD_CURRENCY, REMOVE_CURRENCY, DELETE_IDENT
    }

    private String tenant = "testautomation";
    private Environments environment = Environments.UAT;


//    private String[] uuids = {"JM-01","JM-02"};
//    private Currency[] currencies = {EUR,EUR};
//    private String[] versions = {"3.12","3.13"};
//    private Map<String, CCoDSession> sessions;
    private CCoDSession logger;

    private CCoDSession simulatorProtocol12;
    private CCoDSession simulatorProtocol14;
    private String protocol12UUid = "JM-01";
    private String protocol14UUid = "JM-02";
    private String protocol12Version = "3.12";
    private String protocol14Version = "3.14";

    private long runtimeMin = 0;
    private List<MachineUserCCC> machineUsers;
    private WokrUnitZTSCCC[] workUnits;
    private MachineUserRoleCCC[] machineUserRoles;

    private RestHandler rh;
    private List<DeviceCCC> machines;

    private List<IdentPayloadDLS> dlSettingsIdents;
    private String testFailed = "TEST-FAILED";
    private String testPassed = "TEST-PASSED";
    private String testEval = "TEST-EVAL";

    private static String machineGroupToken="-1";

    public static void main(String[] args) {
        try {

            DLSRobustnessZTS dlsRobustness = new DLSRobustnessZTS();
            dlsRobustness.init();
            dlsRobustness.removeAllDLSFromTenant();
            dlsRobustness.testSettingsSynchronization(machineGroupToken);

            dlsRobustness.testStateSynchronization();



            dlsRobustness.end();

        } catch (IOException e) {
            e.printStackTrace();
        }  catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ZTSException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void end() throws InterruptedException, MqttException, IOException {
        simulatorProtocol12.stopSession();
        simulatorProtocol14.stopSession();
        saveSessionDataStore();
        System.exit(0);
    }

    private void init() throws IOException, URISyntaxException, ZTSException, MqttException {
        System.out.println("Initiating simulators & testdata");
        logger = initCCodSimulator("logger","3.14",EUR,environment,tenant,runtimeMin);
        simulatorProtocol12 = initCCodSimulator(protocol12UUid,protocol12Version,EUR,environment,tenant,runtimeMin);
        simulatorProtocol14 = initCCodSimulator(protocol14UUid,protocol14Version,EUR,environment,tenant,runtimeMin);
        dlSettingsIdents = new LinkedList<>();
        //initCCoDSimulators();
        initTestData();
        initRestHandler();
    }

    private void initRestHandler() throws IOException {
        rh = new RestHandler(environment);
        rh.getToken();
    }

    private void initTestData() throws IOException, ZTSException, URISyntaxException {
        RestHandler rest = new RestHandler(environment);
        rest.getToken();
        workUnits = rest.getWorkUnits(tenant);

        logger.log("DLS INIT TEST DATA");
        logger.log("DLS GET TEST DATA - GET workunits result: ");
        String message;
        if (workUnits == null){
            message = "GET distributeddispenselimit/work-units did not return 200, terminating";
            logger.log(message);
            throw new ZTSException(message);
        }

        if(workUnits.length<=0){
            message = "No workunits synced to the tenant: "+tenant;
            logger.log(message);
            throw new ZTSException(message);
        }

        for (int i =0 ; i<workUnits.length;i++){
            logger.log("\t"+new Gson().toJson(workUnits[i]));
        }

        logger.log("DLS GET TEST DATA - GET machine user roles result: ");
        machineUserRoles = rest.getMachineUserRoles(tenant);
        if (machineUserRoles == null){
            message = "GET distributeddispenselimit/work-units did not return 200, terminating";
            logger.log(message);
            throw new ZTSException(message);
        }

        if(machineUserRoles.length<=0){
            message = "No workunits synced to the tenant: "+tenant;
            logger.log(message);
            throw new ZTSException(message);
        }

        for (int i =0 ; i<machineUserRoles.length;i++){
            logger.log("\t"+new Gson().toJson(machineUserRoles[i]));
        }


        logger.log("DLS GET TEST DATA - GET machine uses result: ");
        machineUsers= rest.getMachineUsers(tenant);
        if(machineUsers == null ){
            message = "GET /machineusers?sortOrder=asc did not return 200, terminating";
            logger.log(message);
            throw new ZTSException(message);
        }else if ( machineUsers.isEmpty()){
            message = "No machineusers defined on the tenant, make sure to sync machineusers";
            logger.log(message);
            throw new ZTSException(message);
        }

        int printUsers = machineUsers.size();
        if(printUsers>10)
            printUsers = 10;
        logger.log("Number of machine users on tenant: "+machineUsers.size()+" printing first 10: ");
        for (int i = 0 ; i<printUsers;i++){
            logger.log(new Gson().toJson(machineUsers.get(i)));
        }

        logger.log("DLS GET TEST DATA - GET devices result: ");
        machines = rest.getMachines(tenant);

        if(machines==null || machines.isEmpty()){
            message="GET devices did not return 200, terminating";
            logger.log(message);
            throw new ZTSException(message);
        }

        logger.log("mahines loaded, amount: "+machines.size());
        logger.log("DLS INIT TEST DATA END");


    }

    private void testStateSynchronization() throws IOException {
        logger.log("DLS - STATE SYNCHRONIZATION TESTING");

        logger.log("DLS - STATE SYNCHRONIZATION TESTING END");
    }

    private void testSettingsSynchronization(String machineGroupToken) throws IOException, URISyntaxException, ZTSException, MqttException, InterruptedException {
        logger.log("DLS - SETTINGS SYNCHRONIZATION TESTING");

        String rmMessage;
        String response;

        logger.log(testEval+" sync on empty DLS from machine");
        postDLSettings(machineGroupToken);
        activateMachineGroup(machineGroupToken,true);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12, true));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        //14 shouldn't get DLS on SS.SETTINGS = null
        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(
                simulatorProtocol14.getMachine().getMachineUUID(),
                "Boot",
                "Disconnected",
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                simulatorProtocol14.sessionData.messageSequence,
                simulatorProtocol14.getMachine().getMachineUserSyncState().getFileVersion()
        );
        ss.VERSION = simulatorProtocol14.getMachine().getSfVersion();
        rmMessage = RmHandler.parseRmToString(ss);
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);

        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,false);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14, true));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        logger.log(testEval+" sync non empty DLS from machine");
        postDLSettings(machineGroupToken);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,false));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,false));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        logger.log(testEval+" settings not resent");
        ss = RmHandler.generateSystemStatusMessage(
                simulatorProtocol12.getMachine().getMachineUUID(),
                "Idle",
                "Idle",
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                simulatorProtocol12.sessionData.messageSequence,
                simulatorProtocol12.getMachine().getSfVersion()
        );
        ss.VERSION = simulatorProtocol12.getMachine().getSfVersion();
        rmMessage = RmHandler.parseRmToString(ss);
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,false);

        ss = RmHandler.generateSystemStatusMessage(
                simulatorProtocol14.getMachine().getMachineUUID(),
                "Idle",
                "Idle",
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                simulatorProtocol14.sessionData.messageSequence,
                simulatorProtocol14.getMachine().getSfVersion()
        );
        ss.SETTINGS = new RmSettings();
        ss.VERSION = simulatorProtocol14.getMachine().getSfVersion();

        rmMessage = RmHandler.parseRmToString(ss);
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,false);

        logger.log(testEval+" edit settings,add rule - settings are sent");
        postDLSettings(machineGroupToken);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,true));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,true));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);


        logger.log(testEval+" edit settings - edit rule, add currency limit - settings are sent");
        editIdent(ADD_CURRENCY,machineGroupToken);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,true));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,true));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);


        logger.log(testEval+" edit settings - edit rule, change currency limit - settings are sent");
        editIdent(CHANGE_CURRENCY, machineGroupToken);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,true));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,true));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        logger.log(testEval+" edit settings - edit rule, remove currency limit - settings are sent");
        editIdent(REMOVE_CURRENCY,machineGroupToken);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,true));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,true));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        logger.log(testEval+" edit settings - delete rule - settings are sent");
        editIdent(DELETE_IDENT,machineGroupToken);
        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,true));
        response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,true));
        response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                rmMessage);
        evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,true);

        //Todo - eval everEnabled settings sending only empty dls, no idents
        logger.log(testEval+" disable dls settings (and edit a rule) - eval settings are not sent");
        if(activateMachineGroup(machineGroupToken,false)==true){
            postDLSettings(machineGroupToken);
            rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol12,true));
            response = simulatorProtocol12.sendMqttEvent(EventType.SYSTEM_STATUS,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    rmMessage);
            evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,false);

            rmMessage = RmHandler.parseRmToString(generateSystemStatsusMessage(simulatorProtocol14,true));
            response = simulatorProtocol14.sendMqttEvent(EventType.SYSTEM_STATUS,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    rmMessage);
            evalSettingsSynchronization(dlSettingsIdents,rmMessage,response,false);
        }

        //create a new machine group - move machines to new group
        if(machineGroupToken.equals("-1")){
            String message="Attempting to create a new machine group and move machines - ";

            machineGroupToken = createNewMachineGroupAndMoveMachines();
            if(machineGroupToken!=null){
                testSettingsSynchronization(machineGroupToken);

            }else{
                machineGroupToken="-1";
                logger.log(testFailed+message+"500");
            }

        }
        logger.log("DLS - SETTINGS SYNCHRONIZATION TESTING DONE");

    }

    private String createNewMachineGroupAndMoveMachines() throws IOException, URISyntaxException {
        rh = new RestHandler(environment);
        rh.getToken();
        MachineGroupCCC machineGroupCCC = new MachineGroupCCC();
        machineGroupCCC.machines = new LinkedList<>();
        machineGroupCCC.name="ccodSimulator-dlsRobustness-smg-"+simulatorProtocol12.sessionData.messageSequence;
        machineGroupCCC.machines.add(simulatorProtocol12.getMachine().getMachineUUID());
        machineGroupCCC.machines.add(simulatorProtocol14.getMachine().getMachineUUID());
        String payload = new ObjectMapper().writeValueAsString(machineGroupCCC);
        logger.log("Attempting to create new machinegroup, payload:\n\t"+payload);
        MachineGroupCCC response=  rh.postMachineGroup(tenant,payload);
        if(response==null){
            logger.log("Posting to /api/v1/machinegroup did not retun 200");
            return null;
        }else {
            logger.log("response: "+new ObjectMapper().writeValueAsString(response));
            return response.token;
        }
    }

    private boolean activateMachineGroup(String machineGroupToken, boolean enable) throws IOException, URISyntaxException, ZTSException {
        DispenseLimitSettingsWrapperCCCResponse machineGroup = rh.getMachineGroupDLSettings(tenant,machineGroupToken);
        if(machineGroup==null)
            throw new ZTSException("Could not find machine group: "+machineGroupToken);

        machineGroup.enabled = enable;
        machineGroup.transactionTypeAffectingLimits = new TTALWrapper();
        machineGroup.transactionTypeAffectingLimits.MACHINE = new TTAL();
        machineGroup.transactionTypeAffectingLimits.WORK_UNIT = new TTAL();
        machineGroup.transactionTypeAffectingLimits.USER = new TTAL();
        machineGroup.transactionTypeAffectingLimits.MACHINE.transactionTypesAffectingLimits=new LinkedList<>();
        machineGroup.transactionTypeAffectingLimits.USER.transactionTypesAffectingLimits=new LinkedList<>();
        machineGroup.transactionTypeAffectingLimits.WORK_UNIT.transactionTypesAffectingLimits=new LinkedList<>();

        machineGroup.transactionTypeAffectingLimits.MACHINE.transactionTypesAffectingLimits.add("Dispense");
        machineGroup.transactionTypeAffectingLimits.USER.transactionTypesAffectingLimits.add("Dispense");
        machineGroup.transactionTypeAffectingLimits.WORK_UNIT.transactionTypesAffectingLimits.add("Dispense");

        String enableOrNot="";
        if(enable)
            enableOrNot="enable";
        else
            enableOrNot="disable";
        String message= "Attempting to "+enableOrNot+" machinegroup: "+machineGroupToken;
        String payload = new ObjectMapper().writeValueAsString(machineGroup);
        DispenseLimitSettingsWrapperCCCResponse response = rh.putMachineGroupDLSettings(tenant,machineGroupToken,payload);
        logger.log(message+" - \n"+payload+" - "+new ObjectMapper().writeValueAsString(response));
        if(response==null)
            return false;
        return true;
    }

    private IdentDLS editIdent(EditIdentType editType, String machineGroupToken) throws IOException, URISyntaxException, ZTSException {
        rh = new RestHandler(environment);
        rh.getToken();
        System.out.println("Editing Ident - "+editType);
        DispenseLimitSettingsCCC[] current = rh.getDispenseLimitSettings(tenant, machineGroupToken);
        String message;
        if(current== null || current.length<1){
            message = "GET distributeddispenselimit/configuration returned null | empty rules, terminating";
            logger.log(message);
            throw  new ZTSException(message);
        }

        DispenseLimitSettingsCCC toModify = current[current.length-1];

        IdentDLS identDls=null;
        switch (editType){
            case ADD_CURRENCY:
                identDls = new IdentDLS();
                identDls.initial = identDls.amount = 2500;
                identDls.currency = CLP.name();
                identDls.decimals = 0;
                toModify.dispenseAmounts.add(identDls);
                break;
            case CHANGE_CURRENCY:
                 identDls = toModify.dispenseAmounts.get(toModify.dispenseAmounts.size()-1);
                 identDls.amount = identDls.initial = identDls.amount+310;
                break;
            case REMOVE_CURRENCY:
                if(toModify.dispenseAmounts.size()<2){
                    logger.log("Unable to remove currency from ident, not enough currencies set on:"+new ObjectMapper().writeValueAsString(toModify));
                }
                else{
                    identDls =  toModify.dispenseAmounts.remove(toModify.dispenseAmounts.size()-1);
                }

                break;
        }


        String payload = new ObjectMapper().writeValueAsString(toModify);
        if (editType!=DELETE_IDENT){
            message = "Attempting to PUT ident: "+payload;
            int responseCode = rh.putDLSettingIdent(machineGroupToken,tenant,payload);
            logger.log(message +" - "+responseCode);

            if(responseCode!=200)
                return null;
            else
                return identDls;
        }
        else {
            message = "Attempting to DELETE ident: "+payload;
            int responseCode = rh.deleteIdentFromGroup(machineGroupToken,payload,tenant,false);
            logger.log(message+" - "+responseCode);

            if(responseCode!=200)
                return null;
            else
                return identDls;
        }


    }

    private void evalSettingsSynchronization(List<IdentPayloadDLS> dlSettingsIdents, String rmMessage, String response, boolean cccShouldSend) throws IOException {
        System.out.println("Evaluating SETTINGS syncronization");
        logger.log(rmMessage+" - "+response);
        RmServerReply reply = RmHandler.parseStringToRmServerReply(response);
        RmSystemStatus ss = RmHandler.parseStringToRmSystemStatus(rmMessage);

        if(!cccShouldSend){
            if((reply.SETTINGS != null ||reply.DISPENSE_LIMIT_SETTINGS != null))
                logger.log(testFailed+" did not expect settings being resent to the machine"+ss.VERSION);
            else
                logger.log(testPassed+" CCC did not resent old settings - ccod version: "+ss.VERSION);
        }else{

            switch (ss.VERSION){
                case "3.12":
//                    if(reply.SETTINGS!=null && reply.SETTINGS.SetDispenseLimitSettings != null)
//                        logger.log(testFailed+ " expected DISPENSE_LIMIT_SETTINGS, got SetDisepenseLimitSetting - ccod version:"+ss.VERSION);
                    if(reply.DISPENSE_LIMIT_SETTINGS == null)
                        logger.log(testFailed+ " expected DISPENSE_LIMIT_SETTINGS  - ccod version:"+ss.VERSION);
                    else{
                        logger.log(testPassed+ " got expected DISPENSE_LIMIT_SETTINGS - cood version:"+ss.VERSION);
                        evalIdents(dlSettingsIdents,reply.DISPENSE_LIMIT_SETTINGS);
                    }
                    break;
                case "3.14":
//                    if(reply.DISPENSE_LIMIT_SETTINGS != null)
//                        logger.log(testFailed+ " expected SetDisepenseLimitSetting, got  DISPENSE_LIMIT_SETTINGS- ccod version:"+ss.VERSION);
                    if(reply.SETTINGS == null)
                        logger.log(testFailed+ " expected SETTINGS is null - ccod version:"+ss.VERSION);
                    else if(reply.SETTINGS.SetDispenseLimitSettings == null)
                        logger.log(testFailed+ " expected SetDisepenseLimitSettings is null - ccod version:"+ss.VERSION);
                    else{
                        logger.log(testPassed+ " got expected SetDisepenseLimitSettings- ccod version:"+ss.VERSION);
                        evalIdents(dlSettingsIdents,reply.SETTINGS.SetDispenseLimitSettings);
                    }
                    break;
            }

        }

    }

    private void evalIdents(List<IdentPayloadDLS> expected, RmDispenseLimitSettingsOldProtocol actual) {
        System.out.println("Evaluating idents in SETTINGS synchronization");
        //TODO add ident evaluation


    }

    private RmSystemStatus generateSystemStatsusMessage(CCoDSession cCoDSession, boolean sendEmptySettings) {
        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(
                cCoDSession.getMachine().getMachineUUID(),
                "Idle",
                "Unknown",
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                timeZoneOffset,
                cCoDSession.sessionData.messageSequence,
                cCoDSession.getMachine().getMachineUserSyncState().getFileVersion());

        ss.VERSION = cCoDSession.getMachine().getSfVersion();
        if(sendEmptySettings){
            if(cCoDSession.getMachine().getSfVersion().equals("3.12")){
                ss.DISPENSE_LIMIT_SETTINGS = null;
            }
            else if (cCoDSession.getMachine().getSfVersion().equals("3.14")){
                ss.SETTINGS = new RmSettings();
            }
        }else {
            if(cCoDSession.getMachine().getSfVersion().equals("3.12")){
                ss.DISPENSE_LIMIT_SETTINGS = new RmDispenseLimitSettingsOldProtocol();
                ss.DISPENSE_LIMIT_SETTINGS.CAN_EXCEED_INITIAL = true;
                ss.DISPENSE_LIMIT_SETTINGS.DAY_BREAK_HOUR=0;
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS = new RmTransactionTypeAffectingLimitsOldProtocol();
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.MACHINE = new LinkedList<>();
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.USER = new LinkedList<>();
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.WORK_UNIT = new LinkedList<>();
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.RESET = new LinkedList<>();
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.MACHINE.add(TransactionType.DISPENSE.name());
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.USER.add(TransactionType.DISPENSE.name());
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.WORK_UNIT.add(TransactionType.DISPENSE.name());
                ss.DISPENSE_LIMIT_SETTINGS.TRANSACTION_TYPES_AFFECTING_LIMITS.RESET.add(TransactionType.END_OF_SHIFT.name());
                ss.DISPENSE_LIMIT_SETTINGS.DAILY_DISPENSE_LIMITS = new RmDailyDispenseLimitOldProtocol();

                RmDispenseLimitIdent ident = new RmDispenseLimitIdent();
                ident.AMOUNT_LIST = new LinkedList<>();
                ident.DAY_BREAK_HOUR = -1;
                ident.IDENT = "cashier";
                RmDispenseLimitIdentAmount amount = new RmDispenseLimitIdentAmount();
                amount.CURRENCY=EUR.name();
                amount.DECIMALS = 2;
                amount.TOTAL = 1000;
                ident.AMOUNT_LIST.add(amount);

                ss.DISPENSE_LIMIT_SETTINGS.DAILY_DISPENSE_LIMITS.ROLE = new LinkedList<>();
                ss.DISPENSE_LIMIT_SETTINGS.DAILY_DISPENSE_LIMITS.ROLE.add(ident);


            }
            else if (cCoDSession.getMachine().getSfVersion().equals("3.14")){
                ss.SETTINGS = new RmSettings();
                ss.SETTINGS.SetDispenseLimitSettings = new RmDispenseLimitSettingsOldProtocol();
                ss.SETTINGS.SetDispenseLimitSettings.CAN_EXCEED_INITIAL = false;
                ss.SETTINGS.SetDispenseLimitSettings.DAY_BREAK_HOUR = 12;
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS = new RmTransactionTypeAffectingLimitsOldProtocol();
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.MACHINE = new LinkedList<>();
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.USER = new LinkedList<>();
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.WORK_UNIT = new LinkedList<>();
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.RESET = new LinkedList<>();
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.MACHINE.add(TransactionType.DISPENSE.name());
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.USER.add(TransactionType.DISPENSE.name());
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.WORK_UNIT.add(TransactionType.DISPENSE.name());
                ss.SETTINGS.SetDispenseLimitSettings.TRANSACTION_TYPES_AFFECTING_LIMITS.RESET.add(TransactionType.END_OF_SHIFT.name());
                ss.SETTINGS.SetDispenseLimitSettings.DAILY_DISPENSE_LIMITS = new RmDailyDispenseLimitOldProtocol();

                RmDispenseLimitIdent ident = new RmDispenseLimitIdent();
                ident.AMOUNT_LIST = new LinkedList<>();
                ident.DAY_BREAK_HOUR = -1;
                ident.IDENT = "cashier";
                RmDispenseLimitIdentAmount amount = new RmDispenseLimitIdentAmount();
                amount.CURRENCY=EUR.name();
                amount.DECIMALS = 2;
                amount.TOTAL = 1000;
                ident.AMOUNT_LIST.add(amount);

                ss.SETTINGS.SetDispenseLimitSettings.DAILY_DISPENSE_LIMITS.ROLE = new LinkedList<>();
                ss.SETTINGS.SetDispenseLimitSettings.DAILY_DISPENSE_LIMITS.ROLE.add(ident);


            }
        }
        return ss;
    }


    private boolean postDLSettings(String machineGroupToken) throws IOException, URISyntaxException, ZTSException {
        String message;
        System.out.println("Posting DL Settings in tenant");
        logger.log("DLS POST SETTINGS START");
        if(machineGroupToken == null || machineGroupToken.isEmpty()){
            message = "invalid token";
            logger.log(message);
            throw new ZTSException(message);
        }
        rh = new RestHandler(environment);
        rh.getToken();
        List<MachineGroupCCC> machineGroups = rh.getMachineGroups(tenant);

        if(!machineGroupToken.equalsIgnoreCase("-1") && (machineGroups!=null || !machineGroups.isEmpty())){
//            if(!machineGroups.contains(machineGroupToken)){
//                message = "machineGroupToken "+machineGroupToken+"does not exist on the tenant, terminating postDLSettings";
//                logger.log(message);
//                throw new ZTSException(message);
//            }
            boolean found = false;
            for(MachineGroupCCC machineGroup : machineGroups){
                if(machineGroup.token.equals(machineGroupToken))
                    found = true;
            }

            if(!found){
                message = "machineGroupToken "+machineGroupToken+"does not exist on the tenant, terminating postDLSettings";
                logger.log(message);
                throw new ZTSException(message);
            }
        }

        //initTestData();
        DispenseLimitSettingsCCC[] dlSettingsCurrent = rh.getDispenseLimitSettings(tenant, machineGroupToken);

        logger.log("DLS POST generating payload data");
        List<IdentPayloadDLS> payload = new LinkedList<>();
        payload.add(createDLRules(dlSettingsCurrent, "ROLE"));
        payload.add(createDLRules(dlSettingsCurrent, "USER"));
        payload.add(createDLRules(dlSettingsCurrent, "MACHINE"));
        payload.add(createDLRules(dlSettingsCurrent, "WORK_UNIT"));

        List<IdentPayloadDLS> successfullIdents = new LinkedList<>();
        ObjectMapper om = new ObjectMapper();
        for(IdentPayloadDLS p : payload){
            String sPayload = om.writeValueAsString(p);
            message = "Attempting to POST: "+sPayload;

            DispenseLimitSettingsWrapperCCCResponse response = rh.postDLSSettings(machineGroupToken, sPayload,tenant);
            if (response== null)
                message+="- failed";
            else{
                message+="- 200";
                successfullIdents.add(p);
            }

            logger.log(message);

        }
        evalDLSettings(successfullIdents, rh.getDispenseLimitSettings(tenant,machineGroupToken));
        System.out.println();
        return true;

    }

    private void evalDLSettings(List<IdentPayloadDLS> dlSettingsExpected, DispenseLimitSettingsCCC[] dlSettingsActual) throws IOException {
        logger.log("DLS POST SETTINGS EVALUATION");
        int successfull = 0;
        for(IdentPayloadDLS p : dlSettingsExpected){
            boolean found = false;
            for (int i = 0; i< dlSettingsActual.length;i++){
                if(p.type.equals(dlSettingsActual[i].type) && p.idents.get(0).equals(dlSettingsActual[i].ident)){
                    found = true;
                    successfull++;
                    break;
                }
            }
            if(!found)
                logger.log("DID not find expected ident: "+new ObjectMapper().writeValueAsString(p));
            else
                dlSettingsIdents.add(p);
        }
        logger.log("DLS POST SETTINGS EVALUATION DONE - all "+successfull+" posted idents found");
    }

    private IdentPayloadDLS createDLRules(DispenseLimitSettingsCCC[] dlSettingsCurrent, String ruleType) {

        switch (ruleType){
            case "ROLE":
                 return createDLRulesRole(dlSettingsCurrent);
            case "USER":
                return  createDLSRulesUser(dlSettingsCurrent);
            case "WORK_UNIT":
                return createDLSRulesWorkUnit(dlSettingsCurrent);
            case "MACHINE":
                return createDlsRulesMachine(dlSettingsCurrent);
        }
        return null;
    }

    private IdentPayloadDLS createDlsRulesMachine(DispenseLimitSettingsCCC[] dlSettingsCurrent) {
        for(int i = 0; i<machines.size(); i++){
            boolean identFound = false;
            for(int j = 0 ;j<dlSettingsCurrent.length;j++){
                if(machines.get(i).hardwareId.equalsIgnoreCase(dlSettingsCurrent[j].ident)){
                    identFound = true;
                    break;
                }
            }
            if(!identFound){
                IdentPayloadDLS identPayload = new IdentPayloadDLS();
                identPayload.ident = "";
                identPayload.idents = new LinkedList<>();
                identPayload.idents.add(machines.get(i).hardwareId);
                identPayload.resetHour=12;
                identPayload.dispenseAmounts = new LinkedList<>();
                IdentDLS id = new IdentDLS();
                id.decimals =2;
                id.currency=EUR.name();
                id.amount = 500000;
                id.initial = 500000;
                identPayload.dispenseAmounts.add(id);

                id = new IdentDLS();
                id.decimals =2;
                id.currency=USD.name();
                id.amount = 400000;
                id.initial = 400000;
                identPayload.dispenseAmounts.add(id);
                identPayload.type="MACHINE";

                return identPayload;
            }
        }
        return null;
    }

    private IdentPayloadDLS createDLSRulesWorkUnit(DispenseLimitSettingsCCC[] dlSettingsCurrent) {
        for(int h = 0; h<workUnits.length;h++){
            for(int i = 0; i<workUnits[h].CHILDREN.size(); i++){
                boolean identFound = false;
                for(int j = 0 ;j<dlSettingsCurrent.length;j++){
                    if(workUnits[h].CHILDREN.get(i).NAME.equalsIgnoreCase(dlSettingsCurrent[j].ident)){
                        identFound = true;
                        break;
                    }
                }
                if(!identFound){
                    IdentPayloadDLS identPayload = new IdentPayloadDLS();
                    identPayload.ident = "";
                    identPayload.idents = new LinkedList<>();
                    identPayload.idents.add(workUnits[h].CHILDREN.get(i).NAME);
                    identPayload.resetHour=13;
                    identPayload.dispenseAmounts = new LinkedList<>();
                    IdentDLS id = new IdentDLS();
                    id.decimals =2;
                    id.currency=EUR.name();
                    id.amount = 50000;
                    id.initial = 50000;
                    identPayload.dispenseAmounts.add(id);

                    id = new IdentDLS();
                    id.decimals =2;
                    id.currency=USD.name();
                    id.amount = 40000;
                    id.initial = 40000;
                    identPayload.dispenseAmounts.add(id);
                    identPayload.type="WORK_UNIT";

                    return identPayload;
                }
            }

        }
        return null;
    }

    private IdentPayloadDLS createDLSRulesUser(DispenseLimitSettingsCCC[] dlSettingsCurrent) {
        for(int i = 0; i<machineUsers.size(); i++){
            boolean identFound = false;
            for(int j = 0 ;j<dlSettingsCurrent.length;j++){
                if(machineUsers.get(i).userId.equalsIgnoreCase(dlSettingsCurrent[j].ident)){
                    identFound = true;
                    break;
                }
            }
            if(!identFound){
                IdentPayloadDLS identPayload = new IdentPayloadDLS();
                identPayload.ident = "";
                identPayload.idents = new LinkedList<>();
                identPayload.idents.add(machineUsers.get(i).userId);
                identPayload.resetHour=12;
                identPayload.dispenseAmounts = new LinkedList<>();
                IdentDLS id = new IdentDLS();
                id.decimals =2;
                id.currency=EUR.name();
                id.amount = 1000;
                id.initial = 1000;
                identPayload.dispenseAmounts.add(id);

                id = new IdentDLS();
                id.decimals =2;
                id.currency=USD.name();
                id.amount = 1500;
                id.initial = 1500;
                identPayload.dispenseAmounts.add(id);
                identPayload.type="USER";

                return identPayload;
            }
        }
        return null;
    }

    private IdentPayloadDLS createDLRulesRole(DispenseLimitSettingsCCC[] dlSettingsCurrent) {
        for(int i = 0; i<machineUserRoles.length; i++){
            boolean identFound = false;
            for(int j = 0 ;j<dlSettingsCurrent.length;j++){
                if(machineUserRoles[i].ROLE_ID.equalsIgnoreCase(dlSettingsCurrent[j].ident)){
                    identFound = true;
                    break;
                }
            }
            if(!identFound){
                IdentPayloadDLS identPayload = new IdentPayloadDLS();
                identPayload.ident = "";
                identPayload.idents = new LinkedList<>();
                identPayload.idents.add(machineUserRoles[i].ROLE_ID);
                identPayload.resetHour=-1;
                identPayload.dispenseAmounts = new LinkedList<>();
                IdentDLS id = new IdentDLS();
                id.decimals =2;
                id.currency=EUR.name();
                id.amount = 5000;
                id.initial = 5000;
                identPayload.dispenseAmounts.add(id);

                id = new IdentDLS();
                id.decimals =2;
                id.currency=USD.name();
                id.amount = 4000;
                id.initial = 4000;
                identPayload.dispenseAmounts.add(id);
                identPayload.type="ROLE";

                return identPayload;
            }
        }
        return null;
    }

    private boolean removeAllDLSFromTenant() throws IOException, URISyntaxException {
        System.out.println("Removing all dls settings from tenant");
        logger.log("Cleaning tenant from existing dls settings");
        RestHandler restHandler = new RestHandler(environment);
        restHandler.getToken();
        List<MachineGroupCCC> machinegroups =  restHandler.getMachineGroups(tenant);
        if(machinegroups == null){
            logger.log("GET machinegroups did not return 200, terminating");
            return false;
        }else if (machinegroups.isEmpty()){
            logger.log("No machine groups defined on the tenant");
        }else {
            for (MachineGroupCCC mg : machinegroups){
                String message = "attempting to delete machine group: "+mg.token;
                int responseCode = deleteMachineGroup(mg.token);
                logger.log(message+ " - "+responseCode);
                DispenseLimitSettingsCCC[] dlsGrouped = restHandler.getDispenseLimitSettings(tenant,mg.token);
                if(dlsGrouped==null){
                    logger.log("GET distributeddispenselimit/configuration?"+"groupToken="+mg.token+" did not return 200, terminating");
                    return false;
                }
                message = "Attempting to remove all rules for group: "+mg.token;
                responseCode = removeDLSIdent(mg.token,dlsGrouped[0].ident,true);
                logger.log(message+ " - "+responseCode);
                if(responseCode!=200){
                    logger.log("Delete all rules in group failed, terminating");
                    return false;
                }

            }
        }

        logger.log("Attempting to delete all idents in machineGroup -1");
        DispenseLimitSettingsCCC[] dlsUngouped =  restHandler.getDispenseLimitSettings(tenant, "-1");
        if(dlsUngouped==null){
            logger.log("GET distributeddispenselimit/configuration did not retun 200, terminating ");
            return false;
        }
        else if (dlsUngouped[0].ident.equalsIgnoreCase("-1")){
            logger.log("No rules found in group -1");
            return true;
        }else{
            for(int i = 0; i<dlsUngouped.length;i++){
                String message = "Attempting to remove rule: "+dlsUngouped[i].ident;
                int responseCode = removeDLSIdent("-1",dlsUngouped[i].ident,false);
                logger.log(message+" - "+responseCode);
            }
        }
        return true;
    }

    private int removeDLSIdent(String groupToken, String ident, boolean deleteAll) throws IOException, URISyntaxException {
        RestHandler restHandler = new RestHandler(environment);
        restHandler.getToken();
        return restHandler.deleteIdentFromGroup(groupToken,ident,tenant,deleteAll);
    }

    private int deleteMachineGroup(String token) throws IOException, URISyntaxException {
        RestHandler restHandler = new RestHandler(environment);
        restHandler.getToken();
        return restHandler.deleteMachineGroup(token,tenant);
    }

    public CCoDSession initCCodSimulator(String uuid, String version, Currency currency, Environments environment, String tenant, long runtimeMin) throws IOException, MqttException {
        String key = "DLS_"+environment.name()+"_"+tenant+"_"+uuid;
        SessionDataStore sessionDataStore = loadSessionDataStore();

        CCoDSession session = new CCoDSession(
                MachineModels.RCS_500,
                version,
                uuid,
                currency,
                0,
                0,
                DefaultStrings.getBrokerHost(environment),
                tenant,
                runtimeMin,
                30,
                1,
                environment,
                key,
                sessionDataStore.get(key),
                this
        );

        session.initMessageData();
        session.connectToMqtt();
        return session;

    }
}
