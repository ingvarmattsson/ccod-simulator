package ztsAutomation;

import cccObjects.*;
import ccodSession.CCoDSession;
import ccodSession.Environments;
import ccodSession.EventType;
import com.google.gson.Gson;
import data.Utilities;
import exceptions.ZTSException;
import org.eclipse.paho.client.mqttv3.MqttException;
import rest.RestHandler;
import rmObjects.RmError;
import rmObjects.RmHandler;
import rmObjects.RmSettings;
import rmObjects.RmSystemStatus;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class RCRSURobustnessTestingZTS {

    private CCoDSession ccoDSession;
    private Environments environment;
    private String tenant;
    private String machineUUID;
    private String utcOffset;
    private long sequence=0;

    public RemoteConfigurationCCC remoteConfigurationTemplate;
    public RemoteConfigurationCCC deploymentStateRemoteConfiguration;
    public RemoteSoftwareUpgradeCCC remoteSoftwareUpgradeInstallation;
    public RemoteSoftwareUpgradeCCC deploymentStateRemoteSoftwareUpgrade;

    public RCRSURobustnessTestingZTS(CCoDSession ccoDSession, Environments environment, String tenant, String machineUUID, String utcOffset, long sequence) {
        this.ccoDSession = ccoDSession;
        this.environment = environment;
        this.tenant = tenant;
        this.machineUUID = machineUUID;
        this.utcOffset = utcOffset;
        this.sequence = sequence;
    }



    public void sendWaitingRemoteConfiguration() throws IOException, InterruptedException, MqttException {

        if(remoteConfigurationTemplate==null){
            remoteConfigurationTemplate = new RemoteConfigurationCCC();
            remoteConfigurationTemplate.version="ccod-sim-dummy";
        }

        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetConfigurationUpdate = new SetConfigurationUpdate();
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).WAITING =new ZTSPackage();
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).WAITING.configuration=remoteConfigurationTemplate.version;
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).WAITING.receipts=remoteConfigurationTemplate.version;
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).WAITING.i18n=remoteConfigurationTemplate.version;

        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

    }


    public void sendWaitingRemoteSoftwareUpgrade() throws IOException, InterruptedException, MqttException {

        if(remoteSoftwareUpgradeInstallation==null){
            remoteSoftwareUpgradeInstallation = new RemoteSoftwareUpgradeCCC();
            remoteSoftwareUpgradeInstallation.name="ccod-simulator";
            remoteSoftwareUpgradeInstallation.id="ccod-simulator";
            remoteSoftwareUpgradeInstallation.label="ccod-simulator";
            remoteSoftwareUpgradeInstallation.version="ccod-simulator";
            remoteSoftwareUpgradeInstallation.revision="ccod-simulator";
        }

        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetSoftwareUpgrade = new SetSoftwareUpgrade();
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).WAITING =new LinkedList<ZTSPackage>();
        ZTSPackage payload = new ZTSPackage();
        payload.name=remoteSoftwareUpgradeInstallation.name;
        payload.id=remoteSoftwareUpgradeInstallation.id;
        payload.label=remoteSoftwareUpgradeInstallation.label;
        payload.version=remoteSoftwareUpgradeInstallation.version;
        payload.revision=remoteSoftwareUpgradeInstallation.revision;
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).WAITING.add(payload);
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
    }

    public void sendDoneRemoteUpgrade() throws IOException, InterruptedException, MqttException {

        if(remoteSoftwareUpgradeInstallation==null){
            remoteSoftwareUpgradeInstallation = new RemoteSoftwareUpgradeCCC();
            remoteSoftwareUpgradeInstallation.name="ccod-simulator";
            remoteSoftwareUpgradeInstallation.id="ccod-simulator";
            remoteSoftwareUpgradeInstallation.label="ccod-simulator";
            remoteSoftwareUpgradeInstallation.version="ccod-simulator";
            remoteSoftwareUpgradeInstallation.revision="ccod-simulator";
        }

        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetSoftwareUpgrade = new SetSoftwareUpgrade();
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT =new ZTSPackage();


        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT.name=remoteSoftwareUpgradeInstallation.productName;
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT.id=remoteSoftwareUpgradeInstallation.id;
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT.label=remoteSoftwareUpgradeInstallation.label;
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT.version=remoteSoftwareUpgradeInstallation.version;
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT.revision=remoteSoftwareUpgradeInstallation.revision;

        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
    }


    public void sendDoneRemoteConfiguration() throws IOException, InterruptedException, MqttException {

        if(remoteConfigurationTemplate==null){
            remoteConfigurationTemplate = new RemoteConfigurationCCC();
            remoteConfigurationTemplate.version="ccod-sim-dummy";
        }

        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetConfigurationUpdate = new SetConfigurationUpdate();
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT =new ZTSPackage();
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT.configuration=remoteConfigurationTemplate.version;
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT.receipts=remoteConfigurationTemplate.version;
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT.i18n=remoteConfigurationTemplate.version;
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT.themes=remoteConfigurationTemplate.version;
        if(remoteConfigurationTemplate.parts!=null && remoteConfigurationTemplate.parts.contains("sdk"))
            ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT.sdk=remoteConfigurationTemplate.version;

        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
    }

    public void sendSystemStatuses(boolean sendAllMessages) throws IOException, InterruptedException, MqttException {


        //ss SETTINGS=null
        RmSystemStatus ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

        if(!sendAllMessages)
            return;

        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));



        //ss SETTINGS:{}
        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

        //ss SETTINGS has CURRENT only  SetConfigurationUpdate
        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetConfigurationUpdate = new SetConfigurationUpdate();
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

        //ss SETTINGS has CURRENT only  SetSoftwareUpgrade
        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetSoftwareUpgrade = new SetSoftwareUpgrade();
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT =new ZTSPackage().initDefaultsValuesSoftwareUpgrade();
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

        //ss SETTINGS has CURRENT both  SetSoftwareUpgrade && SetConfigurationUpdate
        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
        ss.SETTINGS = new RmSettings();
        ss.SETTINGS.SetConfigurationUpdate = new SetConfigurationUpdate();
        ((SetConfigurationUpdate) ss.SETTINGS.SetConfigurationUpdate).CURRENT =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
        ss.SETTINGS.SetSoftwareUpgrade = new SetSoftwareUpgrade();
        ((SetSoftwareUpgrade) ss.SETTINGS.SetSoftwareUpgrade).CURRENT =new ZTSPackage().initDefaultsValuesSoftwareUpgrade();
        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

//        //ss SETTINGS has WAITING only  SetConfigurationUpdate incorrect version
//        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
//        ss.SETTINGS = new RmSettings();
//        ss.SETTINGS.SetConfigurationUpdate = new ZTSPAckageObject();
//        ss.SETTINGS.SetConfigurationUpdate.WAITING =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
//        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
//
//        //ss SETTINGS has WAITING only  SetSoftwareUpgrade incorrect version
//        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
//        ss.SETTINGS = new RmSettings();
//        ss.SETTINGS.SetSoftwareUpgrade= new ZTSPAckageObject();
//        ss.SETTINGS.SetSoftwareUpgrade.WAITING =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
//        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
//
//
//        //ss SETTINGS has WAITING both  SetConfigurationUpdate && SetSoftwareUpgrade incorrect versions
//        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
//        ss.SETTINGS = new RmSettings();
//        ss.SETTINGS.SetConfigurationUpdate = new ZTSPAckageObject();
//        ss.SETTINGS.SetConfigurationUpdate.WAITING =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
//        ss.SETTINGS.SetSoftwareUpgrade = new ZTSPAckageObject();
//        ss.SETTINGS.SetSoftwareUpgrade.WAITING=new ZTSPackage().initDefaultsValuesSoftwareUpgrade();
//        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));
//
//        //ss SETTINGS has WAITING && CURRENT both  SetConfigurationUpdate && SetSoftwareUpgrade incorrect versions
//        ss = RmHandler.generateSystemStatusMessage(machineUUID,"Idle","Idle", Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++,"0");
//        ss.SETTINGS = new RmSettings();
//        ss.SETTINGS.SetConfigurationUpdate = new ZTSPAckageObject();
//        ss.SETTINGS.SetConfigurationUpdate.WAITING =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
//        ss.SETTINGS.SetConfigurationUpdate.CURRENT =new ZTSPackage().initDefaultsValuesConFigurationUpdate();
//        ss.SETTINGS.SetSoftwareUpgrade = new ZTSPAckageObject();
//        ss.SETTINGS.SetSoftwareUpgrade.WAITING=new ZTSPackage().initDefaultsValuesSoftwareUpgrade();
//        ss.SETTINGS.SetSoftwareUpgrade.CURRENT=new ZTSPackage().initDefaultsValuesSoftwareUpgrade();
//        ccoDSession.sendMqttEvent(EventType.SYSTEM_STATUS,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(ss));

    }

    public void initRemoteSoftwareUpgradeForMachine(CCoDSession.SessionLogger logger) throws IOException, URISyntaxException, ZTSException {
        RestHandler restHandler = new RestHandler(environment);
        restHandler.getToken();
        List<RemoteSoftwareUpgradeCCC> results = restHandler.getRemoteSoftwareInstallations(tenant);

        if(results==null || results.isEmpty()){
            String message = "no rsu installations found on tenant"+tenant+
                    "\nUpload a installation in CCC";
            throw new ZTSException(message);
        }


        RemoteSoftwareUpgradeCCC rsuPayload = results.get(results.size()-1);

        restHandler = new RestHandler(environment);
        restHandler.getToken();

        StringBuilder sb = new StringBuilder();
        sb.append("Attempting to deploy following installation on tenant: "+tenant+" and machine: "+machineUUID);
        sb.append(" - "+new Gson().toJson(rsuPayload));

        RemoteSoftwareUpgradeCCCResponse response = restHandler.postRemoteSoftwareUpgradeForMachine(
                new Gson().toJson(
                        new RemoteSoftwareUpgradeDeployPayload(
                                machineUUID,
                                "now"
                                ,rsuPayload.packageId
                                )),tenant);
        if(response==null){
            String message ="POST to remotesoftwareupgrade/installations failed";
            throw new ZTSException(message);
        }else if(response.results==null || response.results.isEmpty())
            throw new ZTSException("POST to remotesoftwareupgrade/installations passed but the response is empty - Make sure there are no installations for the machine with state=PENDING/SENT/WAITING");

        deploymentStateRemoteSoftwareUpgrade = response.results.get(0);

        remoteSoftwareUpgradeInstallation = rsuPayload;

        if(remoteSoftwareUpgradeInstallation==null)
            throw new ZTSException("coud not find the deployed installation");

        sb.append( "\n Deployment State:"+new Gson().toJson(deploymentStateRemoteSoftwareUpgrade));
        sb.append("\n Deployed installation: "+new Gson().toJson(remoteSoftwareUpgradeInstallation));
        logger.log(sb.toString());
    }

    private int n=1;

    public void initRemoteConfigurationForMachine(CCoDSession.SessionLogger logger) throws IOException, URISyntaxException, ZTSException {
        RemoteConfigurationCCC rcPayload=null;
        RestHandler restHandler = null;


        restHandler = new RestHandler(environment);
        restHandler.getToken();
        List<RemoteConfigurationCCC> results = restHandler.getRemoteConfigurations(tenant);

        if(results==null || results.isEmpty()){
            throw new ZTSException("no configuration packages found on tenant"+tenant+
                    "\nUpload a configuration package in CCC");
        }

        if(results.size()<4)
            throw new ZTSException("Not enough remote configuration templates uploaded on the tenant. Minimum required: 4");



        rcPayload = results.get(results.size()-n++);



        restHandler = new RestHandler(environment);
        restHandler.getToken();

        StringBuilder sb = new StringBuilder();
        sb.append("Attempting to deploy following package on tenant: "+tenant+" and machine: "+machineUUID);
        sb.append(" - "+new Gson().toJson(rcPayload));

        RemoteConfigurationCCCResponse response = restHandler.postRemoteConfigurationForMachine(
                new Gson().toJson(
                        new RemoteConfigurationDeployPayload(
                                machineUUID,
                                "now"
                                ,rcPayload.uuid
                                ,true)),tenant);
        if(response==null){
            String message ="POST to remoteconfiguration/deploy failed";
            throw new ZTSException(message);
        }else if (response.results==null || response.results.isEmpty())
            throw new ZTSException("POST to remoteconfiguration/deploy passed but response is empty - Make sure there are no deployments for the machine with state=PENDING/SENT/WAITING");

        deploymentStateRemoteConfiguration = response.results.get(0);

        remoteConfigurationTemplate = rcPayload;

        if(remoteConfigurationTemplate==null)
            throw new ZTSException("coud not find the deployed template");

        sb.append( "\n Deployment State:"+new Gson().toJson(deploymentStateRemoteConfiguration));
        sb.append("\n Deployed template: "+new Gson().toJson(remoteConfigurationTemplate));
        logger.log(sb.toString());

    }


    public RemoteConfigurationCCC getMachineStateRemoteContifuration(String tenant) throws IOException, URISyntaxException, ZTSException {
        RestHandler restHandler = new RestHandler(environment);
        restHandler.getToken();
        List<RemoteConfigurationCCC> results = restHandler.getRemoteConfigurationStates(tenant);

        if (results == null){
            throw new ZTSException("GET remoteconfiguration/deploy failed");
        }

        for(RemoteConfigurationCCC r : results){
            if(r.uuid.equalsIgnoreCase(deploymentStateRemoteConfiguration.uuid) && r.machineUuid.equalsIgnoreCase(deploymentStateRemoteConfiguration.machineUuid))
               // deploymentStateRemoteConfiguration = r;
                return r;
        }
        return null;
    }

    public RemoteSoftwareUpgradeCCC getMachineStateRemoteSoftwareUpgrade(String tenant) throws IOException, URISyntaxException, ZTSException {
        RestHandler restHandler = new RestHandler(environment);
        restHandler.getToken();
        List<RemoteSoftwareUpgradeCCC> results = restHandler.getRemoteSoftwareUpgradeStates(tenant);

        if (results == null){
            throw new ZTSException("GET remoteconfiguration/deploy failed");
        }

        for(RemoteSoftwareUpgradeCCC r : results){
            if(r.installationId.equals(deploymentStateRemoteSoftwareUpgrade.installationId) && r.machineUuid.equals(deploymentStateRemoteSoftwareUpgrade.machineUuid))
                return r;
        }
        return null;
    }


    public void sendErrorSZT(String kind) throws IOException, InterruptedException, MqttException {
        RmError error = RmHandler.generateAlertMessage(machineUUID,
                false,
                kind,
                "Software",
                "Warning",
                kind+" ccod-simulator trash info",
                Utilities.getCurrentDateStampIncludedCatchingUp(),utcOffset,sequence++);
        ccoDSession.sendMqttEvent(EventType.ERROR,Utilities.getCurrentDateStampIncludedCatchingUp(),RmHandler.parseRmToString(error));
    }
}
