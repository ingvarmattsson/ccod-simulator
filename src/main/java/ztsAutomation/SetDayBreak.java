package ztsAutomation;

public class SetDayBreak implements ZTSPAckageObjectCommon {
    public int HOUR;
    public int MINUTE;

    public SetDayBreak(int HOUR, int MINUTE) {
        this.HOUR = HOUR;
        this.MINUTE = MINUTE;
    }
}
