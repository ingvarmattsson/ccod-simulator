package ztsAutomation;


public class ZTSPackage {

    public String receipts;
    public String themes;
    public String configuration;
    public String i18n;
    public String sdk;

    public String name;
    public String id;
    public String label;
    public String version;
    public String revision;


    public ZTSPackage initDefaultsValuesConFigurationUpdate() {
        ZTSPackage ztsPackage = new ZTSPackage();
        ztsPackage.receipts="20200513_0948-ccod-simulator";
        ztsPackage.themes="20200513_0948-ccod-simulator";
        ztsPackage.configuration="20200513_0948-ccod-simulator";
        ztsPackage.i18n="20200513_0948-ccod-simulator";

        return ztsPackage;
    }

    public ZTSPackage initDefaultsValuesSoftwareUpgrade() {
        ZTSPackage ztsPackage = new ZTSPackage();
        ztsPackage.name="CashComplete Demo - CCoD Simulator";
        ztsPackage.id="products.rcs.lv.prosegur.demo";
        ztsPackage.label="20200904_1320 - CCoD Simulator";
        ztsPackage.version="3.14.0-beta17 - CCoD Simulator";
        ztsPackage.revision="106777 - CCoD Simulator";

        return ztsPackage;
    }
}
