package ztsAutomation;

import rmObjects.RmLaneAllocation;
import rmObjects.RmUserCommon;

import java.util.List;

public class LaneAllocationData {
    public final List<RmLaneAllocation> laneAllocations;
    public final List<RmUserCommon> rmUsers;

    public LaneAllocationData(List<RmLaneAllocation> laneAllocations, List<RmUserCommon> rmUsers) {
        this.laneAllocations = laneAllocations;
        this.rmUsers = rmUsers;
    }
}
