package resultGenerator;

import ccodSession.EventType;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ResultEvent  {

    public String tenant;
    public List<String> lines;

    public ResultEvent(String tenant, List<String> lines){
        this.lines = lines;
        this.tenant = tenant;
    }
}
