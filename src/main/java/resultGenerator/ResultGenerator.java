package resultGenerator;

import ccodSession.Environments;
import data.Utilities;
import rmReplay.Anonymizer;

import java.io.*;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.*;

import static data.DefaultStrings.csvDelimeter;
import static data.DefaultStrings.pathSeparator;
import static data.DefaultStrings.resultTenantIndexer;

public class ResultGenerator {

    private final boolean sendAlerts;
    private final boolean sendBoxContents;
    private final boolean sendSystemStatuses;
    private final boolean sendTransactions;
    private final boolean sendUserUpdates;
    private boolean waitForRmReply;
    private String dateFrom;
    private String date;
    private int runFor;
    private Environments environment;
    private int amountOfMachines;
    private int sleepMs;
    private int failedMachines;
    private long runTimeMin;
    private int sessionEventLoopDelayMs,eventDelayMs,machineRampUpMs;

    public String pathAlerts;
    public String pathBoxContents;
    public String pathSystemStatuses;
    public static String pathTransactions;
    public String pathUserUpdates;
    private String pathPrefix;
    private String pathResult;
    private String fileName;
    private String fileNameFailedEvents ="_failed_events.txt";
    private String fileNotFoundTransactions ="_missing_transactions.txt";




    public ResultGenerator(Environments environment, String date, long runFor, int amountOfMachines, int failedMachines,
                           long runTimeMin, int sessionEventLoopDelayMs, int eventDelayMs, int machineRampUpMs, String dateFromQueryParam, int sleepBeforeValidationMs, boolean sendAlerts, boolean sendBoxContents, boolean sendSystemStatuses, boolean sendTransactions, boolean sendUserUpdates, boolean waitForRMReply) {
        this.environment=environment;
        this.runFor = (int) runFor;
        this.date=date;
        this.amountOfMachines = amountOfMachines;
        this.failedMachines=failedMachines;
        this.runTimeMin=runTimeMin;
        this.sessionEventLoopDelayMs=sessionEventLoopDelayMs;
        this.eventDelayMs=eventDelayMs;
        this.machineRampUpMs=machineRampUpMs;
        this.dateFrom = dateFromQueryParam;
        this.sleepMs = sleepBeforeValidationMs;
        this.sendAlerts=sendAlerts;
        this.sendBoxContents=sendBoxContents;
        this.sendSystemStatuses=sendSystemStatuses;
        this.sendTransactions=sendTransactions;
        this.sendUserUpdates=sendUserUpdates;
        this.waitForRmReply=waitForRMReply;

        pathPrefix = System.getProperty("user.dir")+pathSeparator+"sessionData"+pathSeparator;

        pathAlerts=pathPrefix+"rmAlerts"+pathSeparator;
        pathBoxContents=pathPrefix+"rmBoxContents"+pathSeparator;
        pathSystemStatuses=pathPrefix+"rmSystemStatuses"+pathSeparator;
        pathTransactions=pathPrefix+"rmTransactions"+pathSeparator;
        pathUserUpdates=pathPrefix+"rmUserUpdates"+pathSeparator;

        pathResult=pathPrefix+"sessionResults"+pathSeparator;

    }


    public void processResults(){

        Map<String, List<String>> results = new HashMap<>();
        Map<String, List<String>> failedEvents = new HashMap<>();

        if(sendAlerts==true)
            results.put(pathAlerts, new Anonymizer(pathAlerts, environment, date).getResults());
        if(sendBoxContents==true)
            results.put(pathBoxContents, new Anonymizer(pathBoxContents, environment, date).getResults());
        if(sendSystemStatuses==true)
            results.put(pathSystemStatuses, new Anonymizer(pathSystemStatuses, environment, date).getResults());
        if(sendTransactions==true)
            results.put(pathTransactions, new Anonymizer(pathTransactions, environment, date).getResults());
        if(sendUserUpdates==true)
            results.put(pathUserUpdates, new Anonymizer(pathUserUpdates, environment, date).getResults());

        fileName = environment.name()+"_"+date+"_result.txt";

        try {
            File parent = new File(pathResult);
            if(!parent.exists())
                parent.mkdir();

            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(parent,fileName)));
            List<String> keys = new LinkedList<>(results.keySet());
            Collections.sort(keys);
            String message = "Results for session "+environment+" "+date+"\n\nSession Configuration:"+
                    "\n\tRun time expected: "+runTimeMin+"m, actual: "+runFor+"m"+
                    "\n\tAmount of machines: "+amountOfMachines+
                    "\n\tAmount of prematurely failed machines: "+failedMachines+
                    "\n\tEvent loop delay: "+sessionEventLoopDelayMs+"ms"+
                    "\n\tEvent delay: "+eventDelayMs+"ms"+
                    "\n\tMachine ramp up time: "+machineRampUpMs+"ms";

            System.out.println(message);
            bw.write(message+"\n");

            int eventsTotal=0, failedEventsTotal=0;
            int minDurationTotal = Integer.MAX_VALUE;
            int maxDurationTotal = Integer.MIN_VALUE;
            List<Integer> allDurations = new LinkedList<>();

            for (String key : keys){
                List<String> cleanLines= new LinkedList<>();
                int minDuration = Integer.MAX_VALUE;
                int maxDuration = Integer.MIN_VALUE;
                int failedEventAmount = 0;

                List<Integer> durations = new LinkedList<>();

                int i = key.indexOf("rm");
                message = key.substring(i,key.length()-2);
                System.out.println(message);
                bw.write(message+":\n");

                List<String> lines = results.get(key);
                String tenant="";

                for(int index = 0; index<lines.size();index++){
                    String line = lines.get(index);

                    if(line.startsWith(resultTenantIndexer)){
                        tenant = line.substring(resultTenantIndexer.length());
                        continue;
                    }
                    //timeStamp;eventType;responseTimeMs;payload;response
                    String[] row = line.split(csvDelimeter);
                    cleanLines.add(line);

                    if(row[4].startsWith("failed")){
                        if (failedEvents.get(tenant)==null){
                            List<String> list = new LinkedList<>();
                            list.add(row[1]+": "+row[3]);
                            failedEvents.put(tenant, list);
                        }
                        else
                            failedEvents.get(tenant).add(row[1]+": "+row[3]);
                        failedEventAmount++;
                    }
                    else {
                        int responseTimeMs = Integer.parseInt(row[2]);
                        if(responseTimeMs<=minDuration)
                            minDuration = responseTimeMs;
                        if(responseTimeMs>=maxDuration)
                            maxDuration = responseTimeMs;

                        durations.add(responseTimeMs);
                        allDurations.add(responseTimeMs);

                    }

                }

                if(minDuration<=minDurationTotal)
                    minDurationTotal = minDuration;
                if(maxDuration>=maxDurationTotal)
                    maxDurationTotal = maxDuration;

                eventsTotal+=lines.size();
                failedEventsTotal+=failedEventAmount;

                //write stuff
                message = "\tAmount of events: "+cleanLines.size();
                System.out.println(message);
                bw.write(message+"\n");
                message = "\tFailed amounts: "+failedEventAmount;
                System.out.println(message);
                bw.write(message+"\n");
                if(cleanLines.size()>0){
                    double d = (double) (((double)(cleanLines.size()-failedEventAmount)/(double)cleanLines.size()))*100;
                    message = "\tSuccessfull: "+String.format("%.2f",d)+"%";
                    System.out.println(message);
                    bw.write(message+"\n");
                }

                calculateMetrics(durations,bw, minDuration,maxDuration);


            }
            message="\n###Total###";
            System.out.println(message);
            bw.write(message+"\n");
            message="\tAmount of events: "+eventsTotal;
            System.out.println(message);
            bw.write(message+"\n");
            message="\tFailed amounts: "+failedEventsTotal;
            System.out.println(message);
            bw.write(message+"\n");

            int denominator = (runFor>0 ? runFor : 1)*60;
            if(eventsTotal>0){

                double throughput = (double)eventsTotal/(double)denominator;
                message="\tThroughput: "+String.format("%.2f",throughput)+" events/second";
                System.out.println(message);
                bw.write(message+"\n");
                double successRate;
                successRate = ( (double)(eventsTotal-failedEventsTotal)/(double)eventsTotal)*100;
                message = "\tSuccessfull: "+String.format("%.2f",successRate)+"%";
                System.out.println(message);
                bw.write(message+"\n");

            }

            calculateMetrics(allDurations,bw,minDurationTotal,maxDurationTotal);
            if(environment!=Environments.OTHER && waitForRmReply == true)
                appendValidatedResults(bw,new DataValidator(results, dateFrom, environment).validateTransactions(sleepMs,sendTransactions));

            bw.flush();
            bw.close();

            if(failedEventsTotal>0) {
                generateFailedEventsDoc(failedEvents, fileNameFailedEvents);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        compareWithHistoricResults();
    }


    private void appendValidatedResults(BufferedWriter bw, ValidatedData validate) throws IOException {
        String message="\n###Data Validator###";
        System.out.println(message);
        bw.write(message+"\n");
        message="dateFrom: "+validate.dateFrom+", dateTo: "+validate.dateTo;
        System.out.println(message);
        bw.write(message+"\n");
        double successRate = ((double)validate.transactionsActualAmount/(double)validate.transactionsExpectedAmount)*100;
        message="\nTransactions\n\tSuccessfull: "+String.format("%.2f",successRate)+"%";
        System.out.println(message);
        bw.write(message);
        if(validate.failedTransactions!=null && !validate.failedTransactions.isEmpty())
            generateFailedEventsDoc(validate.failedTransactions,fileNotFoundTransactions);



    }

    private void compareWithHistoricResults() {
        //TODO - compare with historic results and rank on total percentage, total median, (total mode) and successfullResponse but missing in solR
        //open current sessionResult, extract values
        //open all historic sessionresults, extract values
        //compare current to historic, rank on successratio and metrics

    }

    private void generateFailedEventsDoc(Map<String, List<String>> failedEvents, String fileName) throws IOException {
        File file = new File(new File(pathResult),environment.name()+"_"+date +fileName);
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));

        String msg="";
        if(fileName.equals(fileNameFailedEvents))
            msg="failed rm events";
        if(fileName.equals(fileNotFoundTransactions))
            msg="not found transactions";


        System.out.println("\nStoring "+msg+"in: "+file.getAbsoluteFile());

        for(String key : failedEvents.keySet()){
            List<String> list = failedEvents.get(key);
            if(list!=null && !list.isEmpty()){
                bw.write(key+"\n");
                for(String row : list){
                    bw.write("\t"+row+"\n");
                }
            }
        }
        bw.flush();
        bw.close();
    }

    private void calculateMetrics(List<Integer> durations, BufferedWriter bw, int minDuration, int maxDuration) throws IOException {
        if(durations.size()>0){
            int mean=0, median=0, mode=0;

            for(int j : durations)
                mean+=j;
            mean = mean/durations.size();

            List<Integer> sortedDurations = new LinkedList<>(durations);
            Collections.sort(sortedDurations);
            median = sortedDurations.get(sortedDurations.size()/2);

            Map<Integer,Integer> modeMap = new HashMap<>();
            for(Integer duration : durations){
                Integer value = modeMap.get(duration);
                if(value==null)
                    modeMap.put(duration,1);
                else{
                    value++;
                    modeMap.put(duration,value);
                }
            }

            int highestMode=Integer.MIN_VALUE;
            for(Integer keyDuration : modeMap.keySet()){
                int amount = modeMap.get(keyDuration);
                if(amount>=highestMode){
                    highestMode=amount;
                    mode=keyDuration;
                }
            }

            String  message ="\tLatency (ms) - lowest: "+minDuration+", highest: "+maxDuration +", mean: "+mean+", median: "+median+", mode: "+mode;
            System.out.println(message);
            bw.write(message+"\n");

        }
    }

}
