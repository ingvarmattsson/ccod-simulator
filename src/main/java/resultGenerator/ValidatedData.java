package resultGenerator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ValidatedData {

    public Map<String,List<String>> failedTransactions;
    public int transactionsExpectedAmount;
    public int transactionsActualAmount;
    public String dateFrom, dateTo;

    public ValidatedData(){
        failedTransactions = new HashMap<>();
    }


    public void addFailedTransaction(String tenant, String transaction) {
        if (failedTransactions.get(tenant)==null)
            failedTransactions.put(tenant,new LinkedList<>());
        failedTransactions.get(tenant).add(transaction);
    }
}
