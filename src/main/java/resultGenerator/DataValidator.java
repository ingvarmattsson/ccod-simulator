package resultGenerator;

import ccodSession.Environments;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import data.Utilities;
import rest.RestHandler;
import cccObjects.TransactionCCC;
import rmObjects.RmHandler;
import rmObjects.RmTransaction;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static data.DefaultStrings.csvDelimeter;
import static data.DefaultStrings.resultTenantIndexer;
import static resultGenerator.ResultGenerator.pathTransactions;

public class DataValidator {


    private String dateFrom;
    private Map<String, List<String>> results;
    RestHandler rest;

    public DataValidator(Map<String, List<String>> results, String dateFrom, Environments environment) throws IOException {
        this.results=results;
        this.dateFrom = dateFrom;
        rest = new RestHandler(environment);

    }

    public ValidatedData validateTransactions(int sleepMs, boolean sendTransactions) throws IOException, ParseException, URISyntaxException, InterruptedException {

        Thread.sleep(sleepMs);

        if(rest.getToken()==null){
            System.out.println("Unable to get access token. Data validation terminated");
            return null;
        }

        ValidatedData vData = new ValidatedData();
        vData.dateFrom = dateFrom;
        vData.dateTo = Instant.now().toString();

        //validateTransactions transactions
        if(sendTransactions){
            Map<String,List<RmTransaction>> expectedResult = getTransactionsGroupedOnTenant();
            Map<String,List<TransactionCCC>> actualResult= new HashMap<>();

            List<String> tenants = new LinkedList<>(expectedResult.keySet());
            for (String tenant : tenants)
                actualResult.put(tenant, rest.getActualTransactions(tenant,dateFrom,vData.dateTo));

            validateTransactions(expectedResult,actualResult,vData);
        }



        //validateTransactions Errors
        //validateTransactions boxContents
        //validateTransactions systemStatuses
        return vData;
    }

    private void validateTransactions(Map<String, List<RmTransaction>> expectedResult, Map<String, List<TransactionCCC>> actualResult, ValidatedData vData) throws JsonProcessingException, ParseException {
        int expectedAmount=0, actualAmount=0;

        System.out.println("Valitating transactions...");
        for(String tenant : expectedResult.keySet()){
            List<RmTransaction> expected = expectedResult.get(tenant);
            List<TransactionCCC> actual = actualResult.get(tenant);
            expectedAmount += expected.size();
            actualAmount += actual.size();


            // find missing transactions
            if(expected.size()!=actual.size()){
                for(RmTransaction expectedTransaction : expected){
                    boolean found = false;
                    String expectedDateTime = Utilities.parseToAPIDate(expectedTransaction.DATE_TIME);
                    for (TransactionCCC actualTransaction: actual){
                        if((
                                expectedTransaction.UUID.equals(actualTransaction.machineId)) &&
                                (Long.compare(expectedTransaction.TRANSACTION_SEQ,actualTransaction.transactionSequence)==0) &&
                                (expectedDateTime.equals(actualTransaction.dateTime)))
                        {
                            found = true;
                            break;
                        }
                    }

                    if(!found)
                        vData.addFailedTransaction(tenant,RmHandler.parseRmToString(expectedTransaction));
                }
            }
        }

        vData.transactionsExpectedAmount = expectedAmount;
        vData.transactionsActualAmount = actualAmount;

    }

    private Map<String, List<RmTransaction>> getTransactionsGroupedOnTenant() {
        Map<String, List<RmTransaction>> groupedTransactions = new HashMap<>();
        List<String> transactions = results.get(pathTransactions);
        String currentTenant = null;
        for (String line : transactions) {
            if (line.startsWith(resultTenantIndexer)) {
                currentTenant = line.substring(resultTenantIndexer.length());
                if (groupedTransactions.get(currentTenant) == null) {
                    groupedTransactions.put(currentTenant, new LinkedList<>());
                }
            } else {
                String[] row = line.split(csvDelimeter);
                if (!row[4].startsWith("failed"))
                    groupedTransactions.get(currentTenant).add(new Gson().fromJson(row[3], RmTransaction.class));
            }

        }
        return groupedTransactions;
    }
}
