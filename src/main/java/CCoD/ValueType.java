package CCoD;

public enum ValueType {
    COIN,
    NOTE,
    SUSPECT_NOTES,
    COUNTERFEIT_NOTES,
    CHECK,
    VOUCHER,
    CLAIMED_VALUE,
    MIX_OF_VALUES
}
