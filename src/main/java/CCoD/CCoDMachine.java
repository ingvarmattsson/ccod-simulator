package CCoD;

import data.FillStatus;
import rmObjects.RmBox;
import rmObjects.RmBoxContent;

import java.io.Serializable;
import java.util.*;

import static CCoD.Capability.DEPOSIT;
import static CCoD.Capability.DISPENSE;
import static CCoD.Capability.EMPTY;
import static CCoD.TransactionType.*;
import static CCoD.ValueType.*;


public class CCoDMachine implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sfVersion, type, machineUUID;
    private MachineModels MACHINE_MODEL_TYPE;
    private LinkedList<RmBoxContent> boxContents;
    private LinkedList<RmBox> boxesList;
    private HashMap<Capability,LinkedList<RmBox>> boxesMap;
    private MachineUserSyncState machineUserSyncState;

    //RM Box id's
    public static final String roll0="ROLL00";
    public static final String roll1="ROLL01";
    public static final String roll2="ROLL02";
    public static final String roll3="ROLL03";
    public static final String roll4="ROLL04";
    public static final String roll5="ROLL05";
    public static final String roll6="ROLL06";
    public static final String roll7="ROLL07";
    public static final String roll8="ROLL08";
    public static final String roll9="ROLL09";
    public static final String roll10="ROLL10";
    public static final String roll11="ROLL11";
    public static final String roll12="ROLL12";
    public static final String roll13="ROLL13";
    public static final String bag="BAG";
    public static final String c201="C2-01";
    public static final String trBox="TRBOX";
    public static final String sVault="SVAULT";
    public static final String perc1="PERCONTA.1";
    public static final String perc2="PERCONTA.2";
    public static final String boxdropSafe="Drop Safe";
    public static final String pu01="PU01";
    public static final String pu02="PU02";
    public static final String pu03="PU03";
    public static final String pu04="PU04";
    public static final String rbBoxId="RB";
    public static final String abBoxId="AB";


    //device names
    public final String noteRecycler="Note Recycler", transportBox="Transport Box",smartVault1="SmartVault.1",
            smartVault2="SmartVault.2",smartVault3="SmartVault.3",dropSafe="Drop Safe",coinDispenser1="Coin Dispenser 1", coinDispenser2="Coin Dispenser 2";

    //device ids
    public final String idNoteRecycler="CIMA.1", idTransportBox="TransportBox.1",idSmartVault1=smartVault1,idSmartVault2=smartVault2,
            idSmartVault3=smartVault3,idDropSafe="DROPSAFE.1",idCoinDispenser1="PERCONTA.1",idCoinDispenser2="PERCONTA.2";

    //device types
    public final String typeNoteRecycler="NoteRecycler", typeTransportBox="TransportBox",typeSmartVault1="Safe",typeSmartVault2="Safe",
            typeSmartVault3="safe",typeDropSafe="DropSafe",typeCoinDispenser1="CoinDispense",typeCoinDispenser2="CoinDispense";
    private boolean emptyDropSafe=false;


    public void setBoxContents(LinkedList<RmBoxContent> boxContents) {
        this.boxContents = boxContents;
    }

    public CCoDMachine(MachineModels type, String sfVersion, String machineUUID, Currency currency){
        MACHINE_MODEL_TYPE =type;
        this.sfVersion =sfVersion;
        this.machineUUID =machineUUID;

        switch (MACHINE_MODEL_TYPE){
            case SDS_35:
                this.type ="SDS 35";
                break;
            case SDS_100:
                this.type ="SDS 100";
                break;
            case SDS_600:
                this.type ="SDS 600";
                break;

            case SDS_700:
                this.type ="SDS 700";
                break;

            case CDS_820J:
                this.type ="CDS 820j";
                break;

            case CDS_830:
                this.type ="CDS 830";
                break;

            case CDS_9:
                this.type ="CDS 9";
                break;

            case CDS_9R:
                this.type ="CDS 9R";
                break;

            case SPS700800:
                this.type ="SPS 700/800";
                break;

            case RCS_100:
                this.type ="RCS 100";
                break;

            case RCS_200:
                this.type ="RCS 200";
                break;

            case RCS_300:
                this.type ="RCS 300";
                break;

            case RCS_400:
                this.type ="RCS 400";
                break;

            case RCS_500:
                this.type ="RCS 500";
                break;

            case RCS_600_ACTIVE:
                this.type ="RCS 600 Active";
                break;

            case RCS_600:
                this.type ="RCS 600";
                break;

            case RCS_700:
                this.type ="RCS 700";
                break;

            case RCS_800:
                this.type ="RCS 800";
                break;

            case MuliPro:
                this.type ="MultiPro";
                break;

            case SALLEN_ELECTUM:
                this.type ="Sallen Electum";
                break;

            case NRI_PELICANO:
                this.type ="NRI Pelicano";
                break;

            case AZKOYEN_CASHLOGY_POS1000:
                this.type ="Azkoyen Cashlogy POS1000";
                break;

            case CIMA_DEPOSIT:
                this.type ="CIMA Deposit";
                break;

            case CIMA_RECYCLING:
                this.type ="CIMA Recycling";
                break;

            case GUNNEBO_CRC100:
                this.type ="Gunnebo CRC100";
                break;
        }
        initBoxContent(MACHINE_MODEL_TYPE, currency);
        initBoxMap();
        machineUserSyncState = new MachineUserSyncState();
    }

    public String getParentBoxContentDeviceId(RmBox boxToSearch){
        String toReturn = null;
        for(RmBoxContent boxContent : boxContents){
            for(RmBox boxes : boxContent.BOX_CONTENTS){
                if(boxes.BOX_ID.equals(boxToSearch.BOX_ID) && boxes.NAME.equals(boxToSearch.NAME)){
                    toReturn = boxContent.DEVICE_ID;
                    break;
                }
            }
        }

        return toReturn;
    }

    private void initBoxMap() {
        boxesMap = new HashMap<>();
        LinkedList<RmBox> dispenseBoxes = new LinkedList<>();
        LinkedList<RmBox> depositBoxes = new LinkedList<>();
        LinkedList<RmBox> emptyBoxes = new LinkedList<>();

        for (RmBox box : boxesList){
            List<String> capabilities = box.CAPABILITIES;
            for( String capability : capabilities){
                if(capability.equals(DEPOSIT.name()))
                    depositBoxes.add(box);
                if(capability.equals(DISPENSE.name()))
                    dispenseBoxes.add(box);
                if(capability.equals(EMPTY.name()))
                    emptyBoxes.add(box);

            }
        }

        boxesMap.put(DEPOSIT,depositBoxes);
        boxesMap.put(DISPENSE,dispenseBoxes);
        boxesMap.put(EMPTY,emptyBoxes);
    }

    public String getSfVersion() {
        return sfVersion;
    }

    public LinkedList<RmBox> getBoxesByFillStatus(Capability capability){
        return boxesMap.get(capability);
    }

    private void initBoxContent(MachineModels machineModel, Currency currency) {
        boxContents = new LinkedList<>();
        boxesList = new LinkedList<>();

        HashMap<String,String[]> deviceNameIdType = getDeviceNames(machineModel);

        Set<String> keys = deviceNameIdType.keySet();

        //transportbox depends on coindispensers, they need to be handled first
        List<String> deviceNames = new LinkedList<>(keys);
        Collections.sort(deviceNames);

        for(String deviceName : deviceNames){
            String[] idType = deviceNameIdType.get(deviceName);
            RmBoxContent boxContent = new RmBoxContent();

            boxContent.DEVICE = deviceName;
            boxContent.DEVICE_ID = idType[0];
            boxContent.DEVICE_TYPE = idType[1];
            ValueType valueType = getValueType(deviceName);
            boxContent.VALUE_TYPE = valueType.name();
            boxContent.BOX_CONTENTS = generateBoxContents(machineModel,deviceName,valueType, currency);
            boxContent.HIDE_CONTENTS=false;

            boxContents.add(boxContent);

        }

    }

    private List<RmBox> tempCoinBoxes = new LinkedList<>();

    private List<RmBox> generateBoxContents(MachineModels machineModel, String deviceName,ValueType valueType, Currency currency) {
        List<String> boxIds = new LinkedList<>();
        switch (machineModel){
            case RCS_500:

                if(deviceName.equals(noteRecycler)){
                    boxIds.add(roll0);
                    boxIds.add(roll1);
                    boxIds.add(roll2);
                    boxIds.add(roll3);
                    boxIds.add(roll4);
                    boxIds.add(roll5);
                    boxIds.add(roll6);
                    boxIds.add(roll7);
                    boxIds.add(roll8);
                    boxIds.add(roll9);
                    boxIds.add(roll10);
                    boxIds.add(roll11);
                    boxIds.add(roll12);
                    boxIds.add(roll13);
                    boxIds.add(c201);
                    boxIds.add(bag);

                }
                if(deviceName.equals(transportBox)){
                    boxIds.add(trBox);

                }
                if(deviceName.equals(smartVault1) || deviceName.equals(smartVault2) || deviceName.equals(smartVault3)){
                    boxIds.add(sVault);
                }
                if(deviceName.equals(coinDispenser1) || deviceName.equals(coinDispenser2)){
                    boxIds.add(pu01);
                    boxIds.add(pu02);
                    boxIds.add(pu03);
                    boxIds.add(pu04);



                }
                if (deviceName.equals(dropSafe)) {
                    boxIds.add(boxdropSafe);
                }

                break;
        }

        return generateBoxes(boxIds, currency,valueType,deviceName);
    }

    private List<RmBox> generateBoxes(List<String> boxIds, Currency currency, ValueType valueType, String deviceName) {
        List<RmBox> boxes = new LinkedList<>();
        for(String boxId : boxIds){
            if(!boxId.equals(bag) && !boxId.equals(trBox)){
                RmBox box = new RmBox();

                RmBoxDefaultValues dv = new RmBoxDefaultValues(boxId,currency,valueType,null);
                box.BOX_ID=boxId;
                box.CONTAINER_ID = boxId;
                box.VALUES =dv.rmValues;
                box.TYPE=valueType.name();
                box.NAME=deviceName+" "+boxId;
                box.DEFAULT_LEVEL=dv.defaultLevel;
                box.MAXIMUM_LEVEL=dv.maximumLevel;
                box.MINIMUM_LEVEL=dv.minimumLevel;
                box.HIGH_LIMIT_LEVEL=dv.highLimitLevel;
                box.LOW_LIMIT_LEVEL=dv.lowLimitLevel;
                box.FILL_STATUS=dv.fillStatus;
                box.FILL_STATUS=dv.fillStatus;
                box.CAPABILITIES=dv.capabilities;
                box.ERROR_STATUS=dv.errorStatus;

                boxes.add(box);
                boxesList.add(box);

                if(deviceName.equals(coinDispenser1) || deviceName.equals(coinDispenser2))
                    tempCoinBoxes.add(box);

            }
        }

        for(String boxId : boxIds){
            if(boxId.equals(bag) || boxId.equals(trBox)){
                RmBox box = new RmBox();
                List<RmBox> boxList = (boxId.equalsIgnoreCase(bag) ? boxes : tempCoinBoxes);

                RmBoxDefaultValues dv = new RmBoxDefaultValues(boxId,currency,valueType,boxList);
                box.BOX_ID=boxId;
                box.CONTAINER_ID = boxId;
                box.VALUES =dv.rmValues;
                box.TYPE=valueType.name();
                box.NAME=deviceName+" "+boxId;
                box.DEFAULT_LEVEL=dv.defaultLevel;
                box.MAXIMUM_LEVEL=dv.maximumLevel;
                box.MINIMUM_LEVEL=dv.minimumLevel;
                box.HIGH_LIMIT_LEVEL=dv.highLimitLevel;
                box.LOW_LIMIT_LEVEL=dv.lowLimitLevel;
                box.FILL_STATUS=dv.fillStatus;
                box.CAPABILITIES=dv.capabilities;
                box.ERROR_STATUS=dv.errorStatus;

                boxes.add(box);
                boxesList.add(box);

            }

        }

        return boxes;
    }

    private ValueType getValueType(String deviceName) {
        ValueType vt = null;
        switch (deviceName){

            //Notes
            case noteRecycler:
                vt = NOTE;
                break;

            //Coins
            case transportBox:
            case coinDispenser1:
            case coinDispenser2:
                vt=COIN;
                break;

            //Mix
            case smartVault1:
            case smartVault2:
            case smartVault3:
                vt= MIX_OF_VALUES;
                break;

            //Claimed Value
            case dropSafe:
                vt = CLAIMED_VALUE;
                break;
        }
        return vt;
    }


    //TODO implement for other models
    private HashMap<String,String[]> getDeviceNames(MachineModels machineModel) {


        HashMap<String,String[]> map = new HashMap<>();
        switch (machineModel){
            case RCS_500:
                map.put(transportBox, new String[]{idTransportBox,typeTransportBox});
                map.put(noteRecycler,new String[]{idNoteRecycler,typeNoteRecycler});
                map.put(smartVault1,new String[]{idSmartVault1,typeSmartVault1});
                map.put(smartVault2,new String[]{idSmartVault2,typeSmartVault2});
                map.put(smartVault3,new String[]{idSmartVault3,typeSmartVault3});
                map.put(dropSafe,new String[]{idDropSafe,typeDropSafe});
                map.put(coinDispenser1,new String[]{idCoinDispenser1,typeCoinDispenser1});
                map.put(coinDispenser2,new String[]{idCoinDispenser2,typeCoinDispenser2});
                break;
            case RCS_300:
                map.put(coinDispenser1,new String[]{idCoinDispenser1,typeCoinDispenser1});
                map.put(coinDispenser2,new String[]{idCoinDispenser2,typeCoinDispenser2});

                break;
        }

        return map;
    }

    public LinkedList<RmBoxContent> getBoxContents() {
        return boxContents;
    }



    public String getMachineUUID() {
        return machineUUID;
    }

    public List<RmBox> getBoxOnDeviceName(String... deviceName) {
        List<RmBox> boxes = new LinkedList<>();
        for(String name : deviceName)
            for(RmBoxContent boxContent : boxContents)
                if(boxContent.DEVICE.equals(name))
                    boxes.addAll(boxContent.BOX_CONTENTS);
        return boxes;
    }

    public LinkedList<RmBox> getBoxesList() {
        return boxesList;
    }

    public void setMachineUUID(String uuid) {
        machineUUID=uuid;
    }

    public Map<RmBox, LinkedList<RmBox>> getBoxesByFillStatus(TransactionType type) {
        Map<RmBox, LinkedList<RmBox>> boxMap = new HashMap<>();

        RmBox toNotes = null, toCoins = null;
        if(type==PURGE){
            toNotes = getBoxOnId("BAG");
            toCoins = getBoxOnId("TRBOX");
        }
        if(type==REFILL || type ==EMPTY_OUT){
            toNotes = new RmBox();
            toNotes.NAME=NOTE.name();
            toCoins = new RmBox();
            toCoins.NAME=COIN.name();
        }
        if(type==MANUAL_REFILL){
            toCoins = new RmBox();
            toCoins.NAME=COIN.name();
            boxMap.put(toCoins ,new LinkedList<RmBox>());
        }

        if(toNotes!=null)
            boxMap.put(toNotes,new LinkedList<RmBox>());
        if(toCoins!=null)
            boxMap.put(toCoins ,new LinkedList<RmBox>());


        for(RmBox box : boxesList){


            switch (type){

                //from note rolls to note bag, from coin boxes to transport box
                case PURGE:
                    if (box.FILL_STATUS.equals(FillStatus.HIGH_LIMIT_LEVEL.name()) || box.FILL_STATUS.equals(FillStatus.MAXIMUM_LEVEL.name())) {
                        if(box.BOX_ID.startsWith("ROLL"))
                            boxMap.get(toNotes).add(box);
                        if(box.BOX_ID.startsWith("PU"))
                            boxMap.get(toCoins).add(box);
                    }
                    break;
                //refill note rolls or coin boxes
                case REFILL:
                    if (box.FILL_STATUS.equals(FillStatus.LOW_LIMIT_LEVEL.name()) || box.FILL_STATUS.equals(FillStatus.MINIMUM_LEVEL.name())) {
                        if(box.BOX_ID.startsWith("ROLL"))
                            boxMap.get(toNotes).add(box);
                        if(box.BOX_ID.startsWith("PU"))
                            boxMap.get(toCoins).add(box);
                    }

                //refilll coins only
                case MANUAL_REFILL:
                    if(box.BOX_ID.startsWith("PU"))
                        if (box.FILL_STATUS.equals(FillStatus.LOW_LIMIT_LEVEL.name()) || box.FILL_STATUS.equals(FillStatus.MINIMUM_LEVEL.name()))
                            boxMap.get(toCoins).add(box);
                    break;
                case EMPTY_OUT:
                    if(box.CAPABILITIES.contains(Capability.EMPTY))
                        if(box.FILL_STATUS.equals(FillStatus.HIGH_LIMIT_LEVEL.name())|| box.FILL_STATUS.equals(FillStatus.MAXIMUM_LEVEL.name()))
                            if(box.BOX_ID.equals("TR_BOX"))
                                boxMap.get(toCoins).add(box);
                            if(box.BOX_ID.equals("BAG"))
                                boxMap.get(toNotes).add(box);
                    break;
            }
        }
        return boxMap;
    }

    private RmBox getBoxOnId(final String boxID) {

        for (RmBox box : boxesList){
            if(box.BOX_ID.equals(boxID))
                return box;
        }
        return null;
    }

    @Override
    public String toString(){
        return machineUUID;
    }


    public MachineUserSyncState getMachineUserSyncState(){
        return machineUserSyncState;
    }

    public RmBoxContent getDropSafe() {
        RmBoxContent toReturn = null;
        for (RmBoxContent bc : boxContents)
            if(bc.DEVICE_ID.equals("DROPSAFE.1"))
                toReturn = bc;

        return toReturn;
    }

    public void setEmptyDropSafe(boolean emptyDropSafe) {
        this.emptyDropSafe = emptyDropSafe;
    }

    public boolean getEmptyDropSafe() {
        return emptyDropSafe;
    }



}
