package CCoD;

import com.sun.xml.bind.v2.model.core.ID;
import rmObjects.RmBox;
import rmObjects.RmValue;

import java.util.LinkedList;
import java.util.List;

import static CCoD.CCoDMachine.bag;
import static CCoD.Currency.*;
import static CCoD.ValueType.COIN;
import static CCoD.ValueType.NOTE;

public class RmBoxDefaultValues {

    protected static final String capDeposit="DEPOSIT", capDispense="DISPENSE",capEmpty="EMPTY";
    private String[] claimedValueTypes;

    protected int defaultLevel, maximumLevel,minimumLevel,highLimitLevel,lowLimitLevel;
    protected String fillStatus;
    protected List<String> capabilities;
    protected List<String> errorStatus;
    protected List<RmValue > rmValues;

    public RmBoxDefaultValues(String boxId, Currency currency, ValueType valueType,List<RmBox> boxes){
        fillStatus="OK";
        errorStatus = new LinkedList<>();
        errorStatus.add(fillStatus);

        capabilities = new LinkedList<>();

        claimedValueTypes = new String[]{"OTHER", "VOUCHER", "COUPON"};

        switch (boxId){
            case CCoDMachine.roll0:
            case CCoDMachine.roll1:
            case CCoDMachine.roll2:
            case CCoDMachine.roll3:
            case CCoDMachine.roll4:
            case CCoDMachine.roll5:
            case CCoDMachine.roll6:
            case CCoDMachine.roll7:
            case CCoDMachine.roll8:
                maximumLevel=600;
                minimumLevel=10;
                defaultLevel=maximumLevel/2;
                highLimitLevel=550;
                lowLimitLevel=50;
                capabilities.add(capDeposit);
                capabilities.add(capDispense);
                break;
            case CCoDMachine.roll9:
            case CCoDMachine.roll10:
            case CCoDMachine.roll11:
            case CCoDMachine.roll12:
            case CCoDMachine.roll13:
                maximumLevel=600;
                minimumLevel=10;
                defaultLevel=maximumLevel/2;
                highLimitLevel=550;
                lowLimitLevel=50;
                capabilities.add(capDeposit);
                break;
            case bag:
                maximumLevel=3000;
                minimumLevel=200;
                defaultLevel=maximumLevel/2;
                highLimitLevel=2600;
                lowLimitLevel=400;
                capabilities.add(capDeposit);
                capabilities.add(capEmpty);
                break;
            case CCoDMachine.c201:
                maximumLevel=600;
                minimumLevel=50;
                defaultLevel=maximumLevel/2;
                highLimitLevel=550;
                lowLimitLevel=50;
                capabilities.add(capDeposit);
                capabilities.add(capEmpty);
                break;
            case CCoDMachine.sVault:
                defaultLevel=0;
                maximumLevel=Integer.MAX_VALUE;
                minimumLevel=0;
                highLimitLevel=Integer.MAX_VALUE-1000;
                lowLimitLevel=1000;
                capabilities.add(capDeposit);
                capabilities.add(capDispense);
                capabilities.add(capEmpty);
                break;
            case CCoDMachine.pu01:
            case CCoDMachine.pu02:
            case CCoDMachine.pu03:
            case CCoDMachine.pu04:
                maximumLevel=1000;
                minimumLevel=30;
                defaultLevel=maximumLevel/2;
                highLimitLevel=750;
                lowLimitLevel=200;
                capabilities.add(capDispense);
                capabilities.add(capDeposit);
                break;
            case CCoDMachine.boxdropSafe:
                defaultLevel=0;
                maximumLevel=100;
                minimumLevel=0;
                highLimitLevel=80;
                lowLimitLevel=20;
                capabilities.add(capDeposit);
                capabilities.add(capEmpty);
                break;
            case CCoDMachine.trBox:
                defaultLevel=0;
                maximumLevel=5500;
                minimumLevel=0;
                highLimitLevel=5000;
                lowLimitLevel=0;
                capabilities.add(capEmpty);
                break;

        }

        setRmValues(boxId,currency,valueType,boxes);

    }

    private void setRmValues(String boxId, Currency currency, ValueType valueType, List<RmBox> boxes) {
        rmValues = new LinkedList<>();
        RmValue rmValue = new RmValue();
        rmValue.CURRENCY=currency.name();
        rmValue.TYPE=valueType.name();
        rmValue.DECIMALS=2;
        rmValue.COUNT=defaultLevel;

        if(currency==CLP)
            rmValue.DECIMALS=0;

        if(currency==EUR || currency == CLP || currency == MAD || currency == RON ||currency == PHP||currency == IDR)
            setEurValues(rmValue,boxId,boxes, currency,valueType);
        if(currency==USD)
            setUsdValues(rmValue,boxId,boxes, currency,valueType);
        /*if(currency==CLP)
            setCLPValues(rmValue,boxId,boxes);*/

    }

    private void setCLPValues(RmValue rmValue, String boxId, List<RmBox> boxes) {

    }

    private void setUsdValues(RmValue rmValue, String boxId, List<RmBox> boxes, Currency currency, ValueType valueType) {
        switch (boxId){
            case CCoDMachine.roll0:
            case CCoDMachine.roll1:
                rmValue.PIECE_VALUE=rmValue.DENOMINATION=100;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;

            case CCoDMachine.roll2:
            case CCoDMachine.roll3:
                rmValue.PIECE_VALUE=rmValue.DENOMINATION=200;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll4:
            case CCoDMachine.roll5:
                rmValue.PIECE_VALUE=rmValue.DENOMINATION=500;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;

            case CCoDMachine.roll6:
            case CCoDMachine.roll7:
                rmValue.PIECE_VALUE=rmValue.DENOMINATION=1000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;

            case CCoDMachine.roll8:
            case CCoDMachine.roll9:
                rmValue.PIECE_VALUE=rmValue.DENOMINATION=2000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll10:
            case CCoDMachine.roll11:
                rmValue.DENOMINATION=5000;
                rmValue.PIECE_VALUE=5000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll12:
            case CCoDMachine.roll13:
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;

            case bag:
            case CCoDMachine.trBox:

                ValueType type;
                if(boxId.equals(bag))
                    type = NOTE;
                else
                    type = COIN;

                for(RmBox box : boxes){
                    if(box.TYPE== type.name()){
                        List<RmValue> boxValues = box.VALUES;
                        for(RmValue v : boxValues){
                            RmValue value = new RmValue();
                            value.CURRENCY = v.CURRENCY;
                            value.TYPE = v.TYPE;
                            value.DECIMALS = v.DECIMALS;
                            value.COUNT=10;
                            value.DENOMINATION = v.DENOMINATION;
                            value.PIECE_VALUE = v.PIECE_VALUE;
                            value.TOTAL = (value.DENOMINATION*value.COUNT);
                            rmValues.add(value);

                        }
                    }
                }
                removeDuplicateDenominations(rmValues);


                break;
            case CCoDMachine.c201:
                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.COUNT=0;
                rmValue.C2_COUNT= 10;
                rmValue.C2_CREDITED=false;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu01:
                rmValue.DENOMINATION=1;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu02:
                rmValue.DENOMINATION=5;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu03:
                rmValue.DENOMINATION=10;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu04:
                rmValue.DENOMINATION=25;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.sVault:

                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.TYPE=NOTE.name();
                rmValue.COUNT=10;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=NOTE.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=10;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=NOTE.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=NOTE.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=10;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);


                break;
            case CCoDMachine.boxdropSafe:
                rmValue.DENOMINATION=15000;
                rmValue.PIECE_VALUE=15000;
                rmValue.COUNT=10;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValue.CLAIMED_VALUE_TYPE = claimedValueTypes[0];
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=valueType.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=2000;
                rmValue.PIECE_VALUE=2000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValue.CLAIMED_VALUE_TYPE = claimedValueTypes[1];
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=valueType.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=2000;
                rmValue.PIECE_VALUE=2000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValue.CLAIMED_VALUE_TYPE = claimedValueTypes[2];
                rmValues.add(rmValue);
                break;

        }
    }

    private void setEurValues(RmValue rmValue, String boxId, List<RmBox> boxes, Currency currency, ValueType valueType) {

        switch (boxId){
            case CCoDMachine.roll0:
            case CCoDMachine.roll1:
                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;

            case CCoDMachine.roll2:
            case CCoDMachine.roll3:
                rmValue.DENOMINATION=1000;
                rmValue.PIECE_VALUE=1000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;


            case CCoDMachine.roll4:
            case CCoDMachine.roll5:
                rmValue.DENOMINATION=2000;
                rmValue.PIECE_VALUE=2000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll6:
                rmValue.DENOMINATION=5000;
                rmValue.PIECE_VALUE=5000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll7:
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll8:
                rmValue.DENOMINATION=50000;
                rmValue.PIECE_VALUE=50000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll9:
                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.COUNT=0;
                rmValue.C2_COUNT=1;
                rmValue.C2_CREDITED = false;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll10:
                rmValue.DENOMINATION=1000;
                rmValue.PIECE_VALUE=1000;
                rmValue.COUNT=0;
                rmValue.C2_COUNT=1;
                rmValue.C2_CREDITED = false;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll11:

                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.COUNT=10;
                rmValue.C4B_COUNT= rmValue.COUNT;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=valueType.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=10;
                rmValue.DENOMINATION=1000;
                rmValue.PIECE_VALUE=1000;
                rmValue.C4B_COUNT= rmValue.COUNT;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.roll12:
            case CCoDMachine.roll13:
                rmValue.DENOMINATION=5000;
                rmValue.PIECE_VALUE=5000;
                rmValue.COUNT=10;
                rmValue.C3_COUNT= rmValue.COUNT;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=valueType.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                break;
            case bag:
            case CCoDMachine.trBox:

                ValueType type;
                if(boxId.equals(bag))
                    type = NOTE;
                else
                    type = COIN;

                for(RmBox box : boxes){
                    if(box.TYPE== type.name()){
                        List<RmValue> boxValues = box.VALUES;
                        for(RmValue v : boxValues){
                            RmValue value = new RmValue();
                            value.CURRENCY = v.CURRENCY;
                            value.TYPE = v.TYPE;
                            value.DECIMALS = v.DECIMALS;
                            value.COUNT=10;
                            value.DENOMINATION = v.DENOMINATION;
                            value.PIECE_VALUE = v.PIECE_VALUE;
                            value.TOTAL = (value.DENOMINATION*value.COUNT);
                            rmValues.add(value);

                        }
                    }
                }
                removeDuplicateDenominations(rmValues);


                break;
            case CCoDMachine.c201:
                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.COUNT=0;
                rmValue.C2_COUNT= 10;
                rmValue.C2_CREDITED=false;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu01:
                rmValue.DENOMINATION=1;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu02:
                rmValue.DENOMINATION=5;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu03:
                rmValue.DENOMINATION=10;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.pu04:
                rmValue.DENOMINATION=20;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);
                break;
            case CCoDMachine.sVault:

                rmValue.DENOMINATION=500;
                rmValue.PIECE_VALUE=500;
                rmValue.TYPE=NOTE.name();
                rmValue.COUNT=10;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=NOTE.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=10;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=NOTE.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=NOTE.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=10;
                rmValue.DENOMINATION=10000;
                rmValue.PIECE_VALUE=10000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValues.add(rmValue);


                break;
            case CCoDMachine.boxdropSafe:
                rmValue.DENOMINATION=15000;
                rmValue.PIECE_VALUE=15000;
                rmValue.COUNT=10;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValue.CLAIMED_VALUE_TYPE = claimedValueTypes[0];
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=valueType.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=2000;
                rmValue.PIECE_VALUE=2000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValue.CLAIMED_VALUE_TYPE = claimedValueTypes[1];
                rmValues.add(rmValue);

                rmValue = new RmValue();
                rmValue.CURRENCY=currency.name();
                rmValue.TYPE=valueType.name();
                rmValue.DECIMALS=2;
                rmValue.COUNT=0;
                rmValue.DENOMINATION=2000;
                rmValue.PIECE_VALUE=2000;
                rmValue.TOTAL=(rmValue.DENOMINATION*rmValue.COUNT);
                rmValue.CLAIMED_VALUE_TYPE = claimedValueTypes[2];
                rmValues.add(rmValue);
                break;

        }
    }

    private void removeDuplicateDenominations(List<RmValue> rmValues) {

        LinkedList<RmValue> temp = new LinkedList<>(rmValues);
        for(RmValue v : temp){
            int denominationCount =0;
            for(int i =0; i<rmValues.size();i++){
                if(v.DENOMINATION == rmValues.get(i).DENOMINATION){
                    denominationCount++;
                }
                if(denominationCount>1){
                    rmValues.remove(i);
                    denominationCount--;
                }
            }
        }

    }
}
