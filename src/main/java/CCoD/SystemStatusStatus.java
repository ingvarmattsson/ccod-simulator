package CCoD;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum SystemStatusStatus {

    OutOfUse,Disconnected,Boot,Idle, Operator, Operator2, User,
    Identify,  OutOfUseByHost, Closed, Unknown;

    public static final List<SystemStatusStatus> SYSTEM_STATUS_TYPE_LIST = Collections.unmodifiableList(Arrays.asList(values()));

    }
