package CCoD;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum ClaimedValueTypes {

    VOUCHER, CREDIT_CARD, GIFT_CARD, COUPON, OTHER;


    public static final List<ClaimedValueTypes> CLAIMED_VALUE_TYPES_LIST= Collections.unmodifiableList(Arrays.asList(values()));

    public static ClaimedValueTypes getRandomClaimedValueType()  {
        return CLAIMED_VALUE_TYPES_LIST.get(new Random().nextInt(CLAIMED_VALUE_TYPES_LIST.size()));
    }
}
