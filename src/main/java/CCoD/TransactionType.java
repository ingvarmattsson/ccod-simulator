package CCoD;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum TransactionType {
    DEPOSIT,
    DISPENSE,
    END_OF_SHIFT,
    POSSIBLE_TAMPERING,
    UNFINISHED_TRANSACTION,
    EXCHANGE,
    PURGE,
    EMPTY_OUT,
    MANUAL_REFILL,
    REFILL,
    MOVE,
    MANUAL_DEPOSIT,
    ZREPORT;

    TransactionType thisTransactionType = null;

    public static final List<TransactionType> TRANSACTION_TYPE_LIST = Collections.unmodifiableList(Arrays.asList(values()));

    public static TransactionType getRandomTransactionType()  {
        return TRANSACTION_TYPE_LIST.get(new Random().nextInt(TRANSACTION_TYPE_LIST.size()));
    }



}
