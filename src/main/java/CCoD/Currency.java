package CCoD;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Currency {
    EUR,SEK,USD, DKK, NOK, CLP, MAD, RON, PHP, IDR;

    public static final List<Currency> CLAIMED_VALUE_TYPES_LIST= Collections.unmodifiableList(Arrays.asList(values()));

    public static Currency getRandomCurrency()  {
        return CLAIMED_VALUE_TYPES_LIST.get(new Random().nextInt(CLAIMED_VALUE_TYPES_LIST.size()));
    }
}
