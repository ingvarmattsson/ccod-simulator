package CCoD;

import ccodSession.EventType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum MachineModels{
    SDS_35,
    SDS_100,
    SDS_600,
    SDS_700,
    CDS_820J,
    CDS_830,
    CDS_9,
    CDS_9R,
    SPS700800,
    RCS_100,
    RCS_200,
    RCS_300,
    RCS_400,
    RCS_500,
    RCS_600_ACTIVE,
    RCS_600,
    RCS_700,
    RCS_800,
    MuliPro,
    SALLEN_ELECTUM,
    NRI_PELICANO,
    AZKOYEN_CASHLOGY_POS1000,
    CIMA_DEPOSIT,
    CIMA_RECYCLING,
    GUNNEBO_CRC100;

    public static final List<MachineModels> MACHINE_MODELS = Collections.unmodifiableList(Arrays.asList(values()));

    public static java.util.List<MachineModels> getMachineModels()  {
        return MACHINE_MODELS;
    }

}
