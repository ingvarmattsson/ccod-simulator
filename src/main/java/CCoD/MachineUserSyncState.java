package CCoD;

import rmObjects.RmUserChange;
import rmObjects.RmUserMuM;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MachineUserSyncState implements Serializable {

    private static final long serialVersionUID = 1L;
    private String fileVersion;
    private Map<String,RmUserMuM> authorizedUsers;

    public MachineUserSyncState() {
        authorizedUsers = Collections.synchronizedMap(new HashMap<>(10000));
        fileVersion="";
    }

    public String getFileVersion() {
        return fileVersion;
    }

    public void setFileVersion(String fileVersion) {
        this.fileVersion = fileVersion;
    }

    public Map<String, RmUserMuM> getAuthorizedUsers() {
        return authorizedUsers;
    }

    public Map<String,RmUserMuM> appendAuthorizedUsers(List<RmUserChange> users) {
        if(users == null || users.isEmpty())
            return null;
        for(RmUserChange u : users)
            authorizedUsers.put(u.USER.USER_ID,u.USER);
        return authorizedUsers;
    }

    public void deleteUsers(List<RmUserChange> usersToSync) {
        for(RmUserChange u : usersToSync)
            authorizedUsers.remove(u.USER.USER_ID);
    }
}
