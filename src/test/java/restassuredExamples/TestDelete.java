package restassuredExamples;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class TestDelete {

    @Test
    public void test1Delete(){

        given().
            when().
                delete("https://reqres.in/api/users/2").
            then().
                statusCode(204).
                log().all();
    }
}
