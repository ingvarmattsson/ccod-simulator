package restassuredExamples;

import com.google.gson.Gson;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class TestPost {

    @Test
    public void test1Post(){

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name","Patrik");
        map.put("job","Tester");
        System.out.println(map);
        String payload = new Gson().toJson(map);
        System.out.println(payload);

        given().
                header("Content_type","application/json").
                body(payload).
            when().
                post("https://reqres.in/api/users").
            then().
                statusCode(201);
    }
}
