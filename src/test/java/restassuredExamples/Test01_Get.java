package restassuredExamples;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;

public class Test01_Get {

    @Test
    void test01(){
        Response res = get("https://reqres.in/api/users?page=2");
       int code = res.getStatusCode();
       ResponseBody b =  res.getBody();
       String s = res.getBody().asString();
       System.out.println(s);
        Assert.assertEquals(code,200);
    }

    @Test
    void  test02(){
        given()
                .get("https://reqres.in/api/users?page=2")
            .then()
                .statusCode(200)
                .body("data.id[0]",equalTo(7));
    }
}
