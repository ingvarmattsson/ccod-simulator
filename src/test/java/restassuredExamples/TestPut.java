package restassuredExamples;

import com.google.gson.Gson;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class TestPut {

    @Test
    public void test1Put(){

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name","Patrik");
        map.put("job","Tester2");
        System.out.println(map);
        String payload = new Gson().toJson(map);
        System.out.println(payload);

        given().
            header("Content_type","application/json").
            body(payload).
        when().
            put("https://reqres.in/api/users").
        then().
            statusCode(200).
            log().all();
    }
}
