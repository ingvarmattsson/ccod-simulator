package restassuredExamples;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.given;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class TestsGet {

    @Test
    void test1(){
        given().

                get("https://reqres.in/api/users?page=2").
            then().
                statusCode(200).
                body("data.id[1]",equalTo(8)).
                body("data.id[0]",equalTo(7)).
                body("data.first_name", hasItems("Michael","Lindsay","John")).
                log().all();
    }
}
