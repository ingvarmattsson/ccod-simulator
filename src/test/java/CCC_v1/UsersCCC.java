package CCC_v1;

import cccObjects.UserCCC;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class UsersCCC {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/users";
    private String urlLogin=DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/auth/token";
    Map<String,String> headers;
    List<UserCCC> users;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        users = new LinkedList<UserCCC>();
    }

    @Test
    public void getUsers(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.email", hasItems("connect@cashcomplete.com", "patrik.lind@suzohapp.com","robin@robin.com"));
    }

    @Test(priority = 1)
    public void postUserAdmin(){

        String payload = "{\"status\":\"Active\",\"countryCode\":\"+1\",\"role\":\"Admin\",\"phone\":\"+1123456\",\"username\":\"ra-t1@suzohapp.com\",\"password\":\"password-ra-t1\",\"firstName\":\"Rest\",\"lastName\":\"Assured Admin\",\"email\":\"ra-t1@suzohapp.com\",\"metadata\":{}}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("username", equalTo("ra-t1@suzohapp.com")).
                extract().asString();

        users.add(new Gson().fromJson(response, UserCCC.class));
        System.out.println();
    }

    @Test(priority = 2)
    public void postUserNonAdmin(){
        String payload = "{\"status\":\"Active\",\"countryCode\":\"+46\",\"role\":\"User\",\"phone\":\"+46123456\",\"username\":\"ra-t2@suzohapp.com\",\"password\":\"password-ra-t2\",\"firstName\":\"Rest Assured\",\"lastName\":\"Nonadmin\",\"email\":\"ra-t2@suzohapp.com\",\"metadata\":{\"autoRefreshEnabled\":true}}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url).
                then().
                statusCode(201).
                body("username", equalTo("ra-t2@suzohapp.com")).
                extract().asString();

        users.add(new Gson().fromJson(response, UserCCC.class));
    }

    @Test(dependsOnMethods = {"postUserAdmin"},priority = 3)
    public void loginAdmin(){
        given().
                header("Content-type","application/json").
                queryParam("username","ra-t1@suzohapp.com").
                queryParam("hashedPassword","cec17c0ed48bd8a6e84d36a321d007a56fc053ca").
            when().
                post(urlLogin).
            then().
                statusCode(200);
    }

    @Test(dependsOnMethods = {"postUserNonAdmin"},priority = 4)
    public void loginNonAdmin(){
        given().
                header("Content-type","application/json").
                queryParam("username","ra-t2@suzohapp.com").
                queryParam("hashedPassword","b857095e11e4c1a5e4d9630781b7ca644540d210").
                when().
                post(urlLogin).
                then().
                statusCode(200);
    }

    @Test(dependsOnMethods = {"postUserAdmin"})
    public void postUserConflict(){
        String payload = "{\"status\":\"Active\",\"countryCode\":\"+1\",\"role\":\"Admin\",\"phone\":\"+1123456\",\"username\":\"ra-t1@suzohapp.com\",\"password\":\"password-ra-t1\",\"firstName\":\"Rest\",\"lastName\":\"Assured Admin\",\"email\":\"ra-t1@suzohapp.com\",\"metadata\":{}}";

         given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(409);
    }

    @Test(dependsOnMethods = {"postUserAdmin"})
    public void putUser1(){
        UserCCC user = users.get(0);
        user.firstName+=" - edit";
        user.lastName+=" - edit";
        user.role="User";

        String payload = new Gson().toJson(user);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+user.email).
            then().
                statusCode(200);

    }

    @Test(dependsOnMethods = {"postUserAdmin","postUserNonAdmin"},priority = 5)
    public void deleteUser(){
        for(UserCCC u : users){

            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/"+u.email).
                then().
                    statusCode(200);
        }
    }

}
