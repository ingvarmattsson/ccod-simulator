package CCC_v1;

import CCoD.TransactionType;
import cccObjects.Alert;
import cccObjects.ScheduledReport;
import cccObjects.WebHookCCC;
import ccodSession.Environments;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;
import webhookReceiver.WebhookReceiver;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static CCC_v1.Webhooks.WebhookTypes.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Webhooks {

    public enum WebhookTypes{
        Transaction, TransactionMachineContent, Error, SystemStatus, MachineOnlineStatusChange, triggerTypeInvalid;
    }

    Environments env = DefaultStrings.environmentForRestAssured;
    String url = DefaultStrings.getBaseURL(env)+"/api/v1/webhook/hooks";
    String tenant = DefaultStrings.tenantRestAssured;
    Map<String,String> headers;

    List<WebHookCCC> webhooks;
    WebhookReceiver webhookReceiver;

    public String destinationHttps="https://webhook.site/dae24c22-90ad-4862-aa92-7dd59060eb84";
    public String destinationNonHttps="http://webhook.site/dae24c22-90ad-4862-aa92-7dd59060eb84";

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",tenant);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        webhooks = new LinkedList<WebHookCCC>();
        webhookReceiver = new WebhookReceiver();

    }

    private String generatePayLoad(String name, WebhookTypes triggerType, String destination, Boolean allowNonHttps) throws JsonProcessingException {
        WebHookCCC webHookCCC = new WebHookCCC();
        webHookCCC.name=name;
        webHookCCC.triggerType = triggerType.name();
        webHookCCC.destination = destination;
        webHookCCC.allowNonHttps = allowNonHttps;
        return new ObjectMapper().writeValueAsString(webHookCCC);
    }

    @Test (priority = 1)
    public void post1TransactionValidDestinationNonHttpsTrue() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post1TransactionValidDestinationNonHttpsTrue", Transaction,destinationNonHttps,true)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post1TransactionValidDestinationNonHttpsTrue")).
                body("triggerType",equalTo("Transaction")).
                body("destination",equalTo(destinationNonHttps)).
                body("allowNonHttps",equalTo(true)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 2)
    public void post2TransactionValidDestinationNonHttpsFalse() throws JsonProcessingException {

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post2TransactionValidDestinationNonHttpsFalse", Transaction,destinationHttps,false)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post2TransactionValidDestinationNonHttpsFalse")).
                body("triggerType",equalTo("Transaction")).
                body("destination",equalTo(destinationHttps)).
                body("allowNonHttps",equalTo(false)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 3)
    public void post3TransactionInvalidDestinationNonHttpsFalse() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post3TransactionInvalidDestinationNonHttpsFalse", Transaction,destinationNonHttps,false)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all().
                extract().asString();

    }

    @Test (priority = 4)
    public void post4TransactionMachineContentValidDestinationNonHttpsTrue() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post4TransactionMachineContentValidDestinationNonHttpsTrue", TransactionMachineContent,destinationNonHttps,true)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post4TransactionMachineContentValidDestinationNonHttpsTrue")).
                body("triggerType",equalTo(TransactionMachineContent.name())).
                body("destination",equalTo(destinationNonHttps)).
                body("allowNonHttps",equalTo(true)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 5)
    public void post5TransactionMachineContentValidDestinationNonHttpsFalse() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post5TransactionMachineContentValidDestinationNonHttpsFalse", TransactionMachineContent,destinationHttps,false)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post5TransactionMachineContentValidDestinationNonHttpsFalse")).
                body("triggerType",equalTo(TransactionMachineContent.name())).
                body("destination",equalTo(destinationHttps)).
                body("allowNonHttps",equalTo(false)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 6)
    public void post6TransactionMachineContentInvalidDestinationNonHttpsFalse() throws JsonProcessingException {
        given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post6TransactionMachineContentInvalidDestinationNonHttpsFalse", TransactionMachineContent,destinationNonHttps,false)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 7)
    public void post7ErrorValidDestinationNonHttpsTrue() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post7ErrorValidDestinationNonHttpsTrue", Error,destinationNonHttps,true)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post7ErrorValidDestinationNonHttpsTrue")).
                body("triggerType",equalTo(Error.name())).
                body("destination",equalTo(destinationNonHttps)).
                body("allowNonHttps",equalTo(true)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 8)
    public void post8ErrorValidDestinationNonHttpsFalse() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post8ErrorValidDestinationNonHttpsFalse", Error,destinationHttps,false)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post8ErrorValidDestinationNonHttpsFalse")).
                body("triggerType",equalTo(Error.name())).
                body("destination",equalTo(destinationHttps)).
                body("allowNonHttps",equalTo(false)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 9)
    public void post9ErrorInvalidDestinationNonHttpsFalse() throws JsonProcessingException {
        given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post9ErrorInvalidDestinationNonHttpsFalse", WebhookTypes.Error,destinationNonHttps,false)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 10)
    public void post10SystemStatusValidDestinationNonHttpsTrue() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post10SystemStatusValidDestinationNonHttpsTrue", SystemStatus,destinationNonHttps,true)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post10SystemStatusValidDestinationNonHttpsTrue")).
                body("triggerType",equalTo(SystemStatus.name())).
                body("destination",equalTo(destinationNonHttps)).
                body("allowNonHttps",equalTo(true)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 11)
    public void post11SystemStatusValidDestinationNonHttpsFalse() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post11SystemStatusValidDestinationNonHttpsFalse", SystemStatus,destinationHttps,false)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post11SystemStatusValidDestinationNonHttpsFalse")).
                body("triggerType",equalTo(SystemStatus.name())).
                body("destination",equalTo(destinationHttps)).
                body("allowNonHttps",equalTo(false)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 12)
    public void post12SystemStatusInvalidDestinationNonHttpsFalse() throws JsonProcessingException {
        given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post12SystemStatusInvalidDestinationNonHttpsFalse", SystemStatus,destinationNonHttps,false)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 13)
    public void post13MachineOnlineStatusChangeValidDestinationNonHttpsTrue() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post13MachineOnlineStatusChangeValidDestinationNonHttpsTrue", MachineOnlineStatusChange,destinationNonHttps,true)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post13MachineOnlineStatusChangeValidDestinationNonHttpsTrue")).
                body("triggerType",equalTo(MachineOnlineStatusChange.name())).
                body("destination",equalTo(destinationNonHttps)).
                body("allowNonHttps",equalTo(true)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 14)
    public void post14MachineOnlineStatusChangeValidDestinationNonHttpsFalse() throws JsonProcessingException {


        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post14MachineOnlineStatusChangeValidDestinationNonHttpsFalse", MachineOnlineStatusChange,destinationHttps,false)).
            when().
                post(url).
            then().
                statusCode(201).
                body("name",equalTo("ra-post14MachineOnlineStatusChangeValidDestinationNonHttpsFalse")).
                body("triggerType",equalTo(MachineOnlineStatusChange.name())).
                body("destination",equalTo(destinationHttps)).
                body("allowNonHttps",equalTo(false)).
                body("token",notNullValue()).
                log().all().extract().asString();

        webhooks.add(new Gson().fromJson(response, WebHookCCC.class));
    }

    @Test (priority = 15)
    public void post15MachineOnlineStatusChangeInvalidDestinationNonHttpsFalse() throws JsonProcessingException {
        given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post15MachineOnlineStatusChangeInvalidDestinationNonHttpsFalse", MachineOnlineStatusChange,destinationNonHttps,false)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 16)
    public void post16InvalidPayLoadWrongTriggerType() throws JsonProcessingException {
        given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post16InvalidPayLoadWrongTriggerType", triggerTypeInvalid,destinationNonHttps,false)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 17)
    public void post17InvalidPayLoadMissingDestination() throws JsonProcessingException {
        given().
                headers(headers).
                header("Content-type","application/json").
                body(generatePayLoad("ra-post17InvalidPayLoadMissingDestination", Transaction,null,true)).
            when().
                post(url).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 18)
    public void put1EditName(){
        WebHookCCC webHookCCC = webhooks.get(0);
        webHookCCC.name+=" - put1EditName";
        String payload = new Gson().toJson(webHookCCC);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+webHookCCC.token).
            then().
                statusCode(200).
                body("name",equalTo(webHookCCC.name)).log().all();
    }

    @Test (priority = 19)
    public void put2EditTriggerType(){
        WebHookCCC webHookCCC = webhooks.get(0);
        webHookCCC.triggerType=Error.name();
        String payload = new Gson().toJson(webHookCCC);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+webHookCCC.token).
            then().
                statusCode(200).
                body("triggerType",equalTo(Error.name())).log().all();
    }

    @Test (priority = 19)
    public void put3EditDestinationWithAllowNonHttpsTrue(){
        WebHookCCC webHookCCC = webhooks.get(0);
        webHookCCC.destination+="-12";
        String payload = new Gson().toJson(webHookCCC);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+webHookCCC.token).
            then().
                statusCode(200).
                body("destination",equalTo(webHookCCC.destination)).log().all();
    }

    @Test (priority = 19)
    public void put4EditDestinationWithAllowNonHttpsFalse(){
        WebHookCCC webHookCCC = webhooks.get(1);
        webHookCCC.destination+="-12";
        String payload = new Gson().toJson(webHookCCC);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+webHookCCC.token).
            then().
                statusCode(200).
                body("destination",equalTo(webHookCCC.destination)).log().all();
    }

    @Test (priority = 19)
    public void put5EditAllowNonHttpsWhereDestinationIsHttps(){
        WebHookCCC webHookCCC = webhooks.get(3);
        webHookCCC.allowNonHttps=true;
        String payload = new Gson().toJson(webHookCCC);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+webHookCCC.token).
            then().
                statusCode(200).
                body("allowNonHttps",equalTo(webHookCCC.allowNonHttps)).log().all();
    }

    @Test (priority = 19)
    public void put6EditAllowNonFalseNonHttpsDestination(){
        WebHookCCC webHookCCC = webhooks.get(0);
        webHookCCC.allowNonHttps=false;
        String payload = new Gson().toJson(webHookCCC);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+webHookCCC.token).
            then().
                statusCode(400).
                log().all();
    }

    @Test (priority = 20)
    public void getRules1AllRules(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.token", hasItems(webhooks.get(0).token, webhooks.get(5).token,webhooks.get(webhooks.size()-1).token)).
                body("numResults", equalTo(webhooks.size()));
    }

    @Test (priority = 21)
    public void getRules2OnToken(){
        WebHookCCC webHookCCC = webhooks.get(webhooks.size()-1);
        given().
                headers(headers).
            when().
                get(url+"/"+webHookCCC.token).
            then().
                statusCode(200).
                body("token", equalTo(webHookCCC.token)).
                body("name", equalTo(webHookCCC.name)).
                body("triggerType", equalTo(webHookCCC.triggerType)).
                body("destination", equalTo(webHookCCC.destination)).
                body("allowNonHttps", equalTo(webHookCCC.allowNonHttps));
    }

    @Test(priority = 22)
    public void deleteWebhookRules(){

        for (WebHookCCC webHookCCC : webhooks){
            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/"+webHookCCC.token).
                then().
                    statusCode(200);
        }

    }



}
