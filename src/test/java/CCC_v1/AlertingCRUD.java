package CCC_v1;

import cccObjects.Alert;
import cccObjects.ScheduledReport;
import ccodSession.Environments;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class AlertingCRUD {
    Environments env = DefaultStrings.environmentForRestAssured;
    String url = DefaultStrings.getBaseURL(env)+"/api/v1/alerts2";
    Map<String,String> headers;
    List<Alert> alertErrors;
    List<Alert> alertBoxContent;
    List<Alert> alertOnlineStatus;
    List<Alert> alertSystemStatus;
    List<Alert> alertTransactions;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        alertErrors = new LinkedList<Alert>();
        alertBoxContent = new LinkedList<Alert>();
        alertOnlineStatus = new LinkedList<Alert>();
        alertSystemStatus = new LinkedList<Alert>();
        alertTransactions = new LinkedList<Alert>();
    }

    @Test (priority = 1)
    public void getUsers(){
        given().
                headers(headers).
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/users").
            then().
                statusCode(200).
                body("results.email", hasItems("connect@cashcomplete.com", "zts-a-03@gmail.com","nicole.thornber@paycomplete.com"));
    }

    @Test (priority = 2)
    public void getMachinesPaged(){
        given().
                headers(headers).
                queryParam("pageSize","1000").
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/machines").
            then().
                statusCode(200).
                body("results.machineId", hasItems("48f86a59-3e70-4604-895a-0f86b9272ce4", "JM-6892","JM-5911"));

        given().
                headers(headers).
                queryParam("pageSize","1000").
                queryParam("page","2").
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/machines").
            then().
                statusCode(200).
                body("results.machineId", hasItems("JM-5910", "JM-5892","JM-4911"));
    }

    @Test (priority = 3)
    public void getRule(){
        given().
                headers(headers).
                queryParam("pageSize","2000").
            when().
                get(url+"/rule").
            then().
                statusCode(200).
                body("results.token", hasItems("af4b8b90-379a-4070-a47b-3131cf5e6e26", "8e352a6a-22b4-4f7e-99d2-70e67b15ab06","5146a538-6ee4-43ca-bdb0-d455d773a3de"));
    }

    @Test(priority = 4)
    public void postNext1(){
        String payload = "{\"name\":\"\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"\"}]}";

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/next").
            then().
                statusCode(200).
                body("[0].messageTypeFilter", hasItems(
                        "Transaction",
                        "MachineContentChange",
                        "Error",
                        "SystemStatus",
                        "MachineOnlineStatusChange"
                        )).
                body("[0].fromTime",hasItems("18:00")).
                body("[0].type", hasItems(
                        "SiteFilter",
                        "MachineIdFilter",
                        "Or",
                        "And",
                        "ErrorUncleared"));


    }

    @Test(priority = 5)
    public void postNext2(){
        String payload = "{\"name\":\"\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"\"}],\"trigger\":{\"type\":\"MessageType\"}}";

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/next").
            then().
                statusCode(200).
                body("[0].type", hasItems(
                        "TimeOfDay",
                        "SiteFilter",
                        "MachineIdFilter",
                        "Or",
                        "And"));
    }

    @Test(priority = 6)
    public void postRuleError1NoFiltersRecipeintEmail(){
        String payload = "{\"name\":\"ra errors 1\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra errors 1")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("Error")).
                extract().asString();

        alertErrors.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 7)
    public void postRuleError2SitesTimeOfdayErrorKindRecipientSms(){
        String payload = "{\"name\":\"ra errors 2\",\"recipients\":[{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"12:00\",\"toTime\":\"15:00\",\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"ThresholdEmpty\",\"ThresholdFull\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"07377128-107e-43ba-bc4b-554631da6f67\",\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"]}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra errors 2")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("sms")).
                body("trigger.messageTypeFilter", equalTo("Error")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("12:00")).
                body("trigger.child.toTime", equalTo("15:00")).
                body("trigger.child.child.type", equalTo("ErrorKind")).
                body("trigger.child.child.filter", hasItems("ThresholdEmpty","ThresholdFull")).
                body("trigger.child.child.child.type", equalTo("SiteFilter")).
                body("trigger.child.child.child.filter", hasItems("07377128-107e-43ba-bc4b-554631da6f67","f05a4509-506e-40d8-92e7-c852a898fbfb","a7bb05e3-4b96-44f3-897b-0399563da683")).

                extract().asString();

        alertErrors.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 8)
    public void postRuleError3MachinesSeverityClearedManyRecipients(){
        String payload = "{\"name\":\"ra errors 3\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"email\",\"recipient\":\"suzohapp.ta.1@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Warning\"],\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-02\",\"JM-03\",\"JM-04\",\"JM-05\",\"JM-06\",\"JM-07\",\"JM-08\",\"JM-09\"]}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra errors 3")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[1].type", equalTo("sms")).
                body("trigger.messageTypeFilter", equalTo("Error")).
                body("trigger.child.type", equalTo("ErrorSeverity")).
                body("trigger.child.filter", hasItems("Warning")).
                body("trigger.child.child.filter", hasItems("true")).
                body("trigger.child.child.type", equalTo("ErrorCleared")).
                body("trigger.child.child.child.type", equalTo("MachineIdFilter")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09")).
                extract().asString();

        alertErrors.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 9)
    public void postRuleError4LocationInformation(){
        String payload = "{\"name\":\"ra errors 4\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"ServicePrinter\",\"Software\",\"TillDrawer\",\"TransportBox\",\"UPS\",\"Unknown\"],\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"CoinDeposit\",\"Deposit reject\",\"SomeFreeText\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra errors 4")).
                body("trigger.messageTypeFilter", equalTo("Error")).
                body("trigger.child.type", equalTo("ErrorLocation")).
                body("trigger.child.filter", hasItems("ServicePrinter","TillDrawer","Unknown")).
                body("trigger.child.child.type", equalTo("ErrorInformation")).
                body("trigger.child.child.filter", hasItems("CoinDeposit","Deposit reject","SomeFreeText")).
                extract().asString();

        alertErrors.add(new Gson().fromJson(response, Alert.class));

    }

    @Test(priority = 10)
    public void putRuleError1EditNameAddRecipient(){
        Alert alert = alertErrors.get(0);
        alert.name+=" - edit";

        ScheduledReport.Recipient recipient = new ScheduledReport.Recipient();
        recipient.type="email";
        recipient.recipient="suzohapp.ta.1@gmail.com";
        alert.recipients.add(recipient);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra errors 1 - edit")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[1].type", equalTo("email"));
    }

    @Test(priority = 11)
    public void putRuleError2RemoveRecipient(){
        Alert alert = alertErrors.get(0);
        alert.recipients.remove(1);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra errors 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));
    }

    @Test(priority = 12)
    public void putRuleError3AddFilter(){
        Alert alert = alertErrors.get(0);
        alert.trigger.child = new Alert.Trigger();
        alert.trigger.child.type="TimeOfDay";
        alert.trigger.child.typeClass="timeFilter";
        alert.trigger.child.fromTime="10:00";
        alert.trigger.child.toTime="16:00";

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra errors 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("10:00")).
                body("trigger.child.toTime", equalTo("16:00"));

    }

    @Test (priority = 13)
    public void putRuleError4RemoveFilter(){
        Alert alert = alertErrors.get(0);
        alert.trigger.child = null;

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra errors 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));

    }

    @Test(priority = 14)
    public void putRuleError5AddFilterParam(){
        Alert alert = alertErrors.get(2);
        alert.trigger.child.child.child.filter.add("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra errors 3")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09", "JM-100"));
    }

    @Test(priority = 15)
    public void putRuleError6RemoveFilterParam(){
        Alert alert = alertErrors.get(2);
        alert.trigger.child.child.child.filter.remove("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra errors 3")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09"));
    }


    @Test(priority = 16)
    public void postRuleBoxContent1NoFiltersRecipientEmail(){
        String payload = "{\"name\":\"ra boxContent 1\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra boxContent 1")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("MachineContentChange")).
                extract().asString();

        alertBoxContent.add(new Gson().fromJson(response, Alert.class));
    }

    @Test (priority = 17)
    public void postRuleBoxContent2SitesTimeOfDayFilterRecipientSms(){
        String payload = "{\"name\":\"ra boxContent 2\",\"recipients\":[{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"08:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"07377128-107e-43ba-bc4b-554631da6f67\",\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra boxContent 2")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("sms")).
                body("trigger.messageTypeFilter", equalTo("MachineContentChange")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("08:00")).
                body("trigger.child.toTime", equalTo("12:00")).
                body("trigger.child.child.type", equalTo("SiteFilter")).
                body("trigger.child.child.filter", hasItems("07377128-107e-43ba-bc4b-554631da6f67","f05a4509-506e-40d8-92e7-c852a898fbfb","a7bb05e3-4b96-44f3-897b-0399563da683")).

                extract().asString();

        alertBoxContent.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 18)
    public void postRuleboxContent3MachinesContentExceedsDeviceIdManyRecipients(){
        String payload = "{\"name\":\"ra boxContent 3\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"email\",\"recipient\":\"suzohapp.ta.1@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ContentExceeds\",\"totalAmounts\":[{\"amount\":50000,\"currency\":\"EUR\",\"decimals\":2}],\"child\":{\"type\":\"DeviceId\",\"filter\":[\"SMARTVAULT.1\",\"SMARTVAULT.2\",\"TRANSPORTBOX.1\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-02\",\"JM-03\",\"JM-04\",\"JM-05\",\"JM-06\",\"JM-07\",\"JM-08\",\"JM-09\"]}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra boxContent 3")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[1].type", equalTo("sms")).
                body("recipients[2].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[2].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("MachineContentChange")).
                body("trigger.child.type", equalTo("ContentExceeds")).
                body("trigger.child.totalAmounts[0].amount", equalTo(50000)).
                body("trigger.child.totalAmounts[0].decimals", equalTo(2)).
                body("trigger.child.totalAmounts[0].currency", equalTo("EUR")).
                body("trigger.child.child.type", equalTo("DeviceId")).
                body("trigger.child.child.filter", hasItems("SMARTVAULT.1","SMARTVAULT.2","TRANSPORTBOX.1")).
                body("trigger.child.child.child.type", equalTo("MachineIdFilter")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09")).

                extract().asString();

        alertBoxContent.add(new Gson().fromJson(response, Alert.class));
    }

        @Test(priority = 19)
    public void postRuleboxContent4BoxExceedsAnyBoxExceedsRatio(){
        String payload = "{\"name\":\"ra boxContent 4\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"AnyBoxExceeds\",\"totalAmounts\":[{\"amount\":100000,\"currency\":\"EUR\",\"decimals\":2}],\"child\":{\"type\":\"AnyBoxExceedsCountRatio\",\"ratio\":0.5}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra boxContent 4")).
                body("trigger.messageTypeFilter", equalTo("MachineContentChange")).
                body("trigger.child.type", equalTo("AnyBoxExceeds")).
                body("trigger.child.totalAmounts[0].amount", equalTo(100000)).
                body("trigger.child.totalAmounts[0].decimals", equalTo(2)).
                body("trigger.child.totalAmounts[0].currency", equalTo("EUR")).
                body("trigger.child.child.type", equalTo("AnyBoxExceedsCountRatio")).
                body("trigger.child.child.ratio", equalTo(0.5F)).
                extract().asString();

        alertBoxContent.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 20)
    public void putRuleBoxContent1EditNameAddRecipient(){
        Alert alert = alertBoxContent.get(0);
        alert.name+=" - edit";

        ScheduledReport.Recipient recipient = new ScheduledReport.Recipient();
        recipient.type="email";
        recipient.recipient="suzohapp.ta.1@gmail.com";
        alert.recipients.add(recipient);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra boxContent 1 - edit")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[1].type", equalTo("email"));
    }

    @Test(priority = 21)
    public void putRuleBoxContent2RemoveRecipient(){
        Alert alert = alertBoxContent.get(0);
        alert.recipients.remove(1);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                put(url+"/rule/"+alert.token).
                then().
                statusCode(200).
                body("name",equalTo("ra boxContent 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));
    }

    @Test(priority = 22)
    public void putRuleBoxContent3AddFilter(){
        Alert alert = alertBoxContent.get(0);
        alert.trigger.child = new Alert.Trigger();
        alert.trigger.child.type="TimeOfDay";
        alert.trigger.child.typeClass="timeFilter";
        alert.trigger.child.fromTime="10:00";
        alert.trigger.child.toTime="16:00";

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                put(url+"/rule/"+alert.token).
                then().
                statusCode(200).
                body("name",equalTo("ra boxContent 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("10:00")).
                body("trigger.child.toTime", equalTo("16:00"));

    }

    @Test (priority = 23)
    public void putRuleBoxContent4RemoveFilter(){
        Alert alert = alertBoxContent.get(0);
        alert.trigger.child = null;

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra boxContent 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));

    }

    @Test(priority = 24)
    public void putRuleBoxContent5AddFilterParam(){
        Alert alert = alertBoxContent.get(2);
        alert.trigger.child.child.child.filter.add("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra boxContent 3")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09", "JM-100"));
    }

    @Test(priority = 25)
    public void putRuleBoxContent6RemoveFilterParam(){
        Alert alert = alertBoxContent.get(2);
        alert.trigger.child.child.child.filter.remove("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra boxContent 3")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09"));
    }


    @Test(priority = 26)
    public void postRuleOnlineStatus1NoFiltersRecipeintEmail(){
        String payload = "{\"name\":\"ra online status 1\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra online status 1")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("MachineOnlineStatusChange")).
                extract().asString();

        alertOnlineStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 26)
    public void postRuleOnlineStatus2SitesTimeOfDayRecipientSms(){
        String payload = "{\"name\":\"ra online status 2\",\"recipients\":[{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"00:00\",\"toTime\":\"23:00\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"07377128-107e-43ba-bc4b-554631da6f67\",\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra online status 2")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("sms")).
                body("trigger.messageTypeFilter", equalTo("MachineOnlineStatusChange")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("00:00")).
                body("trigger.child.toTime", equalTo("23:00")).
                body("trigger.child.child.type", equalTo("SiteFilter")).
                body("trigger.child.child.filter", hasItems("07377128-107e-43ba-bc4b-554631da6f67","f05a4509-506e-40d8-92e7-c852a898fbfb","a7bb05e3-4b96-44f3-897b-0399563da683")).

                extract().asString();

        alertOnlineStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 27)
    public void postRuleOnlineStatus3MachinesDisconnectedConnectedManyRecipients(){
        String payload = "{\"name\":\"ra online status 3\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"email\",\"recipient\":\"suzohapp.ta.1@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineDisconnected\",\"child\":{\"type\":\"MachineConnected\",\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-02\",\"JM-03\",\"JM-04\",\"JM-05\",\"JM-06\",\"JM-07\",\"JM-08\",\"JM-09\"]}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra online status 3")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[1].type", equalTo("sms")).
                body("recipients[2].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[2].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("MachineOnlineStatusChange")).
                body("trigger.child.type", equalTo("MachineDisconnected")).
                body("trigger.child.child.type", equalTo("MachineConnected")).
                body("trigger.child.child.child.type", equalTo("MachineIdFilter")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-05","JM-09")).

                extract().asString();

        alertOnlineStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 28)
    public void putRuleOnlineStatus1EditNameAddRecipient(){
        Alert alert = alertOnlineStatus.get(0);
        alert.name+=" - edit";

        ScheduledReport.Recipient recipient = new ScheduledReport.Recipient();
        recipient.type="email";
        recipient.recipient="suzohapp.ta.1@gmail.com";
        alert.recipients.add(recipient);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra online status 1 - edit")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[1].type", equalTo("email"));
    }

    @Test(priority = 29)
    public void putRuleOnlineStatus2RemoveRecipient(){
        Alert alert = alertOnlineStatus.get(0);
        alert.recipients.remove(1);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra online status 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));
    }

    @Test(priority = 30)
    public void putRuleOnlineStatus3AddFilter(){
        Alert alert = alertOnlineStatus.get(0);
        alert.trigger.child = new Alert.Trigger();
        alert.trigger.child.type="TimeOfDay";
        alert.trigger.child.typeClass="timeFilter";
        alert.trigger.child.fromTime="10:00";
        alert.trigger.child.toTime="16:00";

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra online status 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("10:00")).
                body("trigger.child.toTime", equalTo("16:00"));

    }

    @Test (priority = 31)
    public void putRuleOnlineStatus4RemoveFilter(){
        Alert alert = alertOnlineStatus.get(0);
        alert.trigger.child = null;

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra online status 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));

    }

    @Test(priority = 32)
    public void putRuleOnlineStatus5AddFilterParam(){
        Alert alert = alertOnlineStatus.get(2);
        alert.trigger.child.child.child.filter.add("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra online status 3")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09", "JM-100"));
    }

    @Test(priority = 33)
    public void putRuleOnlineStatus6RemoveFilterParam(){
        Alert alert = alertOnlineStatus.get(2);
        alert.trigger.child.child.child.filter.remove("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra online status 3")).
                body("trigger.child.child.child.filter", hasItems("JM-01","JM-02","JM-09"));
    }


    @Test(priority = 34)
    public void postRuleSystemStatus1NoFiltersRecipientEmail(){
        String payload = "{\"name\":\"ra system status 1\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra system status 1")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("SystemStatus")).
                extract().asString();

        alertSystemStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 34)
    public void postRuleSystemStatus2SitesTimeOfDayMessageStatusRecipientSms(){
        String payload = "{\"name\":\"ra system status 2\",\"recipients\":[{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"12:00\",\"toTime\":\"18:00\",\"child\":{\"type\":\"MessageStatus\",\"filter\":[\"Boot\",\"Closed\",\"Disconnected\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"07377128-107e-43ba-bc4b-554631da6f67\",\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"]}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("sms")).
                body("trigger.messageTypeFilter", equalTo("SystemStatus")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("12:00")).
                body("trigger.child.toTime", equalTo("18:00")).
                body("trigger.child.child.type", equalTo("MessageStatus")).
                body("trigger.child.child.filter", hasItems("Boot","Closed","Disconnected")).
                body("trigger.child.child.child.type", equalTo("SiteFilter")).
                body("trigger.child.child.child.filter", hasItems("07377128-107e-43ba-bc4b-554631da6f67","f05a4509-506e-40d8-92e7-c852a898fbfb","a7bb05e3-4b96-44f3-897b-0399563da683")).
                extract().asString();

        alertSystemStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 34)
    public void postRuleSystemStatus3MachinesMessageOldStatusManyRecipients(){
        String payload = "{\"name\":\"ra system status 3\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"email\",\"recipient\":\"suzohapp.ta.1@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MessageOldStatus\",\"filter\":[\"Boot\",\"Idle\",\"User\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-02\",\"JM-03\",\"JM-04\",\"JM-05\",\"JM-06\",\"JM-07\",\"JM-08\",\"JM-09\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[1].type", equalTo("sms")).
                body("recipients[2].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[2].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("SystemStatus")).
                body("trigger.child.type", equalTo("MessageOldStatus")).
                body("trigger.child.filter", hasItems("Boot","Idle","User")).
                body("trigger.child.child.type", equalTo("MachineIdFilter")).
                body("trigger.child.child.filter", hasItems("JM-01","JM-06","JM-09")).
                extract().asString();

        alertSystemStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 35)
    public void postRuleSystemStatus4DoorsOpenClosed(){
        String payload = "{\"name\":\"ra system status 4\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DoorOpen\",\"filter\":[\"COIN_ACCEPTOR\",\"TILL_BOX_SINGLE_TRAY\",\"TRANSPORTBOX_DRAWER\"],\"child\":{\"type\":\"DoorClosed\",\"filter\":[\"TILL_BOX\",\"TILL_BOX_SINGLE_TRAY\",\"TRANSPORTBOX\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("trigger.messageTypeFilter", equalTo("SystemStatus")).
                body("trigger.child.type", equalTo("DoorOpen")).
                body("trigger.child.filter", hasItems("COIN_ACCEPTOR","TILL_BOX_SINGLE_TRAY","TRANSPORTBOX_DRAWER")).
                body("trigger.child.child.type", equalTo("DoorClosed")).
                body("trigger.child.child.filter", hasItems("TILL_BOX","TILL_BOX_SINGLE_TRAY","TRANSPORTBOX")).
                extract().asString();

        alertSystemStatus.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 36)
    public void postRuleTransaction1NoFiltersRecipeintEmail(){
        String payload = "{\"name\":\"ra transactions 1\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra transactions 1")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("Transaction")).
                extract().asString();

        alertTransactions.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 37)
    public void postRuleTransaction2SitesTimeOfDayRecipientSms(){
        String payload = "{\"name\":\"ra transactions 2\",\"recipients\":[{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"01:00\",\"toTime\":\"08:00\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"07377128-107e-43ba-bc4b-554631da6f67\",\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra transactions 2")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("sms")).
                body("trigger.messageTypeFilter", equalTo("Transaction")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("01:00")).
                body("trigger.child.toTime", equalTo("08:00")).
                body("trigger.child.child.type", equalTo("SiteFilter")).
                body("trigger.child.child.filter", hasItems("07377128-107e-43ba-bc4b-554631da6f67","f05a4509-506e-40d8-92e7-c852a898fbfb","a7bb05e3-4b96-44f3-897b-0399563da683")).
                extract().asString();

        alertTransactions.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 38)
    public void postRuleTransaction3MachinesTransactionTypeManyRecipients(){
        String payload = "{\"name\":\"ra transactions 3\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"sms\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}},{\"type\":\"email\",\"recipient\":\"suzohapp.ta.1@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionType\",\"filter\":[\"DEPOSIT\",\"DISPENSE\",\"EMPTY_OUT\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-02\",\"JM-03\",\"JM-04\",\"JM-05\",\"JM-06\",\"JM-07\",\"JM-08\",\"JM-09\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra transactions 3")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[1].type", equalTo("sms")).
                body("recipients[2].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[2].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("Transaction")).
                body("trigger.child.type", equalTo("TransactionType")).
                body("trigger.child.filter", hasItems("DEPOSIT","DISPENSE","EMPTY_OUT")).
                body("trigger.child.child.type", equalTo("MachineIdFilter")).
                body("trigger.child.child.filter", hasItems("JM-01","JM-04","JM-09")).
                extract().asString();

        alertTransactions.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 38)
    public void postRuleTransaction4TransactionSubTypeTotalAmountExceeds(){
        String payload = "{\"name\":\"ra transactions 4\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionSubType\",\"filter\":[\"CHANGE\",\"EXPECTED_AMOUNT\",\"REQUESTED_AMOUNT\"],\"child\":{\"type\":\"TransactionTotalAmountExceeds\",\"totalAmounts\":[{\"amount\":50000,\"currency\":\"EUR\",\"decimals\":2}]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra transactions 4")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("Transaction")).
                body("trigger.child.type", equalTo("TransactionSubType")).
                body("trigger.child.filter", hasItems("CHANGE","EXPECTED_AMOUNT","REQUESTED_AMOUNT")).
                body("trigger.child.child.type", equalTo("TransactionTotalAmountExceeds")).
                body("trigger.child.child.totalAmounts[0].amount", equalTo(50000)).
                body("trigger.child.child.totalAmounts[0].decimals", equalTo(2)).
                body("trigger.child.child.totalAmounts[0].currency", equalTo("EUR")).
                extract().asString();

        alertTransactions.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 39)
    public void postRuleTransaction5BoxGoesBelowAboveCounts(){
        String payload = "{\"name\":\"ra transactions 5\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohapp.ta.2@gmail.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"BoxGoesBelowCount\",\"count\":\"100\",\"child\":{\"type\":\"BoxGoesAboveCount\",\"count\":\"800\"}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                body("name",equalTo("ra transactions 5")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.messageTypeFilter", equalTo("Transaction")).
                body("trigger.child.type", equalTo("BoxGoesBelowCount")).
                body("trigger.child.count", equalTo(100)).
                body("trigger.child.child.type", equalTo("BoxGoesAboveCount")).
                body("trigger.child.child.count", equalTo(800)).
                extract().asString();

        alertTransactions.add(new Gson().fromJson(response, Alert.class));
    }

    @Test(priority = 40)
    public void putRuleTransaction1EditNameAddRecipient(){
        Alert alert = alertTransactions.get(0);
        alert.name+=" - edit";

        ScheduledReport.Recipient recipient = new ScheduledReport.Recipient();
        recipient.type="email";
        recipient.recipient="suzohapp.ta.1@gmail.com";
        alert.recipients.add(recipient);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra transactions 1 - edit")).
                body("recipients[1].recipient", equalTo("suzohapp.ta.1@gmail.com")).
                body("recipients[1].type", equalTo("email"));
    }

    @Test(priority = 41)
    public void putRuleTransaction2RemoveRecipient(){
        Alert alert = alertTransactions.get(0);
        alert.recipients.remove(1);

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra transactions 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));
    }

    @Test(priority = 42)
    public void putRuleTransaction3AddFilter(){
        Alert alert = alertTransactions.get(0);
        alert.trigger.child = new Alert.Trigger();
        alert.trigger.child.type="TimeOfDay";
        alert.trigger.child.typeClass="timeFilter";
        alert.trigger.child.fromTime="10:00";
        alert.trigger.child.toTime="16:00";

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra transactions 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email")).
                body("trigger.child.type", equalTo("TimeOfDay")).
                body("trigger.child.fromTime", equalTo("10:00")).
                body("trigger.child.toTime", equalTo("16:00"));

    }

    @Test (priority = 43)
    public void putRuleTransaction4RemoveFilter(){
        Alert alert = alertTransactions.get(0);
        alert.trigger.child = null;

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra transactions 1 - edit")).
                body("recipients[0].recipient", equalTo("suzohapp.ta.2@gmail.com")).
                body("recipients[0].type", equalTo("email"));

    }

    @Test(priority = 44)
    public void putRuleTransaction5AddFilterParam(){
        Alert alert = alertTransactions.get(2);
        alert.trigger.child.child.filter.add("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/rule/"+alert.token).
            then().
                statusCode(200).
                body("name",equalTo("ra transactions 3")).
                body("trigger.child.child.filter", hasItems("JM-01","JM-02","JM-09", "JM-100"));
    }

    @Test(priority = 45)
    public void putRuleTransaction6RemoveFilterParam(){
        Alert alert = alertTransactions.get(2);
        alert.trigger.child.child.filter.remove("JM-100");

        String payload = new Gson().toJson(alert);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                put(url+"/rule/"+alert.token).
                then().
                statusCode(200).
                body("name",equalTo("ra transactions 3")).
                body("trigger.child.child.filter", hasItems("JM-01","JM-02","JM-09"));
    }

    @Test(priority = 1000)
    public void deleteAlertRules(){
        List<Alert> list = Stream.of(alertErrors,alertBoxContent,alertOnlineStatus,alertSystemStatus,alertTransactions)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        for (Alert alert : list){
            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/rule/"+alert.token).
                then().
                    statusCode(200);
        }

    }

}
