package CCC_v1;

import cccObjects.UserGroup;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class SmartUserGroup {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/machineusers/groups";
    Map<String,String> headers;
    List<UserGroup> groups;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        groups = new LinkedList<UserGroup>();
    }

    @Test (priority = 1)
    public void getMachineUsers(){
        given().
                headers(headers).
            when().
                get(DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/machineusers?includePin=true").
            then().
                statusCode(200).
                body("results.name", hasItems("Annamae Abernathy", "Chad Williamson","Cheyanne Kulas"));
    }

    @Test (priority = 2)
    public void getSmartUserGroup(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.name", hasItems("dls 01", "dls 02","large-02"));
    }

    @Test(priority = 3)
    public void postUserGroupEmpty(){
        String payload ="{\"name\":\"ra-t1\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t1")).
                extract().asString();

        groups.add(new Gson().fromJson(response, UserGroup.class));
    }

    @Test (priority = 4)
    public void postUserGroup1User(){
        String payload="{\"name\":\"ra-t2\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"assignedUsers\":[\"7000\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t2")).
                body("assignedUsers", hasItems("7000")).
                extract().asString();

        groups.add(new Gson().fromJson(response, UserGroup.class));
    }

    @Test (priority = 5)
    public void postUserGroupManyUsers(){
        String payload="{\"name\":\"ra-t3\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"assignedUsers\":[\"7000\",\"7002\",\"7001\",\"7004\",\"7003\",\"7006\",\"7005\",\"7008\",\"7007\",\"7009\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t3")).
                body("assignedUsers", hasItems("7000","7009","7005")).
                extract().asString();

        groups.add(new Gson().fromJson(response, UserGroup.class));
    }

    @Test(priority = 6)
    public void postUserGroup1Machine(){
        String payload="{\"name\":\"ra-t4\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"assignedMachines\":[\"JM-01\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t4")).
                body("assignedMachines", hasItems("JM-01")).
                extract().asString();

        groups.add(new Gson().fromJson(response, UserGroup.class));
    }

    @Test(priority = 7)
    public void postUserGroupManyMachines(){
        String payload="{\"name\":\"ra-t5\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"assignedMachines\":[\"JM-09\",\"JM-08\",\"JM-07\",\"JM-06\",\"JM-05\",\"JM-04\",\"JM-03\",\"JM-02\",\"JM-01\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t5")).
                body("assignedMachines", hasItems("JM-01","JM-09","JM-05")).
                extract().asString();

        groups.add(new Gson().fromJson(response, UserGroup.class));
    }

    @Test(priority = 8)
    public void postUserGroupUsersAndMachines(){
        String payload="{\"name\":\"ra-t6\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"assignedUsers\":[\"7000\",\"7002\",\"7001\",\"7004\",\"7003\",\"7006\",\"7005\",\"7008\",\"7007\",\"7009\"],\"assignedMachines\":[\"JM-09\",\"JM-08\",\"JM-07\",\"JM-06\",\"JM-05\",\"JM-04\",\"JM-03\",\"JM-02\",\"JM-01\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t6")).
                body("assignedMachines", hasItems("JM-01","JM-09","JM-05")).
                body("assignedUsers", hasItems("7000","7009","7004")).
                extract().asString();

        groups.add(new Gson().fromJson(response, UserGroup.class));
    }

    @Test (priority = 9)
    public void putUserGroupAddUser(){
        UserGroup u  = groups.get(0);
        u.assignedUsers = new LinkedList<String>();
        u.assignedUsers.add("7000");
        String payload = new Gson().toJson(u);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+u.token).
            then().
                statusCode(200).
                body("assignedUsers", hasItems("7000"));
    }

    @Test (priority = 10)
    public void putUserGroupAddMachine(){
        UserGroup u  = groups.get(0);
        u.assignedMachines = new LinkedList<String>();
        u.assignedMachines.add("JM-01");
        String payload = new Gson().toJson(u);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+u.token).
            then().
                statusCode(200).
                body("assignedMachines", hasItems("JM-01"));
    }

    @Test (priority = 11)
    public void putUserGroupAddUserAndMachine(){
        UserGroup u  = groups.get(0);
        u.assignedUsers.add("7001");
        u.assignedMachines.add("JM-02");
        String payload = new Gson().toJson(u);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+u.token).
            then().
                statusCode(200).
                body("assignedUsers", hasItems("7000", "7001")).
                body("assignedMachines", hasItems("JM-01", "JM-02"));
    }

    @Test(priority = 12)
    public void putUserGroupRemoveUser(){
        UserGroup u  = groups.get(0);
        u.assignedUsers.remove("7001");
        String payload = new Gson().toJson(u);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+u.token).
            then().
                statusCode(200).
                body("assignedUsers", hasItems("7000")).
                body("assignedMachines", hasItems("JM-01", "JM-02"));
    }

    @Test(priority = 13)
    public void putUserGroupRemoveMachine(){
        UserGroup u  = groups.get(0);
        u.assignedMachines.remove("JM-02");
        String payload = new Gson().toJson(u);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+u.token).
            then().
                statusCode(200).
                body("assignedUsers", hasItems("7000")).
                body("assignedMachines", hasItems("JM-01"));
    }

    @Test(priority = 100)
    public void deleteUserGroup(){
        for (UserGroup u : groups){

            given().
                    headers(headers).
                    header("Content-type","application/json").

                when().
                    delete(url+"/"+u.token).
                then().
                    statusCode(200);
        }
    }
}
