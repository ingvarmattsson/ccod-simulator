package CCC_v1;

import cccObjects.SiteCCC;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class Sites {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/sites";
    Map<String,String> headers;
    List<SiteCCC> sites;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        sites = new LinkedList<SiteCCC>();
    }

    @Test
    public void getSites(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.name", hasItems("B Box Site", "Site B","VM Site C"));
    }

    @Test
    public void getSitesIncludeAssignments(){
        given().
                headers(headers).
                queryParam("includeDeleted",false).
                queryParam("includeAssignments",true).
                queryParam("pageSize",1000).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.name", hasItems("B Box Site", "Site B","VM Site C")).log().all();
    }

    @Test(priority = 1)
    public void postSite1requiredFields() throws JsonProcessingException {

        String payload = "{\"map\":{\"type\":\"openstreetmap\",\"metadata\":{}},\"metadata\":{\"customerReference\":\"\"},\"name\":\"ra-t1\",\"imageUrl\":\"https://connect.cashcomplete.com/assets/img/favicon/favicon.ico\",\"description\":\"\"}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(200).
                body("name", equalTo("ra-t1")).
                extract().asString();

        sites.add(new Gson().fromJson(response, SiteCCC.class));
    }

    @Test(priority = 2)
    public void postSite2optionalFields(){

        String payload = "{\"map\":{\"type\":\"openstreetmap\",\"metadata\":{\"zoomLevel\":7,\"centerLatitude\":55.591151406351784,\"centerLongitude\":12.972793579101562}},\"metadata\":{\"customerReference\":\"ra-ta-ref:01\",\"city\":\"R10663667\",\"country\":\"se\"},\"name\":\"ra-t2\",\"description\":\"Automated by restassured\",\"imageUrl\":\"https://connect.cashcomplete.com/assets/img/favicon/favicon.ico\"}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(200).
                body("name", equalTo("ra-t2"))
                .extract().asString();

        sites.add(new Gson().fromJson(response, SiteCCC.class));

    }

    @Test(dependsOnMethods = {"postSite1requiredFields"},priority = 3)
    public void putSite1(){
        SiteCCC s = sites.get(0);
        s.name+=" - edit";
        s.description+=" - edit";
        String siteToken = s.token;

        String payload = new Gson().toJson(s);

       given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+siteToken).
            then().
                statusCode(200);
    }

    @Test(dependsOnMethods = {"postSite1requiredFields","postSite2optionalFields"},priority = 4)
    public void deleteSite(){
        for (SiteCCC s : sites){

            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/"+s.token).
                then().
                    statusCode(200);

        }
    }

}
