package CCC_v1;

import cccObjects.POSTransaction;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class POSTransactions {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/pointofsale/cashier/zreports";
    Map<String,String> headers;
    List<POSTransaction> posTransactions;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        posTransactions = new LinkedList<POSTransaction>();
    }

    @Test (priority = 1)
    public void getPOSTransactions1SmallResonse(){
        given().
                headers(headers).
                queryParam("pageSize","1000").
                queryParam("fromDate","2021-01-01T00:00:00Z").
                queryParam("toDate","2021-05-19T00:00:00Z").
                queryParam("force","true").
            when().
                get(url).
            then().
                statusCode(200).
                body("numResults",equalTo(811)).
                body("results[0].token",equalTo("0000000-0001-2021-01-03T145152Z")).
                body("results[0].machineId",equalTo("JM-01")).
                body("results[0].cashTotals[0].amount",equalTo(5729)).
                body("results[0].cashTotals[0].currency",equalTo("USD")).
                body("results[0].cashTotals[0].decimals",equalTo(2)).
                body("results[0].cashTotals[0].type",equalTo("TOTAL")).
                body("results[0].cashValues[0].amount",equalTo(7615)).
                body("results[0].cashValues[0].currency",equalTo("USD")).
                body("results[0].cashValues[0].decimals",equalTo(2)).
                body("results[0].cashValues[0].type",equalTo("CASH_SALES")).
                body("results[0].user.userName",equalTo("Tobias Ekholm")).

                body("results[500].token",equalTo("0000006-0002 -2021-01-20T163507Z")).
                body("results[500].machineId",equalTo("JM-01")).
                body("results[500].cashTotals[0].amount",equalTo(27421)).
                body("results[500].cashTotals[0].currency",equalTo("EUR")).
                body("results[500].cashTotals[0].decimals",equalTo(2)).
                body("results[500].cashTotals[0].type",equalTo("TOTAL")).
                body("results[500].cashValues[0].amount",equalTo(39692)).
                body("results[500].cashValues[0].currency",equalTo("EUR")).
                body("results[500].cashValues[0].decimals",equalTo(2)).
                body("results[500].cashValues[0].type",equalTo("CASH_SALES")).
                body("results[500].user.userName",equalTo("Uwe E. Sanders")).

                body("results[810].token",equalTo("0000009-0005-2021-03-28T154649Z")).
                body("results[810].machineId",equalTo("JM-01")).
                body("results[810].cashTotals[0].amount",equalTo(44461)).
                body("results[810].cashTotals[0].currency",equalTo("EUR")).
                body("results[810].cashTotals[0].decimals",equalTo(2)).
                body("results[810].cashTotals[0].type",equalTo("TOTAL")).
                body("results[810].cashValues[0].amount",equalTo(53709)).
                body("results[810].cashValues[0].currency",equalTo("EUR")).
                body("results[810].cashValues[0].decimals",equalTo(2)).
                body("results[810].cashValues[0].type",equalTo("CASH_SALES")).
                body("results[810].user.userName",equalTo("Tina T. Wood"));
    }

    @Test (priority = 2)
    public void getPOSTransactions2Paged(){
        given().
                headers(headers).
                queryParam("pageSize","1000").
                queryParam("fromDate","2020-09-01T00:00:00Z").
                queryParam("toDate","2021-05-19T00:00:00Z").
                queryParam("force","true").
            when().
                get(url).
            then().
                statusCode(200).
                body("numResults",equalTo(1037));

        given().
                headers(headers).
                queryParam("pageSize","1000").
                queryParam("fromDate","2020-09-01T00:00:00Z").
                queryParam("toDate","2021-05-19T00:00:00Z").
                queryParam("force","true").
                queryParam("page","2").
            when().
                get(url).
            then().
                statusCode(200).
                body("numResults",equalTo(1037)).
                body("results[0].token",equalTo("0000000-0002-2021-03-30T150642Z")).
                body("results[0].machineId",equalTo("JM-01")).
                body("results[0].cashTotals[0].amount",equalTo(9278)).
                body("results[0].cashTotals[0].currency",equalTo("EUR")).
                body("results[0].cashTotals[0].decimals",equalTo(2)).
                body("results[0].cashTotals[0].type",equalTo("TOTAL")).
                body("results[0].cashValues[0].amount",equalTo(11090)).
                body("results[0].cashValues[0].currency",equalTo("EUR")).
                body("results[0].cashValues[0].decimals",equalTo(2)).
                body("results[0].cashValues[0].type",equalTo("CASH_SALES")).
                body("results[0].user.userName",equalTo("Tobias Ekholm")).

                body("results[18].token",equalTo("0000005-0002-2021-03-29T164532Z")).
                body("results[18].machineId",equalTo("JM-01")).
                body("results[18].cashTotals[0].amount",equalTo(19990)).
                body("results[18].cashTotals[0].currency",equalTo("EUR")).
                body("results[18].cashTotals[0].decimals",equalTo(2)).
                body("results[18].cashTotals[0].type",equalTo("TOTAL")).
                body("results[18].cashValues[0].amount",equalTo(27511)).
                body("results[18].cashValues[0].currency",equalTo("EUR")).
                body("results[18].cashValues[0].decimals",equalTo(2)).
                body("results[18].cashValues[0].type",equalTo("CASH_SALES")).
                body("results[18].user.userName",equalTo("Laura D. Isengard")).

                body("results[36].token",equalTo("0000009-0005-2021-03-28T154649Z")).
                body("results[36].machineId",equalTo("JM-01")).
                body("results[36].cashTotals[0].amount",equalTo(44461)).
                body("results[36].cashTotals[0].currency",equalTo("EUR")).
                body("results[36].cashTotals[0].decimals",equalTo(2)).
                body("results[36].cashTotals[0].type",equalTo("TOTAL")).
                body("results[36].cashValues[0].amount",equalTo(53709)).
                body("results[36].cashValues[0].currency",equalTo("EUR")).
                body("results[36].cashValues[0].decimals",equalTo(2)).
                body("results[36].cashValues[0].type",equalTo("CASH_SALES")).
                body("results[36].user.userName",equalTo("Tina T. Wood"));
    }

    @Test(priority = 3)
    public void getPOSTransactions3LargeResponsePaged10Pages(){

        for(int page = 1; page<=10; page++){
            given().
                    headers(headers).
                    queryParam("pageSize","1000").
                    queryParam("fromDate","2020-01-01T00:00:00Z").
                    queryParam("toDate","2021-05-19T00:00:00Z").
                    queryParam("force","true").
                    queryParam("page", Integer.toString(page)).
                when().
                    get(url).
                then().
                    statusCode(200).
                    body("numResults",equalTo(128707));
        }
    }

    @Test(priority = 4)
    public void getPosTransactionDetails(){
        given().
                headers(headers).
            when().
                get(url+"/0000012-0002-2019-07-01T112614Z").
            then().
                statusCode(200).
                body("accountingDate",equalTo("2020-09-07T00:00:00Z")).
                body("cashTotals[0].amount",equalTo(27068)).
                body("cashValues[0].amount",equalTo(29192)).
                body("cashValues[1].amount",equalTo(-2124)).
                body("machineId",equalTo("JM-20")).
                body("outlet",equalTo("The store")).
                body("user.userId",equalTo("0000012"));

        String response = given().
                headers(headers).
            when().
                get(url+"/0000000-0002-2021-04-19T160916Z").
            then().
                statusCode(200).
                extract().asString();

        posTransactions.add(new Gson().fromJson(response, POSTransaction.class));
        System.out.println();

    }

}
