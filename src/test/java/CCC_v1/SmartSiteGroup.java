package CCC_v1;

import cccObjects.SiteGroup;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class SmartSiteGroup {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/sitegroups";
    Map<String,String> headers;
    List<SiteGroup> siteGroups;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        siteGroups = new LinkedList<SiteGroup>();
    }

    @Test
    public void getSiteGroup(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.name", hasItems("B Boxes and Safes SG - 01", "JM SG - 03","VM SG - 01 "));
    }

    @Test(priority = 1)
    public void postSiteGroup1GroupOnOneSite(){

        String payload="{\"name\":\"ra-t1\",\"sites\":[\"f05a4509-506e-40d8-92e7-c852a898fbfb\"],\"cities\":[],\"countries\":[],\"authorizedUsers\":[]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t1")).
                body("sites[0]",equalTo("f05a4509-506e-40d8-92e7-c852a898fbfb")).
                extract().asString();

        siteGroups.add(new Gson().fromJson(response, SiteGroup.class));
    }

    @Test(priority = 2)
    public void postSiteGroup2GroupOnManySites(){

        String payload="{\"name\":\"ra-t2\",\"sites\":[\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"07377128-107e-43ba-bc4b-554631da6f67\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"],\"cities\":[],\"countries\":[],\"authorizedUsers\":[]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t2")).
                body("sites[0]",equalTo("f05a4509-506e-40d8-92e7-c852a898fbfb")).
                body("sites[1]",equalTo("07377128-107e-43ba-bc4b-554631da6f67")).
                body("sites[2]",equalTo("a7bb05e3-4b96-44f3-897b-0399563da683")).
                extract().asString();

        siteGroups.add(new Gson().fromJson(response, SiteGroup.class));
    }

    @Test(priority = 3)
    public void postSiteGroup3GroupOnCountry(){

        String payload="{\"name\":\"ra-t3\",\"sites\":[],\"cities\":[],\"countries\":[\"de\"],\"authorizedUsers\":[]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t3")).
                body("countries[0]",equalTo("de")).
                extract().asString();

        siteGroups.add(new Gson().fromJson(response, SiteGroup.class));
    }

    @Test(priority = 4)
    public void postSiteGroup4GroupOnCity(){

        String payload="{\"name\":\"ra-t4\",\"sites\":[],\"cities\":[\"N240109189\",\"N20833623\"],\"countries\":[],\"authorizedUsers\":[]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t4")).
                body("cities[0]",equalTo("N240109189")).
                body("cities[1]",equalTo("N20833623")).
                extract().asString();

        siteGroups.add(new Gson().fromJson(response, SiteGroup.class));
    }

    @Test(priority = 5)
    public void postSiteGroup5GroupOnAll(){

        String payload="{\"name\":\"ra-t5\",\"sites\":[\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"07377128-107e-43ba-bc4b-554631da6f67\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"],\"cities\":[\"N15412058\"],\"countries\":[\"de\"],\"authorizedUsers\":[]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t5")).
                body("cities[0]",equalTo("N15412058")).
                body("countries[0]",equalTo("de")).
                body("sites[0]",equalTo("f05a4509-506e-40d8-92e7-c852a898fbfb")).
                body("sites[1]",equalTo("07377128-107e-43ba-bc4b-554631da6f67")).
                body("sites[2]",equalTo("a7bb05e3-4b96-44f3-897b-0399563da683")).
                extract().asString();

        siteGroups.add(new Gson().fromJson(response, SiteGroup.class));
    }

    @Test(priority = 6)
    public void postSiteGroup6AuthorizeUsers(){
        String payload="{\"name\":\"ra-t6\",\"sites\":[\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"07377128-107e-43ba-bc4b-554631da6f67\",\"a7bb05e3-4b96-44f3-897b-0399563da683\"],\"cities\":[\"N15412058\"],\"countries\":[\"de\"],\"authorizedUsers\":[\"suzohapp.ta.1@gmail.com\",\"suzohapp.ta.2@gmail.com\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("name", equalTo("ra-t6")).
                body("cities[0]",equalTo("N15412058")).
                body("countries[0]",equalTo("de")).
                body("sites[0]",equalTo("f05a4509-506e-40d8-92e7-c852a898fbfb")).
                body("sites[1]",equalTo("07377128-107e-43ba-bc4b-554631da6f67")).
                body("sites[2]",equalTo("a7bb05e3-4b96-44f3-897b-0399563da683")).
                extract().asString();

        siteGroups.add(new Gson().fromJson(response, SiteGroup.class));
    }

    @Test(priority = 7,dependsOnMethods = "postSiteGroup1GroupOnOneSite")
    public void putSiteGroup1EditName(){
        SiteGroup s = siteGroups.get(0);
        s.name +=" - edit";
        String payload= new Gson().toJson(s);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+s.token).
            then().
                statusCode(200).
                body("name", equalTo("ra-t1 - edit"));
    }

    @Test(priority = 100)
    public void deleteSiteGroup(){
        for(SiteGroup s : siteGroups){
            given().
                    headers(headers).
                    header("Content-type","application/json").
                when().
                    delete(url+"/"+s.token).
                then().
                    statusCode(200);
        }

    }


}
