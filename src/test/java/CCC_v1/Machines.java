package CCC_v1;

import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class Machines {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/";
    Map<String,String> headers;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

    }

    @Test (priority = 1)
    public void getMachineUserGroups(){
        given().
                headers(headers).
            when().
                get(url+"machineusers/groups/").
            then().
                statusCode(200).
                body("results.name", hasItems("large-02", "dls 01","dls_la_group7XXX"));
    }

    @Test (priority = 2)
    public void getMachineUsers(){
        given().
                headers(headers).
            when().
                get(url+"machineusers").
            then().
                statusCode(200).
                body("results.name", hasItems("Annamae Abernathy", "Queenie Osinski","Cheyanne Kulas"));
    }

    @Test (priority = 3)
    public void getRemoteConfigurationTemplate(){
        given().
                headers(headers).
            when().
                get(url+"remoteconfiguration/template").
            then().
                statusCode(200).
                body("results.name", hasItems("Dark - 01", "ddl config - 01","default"));
    }

    @Test (priority = 4)
    public void getRemoteSoftwareUpgradePackages(){
        given().
                headers(headers).
            when().
                get(url+"remotesoftwareupgrade/packages").
            then().
                statusCode(200).
                body("name", hasItems("RCS-800-01", "CDS-02","RCS-800-01"));

    }

    @Test(priority = 5)
    public void getRemoteConfigurationDeploy(){
        given().
                headers(headers).
            when().
                get(url+"remoteconfiguration/deploy").
            then().
                statusCode(200).
                body("results.statusChangedDate", hasItems("2020-12-10T15:18:56.934079Z", "2021-02-11T15:03:32.598607Z","2021-02-11T15:03:32.758047Z"));
    }

    @Test(priority = 6)
    public void getSpecificationsWithQueryParams(){
        given().
                headers(headers).
                queryParam("sortField","name").
                queryParam("sortOrder","asc").
                queryParam("pageSize","1000").
            when().
                get(url+"specifications").
            then().
                statusCode(200).
                body("results.name", hasItems("B Box", "RCS 400","tomasspecification"));
    }

    @Test (priority = 7)
    public void getSites(){
        given().
                headers(headers).
                queryParam("sortField","name").
                queryParam("sortOrder","asc").
                queryParam("pageSize","1000").
            when().
                get(url+"sites").
            then().
                statusCode(200).
                body("results.name", hasItems("B Box Site", "VM Site C","testEdit"));

    }

    @Test (priority = 8)
    public void getLicenseIds(){
        given().
                headers(headers).
            when().
                get(url+"machines/licenseIds").
            then().
                statusCode(200).
                body("0bd1c809-1201-4497-8261-5ea969430cef", equalTo("qLSs8Cn97NvWtcy0Vm13JwQrOTmT+tl/EsjKOWUAX64=")).
                body("699bc792-1141-42f1-84cc-90b107d5bb1b",equalTo("pIlFUjHoyUyFNMR2g1LoOBceo1ARlHUosFEcOKOozq8=")).
                body("ffc7abea-c789-4a88-ba99-305395a95927",equalTo("CDdoUU0qTT0ysoai51TW/9pEcitr9+axRXUcgUQLK8A="));
    }

//    @Test(priority = 9)
//    public void getOnboardingDevices(){
//        given().
//                headers(headers).
//            when().
//                get("https://test-api.cashcomplete.com/api/v1/onboarding/devices").
//            then().
//                statusCode(200).log().all();
//    }

    @Test(priority = 10)
    public void getCCoDLicense(){
        given().
                headers(headers).
            when().
                get(url+"ccodlicense").
            then().
                statusCode(200).
                body("results.licenseId", hasItems("qLSs8Cn97NvWtcy0Vm13JwQrOTmT+tl/EsjKOWUAX64=", "a5o+qQ5CvrddsXE+CryAIv4yNfHZRdxxteMddqon/QU=","1W4l1AhznaQQceycmbjYwF44zxSyULOqiK6olCIYOAc="));
    }

    @Test(priority = 11)
    public void getDevices(){
        given().
                headers(headers).
                queryParam("includeSpecification","true").
                queryParam("includeAssignment","true").
                queryParam("includeSite","true").
                queryParam("pageSize","100").
                queryParam("page","1").
            when().
                get(url+"devices").
            then().
                statusCode(200).
                body("results.hardwareId", hasItems("48f86a59-3e70-4604-895a-0f86b9272ce4","JM-6891","JM-6811"));
    }

    @Test (priority = 12)
    public void getRemoteSoftwareUpgradeInstallations(){
        given().
                headers(headers).
            when().
                get(url+"remotesoftwareupgrade/installations").
            then().
                statusCode(200).
                body("results.installationId", hasItems("b2015b53-9f1b-4bb8-99e9-4376b7f37631", "f0160bb3-f289-478f-8d91-399a0f5b5a64"));
    }

    @Test(priority = 13)
    public void getSpecifications(){
        given().
                headers(headers).
            when().
                get(url+"specifications").
            then().
                statusCode(200).
                body("results.assetName", hasItems("B-Box", "RCS 300","tomasasset"));
    }

}
