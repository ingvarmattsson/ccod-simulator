package CCC_v1;

import cccObjects.MachineGroup;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class SmartMachineGroup {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/machinegroup";
    Map<String,String> headers;
    List<MachineGroup> groups;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant", DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        groups = new LinkedList<MachineGroup>();
    }

    @Test (priority = 1)
    public void getAvailableMachines(){
        given().
                headers(headers).
            when().
                get(url+"/available-machines").
            then().
                statusCode(200).
                body("0b64c949-33b0-4215-8e26-803b0cd2e880", equalTo("on-reg51-surface-04")).
                body("JM-10", equalTo("LaneAlloc - JM-10")).
                body("testreg46", equalTo("testreg46-alias"));
    }

    @Test (priority = 2)
    public void getMachineGroup(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.name", hasItems("ra smg 01","ra smg 01","SMG - 03"));
    }

    @Test(priority = 3)
    public void postMachineGroup1Machine(){
        String payload = "{\"name\":\"ra-t1\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"machines\":[\"JM-01\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(200).
                body("name", equalTo("ra-t1")).
                body("machines",hasItems("JM-01"))
                .extract().asString();

        groups.add(new Gson().fromJson(response, MachineGroup.class));
    }

    @Test (priority = 4)
    public void postMachineGroupManyMachines(){
        String payload = "{\"name\":\"ra-t2\",\"sites\":[],\"cities\":[],\"countries\":[],\"authorizedUsers\":[],\"machines\":[\"JM-08\",\"JM-09\",\"JM-06\",\"JM-07\",\"JM-04\",\"JM-05\",\"JM-02\",\"JM-03\"]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(200).
                body("name", equalTo("ra-t2")).
                body("machines",hasItems("JM-02","JM-09","JM-05"))
                .extract().asString();

        groups.add(new Gson().fromJson(response, MachineGroup.class));
    }

    @Test(priority = 5)
    public void putMachineGroupAdd1Machine(){
        MachineGroup m = groups.get(0);
        m.machines.add("JM-11");
        String payload = new Gson().toJson(m);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("token="+m.token).
                body(payload).
            when().
                put(url).
            then().
                statusCode(200).
                body("name",equalTo("ra-t1")).
                body("machines", hasItems("JM-01","JM-11"));
    }

    @Test(priority = 6)
    public void putMachineGroupAddManyMachines(){
        MachineGroup m = groups.get(0);
        m.machines.add("JM-12");
        m.machines.add("JM-13");
        m.machines.add("JM-14");
        m.machines.add("JM-15");
        m.machines.add("JM-16");
        m.machines.add("JM-17");
        m.machines.add("JM-18");
        m.machines.add("JM-19");
        m.machines.add("JM-20");
        String payload = new Gson().toJson(m);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("token="+m.token).
                body(payload).
            when().
                put(url).
            then().
                statusCode(200).
                body("name",equalTo("ra-t1")).
                body("machines", hasItems("JM-01","JM-11","JM-15","JM-20"));

    }

    @Test(priority = 7)
    public void putMachineGroupRemove1Machine(){
        MachineGroup m = groups.get(0);
        m.machines.remove("JM-12");
        String payload = new Gson().toJson(m);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("token="+m.token).
                body(payload).
            when().
                put(url).
            then().
                statusCode(200).
                body("name",equalTo("ra-t1")).
                body("machines", hasItems("JM-01","JM-11","JM-15","JM-20"));
    }

    @Test (priority = 8)
    public void putMachineGroupRemoveManyMachines(){
        MachineGroup m = groups.get(0);
        m.machines.remove("JM-13");
        m.machines.remove("JM-14");
        m.machines.remove("JM-15");
        m.machines.remove("JM-16");
        m.machines.remove("JM-17");
        m.machines.remove("JM-18");
        m.machines.remove("JM-19");
        m.machines.remove("JM-20");
        String payload = new Gson().toJson(m);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("token="+m.token).
                body(payload).
            when().
                put(url).
            then().
                statusCode(200).
                body("name",equalTo("ra-t1")).
                body("machines", hasItems("JM-01","JM-11"));
    }

    @Test(priority = 100)
    public void deleteMachineGroup(){
        for (MachineGroup m : groups){
            given().
                    headers(headers).
                    header("Content-type","application/json").
                when().
                    delete(url+"?token="+m.token).
                then().
                    statusCode(200);
        }
    }
}
