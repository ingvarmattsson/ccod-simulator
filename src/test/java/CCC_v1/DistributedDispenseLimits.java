package CCC_v1;

import cccObjects.DispenseLimit;
import ccodSession.Environments;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class DistributedDispenseLimits {

    Environments env = DefaultStrings.environmentForRestAssured;
    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/distributeddispenselimit";
    Map<String,String> headers;
    List<DispenseLimit> machineDL;
    private List<DispenseLimit> cashierDL;
    private List<DispenseLimit> machineUserDL;
    private List<DispenseLimit> workUnitDL;
    List<String> identsToDelete;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        machineDL = new LinkedList<DispenseLimit>();
        cashierDL = new LinkedList<DispenseLimit>();
        machineUserDL = new LinkedList<DispenseLimit>();
        workUnitDL = new LinkedList<DispenseLimit>();
        identsToDelete = new LinkedList<>();
    }

    @Test(priority = 1)
    public void getDevicesPaged(){
        given().
                headers(headers).
                queryParam("includeSpecification","true").
                queryParam("includeAssignment","true").
                queryParam("includeSite","true").
                queryParam("pageSize","100").
                queryParam("page","1").
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/devices").
            then().
                statusCode(200).
                body("results.hardwareId",hasItems("48f86a59-3e70-4604-895a-0f86b9272ce4","JM-6893","JM-6811"));

        given().
                headers(headers).
                queryParam("includeSpecification","true").
                queryParam("includeAssignment","true").
                queryParam("includeSite","true").
                queryParam("pageSize","100").
                queryParam("page","2").
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/devices").
            then().
                statusCode(200).
                body("results.hardwareId",hasItems("JM-6810","JM-6795","JM-6711"));


    }

    @Test (priority = 2)
    public void getMachineUsers(){
        given().
                headers(headers).
                queryParam("sortOrder","asc").
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/machineusers").
            then().
                statusCode(200).
                body("results.name",hasItems("Annamae Abernathy","Angela McGlynn","Cheyanne Kulas"));

    }

    @Test (priority = 3)
    public void getMachineUserRoles(){
        given().
                headers(headers).
            when().
                get(url+"/roles").
            then().
                statusCode(200).
                body("ROLE_ID",hasItems("administrator","1_Vault-AuditV","additionalloan")).
                body("PERMISSIONS[0]",hasItems("LOG","BAG_DROP_ALLOW_ZERO_AMOUNT","TOOLBOX:OPEN_CLOSE_SHUTTER")).
                body("PERMISSIONS[26]",hasItems("AUTO_WORK_UNIT:500","CHANGE_USER_LOG_OUT_AFTER","STAY")).
                body("PERMISSIONS[47]",hasItems("FREE_FLOAT","DISPENSE:*"));

    }

    @Test (priority = 4)
    public void getWorkUnits(){
        given().
                headers(headers).
            when().
                get(url+"/work-units").
            then().
                statusCode(200).
                body("DISPLAY_NAME",hasItems("Barcelona WU Group - 01","1 Hour Photo","Paris workUnit Group - 02")).
                body("CHILDREN[0].NAME",hasItems("barca-wu-01","barca-wu-05","barca-wu-10")).
                body("CHILDREN[17].NAME",hasItems("87","88","89")).
                body("CHILDREN[29].NAME",hasItems("paris-workUnit-01","paris-workUnit-05","paris-workUnit-10"));

    }

    @Test (priority = 5)
    public void getMachineGroup(){
        given().
                headers(headers).
            when().
                get(DefaultStrings.getBaseURL(env)+"/api/v1/machinegroup").
            then().
                statusCode(200).
                body("numResults",equalTo(9)).
                body("results.token",hasItems("f9c6f70f-872f-4d99-978a-81d5dac68676","4cbe2da3-4827-4ba8-8071-0d7cc49b4719","2017f387-cadb-42fc-a1c7-a9e4d77e8f5f")).
                body("results.machines[0]",hasItems("asdf-asfd","JM-26","JM-23")).
                body("results.machines[4]",hasItems("82b287bf-ad59-4027-bdfd-b1cb2b069220","bsafedummy1")).
                body("results.machines[6]",hasItems("db4e1182-ca47-43b6-97ad-67f7086c8d6f","e0d69477-8938-4f51-acd6-e45d5c038a90","20933339-50b5-48a5-a14a-6e5ce3ece97c"));

    }

    @Test (priority = 6)
    public void getDispenseLimitConfigurationNonGroupedMachineGroup(){
        given().
                headers(headers).
                queryParam("groupToken","-1").
            when().
                get(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseAmounts[0].amount[0]",equalTo(1500000)).
                body("dispenseAmounts[0].decimals[0]",equalTo(2)).
                body("dispenseAmounts[0].currency[0]",equalTo("EUR")).
                body("dispenseAmounts[0].initial[0]",equalTo(1500000)).
                body("ident[0]",equalTo("cashier")).
                body("resetHour[0]",equalTo(-1)).
                body("type[0]",equalTo("ROLE")).

                body("dispenseAmounts[16].amount[1]",equalTo(400000)).
                body("dispenseAmounts[16].decimals[1]",equalTo(2)).
                body("dispenseAmounts[16].currency[1]",equalTo("USD")).
                body("dispenseAmounts[16].initial[1]",equalTo(400000)).
                body("ident[16]",equalTo("JM-846")).
                body("resetHour[16]",equalTo(12)).
                body("type[16]",equalTo("MACHINE")).


                body("dispenseAmounts[37].amount[1]",equalTo(40000)).
                body("dispenseAmounts[37].decimals[1]",equalTo(2)).
                body("dispenseAmounts[37].currency[1]",equalTo("USD")).
                body("dispenseAmounts[37].initial[1]",equalTo(40000)).
                body("ident[37]",equalTo("lon-wu-09")).
                body("resetHour[37]",equalTo(13)).
                body("type[37]",equalTo("WORK_UNIT"));
    }

    @Test (priority = 7)
    public void getDispenseLimitConfigurationOnMachineGroupToken(){
        given().
                headers(headers).
                queryParam("groupToken","f9c6f70f-872f-4d99-978a-81d5dac68676").
            when().
                get(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseAmounts[0].amount[0]",equalTo(1500000)).
                body("dispenseAmounts[0].decimals[0]",equalTo(2)).
                body("dispenseAmounts[0].currency[0]",equalTo("EUR")).
                body("dispenseAmounts[0].initial[0]",equalTo(1500000)).
                body("ident[0]",equalTo("JM-30")).
                body("resetHour[0]",equalTo(12)).
                body("type[0]",equalTo("MACHINE")).

                body("dispenseAmounts[2].amount[1]",equalTo(100000)).
                body("dispenseAmounts[2].decimals[1]",equalTo(2)).
                body("dispenseAmounts[2].currency[1]",equalTo("USD")).
                body("dispenseAmounts[2].initial[1]",equalTo(100000)).
                body("ident[2]",equalTo("0000")).
                body("resetHour[2]",equalTo(0)).
                body("type[2]",equalTo("USER")).


                body("dispenseAmounts[3].amount[0]",equalTo(350000)).
                body("dispenseAmounts[3].decimals[0]",equalTo(2)).
                body("dispenseAmounts[3].currency[0]",equalTo("EUR")).
                body("dispenseAmounts[3].initial[0]",equalTo(350000)).
                body("ident[3]",equalTo("barca-wu-01")).
                body("resetHour[3]",equalTo(22)).
                body("type[3]",equalTo("WORK_UNIT"));
    }

    @Test (priority = 7)
    public void getMachineGroupsConfigurations(){
        given().
                headers(headers).
            when().
                get(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", hasItems("-1","f9c6f70f-872f-4d99-978a-81d5dac68676")).
                body("enabled[0]", equalTo(true)).
                body("everEnabled[0]", equalTo(true)).
                body("enabled[1]", equalTo(false)).
                body("everEnabled[1]", equalTo(false)).
                body("receivedLatestConfigMachineUuids[1]", hasSize(0));
    }

    @Test (priority = 8)
    public void getDistributedDispenseLimitStatesDashboard(){
        given().
                headers(headers).
            when().
                get(url+"/state/-1").
            then().
                statusCode(200).
                body("user", hasItems("1010","1234","1008")).
                body("dispenseAmounts[0].amount[0]", equalTo(5000)).
                body("dispenseAmounts[6].amount[0]", equalTo(5000)).
                body("dispenseAmounts[17].amount[0]", equalTo(5000))
                ;
    }

    @Test(priority = 9)
    public void postRuleMachine1NotGroupedOneCurrency(){
        String payload = "{\"ident\":\"\",\"idents\":[\"JM-20\"],\"type\":\"MACHINE\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":150000,\"initial\":150000}],\"resetHour\":0}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("-1")).
                body("dispenseLimits.ident", hasItems("JM-20")).
                body("dispenseLimits[40].resetHour", equalTo(0)).
                body("dispenseLimits[40].type", equalTo("MACHINE")).
                body("dispenseLimits[40].dispenseAmounts[0].amount", equalTo(150000)).
                body("dispenseLimits[40].dispenseAmounts[0].currency", equalTo("EUR")).
                extract().asString();

        machineDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("-1#JM-20");
    }

    @Test(priority = 10)
    public void postRuleMachine2GroupedMultipleCurrencies(){
        String payload = "{\"ident\":\"\",\"idents\":[\"JM-233\"],\"type\":\"MACHINE\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":255000,\"initial\":255000},{\"decimals\":2,\"currency\":\"USD\",\"amount\":359000,\"initial\":359000}],\"resetHour\":\"23\"}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","c96b97b8-0dff-448f-84f9-5d91c5209744").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("c96b97b8-0dff-448f-84f9-5d91c5209744")).
                body("dispenseLimits.ident", hasItems("JM-233")).
                body("dispenseLimits[0].resetHour", equalTo(23)).
                body("dispenseLimits[0].type", equalTo("MACHINE")).
                body("dispenseLimits[0].dispenseAmounts[0].amount", equalTo(255000)).
                body("dispenseLimits[0].dispenseAmounts[0].currency", equalTo("EUR")).
                body("dispenseLimits[0].dispenseAmounts[1].amount", equalTo(359000)).
                body("dispenseLimits[0].dispenseAmounts[1].currency", equalTo("USD")).
                extract().asString();

        machineDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("c96b97b8-0dff-448f-84f9-5d91c5209744#JM-233");

    }

    @Test (priority = 11)
    public void putRuleMachine1NotGroupedEditCurrencyChangeTime(){
        DispenseLimit.DispenseLimitIdent dl = machineDL.get(0).dispenseLimits.get(40);
        dl.resetHour = 15;
        dl.dispenseAmounts.get(0).currency="SEK";
        dl.dispenseAmounts.get(0).amount = dl.dispenseAmounts.get(0).initial = 266000;


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[40].resetHour", equalTo(15)).
                body("dispenseLimits[40].dispenseAmounts[0].amount", equalTo(266000)).
                body("dispenseLimits[40].dispenseAmounts[0].currency", equalTo("SEK"));
    }

    @Test (priority = 12)
    public void putRuleMachine2NotGroupedAddCurrency(){
        DispenseLimit.DispenseLimitIdent dl = machineDL.get(0).dispenseLimits.get(40);
        dl.resetHour = 15;
        DispenseLimit.DispenseAmount da = new DispenseLimit.DispenseAmount();
        da.currency="NOK";
        da.decimals =2;
        da.initial = da.amount = 9900;dl.dispenseAmounts.add(da);


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[40].resetHour", equalTo(15)).
                body("dispenseLimits[40].dispenseAmounts[0].amount", equalTo(266000)).
                body("dispenseLimits[40].dispenseAmounts[0].currency", equalTo("SEK")).
                body("dispenseLimits[40].dispenseAmounts[1].amount", equalTo(9900)).
                body("dispenseLimits[40].dispenseAmounts[1].currency", equalTo("NOK"));
    }


    @Test (priority = 13)
    public void putRuleMachine3GroupedRemoveCurrency(){
        DispenseLimit.DispenseLimitIdent dl = machineDL.get(1).dispenseLimits.get(0);
        dl.dispenseAmounts.remove(0);

        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken",machineDL.get(1).machineGroupToken).
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[0].dispenseAmounts[0].amount", equalTo(359000)).
                body("dispenseLimits[0].dispenseAmounts[0].currency", equalTo("USD")).
                body("dispenseLimits[0].dispenseAmounts.size()", is(1));
    }


    @Test(priority = 14)
    public void postRuleRole1NotGroupedOneCurrencyEOS(){
        String payload = "{\"ident\":\"\",\"idents\":[\"Cashier\"],\"type\":\"ROLE\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":56600,\"initial\":56600}],\"resetHour\":-1}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("-1")).
                body("dispenseLimits.ident", hasItems("Cashier")).
                body("dispenseLimits[41].resetHour", equalTo(-1)).
                body("dispenseLimits[41].type", equalTo("ROLE")).
                body("dispenseLimits[41].dispenseAmounts[0].amount", equalTo(56600)).
                body("dispenseLimits[41].dispenseAmounts[0].currency", equalTo("EUR")).
                extract().asString();

        cashierDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("-1#Cashier");
    }

    @Test(priority = 15)
    public void postRuleRole2GroupedMultipleCurrencies(){
        String payload = "{\"ident\":\"\",\"idents\":[\"Cashier\"],\"type\":\"ROLE\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":995000,\"initial\":995000},{\"decimals\":2,\"currency\":\"USD\",\"amount\":123500,\"initial\":123500}],\"resetHour\":\"23\"}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","c96b97b8-0dff-448f-84f9-5d91c5209744").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("c96b97b8-0dff-448f-84f9-5d91c5209744")).
                body("dispenseLimits.ident", hasItems("Cashier")).
                body("dispenseLimits[1].resetHour", equalTo(23)).
                body("dispenseLimits[1].type", equalTo("ROLE")).
                body("dispenseLimits[1].dispenseAmounts[0].amount", equalTo(995000)).
                body("dispenseLimits[1].dispenseAmounts[0].currency", equalTo("EUR")).
                body("dispenseLimits[1].dispenseAmounts[1].amount", equalTo(123500)).
                body("dispenseLimits[1].dispenseAmounts[1].currency", equalTo("USD")).
                extract().asString();

        cashierDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("c96b97b8-0dff-448f-84f9-5d91c5209744#Cashier");

    }

    @Test (priority = 16)
    public void putRuleRole1NotGroupedEditCurrencyChangeTime(){
        DispenseLimit.DispenseLimitIdent dl = cashierDL.get(0).dispenseLimits.get(41);
        dl.resetHour = 16;
        dl.dispenseAmounts.get(0).currency="SEK";
        dl.dispenseAmounts.get(0).amount = dl.dispenseAmounts.get(0).initial = 366000;


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[41].resetHour", equalTo(16)).
                body("dispenseLimits[41].dispenseAmounts[0].amount", equalTo(366000)).
                body("dispenseLimits[41].dispenseAmounts[0].currency", equalTo("SEK"));
    }

    @Test (priority = 17)
    public void putRuleRole2NotGroupedAddCurrency(){
        DispenseLimit.DispenseLimitIdent dl = cashierDL.get(0).dispenseLimits.get(41);
        DispenseLimit.DispenseAmount da = new DispenseLimit.DispenseAmount();
        da.currency="NOK";
        da.decimals =2;
        da.initial = da.amount = 9900;dl.dispenseAmounts.add(da);


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[41].resetHour", equalTo(16)).
                body("dispenseLimits[41].dispenseAmounts[0].amount", equalTo(366000)).
                body("dispenseLimits[41].dispenseAmounts[0].currency", equalTo("SEK")).
                body("dispenseLimits[40].dispenseAmounts[1].amount", equalTo(9900)).
                body("dispenseLimits[40].dispenseAmounts[1].currency", equalTo("NOK"));
    }


    @Test (priority = 18)
    public void putRuleRole3GroupedRemoveCurrency(){
        DispenseLimit.DispenseLimitIdent dl = cashierDL.get(1).dispenseLimits.get(1);
        dl.dispenseAmounts.remove(0);

        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken",machineDL.get(1).machineGroupToken).
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[1].dispenseAmounts[0].amount", equalTo(123500)).
                body("dispenseLimits[1].dispenseAmounts[0].currency", equalTo("USD")).
                body("dispenseLimits[1].dispenseAmounts.size()", is(1));
    }

    @Test(priority = 19)
    public void postRuleMachineUser1NotGroupedOneCurrencyEOS(){
        String payload = "{\"ident\":\"\",\"idents\":[\"0000\"],\"type\":\"USER\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":859000,\"initial\":859000}],\"resetHour\":-1}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("-1")).
                body("dispenseLimits.ident", hasItems("0000")).
                body("dispenseLimits[42].resetHour", equalTo(-1)).
                body("dispenseLimits[42].type", equalTo("USER")).
                body("dispenseLimits[42].dispenseAmounts[0].amount", equalTo(859000)).
                body("dispenseLimits[42].dispenseAmounts[0].currency", equalTo("EUR")).
                extract().asString();

        machineUserDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("-1#0000");
    }

    @Test(priority = 20)
    public void postRuleMachineUser2GroupedManyCurrencies(){
        String payload = "{\"ident\":\"\",\"idents\":[\"0001\"],\"type\":\"USER\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":232300,\"initial\":232300},{\"decimals\":2,\"currency\":\"USD\",\"amount\":636300,\"initial\":636300}],\"resetHour\":0}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","c96b97b8-0dff-448f-84f9-5d91c5209744").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("c96b97b8-0dff-448f-84f9-5d91c5209744")).
                body("dispenseLimits.ident", hasItems("0001")).
                body("dispenseLimits[2].resetHour", equalTo(0)).
                body("dispenseLimits[2].type", equalTo("USER")).
                body("dispenseLimits[2].dispenseAmounts[0].amount", equalTo(232300)).
                body("dispenseLimits[2].dispenseAmounts[0].currency", equalTo("EUR")).
                body("dispenseLimits[2].dispenseAmounts[1].amount", equalTo(636300)).
                body("dispenseLimits[2].dispenseAmounts[1].currency", equalTo("USD")).
                extract().asString();

        machineUserDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("c96b97b8-0dff-448f-84f9-5d91c5209744#0001");
    }

    @Test (priority = 21)
    public void putRuleMachineUser1NotGroupedEditCurrencyChangeTime(){
        DispenseLimit.DispenseLimitIdent dl = machineUserDL.get(0).dispenseLimits.get(42);
        dl.resetHour = 23;
        dl.dispenseAmounts.get(0).currency="SEK";
        dl.dispenseAmounts.get(0).amount = dl.dispenseAmounts.get(0).initial = 747400;


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[42].resetHour", equalTo(23)).
                body("dispenseLimits[42].dispenseAmounts[0].amount", equalTo(747400)).
                body("dispenseLimits[42].dispenseAmounts[0].currency", equalTo("SEK"));
    }

    @Test (priority = 22)
    public void putRuleMachineUser2NotGroupedAddCurrency(){
        DispenseLimit.DispenseLimitIdent dl = machineUserDL.get(0).dispenseLimits.get(42);
        DispenseLimit.DispenseAmount da = new DispenseLimit.DispenseAmount();
        da.currency="DKK";
        da.decimals =2;
        da.initial = da.amount = 8800;dl.dispenseAmounts.add(da);


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[42].resetHour", equalTo(23)).
                body("dispenseLimits[42].dispenseAmounts[0].amount", equalTo(747400)).
                body("dispenseLimits[42].dispenseAmounts[0].currency", equalTo("SEK")).
                body("dispenseLimits[42].dispenseAmounts[1].amount", equalTo(8800)).
                body("dispenseLimits[42].dispenseAmounts[1].currency", equalTo("DKK"));
    }


    @Test (priority = 23)
    public void putRuleMachineUser3GroupedRemoveCurrency(){
        DispenseLimit.DispenseLimitIdent dl = machineUserDL.get(1).dispenseLimits.get(2);
        dl.dispenseAmounts.remove(0);

        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken",machineUserDL.get(1).machineGroupToken).
                body(payload).
                when().
                put(url+"/configuration").
                then().
                statusCode(200).
                body("dispenseLimits[2].dispenseAmounts[0].amount", equalTo(636300)).
                body("dispenseLimits[2].dispenseAmounts[0].currency", equalTo("USD")).
                body("dispenseLimits[2].dispenseAmounts.size()", is(1));
    }

    @Test(priority = 24)
    public void postRuleWorkUnit1NotGroupedOneCurrency(){
        String payload = "{\"ident\":\"\",\"idents\":[\"barca-wu-01\"],\"type\":\"WORK_UNIT\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":654100,\"initial\":654100}],\"resetHour\":0}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("-1")).
                body("dispenseLimits.ident", hasItems("barca-wu-01")).
                body("dispenseLimits[43].resetHour", equalTo(0)).
                body("dispenseLimits[43].type", equalTo("WORK_UNIT")).
                body("dispenseLimits[43].dispenseAmounts[0].amount", equalTo(654100)).
                body("dispenseLimits[43].dispenseAmounts[0].currency", equalTo("EUR")).
                extract().asString();

        workUnitDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("-1#barca-wu-01");
    }

    @Test(priority = 25)
    public void postRuleWorkUnit2GroupedManyCurrencies(){
        String payload = "{\"ident\":\"\",\"idents\":[\"barca-wu-01\"],\"type\":\"WORK_UNIT\",\"dispenseAmounts\":[{\"decimals\":2,\"currency\":\"EUR\",\"amount\":4561000,\"initial\":4561000},{\"decimals\":2,\"currency\":\"USD\",\"amount\":6541000,\"initial\":6541000}],\"resetHour\":\"23\"}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","c96b97b8-0dff-448f-84f9-5d91c5209744").
                body(payload).
            when().
                post(url+"/configuration").
            then().
                statusCode(200).
                body("machineGroupToken", equalTo("c96b97b8-0dff-448f-84f9-5d91c5209744")).
                body("dispenseLimits.ident", hasItems("barca-wu-01")).
                body("dispenseLimits[3].resetHour", equalTo(23)).
                body("dispenseLimits[3].type", equalTo("WORK_UNIT")).
                body("dispenseLimits[3].dispenseAmounts[0].amount", equalTo(4561000)).
                body("dispenseLimits[3].dispenseAmounts[0].currency", equalTo("EUR")).
                body("dispenseLimits[3].dispenseAmounts[1].amount", equalTo(6541000)).
                body("dispenseLimits[3].dispenseAmounts[1].currency", equalTo("USD")).
                extract().asString();

        workUnitDL.add(new Gson().fromJson(response, DispenseLimit.class));
        identsToDelete.add("c96b97b8-0dff-448f-84f9-5d91c5209744#barca-wu-01");
    }

    @Test (priority = 26)
    public void putRuleWorkUnit1NotGroupedEditCurrencyChangeTime(){
        DispenseLimit.DispenseLimitIdent dl = workUnitDL.get(0).dispenseLimits.get(43);
        dl.resetHour = 11;
        dl.dispenseAmounts.get(0).currency="SEK";
        dl.dispenseAmounts.get(0).amount = dl.dispenseAmounts.get(0).initial = 3216;


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[43].resetHour", equalTo(11)).
                body("dispenseLimits[43].dispenseAmounts[0].amount", equalTo(3216)).
                body("dispenseLimits[43].dispenseAmounts[0].currency", equalTo("SEK"));
    }

    @Test (priority = 27)
    public void putRuleWorkUnit2NotGroupedAddCurrency(){
        DispenseLimit.DispenseLimitIdent dl = workUnitDL.get(0).dispenseLimits.get(43);
        DispenseLimit.DispenseAmount da = new DispenseLimit.DispenseAmount();
        da.currency="DKK";
        da.decimals =2;
        da.initial = da.amount = 7845;
        dl.dispenseAmounts.add(da);


        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken","-1").
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[43].resetHour", equalTo(11)).
                body("dispenseLimits[43].dispenseAmounts[0].amount", equalTo(3216)).
                body("dispenseLimits[43].dispenseAmounts[0].currency", equalTo("SEK")).
                body("dispenseLimits[43].dispenseAmounts[1].amount", equalTo(7845)).
                body("dispenseLimits[43].dispenseAmounts[1].currency", equalTo("DKK"));
    }


    @Test (priority = 28)
    public void putRuleWorkUnit3GroupedRemoveCurrency(){
        DispenseLimit.DispenseLimitIdent dl = workUnitDL.get(1).dispenseLimits.get(3);
        dl.dispenseAmounts.remove(0);

        String payload = new Gson().toJson(dl);

        given().
                headers(headers).
                header("Content-type","application/json").
                queryParam("groupToken",machineUserDL.get(1).machineGroupToken).
                body(payload).
            when().
                put(url+"/configuration").
            then().
                statusCode(200).
                body("dispenseLimits[3].dispenseAmounts[0].amount", equalTo(6541000)).
                body("dispenseLimits[3].dispenseAmounts[0].currency", equalTo("USD")).
                body("dispenseLimits[3].dispenseAmounts.size()", is(1));
    }

    @Test(priority = 100)
    public void deleteDispenseLimitRules(){

        for (String i : identsToDelete) {
            String[] dlGroupIdent = i.split("#");
            System.out.println();
            given().
                    headers(headers).
                    header("Content-type", "application/json").
                    queryParam("groupToken", dlGroupIdent[0]).
                    queryParam("id", dlGroupIdent[1]).
                when().
                    delete(url + "/configuration").
                then().
                    statusCode(200);
        }
    }

}
