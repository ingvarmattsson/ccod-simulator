package CCC_v1;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class MachineTypeRegisterer {

    private class HardwareAsset{
        private String id = "rcs700Active";
        private String sku="rcs700Active";
        private String name="RCS-Active/700";
        private String imageUrl="https://ccc-image.s3.amazonaws.com/RCS-Active_RCS700.png";
        private String description="X";
        private Map<String,String> properties;

        private String type;
        private String assetCategoryId;

        private HardwareAsset(){
            properties = new HashMap<>();
        }
    }

    public String[] tenants = {
            "egc",
            "etp",
            "everitreasureisland",
            //"hrcni",
            "jacksonrancheria",
            "twinarrow",
            "westgate"
    };
    private String assetCategory="suzohappPC";
    private String brokerHost="10.155.2.114"; //10.155.2.114
    private String licenseLevel ="FULL";
    private String licenseLabel ="FULL";


    private String requestURLHardwareAssetPost ="http://"+brokerHost+":8080/sitewhere/api/assets/categories/"+assetCategory+"/hardware/";
    private String requestURLSpecificationPost ="http://"+brokerHost+":8080/sitewhere/api/specifications";
    private HardwareAsset asset = new HardwareAsset();
    private Map<String,AssetSpecificationResponse> tenantSpecifications;

    private class AssetSpecification{
        private String assetId;
        private String assetModuleId;
        private String containerPolicy="Standalone";
        private String name;
        private Map<String,String> metadata;

        private AssetSpecification(HardwareAsset asset){
            assetId = asset.id;
            assetModuleId = assetCategory;
            name = asset.name;
            metadata = new HashMap<>();

        }
    }

    private class AssetSpecificationCCoDLicense{
        private String licenseLevel;
        private String licenseLabel;

        public AssetSpecificationCCoDLicense(String licenseLabel, String licenseLevel) {
            this.licenseLabel = licenseLabel;
            this.licenseLevel = licenseLevel;
        }
    }

    private class AssetSpecificationResponse{
        private String createdDate;
        private String createdBy;
        private Boolean deleted;
        private String token;
        private String name;
        private String assetModeuleId;
        private String assetId;
        private String assetName;
        private String assetImageUrl;
        private HardwareAsset asset;
        private String containerPolicy;
        private Map<String,String> metadata;

    }

    private class TenantResponse {
        int numResults;
        List<Tenant> results;
    }
    private class Tenant{
        String authenticationToken;
        String createdBy;
        String createdDate;
        Boolean deleted;
        String id;
        String logoUrl;
        String name;
        String tenantTemplateId;
        String updatedBy;
        String updatedDate;
        List<String> authorizedUserIds;
        Map<String,String> metadata;
    }

    public static void main(String[] args) {
        MachineTypeRegisterer mtr = new MachineTypeRegisterer();
        mtr.fetchAllTenants();
        mtr.postHardwareAsset();
        mtr.postAssetSpecification();
        mtr.postDeviceCommands();
        mtr.addCCoDLicenseMetadata();
    }

    public String[] fetchAllTenants() {
        if ( tenants.length >=1)
            return new String[0];

        System.out.println("No tenants specified in array, fetching all tenants\n");
        List<String> t = new LinkedList<>();
        String response= given().
                header("Content-type","application/json").
                header("Authorization","Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo").
                header("X-SiteWhere-Tenant","sitewhere1234567890").
                queryParam("includeRuntimeInfo","").
                queryParam("page","1").
                queryParam("pageSize","1000").
            when().
                get("http://"+brokerHost+":8080/sitewhere/api/tenants").
            then().
                extract().asString();

        TenantResponse tr = new Gson().fromJson(response, TenantResponse.class);
        System.out.println();
        tenants = new String[tr.numResults];
        for(int i =0; i<tenants.length;i++){
            tenants[i] = tr.results.get(i).authenticationToken;
        }
        return tenants;


    }

    private void addCCoDLicenseMetadata() {
        System.out.println("Adding CCoD License Metadata\n");
        AssetSpecificationCCoDLicense metadataValueObj = new AssetSpecificationCCoDLicense(licenseLabel,licenseLevel);
        String metadataValue = new Gson().toJson(metadataValueObj);

        for (String tenant : tenantSpecifications.keySet()){
            AssetSpecificationResponse specification = tenantSpecifications.get(tenant);
            specification.metadata.put("ccodLicenseMachineType", metadataValue);
            String payload = new Gson().toJson(specification);

            String response= given().
                    header("Content-type","application/json").
                    header("Authorization","Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo").
                    header("X-SiteWhere-Tenant",tenant).
                    body(payload).
                when().
                    put(requestURLSpecificationPost+"/"+tenantSpecifications.get(tenant).token).
                then().
                    extract().asString();

            System.out.println(tenant+" - "+response);
        }
    }

    private void postDeviceCommands() {
        System.out.println("Posting specification commands\n");
        String payloadSendDeviceMqttMessage ="{\"name\":\"sendDeviceMqttMessage\",\"namespace\":null,\"description\":null,\"parameters\":[{\"name\":\"message\",\"type\":\"String\",\"required\":false}],\"metadata\":{}}";
        String payloadSendMqttAlert ="{\"name\":\"sendMqttAlert\",\"namespace\":null,\"description\":null,\"parameters\":[{\"name\":\"message\",\"type\":\"String\",\"required\":false}],\"metadata\":{}}";

     //   List<String> tenants = (List<String>) tenantSpecifications.keySet();
        for(String tenant : tenantSpecifications.keySet()){
            String response= given().
                    header("Content-type","application/json").
                    header("Authorization","Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo").
                    header("X-SiteWhere-Tenant",tenant).
                   body(payloadSendDeviceMqttMessage).
                when().
                    post(requestURLSpecificationPost+"/"+tenantSpecifications.get(tenant).token+"/commands").
                then().
                    extract().asString();

            System.out.println(tenant+" - "+response);

            response= given().
                    header("Content-type","application/json").
                    header("Authorization","Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo").
                    header("X-SiteWhere-Tenant",tenant).
                   body(payloadSendMqttAlert).
                when().
                    post(requestURLSpecificationPost+"/"+tenantSpecifications.get(tenant).token+"/commands").
                then().
                    extract().asString();

            System.out.println(tenant+" - "+response);

        }

    }

    private void postAssetSpecification() {
        AssetSpecification as = new AssetSpecification(asset);
        tenantSpecifications = new HashMap<>();
        String payload = new Gson().toJson(as);

        System.out.println("Posting Specification\n");
        for(int i = 0; i<tenants.length;i++){
            String response= given().
                    header("Content-type","application/json").
                    header("Authorization","Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo").
                    header("X-SiteWhere-Tenant",tenants[i]).
                    body(payload).
                when().
                    post(requestURLSpecificationPost).
                then().
                    extract().asString();

            System.out.println(tenants[i]+" - "+response);
            tenantSpecifications.put(tenants[i],new Gson().fromJson(response, AssetSpecificationResponse.class));
        }
    }

    private void postHardwareAsset() {
        String payload = new Gson().toJson(asset);

        System.out.println("Posting hardware assets\n");
        for (int i = 0; i<tenants.length;i++){
            String response= given().
                    header("Content-type","application/json").
                    header("Authorization","Basic ZXNidXNlcjpyLTI4VXpKRTIjZTI1YWNo").
                    header("X-SiteWhere-Tenant",tenants[i]).
                    body(payload).
                when().
                    post(requestURLHardwareAssetPost).
                then().
                    extract().asString();

            System.out.println(tenants[i]+" - "+response);
        }
    }

}
