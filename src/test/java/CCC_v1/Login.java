package CCC_v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.annotations.Test;
import rest.Token;

import java.io.IOException;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.given;

public class Login {

    static String url="https://test-api.cashcomplete.com/api/v1/auth/token";
    static String username="esbuser";
    String usernameNonadmin="suzohapp.ta.2@gmail.com";
    static String hashedPassword="a779ecb6ccae0f5620fba3a420fe15f5d6c2209b";
    String hashedPasswordNonadmin="faff3c30cb9330ef9d0d5ea5f823a353b801fbd1";


    public static Token getToken(){

        String response=  given().
            header("Content_type","application/json").
            queryParam("username",username).
            queryParam("hashedPassword",hashedPassword).
        when().
            post(url).
        then().
            statusCode(200).
            extract().asString();

        Token token=null;
        try {
            token = new ObjectMapper().readValue(response,Token.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return token;
    }

    @Test
    public void loginAdmin(){
        given().
            header("Content-type","application/json").
            queryParam("username",username).
            queryParam("hashedPassword",hashedPassword).
        when().
            post(url).
        then().
            statusCode(200);
    }

    @Test
    public void loginNonAdmin(){
        given().
            header("Content-type","application/json").
            queryParam("username",usernameNonadmin).
            queryParam("hashedPassword",hashedPasswordNonadmin).
        when().
            post(url).
        then().
            statusCode(200);
    }

    @Test
    public void loginIncorrectUserName(){
        given().
            header("Content-type","application/json").
            queryParam("username","usernamedoesntexist@suzohapp.com").
            queryParam("hashedPassword",hashedPasswordNonadmin).
        when().
            post(url).
        then().
            statusCode(403);
    }

    @Test
    public void loginIncorrectUserPassword(){
        given().
            header("Content-type","application/json").
            queryParam("username",usernameNonadmin).
            queryParam("hashedPassword","hashedPasswordNonadmin").
        when().
            post(url).
        then().
            statusCode(403);
    }
}
