package CCC_v1;

import cccObjects.ScheduledReport;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class ScheduledReportsCRUD {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/report";
    Map<String,String> headers;
    List<ScheduledReport> reports;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        reports = new LinkedList<ScheduledReport>();
    }

    @Test(priority = 1)
    public void getCCCUsers(){
        given().
                headers(headers).
            when().
                get(DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/users").
            then().
                statusCode(200).
                body("results.email", hasItems("connect@cashcomplete.com", "patrik.lind@suzohapp.com","nicole.thornber@paycomplete.com"));
    }

    @Test(priority = 2)
    public void getAvailableReports(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports").
            then().
                statusCode(200).log().all();
    }

    @Test(priority = 3)
    public void getReports(){
        given().
                headers(headers).
            when().
                get(url+"/reportRules").
            then().
                statusCode(200).
                body("results.name", hasItems("ta - tt 1", "ta - tt 2 site","ta - cb 2 site"));
    }

    @Test(priority = 4)
    public void getMachines(){
        given().
                headers(headers).
                queryParam("pageSize","1000").
            when().
                get(DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/machines").
            then().
                statusCode(200).
                body("results.machineId", hasItems("48f86a59-3e70-4604-895a-0f86b9272ce4", "JM-6892","JM-5911"));
    }

    @Test(priority = 5)
    public void getMachineUserRoles(){
        given().
                headers(headers).
                queryParam("pageSize","1000").
            when().
                get(DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/machineusers/roles/").
            then().
                statusCode(200).
                body("results",hasItems("administrator", "cit","manager"));
    }

    @Test(priority = 6)
    public void getTransactionRoleCounts(){
        given().
                headers(headers).
                queryParam("pageSize","1000").
            when().
                get(DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/transactions/roles/counts").
            then().
                statusCode(200).
                body("labels",hasItems("BRINKS GESTION", "ParkeerBeheerder","standard")).
                body("data",hasItems(695, 49639,974));
    }

    @Test(priority = 7)
    public void getMachineInventoryRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineInventory/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Machine Type",
                        "Container",
                        "CCoD version",
                        "Denomination",
                        "Type",
                        "Count",
                        "Amount"));
    }

    @Test(priority = 8)
    public void getTransactionsRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/transaction/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Subtype",
                        "Type",
                        "Transaction Date",
                        "Accounting Date",
                        "Machine Time",
                        "Machine",
                        "Site",
                        "Tran Id",
                        "Sequence",
                        "Fees",
                        "Machine User",
                        "Originating User",
                        "Account",
                        "Work Unit",
                        "Custom Data",
                        "Roles",
                        "Amount"
                        ));
    }

    @Test(priority = 9)
    public void getNetCashRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/netCash/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "opening",
                        "closing",
                        "difference"
                        ));
    }

    @Test(priority = 10)
    public void getRefillRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/netCash/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "opening",
                        "closing",
                        "difference"
                        ));
    }

    @Test(priority = 11)
    public void getCashierBalanceRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashierBalance/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Work Unit ID",
                        "Work Unit",
                        "Cashier ID",
                        "Cashier Name",
                        "Cashier Role",
                        "Site",
                        "Local Shift Date / Time",
                        "Currency",
                        "Opening Amount",
                        "Expected Close",
                        "Actual Close",
                        "Difference",
                        "Change",
                        "Returns",
                        "Sales",
                        "Drops",
                        "Payouts",
                        "Payins",
                        "Cash Tips"
                        ));
    }

    @Test(priority = 12)
    public void getManualDepositsRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/manualDeposit/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Cashier ID",
                        "Cashier Name",
                        "Cashier Role",
                        "Work Unit",
                        "Machine",
                        "Site",
                        "Transaction Type",
                        "Type",
                        "Device",
                        "Bag Id",
                        "Date / Time",
                        "Machine Date / Time",
                        "Amount"
                        ));
    }

    @Test(priority = 13)
    public void getWorkUnitOpsRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/manualDeposit/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Cashier ID",
                        "Cashier Name",
                        "Cashier Role",
                        "Work Unit",
                        "Machine",
                        "Site",
                        "Transaction Type",
                        "Type",
                        "Device",
                        "Bag Id",
                        "Date / Time",
                        "Machine Date / Time",
                        "Amount"

                        ));
    }

    @Test(priority = 14)
    public void getTillThroughputRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/tillthroughput/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Cashier ID",
                        "Cashier Name",
                        "Work kUnit ID",
                        "Work Unit",
                        "Role",
                        "Site",
                        "Local Shift Date / Time",
                        "Currency",
                        "Opening Amount",
                        "Closing Amount",
                        "Difference",
                        "Min",
                        "Max",
                        "Mean",
                        "Range",
                        "Standard Deviation"
                        ));
    }

    @Test(priority = 14)
    public void getCashSummaryRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashsummary/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Currency",
                        "Denomination",
                        "Type",
                        "Dispensable count",
                        "Dispensable amount",
                        "Bank deposit count",
                        "Bank deposit amount",
                        "Total"
                        ));
    }

    @Test(priority = 15)
    public void getMachineDetailRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineDetail/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine Id",
                        "Alias",
                        "Site",
                        "Type",
                        "CCoD Version",
                        "Machine Date",
                        "Accounting Date",
                        "Active Date",
                        "Last Error Date",
                        "Last Error"
                        ));
    }

    @Test(priority = 16)
    public void getMachineOnlineStatusRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineOnlineStatus/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Status",
                        "Time Of Status Change"
                        ));
    }

    @Test(priority = 17)
    public void getMachineReliabilityRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineReliability/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Number Of Sessions",
                        "Succeeded Sessions",
                        "Reliability"
                        ));
    }

    @Test(priority = 18)
    public void getCashierTransactionRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashiertransaction/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Cashier",
                        "Cashier Id",
                        "Role",
                        "Machine",
                        "Site",
                        "Transaction Date",
                        "Accounting Date",
                        "Transaction Type",
                        "Amount",
                        "difference"
                        ));
    }

    @Test(priority = 19)
    public void getCurrentOnlineStatusRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/currentOnlineStatus/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Online Status",
                        "Last Event",
                        "Last Received"
                        ));
    }

    @Test(priority = 12)
    public void getMachineErrorsRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineErrors/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Device",
                        "Machine Date / Time",
                        "Severity",
                        "Kind",
                        "Information",
                        "Extended Information",
                        "Error Code",
                        "Cleared?"
                        ));
    }

    @Test(priority = 13)
    public void getOpenShiftsRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/openShifts/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Cashier",
                        "Cashier Id",
                        "Role",
                        "Date / Time",
                        "Machine Date / Time",
                        "Machine",
                        "Site",
                        "Transaction Type",
                        "Amount"
                        ));
    }

    @Test(priority = 14)
    public void getClaimedValueRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/claimedValue/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Type",
                        "Container",
                        "Currency",
                        "Denomination",
                        "Count",
                        "Total"
                        ));
    }

    @Test(priority = 15)
    public void getSmartVaultRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/smartVault/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Type",
                        "Container",
                        "Currency",
                        "Denomination",
                        "Count",
                        "Total"
                        ));
    }

    @Test(priority = 16)
    public void getUtilizationRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/utilization/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Currency",
                        "Amount Notes",
                        "Number Notes",
                        "Amount Coins",
                        "Number Coins"
                        ));
    }

    @Test(priority = 17)
    public void getCashTotalByLocationRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashTotalsByLocation/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Site",
                        "Machine",
                        "Dispensable Amount",
                        "Bank Deposit Amount",
                        "Drop Safe",
                        "Open Shifts",
                        "Smart Vault"
                        ));
    }

    @Test(priority = 18)
    public void getCashInventoryRuleHeaders(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashInventory/headers").
            then().
                statusCode(200).
                body("$",hasItems(
                        "Machine",
                        "Site",
                        "Machine Type",
                        "Container",
                        "CCoD Version",
                        "Reminders",
                        "Denomination",
                        "Type",
                        "Count",
                        "Amount"
                        )).extract().asString();
    }

    @Test(priority = 19)
    public void getMachineInventoryRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineInventory/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineIds",
                        "timestamp",
                        "headers",
                        "translations"
                )).log().all();

    }

    @Test(priority = 20)
    public void getTransactionRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/transaction/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromDate",
                        "toDate",
                        "fromReceivedDate",
                        "toReceivedDate",
                        "fromAccountingDate",
                        "toAccountingDate",
                        "page",
                        "pageSize",
                        "machineId",
                        "machineUserId",
                        "machineUserName",
                        "machineUserRole",
                        "machineUserAccount",
                        "site",
                        "machineType",
                        "currency",
                        "type",
                        "fromAmount",
                        "toAmount",
                        "fromCommission",
                        "toCommission",
                        "fromSequence",
                        "toSequence",
                        "createdBy",
                        "correctionReason",
                        "filterOnType",
                        "isCorrection",
                        "sortField",
                        "sortOrder",
                        "workUnitName",
                        "headers",
                        "translations",
                        "filetype"
                ));

    }

    @Test(priority = 21)
    public void getNetCashRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/netCash/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineIds",
                        "timestamp",
                        "siteToken",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 22)
    public void getRefillRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/refill/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineIds",
                        "timestamp",
                        "refillThreshold",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 23)
    public void getCashierBalanceRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashierBalance/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "page",
                        "pageSize",
                        "fromDate",
                        "toDate",
                        "site",
                        "siteGroup",
                        "outlet",
                        "endOfDayTime",
                        "roleName",
                        "headers",
                        "translations",
                        "timeZone"
                ));

    }

    @Test(priority = 24)
    public void getManualDepositsRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/manualDeposit/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromDate",
                        "toDate",
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 25)
    public void getWorkUnitOpsRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/workunitOps/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromDate",
                        "toDate",
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 26)
    public void getTillThroughputRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/tillthroughput/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "pageSize",
                        "fromDate",
                        "toDate",
                        "siteGroup",
                        "endOfDayTime",
                        "site",
                        "roleName",
                        "statisticsBasedOn",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 27)
    public void getCashSummaryRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashsummary/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "site",
                        "siteGroup",
                        "date",
                        "pageSize",
                        "page",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 28)
    public void getMachineDetailRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineDetail/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 29)
    public void getMachineOnlineStatusRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineOnlineStatus/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromDate",
                        "toDate",
                        "machineId",
                        "headers",
                        "translations"
                ));

    }
    @Test(priority = 30)
    public void getMachineReliabilityRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineReliability/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineId",
                        "fromDate",
                        "toDate",
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 31)
    public void getCashierTransactionRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashiertransaction/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromAccountingDate",
                        "toAccountingDate",
                        "site",
                        "siteGroup",
                        "machineUserRole",
                        "headers",
                        "translations",
                        "filetype"
                ));

    }

    @Test(priority = 32)
    public void getCurrentOnlineStatusRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/currentOnlineStatus/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 33)
    public void getMachineErrorsRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/machineErrors/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromDate",
                        "toDate",
                        "severity",
                        "kind",
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 34)
    public void getOpenShiftsRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/openShifts/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "date",
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 35)
    public void getClaimedValueRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/claimedValue/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineIds",
                        "siteToken",
                        "timestamp",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 36)
    public void getSmartVaultRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/smartVault/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineIds",
                        "siteToken",
                        "timestamp",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 37)
    public void getUtilizationRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/utilization/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "fromDate",
                        "toDate",
                        "site",
                        "siteGroup",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 38)
    public void getCashTotalsByLocationRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashTotalsByLocation/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "site",
                        "timestamp",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 39)
    public void getCashInventoryRuleParameters(){
        given().
                headers(headers).
            when().
                get(url+"/available/reports/cashInventory/parameters").
            then().
                statusCode(200).
                body("$",hasItems(
                        "machineIds",
                        "timestamp",
                        "site",
                        "headers",
                        "translations"
                ));

    }

    @Test(priority = 40)
    public void postRuleCashInventory1DailyCSV(){
        String payload="{\"name\":\"ra cash inventory 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash inventory 1\"}}],\"reports\":[{\"report\":\"machineInventory\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()+12*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Machine Type\":\"Machine Type\",\"Container\":\"Container\",\"CCoD version\":\"CCoD version\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Count\":\"Count\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash inventory 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 41)
    public void postRuleCashInventory2WeeklyJsonManyRecipients(){
        String payload ="{\"name\":\"ra cash inventory 2\",\"everyWeekly\":[1,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash inventory 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash inventory 2\"}}],\"reports\":[{\"report\":\"machineInventory\",\"fileType\":\"json\",\"params\":{\"timestamp\":\"(day()-7*days+0*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Machine Type\":\"Machine Type\",\"Container\":\"Container\",\"CCoD version\":\"CCoD version\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Count\":\"Count\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash inventory 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 42)
    public void postRuleCashInventory3MonthlyWithMachineParams(){
        String payload ="{\"name\":\"ra cash inventory 3\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash inventory 3\"}}],\"reports\":[{\"report\":\"machineInventory\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(now())\",\"machineIds\":\"JM-01,JM-02\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Machine Type\":\"Machine Type\",\"Container\":\"Container\",\"CCoD version\":\"CCoD version\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Count\":\"Count\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash inventory 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 43)
    public void postRuleCashSummary1DailyCSV(){
        String payload ="{\"name\":\"ra cash summary 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash summary 1\"}}],\"reports\":[{\"report\":\"cashsummary\",\"fileType\":\"csv\",\"params\":{\"date\":\"(day()+12*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Currency\":\"Currency\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Dispensable count\":\"Dispensable count\",\"Dispensable amount\":\"Dispensable amount\",\"Bank deposit count\":\"Bank deposit count\",\"Bank deposit amount\":\"Bank deposit amount\",\"Total\":\"Total\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash summary 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 44)
    public void postRuleCashSummary2WeeklyJSonManyRecipients(){
        String payload ="{\"name\":\"ra cash summary 2\",\"everyWeekly\":[1,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash summary 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash summary 2\"}}],\"reports\":[{\"report\":\"cashsummary\",\"fileType\":\"json\",\"params\":{\"date\":\"(day()-7*days+0*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Currency\":\"Currency\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Dispensable count\":\"Dispensable count\",\"Dispensable amount\":\"Dispensable amount\",\"Bank deposit count\":\"Bank deposit count\",\"Bank deposit amount\":\"Bank deposit amount\",\"Total\":\"Total\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash summary 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 45)
    public void postRuleCashSummary3MonthlySiteFilters(){
        String payload ="{\"name\":\"ra cash summary 3\",\"everyWeekly\":[],\"everyMonthly\":[1,15,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash summary 3\"}}],\"reports\":[{\"report\":\"cashsummary\",\"fileType\":\"csv\",\"params\":{\"date\":\"(now())\",\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Currency\":\"Currency\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Dispensable count\":\"Dispensable count\",\"Dispensable amount\":\"Dispensable amount\",\"Bank deposit count\":\"Bank deposit count\",\"Bank deposit amount\":\"Bank deposit amount\",\"Total\":\"Total\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash summary 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 46)
    public void postRuleCashTotals1CSVDaily(){
        String payload ="{\"name\":\"ra cash totals 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash totals 1\"}}],\"reports\":[{\"report\":\"cashTotalsByLocation\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()+12*hour)\"},\"headerMapping\":{\"Site\":\"Site\",\"Machine\":\"Machine\",\"Dispensable Amount\":\"Dispensable Amount\",\"Bank Deposit Amount\":\"Bank Deposit Amount\",\"Drop Safe\":\"Drop Safe\",\"Open Shifts\":\"Open Shifts\",\"Smart Vault\":\"Smart Vault\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash totals 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 47)
    public void postRuleCashTotals2JSonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra cash totals 2\",\"everyWeekly\":[1,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash totals 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash totals 2\"}}],\"reports\":[{\"report\":\"cashTotalsByLocation\",\"fileType\":\"json\",\"params\":{\"timestamp\":\"(day()-7*days+0*hour)\"},\"headerMapping\":{\"Site\":\"Site\",\"Machine\":\"Machine\",\"Dispensable Amount\":\"Dispensable Amount\",\"Bank Deposit Amount\":\"Bank Deposit Amount\",\"Drop Safe\":\"Drop Safe\",\"Open Shifts\":\"Open Shifts\",\"Smart Vault\":\"Smart Vault\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash totals 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 48)
    public void postRuleCashTotals3MonthlySiteFilters(){
        String payload ="{\"name\":\"ra cash totals 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cash totals 3\"}}],\"reports\":[{\"report\":\"cashTotalsByLocation\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()+23*hour)\",\"site\":\"(f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683)\"},\"headerMapping\":{\"Site\":\"Site\",\"Machine\":\"Machine\",\"Dispensable Amount\":\"Dispensable Amount\",\"Bank Deposit Amount\":\"Bank Deposit Amount\",\"Drop Safe\":\"Drop Safe\",\"Open Shifts\":\"Open Shifts\",\"Smart Vault\":\"Smart Vault\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cash totals 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 49)
    public void postRuleCashierBalancing1CSVDaily(){
        String payload ="{\"name\":\"ra cashier balancing 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier balancing 1\"}}],\"reports\":[{\"report\":\"cashierBalance\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day)\",\"toDate\":\"(day())\",\"endOfDayTime\":\"12:00\"},\"headerMapping\":{\"Work Unit ID\":\"Work Unit ID\",\"Work Unit\":\"Work Unit\",\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Cashier Role\":\"Cashier Role\",\"Site\":\"Site\",\"Local Shift Date / Time\":\"Local Shift Date / Time\",\"Currency\":\"Currency\",\"Opening Amount\":\"Opening Amount\",\"Expected Close\":\"Expected Close\",\"Actual Close\":\"Actual Close\",\"Difference\":\"Difference\",\"Change\":\"Change\",\"Returns\":\"Returns\",\"Sales\":\"Sales\",\"Drops\":\"Drops\",\"Payouts\":\"Payouts\",\"Payins\":\"Payins\",\"Cash Tips\":\"Cash Tips\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cashier balancing 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 50)
    public void postRuleCashierBalancing2JSonWeeklyManyRecipientsSiteFilters(){
        String payload ="{\"name\":\"ra cashier balancing 2\",\"everyWeekly\":[1,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier balancing 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier balancing 2\"}}],\"reports\":[{\"report\":\"cashierBalance\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(day()-6*day)\",\"toDate\":\"(day())\",\"endOfDayTime\":\"00:00\",\"site\":\"(f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683)\"},\"headerMapping\":{\"Work Unit ID\":\"Work Unit ID\",\"Work Unit\":\"Work Unit\",\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Cashier Role\":\"Cashier Role\",\"Site\":\"Site\",\"Local Shift Date / Time\":\"Local Shift Date / Time\",\"Currency\":\"Currency\",\"Opening Amount\":\"Opening Amount\",\"Expected Close\":\"Expected Close\",\"Actual Close\":\"Actual Close\",\"Difference\":\"Difference\",\"Change\":\"Change\",\"Returns\":\"Returns\",\"Sales\":\"Sales\",\"Drops\":\"Drops\",\"Payouts\":\"Payouts\",\"Payins\":\"Payins\",\"Cash Tips\":\"Cash Tips\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cashier balancing 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 51)
    public void postRuleCashierBalancing3WeeklyRoleFilters(){
        String payload ="{\"name\":\"ra cashier balancing 3\",\"everyWeekly\":[1,4,7],\"everyMonthly\":[],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier balancing 3\"}}],\"reports\":[{\"report\":\"cashierBalance\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-3*day)\",\"toDate\":\"(day())\",\"endOfDayTime\":\"23:00\",\"roleName\":\"(cashier maintenance administrator)\"},\"headerMapping\":{\"Work Unit ID\":\"Work Unit ID\",\"Work Unit\":\"Work Unit\",\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Cashier Role\":\"Cashier Role\",\"Site\":\"Site\",\"Local Shift Date / Time\":\"Local Shift Date / Time\",\"Currency\":\"Currency\",\"Opening Amount\":\"Opening Amount\",\"Expected Close\":\"Expected Close\",\"Actual Close\":\"Actual Close\",\"Difference\":\"Difference\",\"Change\":\"Change\",\"Returns\":\"Returns\",\"Sales\":\"Sales\",\"Drops\":\"Drops\",\"Payouts\":\"Payouts\",\"Payins\":\"Payins\",\"Cash Tips\":\"Cash Tips\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cashier balancing 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 52)
    public void postRuleCashierTransactions1CSVDaily(){
        String payload ="{\"name\":\"ra cashier transactions 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier transactions 1\"}}],\"reports\":[{\"report\":\"cashiertransaction\",\"fileType\":\"csv\",\"params\":{\"fromAccountingDate\":\"(day()-1*day+12*hour)\",\"toAccountingDate\":\"(day()+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Cashier\":\"Cashier\",\"Cashier Id\":\"Cashier Id\",\"Role\":\"Role\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Transaction Type\":\"Transaction Type\",\"Amount\":\"Amount\",\"difference\":\"difference\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cashier transactions 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 53)
    public void postRuleCashierTransactions2WeeklyJsonManyRecipients(){
        String payload ="{\"name\":\"ra cashier transactions 2\",\"everyWeekly\":[1,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier transactions 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier transactions 2\"}}],\"reports\":[{\"report\":\"cashiertransaction\",\"fileType\":\"json\",\"params\":{\"fromAccountingDate\":\"(week()-1*week+0*hour)\",\"toAccountingDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Cashier\":\"Cashier\",\"Cashier Id\":\"Cashier Id\",\"Role\":\"Role\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Transaction Type\":\"Transaction Type\",\"Amount\":\"Amount\",\"difference\":\"difference\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cashier transactions 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 53)
    public void postRuleCashierTransactions3MonthlyWithParams(){
        String payload ="{\"name\":\"ra cashier transactions 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra cashier transactions 3\"}}],\"reports\":[{\"report\":\"cashiertransaction\",\"fileType\":\"csv\",\"params\":{\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9\",\"machineUserRole\":\"cashier\",\"fromAccountingDate\":\"(month()-1*month+23*hour)\",\"toAccountingDate\":\"(month()+22*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Cashier\":\"Cashier\",\"Cashier Id\":\"Cashier Id\",\"Role\":\"Role\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Transaction Type\":\"Transaction Type\",\"Amount\":\"Amount\",\"difference\":\"difference\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra cashier transactions 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 53)
    public void postRuleClaimedValues1CSVDaily(){
        String payload ="{\"name\":\"ra claimed values 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra claimed values 1\"}}],\"reports\":[{\"report\":\"claimedValue\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()+12*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Type\":\"Type\",\"Container\":\"Container\",\"Currency\":\"Currency\",\"Denomination\":\"Denomination\",\"Count\":\"Count\",\"Total\":\"Total\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra claimed values 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 54)
    public void postRuleClaimedValues2JSonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra claimed values 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra claimed values 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra claimed values 2\"}}],\"reports\":[{\"report\":\"claimedValue\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(now())\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Type\":\"Type\",\"Container\":\"Container\",\"Currency\":\"Currency\",\"Denomination\":\"Denomination\",\"Count\":\"Count\",\"Total\":\"Total\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra claimed values 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 55)
    public void postRuleClaimedValues3MonthlySiteFilters(){
        String payload ="{\"name\":\"ra claimed values 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra claimed values 3\"}}],\"reports\":[{\"report\":\"claimedValue\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(now())\",\"siteToken\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Type\":\"Type\",\"Container\":\"Container\",\"Currency\":\"Currency\",\"Denomination\":\"Denomination\",\"Count\":\"Count\",\"Total\":\"Total\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra claimed values 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 56)
    public void postRuleCurrentOnlineStatus1CSVDaily(){
        String payload ="{\"name\":\"ra current online status 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra current online status 1\"}}],\"reports\":[{\"report\":\"currentOnlineStatus\",\"fileType\":\"csv\",\"params\":{},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Online Status\":\"Online Status\",\"Last Event\":\"Last Event\",\"Last Received\":\"Last Received\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra current online status 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 57)
    public void postRuleCurrentOnlineStatus2JSonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra current online status 2\",\"everyWeekly\":[1,2,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra current online status 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra current online status 2\"}}],\"reports\":[{\"report\":\"currentOnlineStatus\",\"fileType\":\"json\",\"params\":{},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Online Status\":\"Online Status\",\"Last Event\":\"Last Event\",\"Last Received\":\"Last Received\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra current online status 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 58)
    public void postRuleCurrentOnlineStatus3MonthlyWithParams(){
        String payload ="{\"name\":\"ra current online status 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra current online status 3\"}}],\"reports\":[{\"report\":\"currentOnlineStatus\",\"fileType\":\"csv\",\"params\":{\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Online Status\":\"Online Status\",\"Last Event\":\"Last Event\",\"Last Received\":\"Last Received\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra current online status 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 59)
    public void postRuleMachineDetails1CSVDaily(){
        String payload ="{\"name\":\"ra machine details 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine details 1\"}}],\"reports\":[{\"report\":\"machineDetail\",\"fileType\":\"csv\",\"params\":{},\"headerMapping\":{\"Machine Id\":\"Machine Id\",\"Alias\":\"Alias\",\"Site\":\"Site\",\"Type\":\"Type\",\"CCoD Version\":\"CCoD Version\",\"Machine Date\":\"Machine Date\",\"Accounting Date\":\"Accounting Date\",\"Active Date\":\"Active Date\",\"Last Error Date\":\"Last Error Date\",\"Last Error\":\"Last Error\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine details 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 60)
    public void postRuleMachineDetails2JSonWeeklyManyRecipientsSiteFilters(){
        String payload ="{\"name\":\"ra machine details 2\",\"everyWeekly\":[1,2,4,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine details 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine details 2\"}}],\"reports\":[{\"report\":\"machineDetail\",\"fileType\":\"json\",\"params\":{\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67\"},\"headerMapping\":{\"Machine Id\":\"Machine Id\",\"Alias\":\"Alias\",\"Site\":\"Site\",\"Type\":\"Type\",\"CCoD Version\":\"CCoD Version\",\"Machine Date\":\"Machine Date\",\"Accounting Date\":\"Accounting Date\",\"Active Date\":\"Active Date\",\"Last Error Date\":\"Last Error Date\",\"Last Error\":\"Last Error\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine details 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 61)
    public void postRuleMachineDetails3MonthlySiteGroupFilters(){
        String payload ="{\"name\":\"ra machine details 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine details 3\"}}],\"reports\":[{\"report\":\"machineDetail\",\"fileType\":\"csv\",\"params\":{\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858 23da79c8-f8ed-4280-9260-e7ba1d8615d2\"},\"headerMapping\":{\"Machine Id\":\"Machine Id\",\"Alias\":\"Alias\",\"Site\":\"Site\",\"Type\":\"Type\",\"CCoD Version\":\"CCoD Version\",\"Machine Date\":\"Machine Date\",\"Accounting Date\":\"Accounting Date\",\"Active Date\":\"Active Date\",\"Last Error Date\":\"Last Error Date\",\"Last Error\":\"Last Error\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine details 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 62)
    public void postRuleMachineErrors1CsvDaily(){
        String payload ="{\"name\":\"ra machine errors 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine errors 1\"}}],\"reports\":[{\"report\":\"machineErrors\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Device\":\"Device\",\"Machine Date / Time\":\"Machine Date / Time\",\"Severity\":\"Severity\",\"Kind\":\"Kind\",\"Information\":\"Information\",\"Extended Information\":\"Extended Information\",\"Error Code\":\"Error Code\",\"Cleared?\":\"Cleared?\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine errors 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 63)
    public void postRuleMachineErrors2JSonWeeklySiteFiltersManyRecipients(){
        String payload ="{\"name\":\"ra machine errors 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine errors 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine errors 2\"}}],\"reports\":[{\"report\":\"machineErrors\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\",\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Device\":\"Device\",\"Machine Date / Time\":\"Machine Date / Time\",\"Severity\":\"Severity\",\"Kind\":\"Kind\",\"Information\":\"Information\",\"Extended Information\":\"Extended Information\",\"Error Code\":\"Error Code\",\"Cleared?\":\"Cleared?\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine errors 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 64)
    public void postRuleMachineErrors3MonthlySiteGroupFilter(){
        String payload ="{\"name\":\"ra machine errors 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine errors 3\"}}],\"reports\":[{\"report\":\"machineErrors\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(month()-1*month+23*hour)\",\"toDate\":\"(month()+22*hour+59*minutes+59*seconds)\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858 23da79c8-f8ed-4280-9260-e7ba1d8615d2\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Device\":\"Device\",\"Machine Date / Time\":\"Machine Date / Time\",\"Severity\":\"Severity\",\"Kind\":\"Kind\",\"Information\":\"Information\",\"Extended Information\":\"Extended Information\",\"Error Code\":\"Error Code\",\"Cleared?\":\"Cleared?\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine errors 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 65)
    public void postRuleMachineReliability1CsvDaily(){
        String payload ="{\"name\":\"ra machine reliability 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine reliability 1\"}}],\"reports\":[{\"report\":\"machineReliability\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Number Of Sessions\":\"Number Of Sessions\",\"Succeeded Sessions\":\"Succeeded Sessions\",\"Reliability\":\"Reliability\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine reliability 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 66)
    public void postRuleMachineReliability2JSonWeeklySiteFilterManyRecipients(){
        String payload ="{\"name\":\"ra machine reliability 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine reliability 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine reliability 2\"}}],\"reports\":[{\"report\":\"machineReliability\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\",\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Number Of Sessions\":\"Number Of Sessions\",\"Succeeded Sessions\":\"Succeeded Sessions\",\"Reliability\":\"Reliability\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine reliability 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 67)
    public void postRuleMachineReliability3MonthlySiteGroupMachineFilter(){
        String payload ="{\"name\":\"ra machine reliability 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra machine reliability 3\"}}],\"reports\":[{\"report\":\"machineReliability\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(month()-1*month+23*hour)\",\"toDate\":\"(month()+22*hour+59*minutes+59*seconds)\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858 23da79c8-f8ed-4280-9260-e7ba1d8615d2\",\"machineId\":\"JM-01 JM-02\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Number Of Sessions\":\"Number Of Sessions\",\"Succeeded Sessions\":\"Succeeded Sessions\",\"Reliability\":\"Reliability\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra machine reliability 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 68)
    public void postRuleManualDeposits1CSVDaily(){
        String payload ="{\"name\":\"ra manual deposits 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra manual deposits 1\"}}],\"reports\":[{\"report\":\"manualDeposit\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Cashier Role\":\"Cashier Role\",\"Work Unit\":\"Work Unit\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Type\":\"Transaction Type\",\"Type\":\"Type\",\"Device\":\"Device\",\"Bag Id\":\"Bag Id\",\"Date / Time\":\"Date / Time\",\"Machine Date / Time\":\"Machine Date / Time\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra manual deposits 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 69)
    public void postRuleManualDeposits2JsonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra manual deposits 2\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra manual deposits 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra manual deposits 2\"}}],\"reports\":[{\"report\":\"manualDeposit\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Cashier Role\":\"Cashier Role\",\"Work Unit\":\"Work Unit\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Type\":\"Transaction Type\",\"Type\":\"Type\",\"Device\":\"Device\",\"Bag Id\":\"Bag Id\",\"Date / Time\":\"Date / Time\",\"Machine Date / Time\":\"Machine Date / Time\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra manual deposits 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 70)
    public void postRuleManualDeposits3MonthlySiteFilters(){
        String payload ="{\"name\":\"ra manual deposits 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"New Reportra manual deposits 3\"}}],\"reports\":[{\"report\":\"manualDeposit\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(month()-1*month+23*hour)\",\"toDate\":\"(month()+22*hour+59*minutes+59*seconds)\",\"site\":\"(f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67)\"},\"headerMapping\":{\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Cashier Role\":\"Cashier Role\",\"Work Unit\":\"Work Unit\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Type\":\"Transaction Type\",\"Type\":\"Type\",\"Device\":\"Device\",\"Bag Id\":\"Bag Id\",\"Date / Time\":\"Date / Time\",\"Machine Date / Time\":\"Machine Date / Time\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra manual deposits 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 71)
    public void postRuleMNetCash1DailyCSV(){
        String payload ="{\"name\":\"ra net cash 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra net cash 1\"}}],\"reports\":[{\"report\":\"netCash\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()+12*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"opening\":\"opening\",\"closing\":\"closing\",\"difference\":\"difference\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra net cash 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 72)
    public void postRuleMNetCash2WeeklyJSonSiteFilterManyRecipients(){
        String payload ="{\"name\":\"ra net cash 2\",\"everyWeekly\":[1,2,6,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra net cash 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra net cash 2\"}}],\"reports\":[{\"report\":\"netCash\",\"fileType\":\"json\",\"params\":{\"timestamp\":\"(day()-7*days+0*hour)\",\"siteToken\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"opening\":\"opening\",\"closing\":\"closing\",\"difference\":\"difference\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra net cash 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 73)
    public void postRuleMNetCash3MonthlyMachineFilter(){
        String payload ="{\"name\":\"ra net cash 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra net cash 3\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra net cash 3\"}}],\"reports\":[{\"report\":\"netCash\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(now())\",\"machineIds\":\"JM-01,JM-02,JM-03\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"opening\":\"opening\",\"closing\":\"closing\",\"difference\":\"difference\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra net cash 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 74)
    public void postRuleOnlineStatus1CSVDaily(){
        String payload ="{\"name\":\"ra online status 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra online status 1\"}}],\"reports\":[{\"report\":\"machineOnlineStatus\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Status\":\"Status\",\"Time Of Status Change\":\"Time Of Status Change\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra online status 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 75)
    public void postRuleOnlineStatus2JSonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra online status 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra online status 2\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra online status 2\"}}],\"reports\":[{\"report\":\"machineOnlineStatus\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Status\":\"Status\",\"Time Of Status Change\":\"Time Of Status Change\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra online status 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 76)
    public void postRuleOnlineStatus3MonthlyMachineFilter(){
        String payload ="{\"name\":\"ra online status 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra online status 3\"}}],\"reports\":[{\"report\":\"machineOnlineStatus\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(month()-1*month+23*hour)\",\"toDate\":\"(month()-1*day+22*hour+59*minutes+59*seconds)\",\"machineId\":\"JM-01 JM-02 JM-03\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Status\":\"Status\",\"Time Of Status Change\":\"Time Of Status Change\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra online status 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 77)
    public void postRuleOpenShifts1CsvDaily(){
        String payload ="{\"name\":\"ra open shifts 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra open shifts 1\"}}],\"reports\":[{\"report\":\"openShifts\",\"fileType\":\"csv\",\"params\":{\"date\":\"(day()+12*hour)\"},\"headerMapping\":{\"Cashier\":\"Cashier\",\"Cashier Id\":\"Cashier Id\",\"Role\":\"Role\",\"Date / Time\":\"Date / Time\",\"Machine Date / Time\":\"Machine Date / Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Type\":\"Transaction Type\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra open shifts 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 78)
    public void postRuleOpenShifts2JSonWeeklySiteFilterManyRecipients(){
        String payload ="{\"name\":\"ra open shifts 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra open shifts 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra open shifts 2\"}}],\"reports\":[{\"report\":\"openShifts\",\"fileType\":\"json\",\"params\":{\"date\":\"(now())\",\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Cashier\":\"Cashier\",\"Cashier Id\":\"Cashier Id\",\"Role\":\"Role\",\"Date / Time\":\"Date / Time\",\"Machine Date / Time\":\"Machine Date / Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Type\":\"Transaction Type\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra open shifts 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 79)
    public void postRuleOpenShifts3MonthlySiteGroupFilter(){
        String payload ="{\"name\":\"ra open shifts 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra open shifts 3\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra open shifts 3\"}}],\"reports\":[{\"report\":\"openShifts\",\"fileType\":\"csv\",\"params\":{\"date\":\"(now())\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858 23da79c8-f8ed-4280-9260-e7ba1d8615d2\"},\"headerMapping\":{\"Cashier\":\"Cashier\",\"Cashier Id\":\"Cashier Id\",\"Role\":\"Role\",\"Date / Time\":\"Date / Time\",\"Machine Date / Time\":\"Machine Date / Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Transaction Type\":\"Transaction Type\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra open shifts 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 80)
    public void postRuleRefillCsvDaily(){
        String payload ="{\"name\":\"ra refill1 \",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra refill1\"}}],\"reports\":[{\"report\":\"refill\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()-7*days+12*hour)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Machine Type\":\"Machine Type\",\"Container\":\"Container\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Low Count\":\"Low Count\",\"Current Count\":\"Current Count\",\"High Count\":\"High Count\",\"Refill Count\":\"Refill Count\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra refill1 ")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 81)
    public void postRuleRefil2JSonWeeklyRefillThresholdFilterMAnyRecipients(){
        String payload ="{\"name\":\"ra refill 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra refill 2\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra refill 2\"}}],\"reports\":[{\"report\":\"refill\",\"fileType\":\"json\",\"params\":{\"timestamp\":\"(day()-7*days+2*hour)\",\"refillThreshold\":\"0.1\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Machine Type\":\"Machine Type\",\"Container\":\"Container\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Low Count\":\"Low Count\",\"Current Count\":\"Current Count\",\"High Count\":\"High Count\",\"Refill Count\":\"Refill Count\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra refill 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 82)
    public void postRuleRefil3MonthlyMachineFilters(){
        String payload ="{\"name\":\"ra refill 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra refill 3\"}}],\"reports\":[{\"report\":\"refill\",\"fileType\":\"csv\",\"params\":{\"timestamp\":\"(day()+23*hour)\",\"machineIds\":\"JM-01,JM-02,JM-03\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Machine Type\":\"Machine Type\",\"Container\":\"Container\",\"Denomination\":\"Denomination\",\"Type\":\"Type\",\"Low Count\":\"Low Count\",\"Current Count\":\"Current Count\",\"High Count\":\"High Count\",\"Refill Count\":\"Refill Count\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra refill 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 83)
    public void postRuleTillThroughput1CsvDailyStatisticBasedOnDifference(){
        String payload ="{\"name\":\"ra till throughput 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra till throughput 1\"}}],\"reports\":[{\"report\":\"tillthroughput\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()+11*hour+59*minutes+59*seconds)\",\"statisticsBasedOn\":\"difference\"},\"headerMapping\":{\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Work kUnit ID\":\"Work kUnit ID\",\"Work Unit\":\"Work Unit\",\"Role\":\"Role\",\"Site\":\"Site\",\"Local Shift Date / Time\":\"Local Shift Date / Time\",\"Currency\":\"Currency\",\"Opening Amount\":\"Opening Amount\",\"Closing Amount\":\"Closing Amount\",\"Difference\":\"Difference\",\"Min\":\"Min\",\"Max\":\"Max\",\"Mean\":\"Mean\",\"Range\":\"Range\",\"Standard Deviation\":\"Standard Deviation\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra till throughput 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 84)
    public void postRuleTillThroughput2JSonWeeklyStatisticBasedOnOpeningAmountSiteFilterManyRecipients(){
        String payload ="{\"name\":\"ra till throughput 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra till throughput 2\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra till throughput 2\"}}],\"reports\":[{\"report\":\"tillthroughput\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\",\"statisticsBasedOn\":\"opening\",\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 8ef54d36-d06e-4e06-82c7-dd934d825308 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Work kUnit ID\":\"Work kUnit ID\",\"Work Unit\":\"Work Unit\",\"Role\":\"Role\",\"Site\":\"Site\",\"Local Shift Date / Time\":\"Local Shift Date / Time\",\"Currency\":\"Currency\",\"Opening Amount\":\"Opening Amount\",\"Closing Amount\":\"Closing Amount\",\"Difference\":\"Difference\",\"Min\":\"Min\",\"Max\":\"Max\",\"Mean\":\"Mean\",\"Range\":\"Range\",\"Standard Deviation\":\"Standard Deviation\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra till throughput 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 85)
    public void postRuleTillThroughput3MonthlyStatisticBasedOnClosingAmountSiteGroupFilter(){
        String payload ="{\"name\":\"ra till throughput 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra till throughput 3\"}}],\"reports\":[{\"report\":\"tillthroughput\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(month()-1*month+23*hour)\",\"toDate\":\"(month()-1*day+22*hour+59*minutes+59*seconds)\",\"statisticsBasedOn\":\"actual\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858 23da79c8-f8ed-4280-9260-e7ba1d8615d2\"},\"headerMapping\":{\"Cashier ID\":\"Cashier ID\",\"Cashier Name\":\"Cashier Name\",\"Work kUnit ID\":\"Work kUnit ID\",\"Work Unit\":\"Work Unit\",\"Role\":\"Role\",\"Site\":\"Site\",\"Local Shift Date / Time\":\"Local Shift Date / Time\",\"Currency\":\"Currency\",\"Opening Amount\":\"Opening Amount\",\"Closing Amount\":\"Closing Amount\",\"Difference\":\"Difference\",\"Min\":\"Min\",\"Max\":\"Max\",\"Mean\":\"Mean\",\"Range\":\"Range\",\"Standard Deviation\":\"Standard Deviation\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra till throughput 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 86)
    public void postRuleTransactions1CsvDaily(){
        String payload ="{\"name\":\"ra transactions 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra transactions 1\"}}],\"reports\":[{\"report\":\"transaction\",\"fileType\":\"csv\",\"params\":{\"fromAccountingDate\":\"(day()-1*day+12*hour)\",\"toAccountingDate\":\"(day()-1*day+11*hour+59*minutes+59*seconds)\"},\"reportName\":\"\",\"simplePeriod\":\"\",\"parameters\":{},\"time\":2,\"headerMapping\":{\"Subtype\":\"Subtype\",\"Type\":\"Type\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Machine Time\":\"Machine Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Tran Id\":\"Tran Id\",\"Sequence\":\"Sequence\",\"Fees\":\"Fees\",\"Machine User\":\"Machine User\",\"Originating User\":\"Originating User\",\"Account\":\"Account\",\"Work Unit\":\"Work Unit\",\"Custom Data\":\"Custom Data\",\"Roles\":\"Roles\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra transactions 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 87)
    public void postRuleTransactions2JSonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra transactions 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra transactions 2\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra transactions 2\"}}],\"reports\":[{\"report\":\"transaction\",\"fileType\":\"json\",\"params\":{\"fromAccountingDate\":\"(week()-1*week+0*hour)\",\"toAccountingDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\"},\"reportName\":\"\",\"simplePeriod\":\"\",\"parameters\":{},\"time\":2,\"headerMapping\":{\"Subtype\":\"Subtype\",\"Type\":\"Type\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Machine Time\":\"Machine Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Tran Id\":\"Tran Id\",\"Sequence\":\"Sequence\",\"Fees\":\"Fees\",\"Machine User\":\"Machine User\",\"Originating User\":\"Originating User\",\"Account\":\"Account\",\"Work Unit\":\"Work Unit\",\"Custom Data\":\"Custom Data\",\"Roles\":\"Roles\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra transactions 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 88)
    public void postRuleTransactions3MonthlyParams(){
        String payload ="{\"name\":\"ra transactions 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra transactions 3\"}}],\"reports\":[{\"report\":\"transaction\",\"fileType\":\"csv\",\"params\":{\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb\",\"machineId\":\"JM-01\",\"machineUserId\":\"7001\",\"machineUserName\":\"John Doe\",\"machineUserAccount\":\"7001\",\"machineType\":\"RCS 500\",\"currency\":\"EUR\",\"type\":\"DISPENSE\",\"fromAmount\":\"300\",\"toAmount\":\"500\",\"workUnitName\":\"barca-wu-01\",\"fromAccountingDate\":\"(month()-1*month+23*hour)\",\"toAccountingDate\":\"(month()-1*day+22*hour+59*minutes+59*seconds)\"},\"reportName\":\"\",\"simplePeriod\":\"\",\"parameters\":{},\"time\":2,\"headerMapping\":{\"Subtype\":\"Subtype\",\"Type\":\"Type\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Machine Time\":\"Machine Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Tran Id\":\"Tran Id\",\"Sequence\":\"Sequence\",\"Fees\":\"Fees\",\"Machine User\":\"Machine User\",\"Originating User\":\"Originating User\",\"Account\":\"Account\",\"Work Unit\":\"Work Unit\",\"Custom Data\":\"Custom Data\",\"Roles\":\"Roles\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra transactions 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 89)
    public void postRuleUtilization1CsvDaily(){
        String payload ="{\"name\":\"ra utilization 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra utilization 1\"}}],\"reports\":[{\"report\":\"utilization\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()-1*day+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Currency\":\"Currency\",\"Amount Notes\":\"Amount Notes\",\"Number Notes\":\"Number Notes\",\"Amount Coins\":\"Amount Coins\",\"Number Coins\":\"Number Coins\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra utilization 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 90)
    public void postRuleUtilization2JsonWeeklySiteFilterManyRecipients(){
        String payload ="{\"name\":\"ra utilization 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra utilization 2\"}},{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra utilization 2\"}}],\"reports\":[{\"report\":\"utilization\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\",\"site\":\"f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Currency\":\"Currency\",\"Amount Notes\":\"Amount Notes\",\"Number Notes\":\"Number Notes\",\"Amount Coins\":\"Amount Coins\",\"Number Coins\":\"Number Coins\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra utilization 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 91)
    public void postRuleUtilization3MonthlySiteGroupFilter(){
        String payload ="{\"name\":\"ra utilization 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra utilization 3\"}}],\"reports\":[{\"report\":\"utilization\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(month()-1*month+23*hour)\",\"toDate\":\"(month()-1*day+22*hour+59*minutes+59*seconds)\",\"siteGroup\":\"46cfa2fd-a6ba-4115-b935-7450583a18d9 5100f2db-29bb-474c-a109-9082b03e4858 23da79c8-f8ed-4280-9260-e7ba1d8615d2\"},\"headerMapping\":{\"Machine\":\"Machine\",\"Site\":\"Site\",\"Currency\":\"Currency\",\"Amount Notes\":\"Amount Notes\",\"Number Notes\":\"Number Notes\",\"Amount Coins\":\"Amount Coins\",\"Number Coins\":\"Number Coins\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra utilization 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 92)
    public void postRuleWorkunitOps1CsvDaily(){
        String payload ="{\"name\":\"ra workunit ops 1\",\"everyWeekly\":[1,2,3,4,5,6,7],\"everyMonthly\":[],\"time\":\"12:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra workunit ops 1\"}}],\"reports\":[{\"report\":\"workunitOps\",\"fileType\":\"csv\",\"params\":{\"fromDate\":\"(day()-1*day+12*hour)\",\"toDate\":\"(day()-1*day+11*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Work Unit\":\"Work Unit\",\"Work Unit Group\":\"Work Unit Group\",\"Cashier Id\":\"Cashier Id\",\"Name\":\"Name\",\"Role\":\"Role\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra workunit ops 1")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 93)
    public void postRuleWorkunitOps2JSonWeeklyManyRecipients(){
        String payload ="{\"name\":\"ra workunit ops 2\",\"everyWeekly\":[1,2,7],\"everyMonthly\":[],\"time\":\"00:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.2@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra workunit ops 2\"}},{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra workunit ops 2\"}}],\"reports\":[{\"report\":\"workunitOps\",\"fileType\":\"json\",\"params\":{\"fromDate\":\"(week()-1*week+0*hour)\",\"toDate\":\"(week()-1*day+23*hour+59*minutes+59*seconds)\"},\"headerMapping\":{\"Work Unit\":\"Work Unit\",\"Work Unit Group\":\"Work Unit Group\",\"Cashier Id\":\"Cashier Id\",\"Name\":\"Name\",\"Role\":\"Role\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra workunit ops 2")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    @Test(priority = 94)
    public void postRuleWorkunitOps3MonthlySiteFilters(){
        String payload ="{\"name\":\"ra workunit ops 3\",\"everyWeekly\":[],\"everyMonthly\":[1,18,28],\"time\":\"23:00\",\"timeZone\":\"Europe/Stockholm\",\"recipients\":[{\"recipient\":\"suzohapp.ta.1@gmail.com\",\"type\":\"email\",\"params\":{\"emailTitle\":\"ra workunit ops 3\"}}],\"reports\":[{\"report\":\"transaction\",\"fileType\":\"json\",\"params\":{\"site\":\"(f05a4509-506e-40d8-92e7-c852a898fbfb 07377128-107e-43ba-bc4b-554631da6f67 a7bb05e3-4b96-44f3-897b-0399563da683)\",\"fromAccountingDate\":\"(month()-1*month+23*hour)\",\"toAccountingDate\":\"(month()-1*day+22*hour+59*minutes+59*seconds)\"},\"reportName\":\"\",\"simplePeriod\":\"\",\"parameters\":{},\"time\":2,\"headerMapping\":{\"Subtype\":\"Subtype\",\"Type\":\"Type\",\"Transaction Date\":\"Transaction Date\",\"Accounting Date\":\"Accounting Date\",\"Machine Time\":\"Machine Time\",\"Machine\":\"Machine\",\"Site\":\"Site\",\"Tran Id\":\"Tran Id\",\"Sequence\":\"Sequence\",\"Fees\":\"Fees\",\"Machine User\":\"Machine User\",\"Originating User\":\"Originating User\",\"Account\":\"Account\",\"Work Unit\":\"Work Unit\",\"Custom Data\":\"Custom Data\",\"Roles\":\"Roles\",\"Amount\":\"Amount\"}}]}";

        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                body("name", equalTo("ra workunit ops 3")).
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }


    @Test(priority = 1000)
    public void deleteScheduledReportRules(){
        for (ScheduledReport sr : reports){

            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/reportRules/"+sr.token).
                then().
                    statusCode(200);

        }
    }

}
