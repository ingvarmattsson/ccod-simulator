package CCC_v1;

import cccObjects.UserMachine;
import com.google.gson.Gson;
import data.DefaultStrings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.Token;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class UsersMachine {

    String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/machineusers";
    Map<String,String> headers;
    List<UserMachine> users;

    @BeforeClass
    public void setup(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);

        users = new LinkedList<UserMachine>();
    }

    @Test
    public void getMachineUsers(){
        given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200).
                body("results.userId", hasItems("0001","9173","1234"));
    }

    @Test(priority = 1)
    public void postMachineUser1OnlyRequiredFields(){
        String payload ="{\"deactivated\":false,\"nbrLogins\":-1,\"pin\":\"ra-t1-password\",\"noPin\":false,\"userId\":\"98980001\",\"roleName\":\"administrator\",\"name\":\"ra-t1-firstName ra-ta-lastName\"}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("userId",equalTo("98980001")).
                body("roleName",equalTo("administrator")).
                body("name", equalTo("ra-t1-firstName ra-ta-lastName")).
                extract().asString();

        users.add(new Gson().fromJson(response, UserMachine.class));

    }

    @Test(priority = 2)
    public void postMachineUser2WithOptionalFields(){
        String payload ="{\"deactivated\":false,\"nbrLogins\":600,\"pin\":\"ra-t2-password\",\"noPin\":false,\"changePinOnLogin\":true,\"userId\":\"98980002\",\"cardIdentifier\":\"cardLoginValue\",\"doors\":[\"door1\",\"door2\"],\"roleName\":\"manager\",\"noPinUserId\":true,\"noPinCard\":true,\"noPinBio\":true,\"noPinDoor\":true,\"validFrom\":\"2021-03-31T22:00:00.000Z\",\"validUntil\":\"2021-06-30T22:00:00.000Z\",\"name\":\"ra-t2-firstName ra-t2-lastName\"}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("userId",equalTo("98980002")).
                body("roleName",equalTo("manager")).
                body("name", equalTo("ra-t2-firstName ra-t2-lastName")).
                body("validFrom", equalTo("2021-03-31T22:00:00.000Z")).
                body("validUntil", equalTo("2021-06-30T22:00:00.000Z")).
                extract().asString();

        users.add(new Gson().fromJson(response, UserMachine.class));

    }

    @Test(priority = 3)
    public void postMachineUser3AssignedToGroup(){
        String payload ="{\"deactivated\":\"true\",\"nbrLogins\":-1,\"noPin\":true,\"userId\":\"98980003\",\"roleName\":\"cashier\",\"changePinOnLogin\":true,\"assignedGroups\":[\"9a171f7b-46f4-4ce0-8e0a-6ed97d196168\",\"3ecaa762-318e-4cd9-9753-4dab84034b74\"],\"name\":\"ra-t3-firstName ra-t3-lastName\"}";

        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url).
            then().
                statusCode(201).
                body("userId",equalTo("98980003")).
                body("roleName",equalTo("cashier")).
                body("name", equalTo("ra-t3-firstName ra-t3-lastName")).
                body("assignedGroups[0]", equalTo("9a171f7b-46f4-4ce0-8e0a-6ed97d196168")).
                body("assignedGroups[1]", equalTo("3ecaa762-318e-4cd9-9753-4dab84034b74")).
                extract().asString();

        users.add(new Gson().fromJson(response, UserMachine.class));
    }

    @Test(dependsOnMethods = "postMachineUser1OnlyRequiredFields",priority = 4)
    public void putMachineUser1AddAdditionalFields(){
        UserMachine user = users.get(0);
        user.cardIdentifier="cardidentifierVal-01";
        user.changePinOnLogin=true;
        user.deactivated=true;
        user.roleName="cashier";
        user.validFrom="2021-03-31T22:00:00.000Z";
        user.validUntil="2021-06-30T22:00:00.000Z";

        String payload = new Gson().toJson(user);

        given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                put(url+"/"+user.userId).
            then().
                statusCode(200).
                body("cardIdentifier",equalTo("cardidentifierVal-01")).
                body("roleName",equalTo("cashier")).
                body("validFrom", equalTo("2021-03-31T22:00:00.000Z")).
                body("validUntil",equalTo("2021-06-30T22:00:00.000Z"));
    }

    @Test (dependsOnMethods = {"postMachineUser1OnlyRequiredFields","postMachineUser2WithOptionalFields","postMachineUser3AssignedToGroup"},priority = 10)
    public void deleteMachineUser(){
        for (UserMachine u : users){
            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/"+u.userId).
                then().
                    statusCode(200);
        }
    }
}
