package CCC_v1;

import CCoD.Currency;
import CCoD.TransactionType;
import cccObjects.Alert;
import ccodRestAssured.CCoDRestAssuredRmGenerator;
import ccodRestAssured.EmailPOJO;
import ccodSession.CCoDSession;
import ccodSession.Environments;
import ccodSession.EventType;
import com.google.gson.Gson;
import data.DefaultStrings;
import data.Utilities;
import emailHandler.EmailHandler;
import exceptions.BoxContentEmptyExcception;
import exceptions.BoxContentFullException;
import exceptions.DropSafeFullException;
import exceptions.EmptiableBoxesFullException;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.Token;
import rmObjects.*;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

import static io.restassured.RestAssured.given;

public class AlertingTriggering {

    private Environments env = DefaultStrings.environmentForRestAssured;
    private String tenant = DefaultStrings.tenantRestAssured;
    private String url = DefaultStrings.getBaseURL(env)+"/api/v1/alerts2";
    private Map<String,String> headers;
    private List<Alert> alertRules;

    private CCoDRestAssuredRmGenerator ccodRMGenerator;
    private CCoDSession ccodSession1;
    private CCoDSession ccodSession2;
    private CCoDSession ccodSession3;
    private CCoDSession ccodSession4;

    private EmailHandler emailHandler;
    private long emailwailtMs = 10000;

    private String errInfoTrigger = "Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK";
    private String errInfoDontTrigger = "ICX.1: RCS Active CAM: The content of ICX.1 was changed while CCoD software was not running. A transaction has been added with the difference: EUR 6.90";



    @BeforeClass
    public void setup() throws MessagingException {
        alertRules = new LinkedList<>();

        emailHandler = new EmailHandler();
        ccodRMGenerator = new CCoDRestAssuredRmGenerator();

        emailHandler.connect();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        try {
             ccodSession1 = ccodRMGenerator.initCCodSimulator("JM-01","3.15.0-ra",Currency.EUR,env,tenant,0); // new Site A
             ccodSession2 = ccodRMGenerator.initCCodSimulator("JM-11","3.15.0-ra",Currency.EUR,env,tenant,0); // Site B
             ccodSession3 = ccodRMGenerator.initCCodSimulator("JM-31","3.15.0-ra",Currency.EUR,env,tenant,0); // Site D
             ccodSession4 = ccodRMGenerator.initCCodSimulator("JM-12","3.15.0-ra",Currency.EUR,env,tenant,0); // Site B

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod
    public void getAccessToken(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant",DefaultStrings.tenantRestAssured);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);
    }

    @AfterClass
    public void tearDown() throws InterruptedException, MqttException, IOException {
        deleteAlertRules();
        ccodSession1.stopSession();
        ccodSession2.stopSession();
        ccodSession3.stopSession();
        ccodSession4.stopSession();
        ccodRMGenerator.saveSessionDataStore();

    }

    @Test(priority = 1)
    public void error1NoFiltersEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error1\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload = ccodRMGenerator.generateRmError(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        String emailTitle = emailMessages.get(0).getSubject();
        String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
        System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error1"));
        Assert.assertTrue(emailContent.contains(rmPayload.KIND));
        Assert.assertTrue(emailContent.contains(rmPayload.SEVERITY));
        Assert.assertTrue(emailContent.contains(rmPayload.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test (priority = 2)
    public void error2MachineFilter1MachineEmail() throws IOException, MessagingException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error2MachineFilter1Machine\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );



        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        emailHandler.getEmailContent(emailMessages);
        String emailTitle = emailMessages.get(0).getSubject();
        String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error2MachineFilter1Machine"));
        Assert.assertTrue(emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 3)
    public void error3MachineFilterNMachinesEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error3MachineFilterNMachines\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-11\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession3);
        ccodSession3.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();

        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 4)
    public void error4SiteFilter1SiteEmail() throws IOException, MessagingException, InterruptedException, MqttException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error4SiteFilter1SiteE\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error4SiteFilter1SiteE"));
        Assert.assertTrue(emailContent.contains("new Site A"));
        Assert.assertTrue(emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 5)
    public void error5SiteFilterNSitesEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error5SiteFilterNSites\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\",\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession3);
        ccodSession3.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 6)
    public void error6SiteAndMachineFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error6SiteAndMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],trigger:{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\"}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error6SiteAndMachineFilter"));
        Assert.assertTrue(emailContent.contains("Site B"));
        Assert.assertTrue(emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 7)
    public void error7ErrorKind1KindEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error7ErrorKind1Kind\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Configuration\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.KIND="Configuration";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.KIND="Communication";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error7ErrorKind1Kind"));
        Assert.assertTrue(emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 8)
    public void error8ErrorKindNKindsEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error8ErrorKindNKinds\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Communication\",\"Configuration\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.KIND="Configuration";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.KIND="Communication";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession3);
        rmPayload3.KIND="Reminder";
        ccodSession3.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 9)
    public void error9ErrorKindWithMachineFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error9ErrorKindWithMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Discrepancy\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-11\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.KIND="Discrepancy";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.KIND="Discrepancy";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error9ErrorKindWithMachineFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 10)
    public void error10ErrorKindWithSiteFilterEmail() throws MessagingException, IOException, InterruptedException, MqttException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error10ErrorKindWithSiteFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Statistics\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.KIND="Statistics";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.KIND="Statistics";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error10ErrorKindWithSiteFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }


    @Test(priority = 11)
    public void error11ErrorKindWithSiteAndMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error11ErrorKindWithSiteAndMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],trigger:{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Other\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\"}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.KIND="Other";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.KIND="Other";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error11ErrorKindWithSiteAndMachineFilter"));
        Assert.assertTrue(emailContent.contains("Site B"));
        Assert.assertTrue(emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test (priority = 12)
    public void error11ErrorSeverity1SeverityEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error11ErrorSeverity1Severity\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Error\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.SEVERITY="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error11ErrorSeverity1Severity"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 13)
    public void error12ErrorSeverityNSeveritiesEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error12ErrorSeverityNSeverities\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Unknown\",\"Warning\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.SEVERITY="Warning";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession3);
        rmPayload3.SEVERITY="Error";
        ccodSession3.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 14)
    public void error13ErrorSeverityWithMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error13ErrorSeverityWithMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Error\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.SEVERITY="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.SEVERITY="Error";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error13ErrorSeverityWithMachineFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));


        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 14)
    public void error14ErrorSeverityWithSiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error14ErrorSeverityWithSiteFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Warning\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.SEVERITY="Warning";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error14ErrorSeverityWithSiteFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 15)
    public void error15ErrorSeverityWithKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error15ErrorSeverityWithKindFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Warning\"],\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"IO\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.SEVERITY="Warning";
        rmPayload1.KIND="IO";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.SEVERITY="Warning";
        rmPayload2.KIND="Malfunction";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession3);
        rmPayload3.SEVERITY="Error";
        rmPayload3.KIND="IO";
        ccodSession3.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );


        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error15ErrorSeverityWithKindFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));


        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 16)
    public void error16ErrorSeverityWithSiteAndMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error16ErrorSeverityWithSiteAndMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],trigger:{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Warning\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\"}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.SEVERITY="Warning";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.SEVERITY="Error";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error16ErrorSeverityWithSiteAndMachineFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 17)
    public void error17ErrorSeverityWithKindAndSiteAndMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-trigger-error17ErrorSeverityWithKindAndSiteAndMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],trigger:{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Unknown\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"InputOverLimit\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\"}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.SEVERITY="Unknown";
        rmPayload1.KIND="InputOverLimit";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.SEVERITY="Unknown";
        rmPayload2.KIND="InputOverLimit";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.SEVERITY="Error";
        rmPayload3.KIND="InputOverLimit";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.SEVERITY="Unknown";
        rmPayload4.KIND="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-trigger-error17ErrorSeverityWithKindAndSiteAndMachineFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));


        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 18)
    public void error18ErrorClearedFalseEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error18ErrorClearedFalseEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"false\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.CLEARED=false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error18ErrorClearedFalseEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));


        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 19)
    public void error19ErrorClearedTrueEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error19ErrorClearedTrueEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.CLEARED=false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos = emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error19ErrorClearedTrueEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));


        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 20)
    public void error20ErrorClearedFalseTrueEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error20ErrorClearedFalseTrueEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"false\",\"true\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.CLEARED=false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 21)
    public void error21ErrorClearedMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"error21ErrorClearedFalseMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"false\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.CLEARED=false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("error21ErrorClearedFalseMachineFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

 @Test(priority = 22)
    public void error22ErrorClearedSiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
     //create a rule
     String payload = "{\"name\":\"ra-error22ErrorClearedSiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
     String response = given().
             headers(headers).
             header("Content-type","application/json").
             body(payload).
             when().
             post(url+"/rule").
             then().
             statusCode(201).
             extract().asString();
     alertRules.add(new Gson().fromJson(response, Alert.class));

     //send rm message - trigger
     RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
     rmPayload1.CLEARED=true;
     ccodSession1.sendMqttEvent(
             EventType.ERROR,
             Utilities.getCurrentDateStampIncludedCatchingUp(),
             RmHandler.parseRmToString(rmPayload1)
     );
     //send rm message - don't trigger
     RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
     rmPayload2.CLEARED=false;
     ccodSession1.sendMqttEvent(
             EventType.ERROR,
             Utilities.getCurrentDateStampIncludedCatchingUp(),
             RmHandler.parseRmToString(rmPayload2)
     );
     RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
     rmPayload2.CLEARED=true;
     ccodSession2.sendMqttEvent(
             EventType.ERROR,
             Utilities.getCurrentDateStampIncludedCatchingUp(),
             RmHandler.parseRmToString(rmPayload3)
     );

     //check if email is sent, delete email message
     delay();
     emailHandler.connect();
     List<Message> emailMessages = emailHandler.getMessages();
     Assert.assertNotNull(emailMessages);
     Assert.assertEquals(emailMessages.size(),1);
     List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
     List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
     String emailTitle = emailMessages.get(0).getSubject();
     emailHandler.purgeInbox();
     emailHandler.disconnect();

     //validate message
     Assert.assertTrue(emailTitle.contains("ra-error22ErrorClearedSiteFilterEmail"));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));

     //delete rule
     deleteAlertRules();
     delay();

    }

    @Test(priority = 23)
    public void error23ErrorClearedKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
     //create a rule
     String payload = "{\"name\":\"ra-error23ErrorClearedKindFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"false\"],\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Reminder\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
     String response = given().
             headers(headers).
             header("Content-type","application/json").
             body(payload).
             when().
             post(url+"/rule").
             then().
             statusCode(201).
             extract().asString();
     alertRules.add(new Gson().fromJson(response, Alert.class));

     //send rm message - trigger
     RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
     rmPayload1.CLEARED=false;
     rmPayload1.KIND="Reminder";
     ccodSession1.sendMqttEvent(
             EventType.ERROR,
             Utilities.getCurrentDateStampIncludedCatchingUp(),
             RmHandler.parseRmToString(rmPayload1)
     );
     //send rm message - don't trigger
     RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
     rmPayload2.CLEARED=true;
     rmPayload2.KIND="Reminder";
     ccodSession1.sendMqttEvent(
             EventType.ERROR,
             Utilities.getCurrentDateStampIncludedCatchingUp(),
             RmHandler.parseRmToString(rmPayload2)
     );
     RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
     rmPayload3.CLEARED=false;
     rmPayload3.KIND="Malfunction";

     ccodSession1.sendMqttEvent(
             EventType.ERROR,
             Utilities.getCurrentDateStampIncludedCatchingUp(),
             RmHandler.parseRmToString(rmPayload3)
     );


     //check if email is sent, delete email message
     delay();
     emailHandler.connect();
     List<Message> emailMessages = emailHandler.getMessages();
     Assert.assertNotNull(emailMessages);
     Assert.assertEquals(emailMessages.size(),1);
     List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
     List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
     String emailTitle = emailMessages.get(0).getSubject();
     emailHandler.purgeInbox();
     emailHandler.disconnect();

     //validate message
     Assert.assertTrue(emailTitle.contains("ra-error23ErrorClearedKindFilterEmail"));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));
     Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

     //delete rule
     deleteAlertRules();
     delay();

    }

    @Test(priority = 24)
    public void error24ErrorClearedSeverityFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error24ErrorClearedSeverityFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"],\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Error\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.CLEARED=true;
        rmPayload1.SEVERITY="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.CLEARED=true;
        rmPayload2.SEVERITY="Warning";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.CLEARED=false;
        rmPayload3.KIND="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error24ErrorClearedSeverityFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 25)
    public void error25ErrorClearedMachineSiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error25ErrorClearedMachineSiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"true\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\"}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload3.CLEARED=true;
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error25ErrorClearedMachineSiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 26)
    public void error26ErrorClearedMachineSiteKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error26ErrorClearedMachineSiteKindFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"false\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Jam\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\"}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.CLEARED=false;
        rmPayload1.KIND="Jam";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.CLEARED=false;
        rmPayload2.KIND="Busy";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.CLEARED=true;
        rmPayload3.KIND="Jam";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload4.CLEARED=false;
        rmPayload4.KIND="Jam";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error26ErrorClearedMachineSiteKindFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 27)
    public void error27ErrorClearedMachineSiteKindSeverityFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error27ErrorClearedMachineSiteKindSeverityFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"false\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Jam\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Unknown\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\"}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.CLEARED=false;
        rmPayload1.KIND="Jam";
        rmPayload1.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.CLEARED=true;
        rmPayload2.KIND="Jam";
        rmPayload2.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.CLEARED=false;
        rmPayload3.KIND="Communication";
        rmPayload3.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.CLEARED=false;
        rmPayload4.KIND="Jam";
        rmPayload4.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload5.CLEARED=false;
        rmPayload5.KIND="Jam";
        rmPayload5.SEVERITY="Unknown";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error27ErrorClearedMachineSiteKindSeverityFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 28)
    public void error28ErrorLocation1LocationEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error28ErrorLocation1LocationEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"UPS\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/rule").
            then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="UPS";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.LOCATION="NoteDispense";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error28ErrorLocation1LocationEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 29)
    public void error29ErrorLocationNLocationEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error29ErrorLocationNLocationEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"Barcode\",\"DropSafe\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="Barcode";
        rmPayload1.CLEARED = false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.LOCATION="DropSafe";
        rmPayload2.CLEARED = true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.LOCATION="NoteRecycler";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 30)
    public void error30ErrorLocationMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error30ErrorLocationMachineFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"CoinDispense\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="CoinDispense";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.LOCATION="CoinDispense";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error30ErrorLocationMachineFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 31)
    public void error31ErrorLocationSiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error31ErrorLocationSiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"TillDrawer\",\"TransportBox\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="TransportBox";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.LOCATION="CoinDispense";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.LOCATION="TransportBox";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error31ErrorLocationSiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.LOCATION));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 32)
    public void error32ErrorLocationKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error32ErrorLocationKindFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"Software\"],\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Communication\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="Software";
        rmPayload1.KIND="Communication";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.LOCATION="Keypad";
        rmPayload2.KIND="Communication";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.LOCATION="Software";
        rmPayload3.KIND="Power";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error32ErrorLocationKindFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 33)
    public void error33ErrorLocationSeverityFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error33ErrorLocationSeverityFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"CoinDeposit\"],\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Warning\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="CoinDeposit";
        rmPayload1.SEVERITY="Warning";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.LOCATION="Keypad";
        rmPayload2.SEVERITY="Warning";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.LOCATION="CoinDeposit";
        rmPayload3.SEVERITY="Unknown";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error33ErrorLocationSeverityFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 34)
    public void error34ErrorLocationClearedFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error34ErrorLocationClearedFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"TillDrawer\"],\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.LOCATION="TillDrawer";
        rmPayload1.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.LOCATION="TillDrawer";
        rmPayload2.CLEARED = false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.LOCATION="NoteDeposit";
        rmPayload3.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error34ErrorLocationClearedFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+Boolean.toString(rmPayload1.CLEARED)+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 35)
    public void error35ErrorLocationSiteMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error35ErrorLocationSiteMachineFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"NoteRecycler\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\"}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.LOCATION="NoteRecycler";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.LOCATION="Network";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload3.LOCATION="NoteRecycler";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload4.LOCATION="NoteRecycler";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error35ErrorLocationSiteMachineFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 36)
    public void error36ErrorLocationSiteMachineKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error36ErrorLocationSiteMachineKindFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"NoteRecycler\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Communication\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\"}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.LOCATION="NoteRecycler";
        rmPayload1.KIND="Communication";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.LOCATION="CoinDispense";
        rmPayload2.KIND="Communication";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.LOCATION="NoteRecycler";
        rmPayload3.KIND="RemoteUpdate";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload4.LOCATION="NoteRecycler";
        rmPayload4.KIND="Communication";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error36ErrorLocationSiteMachineKindFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 37)
    public void error37ErrorLocationSiteMachineKindSeverityFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error37ErrorLocationSiteMachineKindSeverity\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"NoteRecycler\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Communication\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Warning\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\"}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.LOCATION="NoteRecycler";
        rmPayload1.KIND="Communication";
        rmPayload1.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.LOCATION="Door";
        rmPayload2.KIND="Communication";
        rmPayload2.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.LOCATION="NoteRecycler";
        rmPayload3.KIND="Power";
        rmPayload3.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.LOCATION="NoteRecycler";
        rmPayload4.KIND="Communication";
        rmPayload4.SEVERITY="Error";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload5.LOCATION="NoteRecycler";
        rmPayload5.KIND="Communication";
        rmPayload5.SEVERITY="Warning";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error37ErrorLocationSiteMachineKindSeverity"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 38)
    public void error38ErrorLocationSiteMachineKindSeverityClearedFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error38ErrorLocationSiteMachineKindSeverityClearedFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"filter\":[\"NoteRecycler\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"filter\":[\"Communication\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Warning\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"true\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\"}}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.LOCATION="NoteRecycler";
        rmPayload1.KIND="Communication";
        rmPayload1.SEVERITY="Warning";
        rmPayload1.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.LOCATION="Alarm";
        rmPayload2.KIND="Communication";
        rmPayload2.SEVERITY="Warning";
        rmPayload2.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.LOCATION="NoteRecycler";
        rmPayload3.KIND="Reminder";
        rmPayload3.SEVERITY="Warning";
        rmPayload3.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.LOCATION="NoteRecycler";
        rmPayload4.KIND="Communication";
        rmPayload4.SEVERITY="Unknown";
        rmPayload4.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.LOCATION="NoteRecycler";
        rmPayload5.KIND="Communication";
        rmPayload5.SEVERITY="Warning";
        rmPayload5.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmError rmPayload6 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload6.LOCATION="Alarm";
        rmPayload6.KIND="Communication";
        rmPayload6.SEVERITY="Warning";
        rmPayload6.CLEARED=true;
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error38ErrorLocationSiteMachineKindSeverityClearedFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 39)
    public void error39ErrorInformationWholeInformationPEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error39ErrorInformationWholeInformationPEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error39ErrorInformationWholeInformationPEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation("+rmPayload1.INFORMATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 40)
    public void error40ErrorInformationPrefixEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error40ErrorInformationPrefixEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License:\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION= errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error40ErrorInformationPrefixEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License:)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 41)
    public void error41ErrorInformationSuffixEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error41ErrorInformationSuffixEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"disable SDK\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION= errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error41ErrorInformationSuffixEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(disable SDK)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 42)
    public void error42ErrorInformationSubStringEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error42ErrorInformationSubStringEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"license does not support custom plugins\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error42ErrorInformationSubStringEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(license does not support custom plugins)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 43)
    public void error43ErrorInformationMachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error43ErrorInformationMachineEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error43ErrorInformationMachineEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 44)
    public void error44ErrorInformationSiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error44ErrorInformationSiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license\"],\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error44ErrorInformationSiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 45)
    public void error45ErrorInformationKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error45ErrorInformationKindFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Other\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.KIND="Other";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        rmPayload2.KIND="Other";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.INFORMATION=errInfoTrigger;
        rmPayload3.KIND="Communication";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error45ErrorInformationKindFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 46)
    public void error46ErrorInformationSeverityFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error46ErrorInformationSeverityFilterEmailerror46ErrorInformationSeverityFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Error\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.SEVERITY="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        rmPayload2.SEVERITY="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.INFORMATION=errInfoTrigger;
        rmPayload3.SEVERITY="Warning";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error46ErrorInformationSeverityFilterEmailerror46ErrorInformationSeverityFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 47)
    public void error47ErrorInformationClearedFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error47ErrorInformationClearedFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        rmPayload2.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.INFORMATION=errInfoTrigger;
        rmPayload3.CLEARED=false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error47ErrorInformationClearedFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 48)
    public void error48ErrorInformationLocationFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error48ErrorInformationLocationFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"Software\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.LOCATION="Software";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        rmPayload2.LOCATION="Software";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.INFORMATION=errInfoTrigger;
        rmPayload3.LOCATION="Barcode";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error48ErrorInformationLocationFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 49)
    public void error49ErrorInformationMachineSiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error49ErrorInformationMachineSiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"type\":\"ErrorInformation\",\"typeClass\":\"listFilter\"}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload3.INFORMATION=errInfoTrigger;
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error49ErrorInformationMachineSiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 50)
    public void error50ErrorInformationMachineSiteKindFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error50ErrorInformationMachineSiteKindFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"type\":\"ErrorInformation\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Malfunction\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\"}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.KIND="Malfunction";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.INFORMATION=errInfoDontTrigger;
        rmPayload2.KIND="Malfunction";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.INFORMATION=errInfoTrigger;
        rmPayload3.KIND="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4= ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload4.INFORMATION=errInfoTrigger;
        rmPayload4.KIND="Malfunction";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error50ErrorInformationMachineSiteKindFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 51)
    public void error51ErrorInformationMachineSiteKindSeverityFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error51ErrorInformationMachineSiteKindSeverityFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"type\":\"ErrorInformation\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Malfunction\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Warning\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\"}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.KIND="Malfunction";
        rmPayload1.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
         RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
         rmPayload2.INFORMATION=errInfoTrigger;
         rmPayload2.KIND="Malfunction";
         rmPayload2.SEVERITY="Warning";
         ccodSession4.sendMqttEvent(
                 EventType.ERROR,
                 Utilities.getCurrentDateStampIncludedCatchingUp(),
                 RmHandler.parseRmToString(rmPayload2)
         );
         RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
         rmPayload3.INFORMATION=errInfoDontTrigger;
         rmPayload3.KIND="Malfunction";
         rmPayload3.SEVERITY="Warning";
         ccodSession2.sendMqttEvent(
                 EventType.ERROR,
                 Utilities.getCurrentDateStampIncludedCatchingUp(),
                 RmHandler.parseRmToString(rmPayload3)
         );
         RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
         rmPayload4.INFORMATION=errInfoTrigger;
         rmPayload4.KIND="Configuration";
         rmPayload4.SEVERITY="Warning";
         ccodSession2.sendMqttEvent(
                 EventType.ERROR,
                 Utilities.getCurrentDateStampIncludedCatchingUp(),
                 RmHandler.parseRmToString(rmPayload4)
         );
         RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
         rmPayload5.INFORMATION=errInfoTrigger;
         rmPayload5.KIND="Malfunction";
         rmPayload5.SEVERITY="Error";
         ccodSession2.sendMqttEvent(
                 EventType.ERROR,
                 Utilities.getCurrentDateStampIncludedCatchingUp(),
                 RmHandler.parseRmToString(rmPayload5)
         );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error51ErrorInformationMachineSiteKindSeverityFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 52)
    public void error52ErrorInformationMachineSiteKindSeverityClearedFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error52ErrorInformationMachineSiteKindSeverityClearedFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"type\":\"ErrorInformation\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Malfunction\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Warning\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"true\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\"}}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.KIND="Malfunction";
        rmPayload1.SEVERITY="Warning";
        rmPayload1.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.INFORMATION=errInfoTrigger;
        rmPayload2.KIND="Malfunction";
        rmPayload2.SEVERITY="Warning";
        rmPayload2.CLEARED=true;
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.INFORMATION=errInfoDontTrigger;
        rmPayload3.KIND="Malfunction";
        rmPayload3.SEVERITY="Warning";
        rmPayload3.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.INFORMATION=errInfoTrigger;
        rmPayload4.KIND="Configuration";
        rmPayload4.SEVERITY="Warning";
        rmPayload4.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.INFORMATION=errInfoTrigger;
        rmPayload5.KIND="Malfunction";
        rmPayload5.SEVERITY="Error";
        rmPayload5.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmError rmPayload6 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload6.INFORMATION=errInfoTrigger;
        rmPayload6.KIND="Malfunction";
        rmPayload6.SEVERITY="Warning";
        rmPayload6.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );
        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error52ErrorInformationMachineSiteKindSeverityClearedFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 53)
    public void error53ErrorInformationMachineSiteKindSeverityClearedLocationFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error53ErrorInformationMachineSiteKindSeverityClearedLocationFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"type\":\"ErrorInformation\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Malfunction\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Warning\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"true\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"CustomerPrinter\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\"}}}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.INFORMATION=errInfoTrigger;
        rmPayload1.KIND="Malfunction";
        rmPayload1.SEVERITY="Warning";
        rmPayload1.CLEARED=true;
        rmPayload1.LOCATION="CustomerPrinter";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.INFORMATION=errInfoTrigger;
        rmPayload2.KIND="Malfunction";
        rmPayload2.SEVERITY="Warning";
        rmPayload2.CLEARED=true;
        rmPayload2.LOCATION="CustomerPrinter";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.INFORMATION=errInfoDontTrigger;
        rmPayload3.KIND="Malfunction";
        rmPayload3.SEVERITY="Warning";
        rmPayload3.CLEARED=true;
        rmPayload3.LOCATION="CustomerPrinter";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.INFORMATION=errInfoTrigger;
        rmPayload4.KIND="Configuration";
        rmPayload4.SEVERITY="Warning";
        rmPayload4.CLEARED=true;
        rmPayload4.LOCATION="CustomerPrinter";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.INFORMATION=errInfoTrigger;
        rmPayload5.KIND="Malfunction";
        rmPayload5.SEVERITY="Error";
        rmPayload5.CLEARED=true;
        rmPayload5.LOCATION="CustomerPrinter";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmError rmPayload6 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload6.INFORMATION=errInfoTrigger;
        rmPayload6.KIND="Malfunction";
        rmPayload6.SEVERITY="Warning";
        rmPayload6.CLEARED=false;
        rmPayload6.LOCATION="CustomerPrinter";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );
        RmError rmPayload7 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload7.INFORMATION=errInfoTrigger;
        rmPayload7.KIND="Malfunction";
        rmPayload7.SEVERITY="Warning";
        rmPayload7.CLEARED=true;
        rmPayload7.LOCATION="Display";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload7)
        );
        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error53ErrorInformationMachineSiteKindSeverityClearedLocationFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation(Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 54)
    public void error54ErrorTimeOfDayEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error54ErrorTimeOfDayEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:59:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:01:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error54ErrorTimeOfDayEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 55)
    public void error55ErrorTimeOfDayBorderValuesEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error55ErrorTimeOfDayBorderValues\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:00:00.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 11:59:59.999";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        //send rm message - don't trigger
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload4.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:59:59.999";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload5.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:00:00.001";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),3);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 56)
    public void error56ErrorTimeOfDaySiteEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error56ErrorTimeOfDaySiteEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error56ErrorTimeOfDaySiteEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 57)
    public void error57ErrorTimeOfDayMachineEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error57ErrorTimeOfDayMachineEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error57ErrorTimeOfDayMachineEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 58)
    public void error58ErrorTimeOfDayKindEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error58ErrorTimeOfDayKindEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"ErrorKind\",\"filter\":[\"Discrepancy\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.KIND="Discrepancy";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload2.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error58ErrorTimeOfDayKindEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 59)
    public void error59ErrorTimeOfDaySeverityEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error59ErrorTimeOfDaySeverityEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"ErrorSeverity\",\"filter\":[\"Unknown\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.SEVERITY="Unknown";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:39:34.601";
        rmPayload2.SEVERITY="Unknown";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload3.SEVERITY="Error";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );


        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error59ErrorTimeOfDaySeverityEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }
    @Test(priority = 56)
    public void error56ErrorTimeOfDayClearedEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error56ErrorTimeOfDayClearedEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"ErrorCleared\",\"filter\":[\"true\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:39:34.601";
        rmPayload2.CLEARED=true;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload3.CLEARED=false;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error56ErrorTimeOfDayClearedEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 57)
    public void error57ErrorTimeOfDayLocationEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error57ErrorTimeOfDayLocation\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"ErrorLocation\",\"filter\":[\"Keypad\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.LOCATION="Keypad";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:39:34.601";
        rmPayload2.LOCATION="Keypad";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload3.LOCATION="NoteDeposit";
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error57ErrorTimeOfDayLocation"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 58)
    public void error58ErrorTimeOfDayInformationEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error58ErrorTimeOfDayInformationEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"ErrorInformation\",\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Error\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:39:34.601";
        rmPayload2.LOCATION=errInfoTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession1);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload3.INFORMATION=errInfoDontTrigger;
        ccodSession1.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error58ErrorTimeOfDayInformationEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation("+rmPayload1.INFORMATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 59)
    public void error59ErrorTimeOfDaySiteMachineEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error59ErrorTimeOfDaySiteMachineEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"type\":\"TimeOfDay\",\"typeClass\":\"timeFilter\"}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error59ErrorTimeOfDaySiteMachineEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 60)
    public void error60ErrorTimeOfDaySiteMachineKindEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error60ErrorTimeOfDaySiteMachineKindEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"type\":\"TimeOfDay\",\"typeClass\":\"timeFilter\",\"child\":{\"filter\":[\"Other\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\"}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.KIND ="Other";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.KIND ="Other";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:39:34.601";
        rmPayload3.KIND ="Other";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload4.KIND ="Power";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error60ErrorTimeOfDaySiteMachineKindEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 61)
    public void error61ErrorTimeOfDaySiteMachineKindSeverityEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error61ErrorTimeOfDaySiteMachineKindSeverityEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"type\":\"TimeOfDay\",\"typeClass\":\"timeFilter\",\"child\":{\"filter\":[\"Other\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Unknown\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\"}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.KIND ="Other";
        rmPayload1.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.KIND ="Other";
        rmPayload2.SEVERITY="Unknown";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 08:39:34.601";
        rmPayload3.KIND ="Other";
        rmPayload3.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload4.KIND ="Communication";
        rmPayload4.SEVERITY="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload5.KIND ="Other";
        rmPayload5.SEVERITY="Warning";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error61ErrorTimeOfDaySiteMachineKindSeverityEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 62)
    public void error62ErrorTimeOfDaySiteMachineKindClearedSeverityEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error62ErrorTimeOfDaySiteMachineKindClearedSeverityEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"type\":\"TimeOfDay\",\"typeClass\":\"timeFilter\",\"child\":{\"filter\":[\"Other\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Unknown\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"false\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\"}}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.KIND ="Other";
        rmPayload1.SEVERITY="Unknown";
        rmPayload1.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.KIND ="Other";
        rmPayload2.SEVERITY="Unknown";
        rmPayload2.CLEARED=false;
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 13:39:34.601";
        rmPayload3.KIND ="Other";
        rmPayload3.SEVERITY="Unknown";
        rmPayload3.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload4.KIND ="Communication";
        rmPayload4.SEVERITY="Unknown";
        rmPayload4.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload5.KIND ="Other";
        rmPayload5.SEVERITY="Error";
        rmPayload5.CLEARED=false;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmError rmPayload6 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload6.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload6.KIND ="Other";
        rmPayload6.SEVERITY="Unknown";
        rmPayload6.CLEARED=true;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error62ErrorTimeOfDaySiteMachineKindClearedSeverityEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 63)
    public void error63ErrorTimeOfDaySiteMachineKindClearedSeverityLocationEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error63ErrorTimeOfDaySiteMachineKindClearedSeverityLocationEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"type\":\"TimeOfDay\",\"typeClass\":\"timeFilter\",\"child\":{\"filter\":[\"Other\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Unknown\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"false\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"NoteRecycler\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\"}}}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.KIND ="Other";
        rmPayload1.SEVERITY="Unknown";
        rmPayload1.CLEARED=false;
        rmPayload1.LOCATION="NoteRecycler";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.KIND ="Other";
        rmPayload2.SEVERITY="Unknown";
        rmPayload2.CLEARED=false;
        rmPayload2.LOCATION="NoteRecycler";
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 13:39:34.601";
        rmPayload3.KIND ="Other";
        rmPayload3.SEVERITY="Unknown";
        rmPayload3.CLEARED=false;
        rmPayload3.LOCATION="NoteRecycler";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload4.KIND ="Communication";
        rmPayload4.SEVERITY="Unknown";
        rmPayload4.CLEARED=false;
        rmPayload4.LOCATION="NoteRecycler";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload5.KIND ="Other";
        rmPayload5.SEVERITY="Error";
        rmPayload5.CLEARED=false;
        rmPayload5.LOCATION="NoteRecycler";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmError rmPayload6 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload6.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload6.KIND ="Other";
        rmPayload6.SEVERITY="Unknown";
        rmPayload6.CLEARED=true;
        rmPayload6.LOCATION="NoteRecycler";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );
        RmError rmPayload7 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload7.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload7.KIND ="Other";
        rmPayload7.SEVERITY="Unknown";
        rmPayload7.CLEARED=true;
        rmPayload7.LOCATION="Sensor";
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload7)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error63ErrorTimeOfDaySiteMachineKindClearedSeverityLocationEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 64)
    public void error64ErrorTimeOfDaySiteMachineKindClearedSeverityLocationInformationEmail() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-error64ErrorTimeOfDaySiteMachineKindClearedSeverityLocationInformationEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"Error\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"freeText\":true,\"filter\":[\"JM-11\"],\"type\":\"MachineIdFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"freeText\":true,\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"],\"type\":\"SiteFilter\",\"typeClass\":\"listFilterLookup\",\"child\":{\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"type\":\"TimeOfDay\",\"typeClass\":\"timeFilter\",\"child\":{\"filter\":[\"Other\"],\"type\":\"ErrorKind\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"Unknown\"],\"type\":\"ErrorSeverity\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"false\"],\"type\":\"ErrorCleared\",\"typeClass\":\"listFilter\",\"child\":{\"filter\":[\"NoteRecycler\"],\"type\":\"ErrorLocation\",\"typeClass\":\"listFilter\",\"child\":{\"freeText\":true,\"filter\":[\"Software License: Your software license does not support custom plugins (SDK). Get a new license or disable SDK\"],\"type\":\"ErrorInformation\",\"typeClass\":\"listFilter\"}}}}}}}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmError rmPayload1 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.KIND ="Other";
        rmPayload1.SEVERITY="Unknown";
        rmPayload1.CLEARED=false;
        rmPayload1.LOCATION="NoteRecycler";
        rmPayload1.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmError rmPayload2 = ccodRMGenerator.generateRmError(ccodSession4);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.KIND ="Other";
        rmPayload2.SEVERITY="Unknown";
        rmPayload2.CLEARED=false;
        rmPayload2.LOCATION="NoteRecycler";
        rmPayload2.INFORMATION=errInfoTrigger;
        ccodSession4.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmError rmPayload3 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 13:39:34.601";
        rmPayload3.KIND ="Other";
        rmPayload3.SEVERITY="Unknown";
        rmPayload3.CLEARED=false;
        rmPayload3.LOCATION="NoteRecycler";
        rmPayload3.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmError rmPayload4 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload4.KIND ="Communication";
        rmPayload4.SEVERITY="Unknown";
        rmPayload4.CLEARED=false;
        rmPayload4.LOCATION="NoteRecycler";
        rmPayload4.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmError rmPayload5 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload5.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload5.KIND ="Other";
        rmPayload5.SEVERITY="Error";
        rmPayload5.CLEARED=false;
        rmPayload5.LOCATION="NoteRecycler";
        rmPayload5.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmError rmPayload6 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload6.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload6.KIND ="Other";
        rmPayload6.SEVERITY="Unknown";
        rmPayload6.CLEARED=true;
        rmPayload6.LOCATION="NoteRecycler";
        rmPayload6.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );
        RmError rmPayload7 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload7.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload7.KIND ="Other";
        rmPayload7.SEVERITY="Unknown";
        rmPayload7.CLEARED=true;
        rmPayload7.LOCATION="Sensor";
        rmPayload7.INFORMATION=errInfoTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload7)
        );
        RmError rmPayload8 = ccodRMGenerator.generateRmError(ccodSession2);
        rmPayload8.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload8.KIND ="Other";
        rmPayload8.SEVERITY="Unknown";
        rmPayload8.CLEARED=true;
        rmPayload8.LOCATION="Sensor";
        rmPayload8.INFORMATION=errInfoDontTrigger;
        ccodSession2.sendMqttEvent(
                EventType.ERROR,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload8)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-error64ErrorTimeOfDaySiteMachineKindClearedSeverityLocationInformationEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.KIND));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.SEVERITY));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.MACHINE_ID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorKind("+rmPayload1.KIND+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorSeverity("+rmPayload1.SEVERITY+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorCleared("+rmPayload1.CLEARED+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorLocation("+rmPayload1.LOCATION+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ErrorInformation("+rmPayload1.INFORMATION+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 65)
    public void systemStatus1NoFiltersEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus1NoFiltersEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus1NoFiltersEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 66)
    public void systemStatus2MachineFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus2MachineFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );


        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus2MachineFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 67)
    public void systemStatus3MachineFilterNMachinesEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus3MachineFilterNMachinesEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-11\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession3);
        ccodSession3.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 67)
    public void systemStatus4SiteFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus4SiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );


        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus4SiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 68)
    public void systemStatus5MessageStatus1MSFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus5MessageStatus1MS\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MessageStatus\",\"filter\":[\"Boot\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.STATUS="Boot";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.STATUS="Idle";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus5MessageStatus1MS"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageStatus("+rmPayload1.STATUS+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 69)
    public void systemStatus6MessageStatusNMSFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus5MessageStatus1MS\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MessageStatus\",\"filter\":[\"Boot\",\"Idle\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.STATUS="Boot";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.STATUS="Idle";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload3.STATUS="Unknown";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 70)
    public void systemStatus7MessageOldStatus1MSFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus6MessageOldStatus1MSFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MessageOldStatus\",\"filter\":[\"Identify\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.OLD_STATUS="Identify";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.OLD_STATUS="Idle";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus6MessageOldStatus1MSFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageOldStatus("+rmPayload1.OLD_STATUS+")"));

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 71)
    public void systemStatus8MessageOldStatusNMOSFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus7MessageOldStatusNMOSFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MessageOldStatus\",\"filter\":[\"OutOfUse\",\"OutOfUseByHost\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.OLD_STATUS="OutOfUse";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.OLD_STATUS="OutOfUseByHost";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload3.OLD_STATUS="Idle";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);
        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();

    }

    @Test(priority = 72)
    public void systemStatus9DoorOpen1DoorFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus8DoorOpen1DoorFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DoorOpen\",\"filter\":[\"COIN_DISPENSER\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.DOORS = new LinkedList<>();
        rmPayload1.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.DOORS = new LinkedList<>();
        rmPayload2.DOORS.add(new RmDoor("OPEN","NOTE_RECYCLER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload3.DOORS = new LinkedList<>();
        rmPayload3.DOORS.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus8DoorOpen1DoorFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DoorOpen("+rmPayload1.DOORS.get(0).NAME+")"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 73)
    public void systemStatus10DoorOpenNDoorsFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus9DoorOpenNDoorsFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DoorOpen\",\"filter\":[\"COIN_DISPENSER\",\"TRANSPORTBOX\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.DOORS = new LinkedList<>();
        rmPayload1.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload2.DOORS = new LinkedList<>();
        rmPayload2.DOORS.add(new RmDoor("OPEN","TRANSPORTBOX"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload3.DOORS = new LinkedList<>();
        rmPayload3.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload3.DOORS.add(new RmDoor("OPEN","TRANSPORTBOX"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload4 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload4.DOORS = new LinkedList<>();
        rmPayload4.DOORS.add(new RmDoor("OPEN","NOTE_RECYCLER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmSystemStatus rmPayload5 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload5.DOORS = new LinkedList<>();
        rmPayload5.DOORS.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        rmPayload5.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),3);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 74)
    public void systemStatus11DoorClosed1DoorFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus8DoorClosed1DoorFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DoorClosed\",\"filter\":[\"COIN_DISPENSER\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.DOORS = new LinkedList<>();
        rmPayload1.DOORS.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.DOORS = new LinkedList<>();
        rmPayload2.DOORS.add(new RmDoor("CLOSED","NOTE_RECYCLER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload3.DOORS = new LinkedList<>();
        rmPayload3.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus8DoorClosed1DoorFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DoorClosed("+rmPayload1.DOORS.get(0).NAME+")"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 75)
    public void systemStatus12DoorClosedNDoorsFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus10DoorClosedNDoorsFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DoorClosed\",\"filter\":[\"COIN_DISPENSER\",\"TRANSPORTBOX\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.DOORS = new LinkedList<>();
        rmPayload1.DOORS.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload2.DOORS = new LinkedList<>();
        rmPayload2.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload3.DOORS = new LinkedList<>();
        rmPayload3.DOORS.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        rmPayload3.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload4 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload4.DOORS = new LinkedList<>();
        rmPayload4.DOORS.add(new RmDoor("CLOSED","NOTE_RECYCLER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmSystemStatus rmPayload5 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload5.DOORS = new LinkedList<>();
        rmPayload5.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload5.DOORS.add(new RmDoor("OPEN","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),3);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 76)
    public void systemStatus13TimeOfDayFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus9TimeOfDayFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus9TimeOfDayFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 77)
    public void systemStatus14TimeOfDayBVAFilterEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus9TimeOfDayFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:00:00.001";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 11:59:59.999";
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:59:59.999";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmSystemStatus rmPayload4 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:00:00.001";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 78)
    public void systemStatus15AllFiltersComboEmail() throws IOException, InterruptedException, MqttException, MessagingException {
        //create a rule
        String payload = "{\"name\":\"ra-systemStatus11AllFiltersComboEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"MessageStatus\",\"filter\":[\"Idle\"],\"child\":{\"type\":\"MessageOldStatus\",\"filter\":[\"User\"],\"child\":{\"type\":\"DoorOpen\",\"filter\":[\"COIN_DISPENSER\"],\"child\":{\"type\":\"DoorClosed\",\"filter\":[\"TRANSPORTBOX\"],\"child\":{\"type\":\"Or\",\"typeClass\":\"BiNode\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"]},\"otherChild\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-11\"]}}}}}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"SystemStatus\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload1.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.STATUS="Idle";
        rmPayload1.OLD_STATUS="User";
        rmPayload1.DOORS = new LinkedList<>();
        rmPayload1.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload1.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession1);
        rmPayload2.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.STATUS="Idle";
        rmPayload2.OLD_STATUS="User";
        rmPayload2.DOORS = new LinkedList<>();
        rmPayload2.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload2.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession1.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmSystemStatus rmPayload3 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload3.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:39:34.601";
        rmPayload3.STATUS="Idle";
        rmPayload3.OLD_STATUS="User";
        rmPayload3.DOORS = new LinkedList<>();
        rmPayload3.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload3.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmSystemStatus rmPayload4 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload4.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload4.STATUS="User";
        rmPayload4.OLD_STATUS="User";
        rmPayload4.DOORS = new LinkedList<>();
        rmPayload4.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload4.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );
        RmSystemStatus rmPayload5 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload5.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload5.STATUS="Idle";
        rmPayload5.OLD_STATUS="Idle";
        rmPayload5.DOORS = new LinkedList<>();
        rmPayload5.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload5.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload5)
        );
        RmSystemStatus rmPayload6 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload6.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload6.STATUS="Idle";
        rmPayload6.OLD_STATUS="User";
        rmPayload6.DOORS = new LinkedList<>();
        rmPayload6.DOORS.add(new RmDoor("CLOSED","COIN_DISPENSER"));
        rmPayload6.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload6)
        );
        RmSystemStatus rmPayload7 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload7.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload7.STATUS="Idle";
        rmPayload7.OLD_STATUS="User";
        rmPayload7.DOORS = new LinkedList<>();
        rmPayload7.DOORS.add(new RmDoor("OPEN","NOTE_RECYCLER"));
        rmPayload7.DOORS.add(new RmDoor("CLOSED","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload7)
        );
        RmSystemStatus rmPayload8 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload8.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload8.STATUS="Idle";
        rmPayload8.OLD_STATUS="User";
        rmPayload8.DOORS = new LinkedList<>();
        rmPayload8.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload8.DOORS.add(new RmDoor("OPEN","TRANSPORTBOX"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload8)
        );
        RmSystemStatus rmPayload9 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload9.DATE_TIME =Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload9.STATUS="Idle";
        rmPayload9.OLD_STATUS="User";
        rmPayload9.DOORS = new LinkedList<>();
        rmPayload9.DOORS.add(new RmDoor("OPEN","COIN_DISPENSER"));
        rmPayload9.DOORS.add(new RmDoor("CLOSED","NOTE_RECYCLER"));
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload9)
        );


        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-systemStatus11AllFiltersComboEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(SystemStatus)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageStatus("+rmPayload1.STATUS+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageOldStatus("+rmPayload1.OLD_STATUS+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DoorOpen("+rmPayload1.DOORS.get(0).NAME+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DoorClosed("+rmPayload1.DOORS.get(1).NAME+")"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID));

        //delete rule
        deleteAlertRules();
        delay();
    }


    @Test(priority = 179)
    public void mos1MachineConnected() throws MessagingException, IOException, MqttException, InterruptedException {
        //create a rule
        String payload = "{\"name\":\"ra-mos1MachineConnected\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineConnected\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        CCoDSession ccodSession = ccodRMGenerator.initCCodSimulator("JM-100","3.15.0-ra",Currency.EUR,env,tenant,0);
        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        ccodSession.stopSession();

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos1MachineConnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineConnected"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 180)
    public void mos2MachineDisconnected() throws MessagingException, IOException, MqttException, InterruptedException {
        delayMOS();
        //create a rule
        String payload = "{\"name\":\"ra-mos2MachineDisconnected\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineDisconnected\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        CCoDSession ccodSession = ccodRMGenerator.initCCodSimulator("JM-201","3.15.0-ra",Currency.EUR,env,tenant,0);
        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger

        ccodSession.stopSession();

        //check if email is sent, delete email message
        delay();

        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),0);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        delayMOS();
        emailHandler.connect();
        emailMessages = emailHandler.getMessages();
        deleteAlertRules();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();
        emailHandler.purgeInbox();
        emailHandler.disconnect();

//        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos2MachineDisconnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineDisconnected"));

        delay();
    }

    @Test(priority = 181)
    public void mos3MachineConnectedDisconnected() throws MessagingException, IOException, MqttException, InterruptedException {
        delayMOS();
        //create a rule
        String payload = "{\"name\":\"ra-mos3MachineConnectedDisconnected\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"messageTypeFilter\":\"MachineOnlineStatusChange\",\"type\":\"MessageType\",\"typeClass\":\"filter\",\"child\":{\"type\":\"Or\",\"typeClass\":\"BiNode\",\"child\":{\"type\":\"MachineConnected\",\"typeClass\":\"noParam\"},\"otherChild\":{\"type\":\"MachineDisconnected\",\"typeClass\":\"noParam\"}}}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        CCoDSession ccodSession = ccodRMGenerator.initCCodSimulator("JM-105","3.15.0-ra",Currency.EUR,env,tenant,0);
        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        ccodSession.stopSession();

        //send rm message - don't trigger


        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos3MachineConnectedDisconnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineConnected"));

        delayMOS();
        emailHandler.connect();
        emailMessages = emailHandler.getMessages();
        deleteAlertRules();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos3MachineConnectedDisconnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineDisconnected"));


        //delete rule
        delay();
    }

    @Test(priority = 182)
    public void mos4MachineConnectedMachineFilter() throws MessagingException, IOException, MqttException, InterruptedException {
        delayMOS();
        //create a rule
        String payload = "{\"name\":\"ra-mos4MachineConnectedMachineFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineConnected\",\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-106\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        CCoDSession ccodSession = ccodRMGenerator.initCCodSimulator("JM-106","3.15.0-ra",Currency.EUR,env,tenant,0);
        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        CCoDSession ccodSession2 = ccodRMGenerator.initCCodSimulator("JM-107","3.15.0-ra",Currency.EUR,env,tenant,0);
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        ccodSession.stopSession();
        ccodSession2.stopSession();

        //check if email is sent, delete email message
        delay();
        deleteAlertRules();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos4MachineConnectedMachineFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineConnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        delay();
    }

    @Test(priority = 183)
    public void mos5MachineConnectedSiteFilter() throws MessagingException, IOException, MqttException, InterruptedException {
        delayMOS();
        //create a rule
        String payload = "{\"name\":\"ra-mos5MachineConnectedSiteFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineConnected\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"fba53037-22c3-423b-9a51-c6d736a9a7ab\"]}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        CCoDSession ccodSession = ccodRMGenerator.initCCodSimulator("JM-108","3.15.0-ra",Currency.EUR,env,tenant,0); //Default site 2
        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        CCoDSession ccodSession2 = ccodRMGenerator.initCCodSimulator("JM-1080","3.15.0-ra",Currency.EUR,env,tenant,0); //Default Site 5
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        ccodSession.stopSession();
        ccodSession2.stopSession();

        //check if email is sent, delete email message
        delay();
        deleteAlertRules();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos5MachineConnectedSiteFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineConnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));

        //delete rule
        delay();
    }

    @Test(priority = 184)
    public void mos6MachineConnectedTimeOfDayFilter() throws MessagingException, IOException, MqttException, InterruptedException {
        delayMOS();
        //create a rule
        String payload = "{\"name\":\"ra-mos6MachineConnectedTimeOfDayFilter\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineConnected\",\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"}},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineOnlineStatusChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        CCoDSession ccodSession = ccodRMGenerator.initCCodSimulator("JM-109","3.15.0-ra",Currency.EUR,env,tenant,0); //Default site 2
        //send rm message - trigger
        RmSystemStatus rmPayload1 = ccodRMGenerator.generateRmSystemStatus(ccodSession);
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        CCoDSession ccodSession2 = ccodRMGenerator.initCCodSimulator("JM-110","3.15.0-ra",Currency.EUR,env,tenant,0); //Default Site 5
        RmSystemStatus rmPayload2 = ccodRMGenerator.generateRmSystemStatus(ccodSession2);
        rmPayload2.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.SYSTEM_STATUS,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        ccodSession.stopSession();
        ccodSession2.stopSession();

        //check if email is sent, delete email message
        delay();
        deleteAlertRules();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-mos6MachineConnectedTimeOfDayFilter"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineOnlineStatusChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineConnected"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));

        //delete rule
        delay();
    }

    @Test(priority = 85)
    public void transaction1NoFiltersEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction1NoFiltersEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction1NoFiltersEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 86)
    public void transaction2SiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction2SiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction2SiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 87)
    public void transaction3Machine1MachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction3MachineFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction3MachineFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 88)
    public void transaction4MachineNMachinesFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction4MachineNMachinesFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\",\"JM-11\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload3 = ccodRMGenerator.generateRmTransaction(ccodSession3, TransactionType.DEPOSIT);
        ccodSession3.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 88)
    public void transaction5TimeOfDayFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction5TimeOfDayFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload1.DATE_TIME = rmPayload1.ACCOUNTING_DATE = Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        rmPayload2.DATE_TIME = rmPayload1.ACCOUNTING_DATE = Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction5TimeOfDayFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 89)
    public void transaction6TimeOfDayBVAFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction5TimeOfDayFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload1.DATE_TIME = rmPayload1.ACCOUNTING_DATE = Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:00:00.001";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload2.DATE_TIME = rmPayload1.ACCOUNTING_DATE = Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 11:59:59.999";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //send rm message - don't trigger
        RmTransaction rmPayload3 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        rmPayload3.DATE_TIME = rmPayload1.ACCOUNTING_DATE = Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:59:59.999";
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );
        RmTransaction rmPayload4 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        rmPayload4.DATE_TIME = rmPayload1.ACCOUNTING_DATE = Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:00:00.001";
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload4)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 90)
    public void transaction7TransactionType1TypeFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction7TransactionType1TypeFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionType\",\"filter\":[\"DEPOSIT\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DISPENSE);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction7TransactionType1TypeFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TransactionType("+rmPayload1.TYPE+")"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 91)
    public void transaction8TransactionTypeNTypesFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction8TransactionTypeNTypesFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionType\",\"filter\":[\"DEPOSIT\",\"DISPENSE\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DISPENSE);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload3 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.POSSIBLE_TAMPERING);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 92)
    public void transaction9TransactionSubType1TypeFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction9TransactionSubType1TypeFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionSubType\",\"filter\":[\"EXPECTED_AMOUNT\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload1.SUB_TYPE="EXPECTED_AMOUNT";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DISPENSE);
        rmPayload2.SUB_TYPE="REQUESTED_AMOUNT";
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction9TransactionSubType1TypeFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TransactionSubType("+rmPayload1.SUB_TYPE+")"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 93)
    public void transaction10TransactionSubTypeNTypesFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction10TransactionSubTypeNTypesFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionSubType\",\"filter\":[\"DISPENSE_RETURN\",\"EXPECTED_AMOUNT\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload1.SUB_TYPE="EXPECTED_AMOUNT";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DISPENSE);
        rmPayload2.SUB_TYPE="DISPENSE_RETURN";
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload3 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.POSSIBLE_TAMPERING);
        rmPayload3.SUB_TYPE="OUT";
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 94)
    public void transaction11TransactionTotalAmountExceedsFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction11TransactionTotalAmountFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TransactionTotalAmountExceeds\",\"totalAmounts\":[{\"amount\":30000,\"currency\":\"EUR\",\"decimals\":2}]},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload1.VALUE_BAGS = new LinkedList<>();
        RmValueBag valueBag1 = new RmValueBag();
        valueBag1.TYPE="NOTE";
        valueBag1.VALUES = new LinkedList<>();
        RmValue value1 = new RmValue();
        value1.TYPE="NOTE";
        value1.CURRENCY="EUR";
        value1.DENOMINATION = value1.PIECE_VALUE = 1000;
        value1.DECIMALS=2;
        value1.COUNT =31;
        value1.TOTAL = value1.DENOMINATION*value1.COUNT;
        valueBag1.VALUES.add(value1);
        valueBag1.TOTAL = value1.TOTAL;
        rmPayload1.VALUE_BAGS.add(valueBag1);

        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        rmPayload2.VALUE_BAGS = new LinkedList<>();
        RmValueBag valueBag2 = new RmValueBag();
        valueBag2.TYPE="NOTE";
        valueBag2.VALUES = new LinkedList<>();
        RmValue value2 = new RmValue();
        value2.TYPE="NOTE";
        value2.CURRENCY="EUR";
        value2.DENOMINATION = value2.PIECE_VALUE = 1000;
        value2.DECIMALS=2;
        value2.COUNT =29;
        value2.TOTAL = value2.DENOMINATION*value2.COUNT;
        valueBag2.VALUES.add(value2);
        valueBag2.TOTAL = value2.TOTAL;
        rmPayload2.VALUE_BAGS.add(valueBag2);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //correct total, incorrect currency
        RmTransaction rmPayload3 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload3.VALUE_BAGS = new LinkedList<>();
        RmValueBag valueBag3 = new RmValueBag();
        valueBag3.TYPE="NOTE";
        valueBag3.VALUES = new LinkedList<>();
        RmValue value3 = new RmValue();
        value3.TYPE="NOTE";
        value3.CURRENCY="USD";
        value3.DENOMINATION = value3.PIECE_VALUE = 1000;
        value3.DECIMALS=2;
        value3.COUNT =31;
        value3.TOTAL = value3.DENOMINATION*value3.COUNT;
        valueBag3.VALUES.add(value3);
        valueBag3.TOTAL = value3.TOTAL;
        rmPayload3.VALUE_BAGS.add(valueBag3);
        rmPayload3.CURRENCY="USD";

        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction11TransactionTotalAmountFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains(rmPayload1.UUID));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TransactionTotalAmountExceeds(300.00 EUR)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DEPOSIT totaling 310.00 EUR"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 95)
    public void transaction12SiteMachineTimeOfDayTransactionTotalAmountExceedsFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //create a rule
        String payload = "{\"name\":\"ra-transaction12SiteMachineTimeOfDayTransactionTotalAmountExceedsFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\",\"child\":{\"type\":\"TransactionType\",\"filter\":[\"DEPOSIT\"],\"child\":{\"type\":\"TransactionSubType\",\"filter\":[\"EXPECTED_AMOUNT\"],\"child\":{\"type\":\"TransactionTotalAmountExceeds\",\"totalAmounts\":[{\"amount\":30000,\"currency\":\"EUR\",\"decimals\":2}],\"child\":{\"type\":\"Or\",\"typeClass\":\"BiNode\",\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"778ca984-d612-4219-8200-ce9ff30ab679\"]},\"otherChild\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-11\"]}}}}}},\"type\":\"MessageType\",\"messageTypeFilter\":\"Transaction\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmTransaction rmPayload1 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        rmPayload1.SUB_TYPE="EXPECTED_AMOUNT";
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.VALUE_BAGS = new LinkedList<>();
        RmValueBag valueBag1 = new RmValueBag();
        valueBag1.TYPE="NOTE";
        valueBag1.VALUES = new LinkedList<>();
        RmValue value1 = new RmValue();
        value1.TYPE="NOTE";
        value1.CURRENCY="EUR";
        value1.DENOMINATION = value1.PIECE_VALUE = 1000;
        value1.DECIMALS=2;
        value1.COUNT =31;
        value1.TOTAL = value1.DENOMINATION*value1.COUNT;
        valueBag1.VALUES.add(value1);
        valueBag1.TOTAL = value1.TOTAL;
        rmPayload1.VALUE_BAGS.add(valueBag1);

        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //send rm message - don't trigger
        //incorret totalamount
        RmTransaction rmPayload2 = ccodRMGenerator.generateRmTransaction(ccodSession2, TransactionType.DEPOSIT);
        rmPayload2.SUB_TYPE="EXPECTED_AMOUNT";
        rmPayload2.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload2.VALUE_BAGS = new LinkedList<>();
        RmValueBag valueBag2 = new RmValueBag();
        valueBag2.TYPE="NOTE";
        valueBag2.VALUES = new LinkedList<>();
        RmValue value2 = new RmValue();
        value2.TYPE="NOTE";
        value2.CURRENCY="EUR";
        value2.DENOMINATION = value2.PIECE_VALUE = 1000;
        value2.DECIMALS=2;
        value2.COUNT =29;
        value2.TOTAL = value2.DENOMINATION*value2.COUNT;
        valueBag2.VALUES.add(value2);
        valueBag2.TOTAL = value2.TOTAL;
        rmPayload2.VALUE_BAGS.add(valueBag2);
        ccodSession2.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        //incorrect currency
        RmTransaction rmPayload3 = ccodRMGenerator.generateRmTransaction(ccodSession1, TransactionType.DEPOSIT);
        rmPayload3.SUB_TYPE="EXPECTED_AMOUNT";
        rmPayload3.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload3.VALUE_BAGS = new LinkedList<>();
        RmValueBag valueBag3 = new RmValueBag();
        valueBag3.TYPE="NOTE";
        valueBag3.VALUES = new LinkedList<>();
        RmValue value3 = new RmValue();
        value3.TYPE="NOTE";
        value3.CURRENCY="USD";
        value3.DENOMINATION = value3.PIECE_VALUE = 1000;
        value3.DECIMALS=2;
        value3.COUNT =31;
        value3.TOTAL = value3.DENOMINATION*value3.COUNT;
        valueBag3.VALUES.add(value3);
        valueBag3.TOTAL = value3.TOTAL;
        rmPayload3.VALUE_BAGS.add(valueBag3);
        rmPayload3.CURRENCY="USD";

        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //incorrect date
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:39:34.601";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //incorrect type
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.TYPE="POSSIBLE_TAMPERING";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //incorrect subType
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:39:34.601";
        rmPayload1.TYPE="DEPOSIT";
        rmPayload1.SUB_TYPE="OUT";
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        //incorrect machine
        rmPayload1.UUID=ccodSession1.getMachine().getMachineUUID();
        ccodSession1.sendMqttEvent(
                EventType.TRANSACTION,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-transaction12SiteMachineTimeOfDayTransactionTotalAmountExceedsFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("JM-11"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(Transaction)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TransactionType(DEPOSIT)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TransactionSubType(EXPECTED_AMOUNT)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TransactionTotalAmountExceeds(300.00 EUR)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DEPOSIT totaling 310.00 EUR"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 96)
    public void boxContent1NoFiltersEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxContent1NoFiltersEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxContent1NoFiltersEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 97)
    public void boxContent2NoFiltersContentDoesntChangeEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxContent2NoFiltersContentDoesntChangeEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),0);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 98)
    public void boxContent3MachineFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmBoxContent rmPayload2 = ccodRMGenerator.generateRmBoxContent(ccodSession2, ccodSession2.getMachine().getBoxContents().get(0));
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxContent3MachineFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"MachineIdFilter\",\"filter\":[\"JM-01\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        rmPayload2.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload2.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload2.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload2.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxContent3MachineFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MachineIdFilter("+rmPayload1.UUID+")"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 99)
    public void boxConten4SiteFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmBoxContent rmPayload2 = ccodRMGenerator.generateRmBoxContent(ccodSession2, ccodSession2.getMachine().getBoxContents().get(0));
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxConten4SiteFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"SiteFilter\",\"filter\":[\"9425e3a2-f903-47cf-aa54-9ac5f6e75ba4\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        rmPayload2.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload2.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload2.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload2.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxConten4SiteFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("SiteFilter(9425e3a2-f903-47cf-aa54-9ac5f6e75ba4"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 100)
    public void boxConten5TimeOfDayFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession2, ccodSession2.getMachine().getBoxContents().get(0));
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=0;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxConten5TimeOfDayFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:08:31.601";
        rmPayload1.TRANSACTION_SEQ = ccodSession2.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession2.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession2.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=11;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:09:31.601";
        rmPayload1.TRANSACTION_SEQ = ccodSession2.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession2.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxConten5TimeOfDayFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("TimeOfDay(10:00 to 12:00)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 101)
    public void boxConten6TimeOfDayBVAFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession2, ccodSession2.getMachine().getBoxContents().get(0));
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxConten5TimeOfDayFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"TimeOfDay\",\"fromTime\":\"10:00\",\"toTime\":\"12:00\"},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 10:00:00.001";

        rmPayload1.TRANSACTION_SEQ = ccodSession2.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession2.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession2.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 11:59:59.999";
        rmPayload1.TRANSACTION_SEQ = ccodSession2.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession2.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession2.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 12:00:00.001";
        rmPayload1.TRANSACTION_SEQ = ccodSession2.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession2.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession2.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        rmPayload1.DATE_TIME=Utilities.getDateOnly(Utilities.getCurrentDateStampIncludedCatchingUp())+" 09:59:59.999";
        rmPayload1.TRANSACTION_SEQ = ccodSession2.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession2.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession2.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession2.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 102)
    public void boxConten7AnyBoxExceedsFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(3));
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=9;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxConten7AnyBoxExceedsFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"AnyBoxExceeds\",\"totalAmounts\":[{\"amount\":5000,\"currency\":\"EUR\",\"decimals\":2}]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=11;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=8;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=10;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxConten7AnyBoxExceedsFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("AnyBoxExceeds(50.00 EUR)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 103)
    public void boxConten8ContentExceedsFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        for (RmBoxContent bc : ccodSession1.getMachine().getBoxContents()){
            for(RmBox box : bc.BOX_CONTENTS)
                for(RmValue value : box.VALUES){
                    value.COUNT = 0;
                    value.TOTAL = value.COUNT*value.DENOMINATION;
                }
            RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1,bc);
            ccodSession1.sendMqttEvent(
                    EventType.BOX_CONTENT,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    RmHandler.parseRmToString(rmPayload1)
            );
        }

        //create a rule
        String payload = "{\"name\":\"ra-boxConten8ContentExceedsFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ContentExceeds\",\"totalAmounts\":[{\"amount\":5000,\"currency\":\"EUR\",\"decimals\":2}]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(3));
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=11;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        //already exceeded
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=12;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //does not reach ContentExceeds(50.00 EUR)
        for (RmBoxContent bc : ccodSession1.getMachine().getBoxContents()){
            for(RmBox box : bc.BOX_CONTENTS)
                for(RmValue value : box.VALUES){
                    value.COUNT = 0;
                    value.TOTAL = value.COUNT*value.DENOMINATION;
                }
            RmBoxContent rmPayload = ccodRMGenerator.generateRmBoxContent(ccodSession1,bc);
            ccodSession1.sendMqttEvent(
                    EventType.BOX_CONTENT,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    RmHandler.parseRmToString(rmPayload)
            );
        }
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=9;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxConten8ContentExceedsFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ContentExceeds(50.00 EUR)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 104)
    public void boxContent9ContentExceedsNBoxesFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        for (RmBoxContent bc : ccodSession1.getMachine().getBoxContents()){
            for(RmBox box : bc.BOX_CONTENTS)
                for(RmValue value : box.VALUES){
                    value.COUNT = 0;
                    value.TOTAL = value.COUNT*value.DENOMINATION;
                }
            RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1,bc);
            ccodSession1.sendMqttEvent(
                    EventType.BOX_CONTENT,
                    Utilities.getCurrentDateStampIncludedCatchingUp(),
                    RmHandler.parseRmToString(rmPayload1)
            );
        }

        //create a rule
        String payload = "{\"name\":\"ra-boxContent9ContentExceedsNBoxesFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"ContentExceeds\",\"totalAmounts\":[{\"amount\":10000,\"currency\":\"EUR\",\"decimals\":2}]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - dont' trigger yet, does not reach ContentExceeds(50.00 EUR)
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(3));
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=10;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),0);
        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //send rm message - trigger, another box reached ContentExceeds(50.00 EUR)
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(1).VALUES.get(0).COUNT=11;
        rmPayload1.BOX_CONTENTS.get(1).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(1).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(1).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
//        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxContent9ContentExceedsNBoxesFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("ContentExceeds(100.00 EUR)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 105)
    public void boxContent10DeviceId1DeviceIdFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmBoxContent rmPayload2 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(1));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxContent10DeviceId1DeviceIdFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DeviceId\",\"filter\":[\"PERCONTA.1\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm message - don't trigger
        rmPayload2.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload2.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload2.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload2.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxContent10DeviceId1DeviceIdFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("DeviceId(PERCONTA.1)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 106)
    public void boxContent11DeviceIdNDevicesIdFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        RmBoxContent rmPayload2 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(1));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload2)
        );
        RmBoxContent rmPayload3 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(2));
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );


        //create a rule
        String payload = "{\"name\":\"ra-boxContent11DeviceIdNDevicesIdFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"DeviceId\",\"filter\":[\"PERCONTA.1\",\"PERCONTA.2\"]},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
         rmPayload2.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
         rmPayload2.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
         rmPayload2.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
         rmPayload2.SEQUENCE = ccodSession1.sessionData.sequennce++;
         rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
         rmPayload2.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
         ccodSession1.sendMqttEvent(
                 EventType.BOX_CONTENT,
                 Utilities.getCurrentDateStampIncludedCatchingUp(),
                 RmHandler.parseRmToString(rmPayload2)
         );

        //send rm message - don't trigger
        rmPayload3.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload3.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload3.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload3.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload3.BOX_CONTENTS.get(0).VALUES.get(0).COUNT++;
        rmPayload3.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload3)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),2);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        emailHandler.extractEmailPojos(emailTitleContent);

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //delete rule
        deleteAlertRules();
        delay();
    }

    @Test(priority = 106)
    public void boxContent12ExceedsRatioFilterEmail() throws MessagingException, IOException, MqttException, InterruptedException, BoxContentFullException, EmptiableBoxesFullException, DropSafeFullException, BoxContentEmptyExcception {
        //send a boxcontent to create a state in CCC
        RmBoxContent rmPayload1 = ccodRMGenerator.generateRmBoxContent(ccodSession1, ccodSession1.getMachine().getBoxContents().get(0));
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=0;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //create a rule
        String payload = "{\"name\":\"ra-boxContent12ExceedsRatioFilterEmail\",\"recipients\":[{\"type\":\"email\",\"recipient\":\"suzohappta1@yahoo.com\",\"params\":{}}],\"trigger\":{\"child\":{\"type\":\"AnyBoxExceedsCountRatio\",\"ratio\":0.5},\"type\":\"MessageType\",\"messageTypeFilter\":\"MachineContentChange\"}}";
        String response = given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
                when().
                post(url+"/rule").
                then().
                statusCode(201).
                extract().asString();
        alertRules.add(new Gson().fromJson(response, Alert.class));

        //send rm message - trigger
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT = (rmPayload1.BOX_CONTENTS.get(0).MAXIMUM_LEVEL/2)+1;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //send rm - don't trigger
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT=0;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );
        rmPayload1.DATE_TIME=Utilities.getCurrentDateStampIncludedCatchingUp();
        rmPayload1.TRANSACTION_SEQ = ccodSession1.sessionData.transactionSequence++;
        rmPayload1.MESSAGE_SEQUENCE = ccodSession1.sessionData.messageSequence++;
        rmPayload1.SEQUENCE = ccodSession1.sessionData.sequennce++;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT = (rmPayload1.BOX_CONTENTS.get(0).MAXIMUM_LEVEL/2)-1;
        rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).TOTAL =rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).COUNT*rmPayload1.BOX_CONTENTS.get(0).VALUES.get(0).DENOMINATION;
        ccodSession1.sendMqttEvent(
                EventType.BOX_CONTENT,
                Utilities.getCurrentDateStampIncludedCatchingUp(),
                RmHandler.parseRmToString(rmPayload1)
        );

        //check if email is sent, delete email message
        delay();
        emailHandler.connect();
        List<Message> emailMessages = emailHandler.getMessages();
        Assert.assertNotNull(emailMessages);
        Assert.assertEquals(emailMessages.size(),1);

        List<String> emailTitleContent = emailHandler.getEmailContent(emailMessages);
        List<EmailPOJO> emailPojos =emailHandler.extractEmailPojos(emailTitleContent);
        String emailTitle = emailMessages.get(0).getSubject();

        emailHandler.purgeInbox();
        emailHandler.disconnect();

        //validate message
        Assert.assertTrue(emailTitle.contains("ra-boxContent12ExceedsRatioFilterEmail"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("MessageType(MachineContentChange)"));
        Assert.assertTrue(emailPojos.get(0).emailContent.contains("AnyBoxExceedsCountRatio(0.5)"));

        //delete rule
        deleteAlertRules();
        delay();
    }

    private void delay() {
        try {
            Thread.sleep(emailwailtMs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void delayMOS(){
        try {
            Thread.sleep(310000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deleteAlertRules(){

        for (Alert alert : alertRules){
            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/rule/"+alert.token).
                then().
                    statusCode(200);
        }

    }
}
