package CCC_v1;

import CCoD.Currency;
import cccObjects.DeviceCCC;
import cccObjects.ScheduledReport;
import cccObjects.SiteCCC;
import ccodRestAssured.CCoDRestAssuredRmGenerator;
import ccodSession.Environments;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import data.DefaultStrings;
import data.Utilities;
import emailHandler.EmailHandler;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.RestUtils;
import rest.Token;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

import static CCC_v1.ScheduledReportsTriggering.ReportType.*;
import static io.restassured.RestAssured.given;

public class ScheduledReportsTriggering {

    private enum ScheduledReportFileType{
        csv, json;
    }
    protected enum ReportType {
        machineInventory,
        transaction,
        netCash,
        refill,
        cashierBalance,
        manualDeposit,
        workunitOps,
        tillthroughput,
        cashsummary,
        machineDetail,
        machineOnlineStatus,
        machineReliability,
        cashiertransaction,
        currentOnlineStatus,
        machineErrors,
        openShifts,
        claimedValue,
        smartVault,
        utilization,
        cashTotalsByLocation,
        claimcashInventoryedValue,

    }

    private int plusMinutes=5;
    private Environments env = DefaultStrings.environmentForRestAssured;
    private String tenant ="sdbusiness";//DefaultStrings.tenantRestAssured;//"sdbusiness";
    private String url = DefaultStrings.getBaseURL(env)+"/api/v1/report";
    private Map<String,String> headers;
    List<ScheduledReport> reports;
    private boolean debug=true;
    private ScheduledReportFileType fileType;
    private CCoDRestAssuredRmGenerator ccodRMGenerator;
    private EmailHandler emailHandler;
    private boolean validateEmail = true;
    private boolean generateRmData = true;
    private int runTimeMinRmDataGeneration=2;

    private String fileNamePrefix=env+"_"+tenant+"_";
    private Map<String, Map<String, List<String>>> contentsToValidate;

    Map<DeviceCCC, SiteCCC> deviceSite;
    private LinkedList<DeviceCCC> devices;
    private LinkedList<SiteCCC> sites;

    //ZReport vals
    private int numUsersZreport=10;
    private String fromDateZReport = Utilities.getDateStampCBDataGenerator(2);
    private String toDateZReport = Utilities.getDateStampCBDataGenerator(0);
    private String timeZoneOffset="+01:00";
    private int endOfDayTime=7200;


    @BeforeClass
    public void setup() throws MessagingException {
        reports = new LinkedList<>();
        ccodRMGenerator = new CCoDRestAssuredRmGenerator();
        emailHandler = new EmailHandler();
        contentsToValidate = new HashMap<>();
        emailHandler.connect();
        emailHandler.purgeInbox();
        emailHandler.disconnect();


        try {
            initDeveceSites();
            if(generateRmData){
                ccodRMGenerator.generateDailyRmData(deviceSite.keySet(),env,tenant, runTimeMinRmDataGeneration, Currency.EUR);
                ccodRMGenerator.generateZReports(deviceSite.keySet(),env,tenant, numUsersZreport, fromDateZReport, toDateZReport, timeZoneOffset,endOfDayTime, Currency.EUR);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void initDeveceSites() throws IOException, URISyntaxException {
        deviceSite = RestUtils.getDevicesFromDifferentSites(env,tenant,3);
        devices = new LinkedList<>();
        sites = new LinkedList<>();
        for(DeviceCCC device : deviceSite.keySet()){
            devices.add(device);
            sites.add(deviceSite.get(device));
        }


    }

    @BeforeMethod
    public void getAccessToken(){
        Token t = Login.getToken();
        String token = t.tokenType+" "+t.accessToken;
        headers =new HashMap<String, String>();
        headers.put("X-SiteWhere-Tenant", tenant);
        headers.put("X-Authorization",DefaultStrings.xAuth);
        headers.put("Authorization",token);
    }

    @AfterClass
    public void tearDown(){
        if(validateEmail)
            deleteScheduledReportRules();
    }

    private void deleteScheduledReportRules(){
        for (ScheduledReport sr : reports){

            given().
                    headers(headers).
                    header("Content-type","application/json").
                    queryParam("force","true").
                when().
                    delete(url+"/reportRules/"+sr.token).
                then().
                    statusCode(200);

        }
    }

    private String[] getReportHeaderMammpings(ReportType reportType){
        System.out.println("Getting header mappings for "+reportType.name());
        String url = DefaultStrings.getBaseURL(DefaultStrings.environmentForRestAssured)+"/api/v1/report/available/reports/"+reportType.name()+"/headers";
        String response = given().
                headers(headers).
            when().
                get(url).
            then().
                statusCode(200)
                .extract().asString();
        System.out.println(response);
        return parseHeadersToArray(response);

    }

    private String generatePayload(String name, String time, ScheduledReportFileType fileType, ReportType reportType, String[] headers, Map filterParams) throws JsonProcessingException {

        ScheduledReport payload = new ScheduledReport();
        payload.name=name;
        payload.everyWeekly = new LinkedList<>();
        payload.everyWeekly.add(1);
        payload.everyWeekly.add(2);
        payload.everyWeekly.add(3);
        payload.everyWeekly.add(4);
        payload.everyWeekly.add(5);
        payload.everyWeekly.add(6);
        payload.everyWeekly.add(7);
        payload.everyMonthly = new LinkedList<>();
        payload.time=time;
        payload.timeZone="Europe/Stockholm";
        payload.recipients = new LinkedList<>();
        ScheduledReport.Recipient recipient = new ScheduledReport.Recipient();
        recipient.recipient=DefaultStrings.yahooUserName;
        recipient.type="email";
        recipient.params = new ScheduledReport.Params();
        recipient.params.emailTitle = payload.name;
        payload.recipients.add(recipient);

        payload.reports = new LinkedList<>();
        ScheduledReport.Report report = new ScheduledReport.Report();
        report.report=reportType.name();
        report.fileType=fileType.name();
        report.params = new HashMap<>();

        if(reportType==machineInventory ||reportType==cashTotalsByLocation || reportType== claimedValue || reportType == netCash ||reportType==refill)
            report.params.put("timestamp","(day()+0*hour)");
        if(reportType==ReportType.cashsummary)
            report.params.put("date","(day()+0*hour)");
        if(reportType==openShifts)
            report.params.put("date","(day()+16*hour)");

        if(reportType== ReportType.cashiertransaction || reportType==transaction){
            report.params.put("fromAccountingDate","(month()-1*month+2*hour)");
            report.params.put("toAccountingDate","(month()+1*hour+59*minutes+59*seconds)");
        }
        if(reportType== currentOnlineStatus || reportType== machineDetail){
            //null params values
        }

        if(reportType== machineErrors || reportType== machineReliability || reportType== manualDeposit || reportType==machineOnlineStatus ||reportType==tillthroughput || reportType==utilization || reportType==workunitOps){
            report.params.put("fromDate","(month()-1*month+0*hour)");
            report.params.put("toDate","(month()-1*day+23*hour+59*minutes+59*seconds)");
        }
        if(reportType==cashierBalance){
            report.params.put("fromDate","(day(4)-7*day)");
            report.params.put("toDate","(day(4))");
            report.params.put("endOfDayTime","00:00");
        }

        report.headerMapping = new HashMap<>();
        for(int i =0;i<headers.length;i++){
            report.headerMapping.put(headers[i],headers[i]);
        }

        if(filterParams!=null)
            report.params.putAll(filterParams);

        payload.reports.add(report);

        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return om.writeValueAsString(payload);
    }

    private String[] parseHeadersToArray(String headerMapping) {
        System.out.println();
        headerMapping = headerMapping.substring(1,headerMapping.length()-1);
        headerMapping =headerMapping.replace("\"","");
        return headerMapping.split(",");
    }

    private void postRule(String payload){
        System.out.println("Posting rule, payload:\n"+payload+"\n");
        String response= given().
                headers(headers).
                header("Content-type","application/json").
                body(payload).
            when().
                post(url+"/reportRules").
            then().
                statusCode(201).
                log().all().
                extract().asString();

        reports.add(new Gson().fromJson(response, ScheduledReport.class));
    }

    private boolean attachmentContent(String attachmentFileName, ScheduledReportFileType fileType) throws IOException {
        if(fileType==ScheduledReportFileType.csv)
            return assertAttachmentContentCSV(attachmentFileName);
        if(fileType==ScheduledReportFileType.json)
            return assertAttachmentContentJson(attachmentFileName);
        return true;
    }

    private boolean assertAttachmentHeaders(String[] headerMappings, String attachmentFileName, ScheduledReportFileType fileType) throws IOException {
        if(fileType==ScheduledReportFileType.csv)
            return assertAttachmentHeadersCSV(headerMappings,attachmentFileName);
        if(fileType==ScheduledReportFileType.json)
            return assertAttachmentHeadersJson(headerMappings,attachmentFileName);
        return true;
    }

    private boolean assertAttachmentContentJson(String attachmentFileName) throws IOException {
        System.out.println("Asserting attachment content JSon");
        List<String> lines = getAttachmentLines(attachmentFileName);
        List<String> columnns = Arrays.asList(lines.get(0).split("],"));
        List<String> columnContent = new LinkedList<>();
        for (String column : columnns)
            columnContent.add(column.substring(column.indexOf("[")));
        boolean hasContent = true;
        for(int i = 0; i< columnContent.size();i++){
            if (i==columnContent.size()-1)
                if (columnContent.get(i).length()<4)
                    hasContent = false;
            else
                if(columnContent.get(i).length()<2)
                    hasContent = false;
        }
        System.out.println();
        return hasContent;
    }

    private void createAttachmentContentTable(String attachmentFileName) throws IOException {
        System.out.println("Asserting attachment content JSon");
        List<String> lines = getAttachmentLines(attachmentFileName);
        List<String> columnns = Arrays.asList(lines.get(0).split("],"));
        List<String> columnnsClean = new LinkedList<>();
        for(int i = 0; i< columnns.size();i++){
            columnnsClean.add(columnns.get(i).replaceAll("\"",""));
        }
        Map<String,List<String>> content = new HashMap<>();
        for (int i = 0;i<columnnsClean.size();i++){
            String c = columnnsClean.get(i);
            String key = c.substring(0,c.indexOf(":["));
            String values = c.substring(c.indexOf("[")+1);
            if(i==0)
                key = key.substring(1);
            if(i==columnnsClean.size()-1)
                values = values.substring(0,values.length()-2);
            String[] vals = values.split(",");
            List<String> v = Arrays.asList(vals);
            content.put(key,v);

        }
        contentsToValidate.put(attachmentFileName,content);
    }

    private boolean attachmentFiltered(String attachmentFileName, String headerName, String tokenNotExpected, String... tokenExpected) throws IOException {
        System.out.println("### Asserting content filtered ###");

        createAttachmentContentTable(attachmentFileName);
        List<String> expectedColumnValues = getColumnExpectedValues(headerName,tokenExpected);
        List<String> notExpectedColumnValues = getColumnExpectedValues(headerName,new String[]{tokenNotExpected});
        List<String> actualColumnValues = contentsToValidate.get(attachmentFileName).get(headerName);
        System.out.println();

        List<Boolean> actuals = new LinkedList<>();
        for(int i = 0; i<expectedColumnValues.size();i++)
            actuals.add(actualColumnValues.contains(expectedColumnValues.get(i)));
        for(int i = 0; i<notExpectedColumnValues.size();i++)
            actuals.add(!actualColumnValues.contains(notExpectedColumnValues.get(i)));
        System.out.println();
        boolean b = !actuals.contains(false);
        return !actuals.contains(false);
    }

    private List<String> getColumnExpectedValues(String headerName, String[] tokens) {
        List<String> expectedValues = new LinkedList<>();
        switch (headerName){
            case "Machine":
                for(int i = 0; i<tokens.length;i++)
                    for (int j=0; j<devices.size();j++)
                        if(tokens[i].equals(devices.get(j).hardwareId))
                            if(devices.get(j).metadata.alias!=null)
                                expectedValues.add(devices.get(j).metadata.alias);
                            else
                                expectedValues.add(devices.get(j).hardwareId);

                break;
            case "Site":
                for (int i = 0;i< tokens.length; i++)
                    for (int j = 0;j<sites.size();j++)
                        if(tokens[i].equals(sites.get(j).token))
                            expectedValues.add(sites.get(j).name);
                break;
        }
        return expectedValues;
    }

    private boolean assertAttachmentContentCSV(String attachmentFileName) throws IOException {
        System.out.println("Asserting attachment content CSV");
        List<String> lines = getAttachmentLines(attachmentFileName);

        return ((lines.size() < 3) ? false : true);
    }

    private boolean assertAttachmentHeadersJson(String[] headersExpected, String attachmentFileName) throws IOException {
        System.out.println("Asserting attachment headers JSon");
        List<String> lines = getAttachmentLines(attachmentFileName);
        List<String> columnns = Arrays.asList(lines.get(0).split("],"));
        List<String> columnHeaders = new LinkedList<>();

        for (String column : columnns){
            columnHeaders.add(column.substring(0,column.indexOf("\":")));
        }

        String[] headersActual = columnHeaders.toArray(new String[columnHeaders.size()]);
        boolean[] hMatches = new boolean[headersExpected.length];
        for(int i = 0; i < headersExpected.length; i++){
            for (int j = 0; j < headersActual.length; j++){
                if(headersActual[j].contains(headersExpected[i])){
                    hMatches[i]=true;
                    break;
                }
            }
        }
        boolean matches = true;
        for(int i = 0; i<hMatches.length;i++){
            if(hMatches[i]==false)
                matches = false;
        }
        System.out.println();
        return matches;
    }

    private boolean assertAttachmentHeadersCSV(String[] headersExpected, String attachmentFileName) throws IOException {
        System.out.println("Asserting attachment headers CSV");
        List<String> lines = getAttachmentLines(attachmentFileName);

        String[] headersActual = lines.get(1).split(",");
        boolean[] hMatches = new boolean[headersExpected.length];
        for(int i = 0; i < headersExpected.length; i++){
            for (int j = 0; j < headersActual.length; j++){
                if(headersActual[j].contains(headersExpected[i])){
                    hMatches[i]=true;
                    break;
                }
            }
        }
        boolean matches = true;
        for(int i = 0; i<hMatches.length;i++){
            if(hMatches[i]==false)
                matches = false;
        }
        return matches;
    }

    private List<String> getAttachmentLines(String attachmentFileName) throws IOException {
        List<String> lines = new LinkedList<>();
        BufferedReader br = Utilities.getFileReader(emailHandler.getEmailAttachmentSaveDirectory()+"\\"+attachmentFileName);
        String line;
        while ((line = br.readLine()) != null){
            if(debug)
                System.out.println(line);
            lines.add(line);
        }
        br.close();
        return lines;
    }

    private void delay() throws InterruptedException {
        int ms = (plusMinutes+2)*60*1000;
        System.out.println("\nWaiting for the scheduled report service to generate message (Waiting for: "+(plusMinutes+2)+"m)");
        Thread.sleep(ms);
    }

    @Test(priority = 1)
    public void cashInventory1CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashInventory1CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineInventory);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineInventory,
                headerMappings,
                null));

        //check email
        if (validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 2)
    public void cashInventory2Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashInventory2Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineInventory);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineInventory,
                headerMappings,
                null));

        //check email
        if (validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            // validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));

        }

    }

    @Test(priority = 3)
    public void cashSummary3CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashSummary3CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashsummary);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashsummary,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));

        }

    }

    @Test(priority = 4)
    public void cashSummary4Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashSummary4Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashsummary);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashsummary,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 5)
    public void cashTotals5CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashTotals5CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashTotalsByLocation);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashTotalsByLocation,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 6)
    public void cashTotals6Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashTotals6Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashTotalsByLocation);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashTotalsByLocation,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 7)
    public void cashierTransactions7CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashierTransactions7CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashiertransaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashiertransaction,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), fileNamePrefix+title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 8)
    public void cashierTransactions8Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashierTransactions8Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashiertransaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashiertransaction,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 9)
    public void claimedValues9CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-claimedValues9CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.claimedValue);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.claimedValue,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), fileNamePrefix+title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 10)
    public void claimedValues10Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-claimedValues10Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.claimedValue);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.claimedValue,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 11)
    public void currentOnlineStatus11CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-currentOnlineStatus11CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.currentOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.currentOnlineStatus,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 12)
    public void currentOnlineStatus12Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-currentOnlineStatus12Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.currentOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.currentOnlineStatus,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 13)
    public void machineDetail13CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineDetail13CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineDetail);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineDetail,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 14)
    public void mchineDetail14Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-mchineDetail14Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineDetail);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineDetail,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 15)
    public void machineError15CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineError15CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineErrors);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineErrors,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 14)
    public void machineError16Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineError16Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineErrors);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineErrors,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 17)
    public void machineReliability17CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineReliability17CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(machineReliability);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineReliability,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 18)
    public void machineReliability18Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineReliability18Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineReliability);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineReliability,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 19)
    public void manualDeposits19CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-manualDeposits19CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(manualDeposit);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                manualDeposit,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 20)
    public void manualDeposits20Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-manualDeposits20Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(manualDeposit);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                manualDeposit,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 21)
    public void netCash21CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-netCash21CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(ReportType.netCash);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.netCash,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 22)
    public void netCash22Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-netCash22Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.netCash);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.netCash,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 23)
    public void machineOnlineStatus23CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineOnlineStatus23CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(machineOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineOnlineStatus,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 24)
    public void machineOnlineStatus24Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineOnlineStatus24Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineOnlineStatus,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),fileNamePrefix+title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 25)
    public void openShifts25CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-openShifts25CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(openShifts);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                openShifts,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 26)
    public void openShifts26Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-openShifts26Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(openShifts);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                openShifts,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 27)
    public void refill27CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-refill27CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(refill);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                refill,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 28)
    public void refill28Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-refill28Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(refill);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                refill,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 29)
    public void tillthroughput29CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-tillthroughput29CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(tillthroughput);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                tillthroughput,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 30)
    public void tillthroughput30Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-tillthroughput30Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(tillthroughput);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                tillthroughput,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 31)
    public void transactions31CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-transactions31CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(transaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                transaction,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 32)
    public void transactions32JSon() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-transactions32JSon";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(transaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                transaction,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 33)
    public void utilization33CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-utilization33CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(utilization);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                utilization,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 34)
    public void utilization34JSon() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-utilization34Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(utilization);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                utilization,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 35)
    public void workunitOps35CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-workunitOps35CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(workunitOps);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                workunitOps,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 36)
    public void workunitOps36JSon() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-workunitOps36JSon";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(workunitOps);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                workunitOps,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 37)
    public void cashierBalancing37CSV() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashierBalancing37CSV";
        fileType = ScheduledReportFileType.csv;
        String[] headerMappings = getReportHeaderMammpings(cashierBalance);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                cashierBalance,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0), title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }

    }

    @Test(priority = 38)
    public void cashierBalancing38Json() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashierBalancing38Json";
        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(cashierBalance);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                cashierBalance,
                headerMappings,
                null));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
        }
    }

    @Test(priority = 39)
    public void cashInventory39JsonParams1Machine() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashInventory39JsonParams1Machine";
        Map filterParams = new HashMap();
        filterParams.put("machineIds",devices.get(0).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineInventory);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineInventory,
                headerMappings,
                filterParams
        ));

        //check email
        if (validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            // validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(1).hardwareId,
                    devices.get(0).hardwareId
            ));

        }

    }



    @Test(priority = 40)
    public void cashInventory40JsonParamsNMachines() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashInventory40JsonParamsNMachines";
        Map filterParams = new HashMap();
        filterParams.put("machineIds",devices.get(0).hardwareId+","+devices.get(1).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.machineInventory);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.machineInventory,
                headerMappings,
                filterParams
        ));

        //check email
        if (validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            // validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId,
                    devices.get(1).hardwareId
            ));

        }

    }

    @Test(priority = 41)
    public void cashSummary41JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashSummary41JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashsummary);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashsummary,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(1).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 42)
    public void cashSummary42JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashSummary42JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(ReportType.cashsummary);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                ReportType.cashsummary,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 43)
    public void cashTotals43JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashTotals43JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(cashTotalsByLocation);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                cashTotalsByLocation,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 44)
    public void cashTotals44JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashTotals44JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site","("+sites.get(0).token+" "+sites.get(1).token+")");

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(cashTotalsByLocation);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                cashTotalsByLocation,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 45)
    public void cashierTransactions45JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashierTransactions45JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(cashiertransaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                cashiertransaction,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 46)
    public void cashierTransactions46JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-cashierTransactions46JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(cashiertransaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                cashiertransaction,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 47)
    public void claimedValues47JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-claimedValues47JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("siteToken",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(claimedValue);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                claimedValue,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 48)
    public void claimedValues48JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-claimedValues48JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("siteToken",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(claimedValue);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                claimedValue,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 49)
    public void currentOnlineStatus49JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-currentOnlineStatus49JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(currentOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                currentOnlineStatus,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 50)
    public void currentOnlineStatus50JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-currentOnlineStatus50JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(currentOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                currentOnlineStatus,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 51)
    public void machineDetail51JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineDetail51JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineDetail);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineDetail,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 52)
    public void machineDetail52JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machineDetail52JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineDetail);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineDetail,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 53)
    public void machinereliability53JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machinereliability53JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineReliability);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineReliability,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 54)
    public void machinereliability54JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-machinereliability54JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineReliability);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineReliability,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 55)
    public void manualDeposits55JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-manualDeposits55JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(manualDeposit);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                manualDeposit,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 56)
    public void manualDeposits56JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-manualDeposits56JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site","("+sites.get(0).token+" "+sites.get(1).token+")");

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(manualDeposit);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                manualDeposit,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 57)
    public void netcash57JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-netcash57JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("siteToken",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(netCash);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                netCash,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 58)
    public void netcash58JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-netcash58JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("siteToken",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(netCash);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                netCash,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 59)
    public void netcash59JsonParams1Machine() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-netcash59JsonParams1Machine";
        Map filterParams = new HashMap();
        filterParams.put("machineIds",devices.get(0).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(netCash);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                netCash,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId
            ));
        }

    }

    @Test(priority = 60)
    public void netcash60JsonParamsNMachines() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-netcash60JsonParamsNMachines";
        Map filterParams = new HashMap();
        filterParams.put("machineIds",devices.get(0).hardwareId+","+devices.get(1).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(netCash);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                netCash,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId,
                    devices.get(1).hardwareId
            ));
        }

    }

    @Test(priority = 61)
    public void onlineStatus61JsonParams1Machine() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-onlineStatus61JsonParams1Machine";
        Map filterParams = new HashMap();
        filterParams.put("machineId",devices.get(0).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineOnlineStatus,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId
            ));
        }

    }

    @Test(priority = 62)
    public void onlineStatus62JsonParamsNMachines() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-onlineStatus62JsonParamsNMachines";
        Map filterParams = new HashMap();
        filterParams.put("machineId",devices.get(0).hardwareId+" "+devices.get(1).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(machineOnlineStatus);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                machineOnlineStatus,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId,
                    devices.get(1).hardwareId
            ));
        }

    }

    @Test(priority = 63)
    public void openShifts63JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-openShifts63JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(openShifts);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                openShifts,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 64)
    public void openShifts64JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-openShifts64JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(openShifts);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                openShifts,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 65)
    public void refill65JsonParams1Machine() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-refill65JsonParams1Machine";
        Map filterParams = new HashMap();
        filterParams.put("machineIds",devices.get(0).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(refill);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                refill,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId
            ));
        }

    }

    @Test(priority = 66)
    public void refill66JsonParamsNMachines() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-refill66JsonParamsNMachines";
        Map filterParams = new HashMap();
        filterParams.put("machineIds",devices.get(0).hardwareId+","+devices.get(1).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(refill);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                refill,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId,
                    devices.get(1).hardwareId
            ));
        }

    }

    @Test(priority = 67)
    public void tillthroughput67JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-tillthroughput67JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(tillthroughput);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                tillthroughput,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 68)
    public void tillthroughput68JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-tillthroughput68JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(1).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(tillthroughput);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                tillthroughput,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 69)
    public void transactions69JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-transactions69JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(transaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                transaction,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 70)
    public void transactions70JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-transactions70JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site","("+sites.get(0).token+" "+sites.get(1).token+")");

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(transaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                transaction,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 71)
    public void transactions71JsonParams1Machine() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-transactions71JsonParams1Machine";
        Map filterParams = new HashMap();
        filterParams.put("machineId",devices.get(0).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(transaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                transaction,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId
            ));
        }

    }

    @Test(priority = 72)
    public void transactions72JsonParamsNMachines() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-transactions72JsonParamsNMachines";
        Map filterParams = new HashMap();
        filterParams.put("machineId",devices.get(0).hardwareId);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(transaction);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                transaction,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Machine",
                    devices.get(2).hardwareId,
                    devices.get(0).hardwareId,
                    devices.get(1).hardwareId
            ));
        }

    }

    @Test(priority = 73)
    public void utilization73JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-utilization73JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(utilization);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                utilization,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 74)
    public void utilization74JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-utilization74JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(utilization);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                utilization,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

    @Test(priority = 75)
    public void workunitops75JsonParams1Site() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-workunitops75JsonParams1Site";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(workunitOps);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                workunitOps,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token
            ));
        }

    }

    @Test(priority = 76)
    public void workunitops76JsonParamsNSites() throws IOException, InterruptedException, MessagingException {

        //post rule
        String title = fileNamePrefix+"ra-trigger-workunitops76JsonParamsNSites";
        Map filterParams = new HashMap();
        filterParams.put("site",sites.get(0).token+" "+sites.get(0).token);

        fileType = ScheduledReportFileType.json;
        String[] headerMappings = getReportHeaderMammpings(workunitOps);
        postRule(generatePayload(
                title,
                Utilities.getCurrentTimeHHmm(plusMinutes),
                fileType,
                workunitOps,
                headerMappings,
                filterParams));

        //check email
        if(validateEmail){
            delay();
            emailHandler.connect();
            List<Message> emailMessages = emailHandler.getMessages();
            Assert.assertNotNull(emailMessages);
            Assert.assertEquals(emailMessages.size(),1);

            String emailTitle = emailMessages.get(0).getSubject();
            String emailContent = emailHandler.getTextFromMessage(emailMessages.get(0));
            String attachmentFileName = emailHandler.getEmailAttachment(emailMessages.get(0),title+"_");
            System.out.println("Email Title: "+emailTitle+"\nEmail Content: "+emailContent);
            emailHandler.purgeInbox();
            emailHandler.disconnect();

            //validate message
            Assert.assertTrue(emailTitle.contains(title));
            Assert.assertNotNull(attachmentFileName);
            Assert.assertTrue(attachmentFileName.endsWith(fileType.name()));
            Assert.assertTrue(assertAttachmentHeaders(headerMappings,attachmentFileName,fileType));
            Assert.assertTrue(attachmentContent(attachmentFileName,fileType));
            Assert.assertTrue(attachmentFiltered(
                    attachmentFileName,
                    "Site",
                    sites.get(2).token,
                    sites.get(0).token,
                    sites.get(1).token
            ));
        }

    }

}
