package testNgExamples;

import org.testng.annotations.Test;

public class SecondTestCase {

    @Test(priority = 1)
    void  setup(){
        System.out.println("2nd setup");
    }

    @Test (priority = 2)
    void login(){
        System.out.println("2nd login");
    }

    @Test (priority = 2)
    void search(){
        System.out.println("2nd search");
    }

    @Test (priority = 3)
    void teardown(){
        System.out.println("2nd closing");
    }
}
