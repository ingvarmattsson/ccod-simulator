package testNgExamples;

import org.testng.annotations.Test;

public class PriorityExample {

    @Test(priority = 1)
    void testOne(){
        System.out.println("P test one");
    }
    @Test(priority = 2)
    void testTwo(){
        System.out.println("P test two");
    }
    @Test(priority = 3)
    void testThree(){
        System.out.println("P test three");
    }
    @Test(priority = 4,enabled = false)
    void testFour(){
        System.out.println("P test four");
    }

}
