package testNgExamples;

import org.testng.annotations.*;

public class TC1 {

    @Test
    void test1(){
        System.out.println("test1");
    }

    @Test
    void test2(){
        System.out.printf("test2");
    }

    @BeforeMethod
    void before(){
        System.out.println("before");
    }

    @AfterMethod
    void after(){
        System.out.println("after");
    }

    @BeforeClass
    void beforeClass(){
        System.out.println("beforeclass");
    }

    @AfterClass
    void afterClass(){
        System.out.println("After class");
    }

    @BeforeTest
    void beforeTest(){
        System.out.println("Before test");
    }

    @AfterTest
    void afterTest(){
        System.out.println("After test");
    }

    @BeforeSuite
    void beforeSuite(){
        System.out.println("Before suite");
    }

    @AfterSuite
    void afterSuite(){
        System.out.println("After suite");
    }
}
