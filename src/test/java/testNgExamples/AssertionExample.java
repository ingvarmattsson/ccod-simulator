package testNgExamples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AssertionExample {

    WebDriver driver;

    @BeforeClass
    void setup(){
        System.setProperty("webdriver.chrome.driver", "D:\\devEnv\\chromedriver2\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com");
    }

    @Test(groups = {"sanity"})
    void logoPresent(){
        WebElement logo = driver.findElement(new By.ByXPath("//*[@id=\"divLogo\"]/img"));
        Assert.assertTrue(logo.isDisplayed());

    }

    @Test(groups = {"sanity"})
    void homepageTitle(){
        String title = driver.getTitle();
        Assert.assertEquals(title,"OrangeHRM");
    }

    @AfterClass
    void tearDown(){

        driver.quit();
    }
}
